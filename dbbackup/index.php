<?php
include('config.php');
dbbackupDump(USERNAME, PASSWORD, HOST, DATABASE, ZIPPASS);

function dbbackupDump($username, $password, $host, $database, $zippass) {   
    //save file
    $files = glob("pms-*");
    foreach ($files as $sfile) {
        unlink($sfile);
    }
    $filename = 'pms-' . date('d-m-Y-H-i-s');
    $File = $filename . '.sql';
    $zipFile = $filename . '.zip';
    
    $execResult = exec('D:\xampp\mysql\bin\mysqldump --user=' . $username . ' --password='. $password .' --host=' . $host . ' '. $database  .'>' . $File);   // Database Info configurable

    $zipExec = exec('zip --password ' . $zippass .' ' . $zipFile . ' ' . $File);
            
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($zipFile));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($zipFile));
    readfile($zipFile);
    exit;
}