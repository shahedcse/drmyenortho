<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Research extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('prescription_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Research";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "research";
            $referenceQry = $this->db->query("select * from reference where ref_under = '1'");
            $data['referencelist'] = $referenceQry->result();
            $data['prescriptionList'] = array();
            $data['category'] = '';
            $data['from'] = '';
            $data['to'] = '';

            //Initial Data Load
            $presDataQr = $this->db->query("SELECT pm.id, pm.pres_id,u.first_name,u.last_name, u.email, u.mobile, u.address, u.blood_group, u.sex, u.age, pm.reference_value, pm.datetime FROM presmaster pm JOIN user u ON pm.patient_id = u.id ORDER BY pm.id DESC");
            $data['presmasterData'] = $presDataQr->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('research/research', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    function getFirstReference1() {
        $id = $this->input->post('id');
        if (is_int((int) $id)):
            $getFstQry = $this->db->query("SELECT * FROM reference where ref_under = '$id'");
            if ($getFstQry->num_rows() > 0):
                $fstQryresult = $getFstQry->result();
                echo '<option value="">Select Area Of Treatment2</option>';
                foreach ($fstQryresult as $datarow):
                    echo '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option>';
                endforeach;
            else:
                echo 'no data found';
            endif;
        else:
            echo 'no data found';
        endif;
    }

    function getFirstReference2() {
        $id = $this->input->post('id');
        if (is_int((int) $id)):
            $getFstQry = $this->db->query("SELECT * FROM reference where ref_under = '$id'");
            if ($getFstQry->num_rows() > 0):
                $fstQryresult = $getFstQry->result();
                echo '<option value="">Select Area Of Treatment3</option>';
                foreach ($fstQryresult as $datarow):
                    echo '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option>';
                endforeach;
            else:
                echo 'no data found';
            endif;
        else:
            echo 'no data found';
        endif;
    }

    function getFirstReference3() {
        $id = $this->input->post('id');
        if (is_int((int) $id)):
            $getFstQry = $this->db->query("SELECT * FROM reference where ref_under = '$id'");
            if ($getFstQry->num_rows() > 0):
                $fstQryresult = $getFstQry->result();
                echo '<option value="">Select Area Of Treatment4</option>';
                foreach ($fstQryresult as $datarow):
                    echo '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option>';
                endforeach;
            else:
                echo 'no data found';
            endif;
        else:
            echo 'no data found';
        endif;
    }

    function getSearchData() {
        $idget = $this->input->post('id');
        $from = $_POST['from'];
        $to = $_POST['to'];
        $data['from'] = $from;
        $data['to'] = $to;
        $datestart = new DateTime($from);
        $startdate = $datestart->format('Y-m-d');
        $startdate = $startdate . " 00:00:00";
        $dateto = new DateTime($to);
        $enddate = $dateto->format('Y-m-d');
        $enddate = $enddate . " 23:59:59";

        $tablebody = "";
        $explodearr = explode(',', $idget);
        $treatmentid = $explodearr[0];
        //$idset = $treatmentid;
        $temidset = "";
        $temidset2 = "";
        $temidset3 = "";
        $searchidset = "";

        $refUnderQr = $this->db->query("SELECT id FROM reference WHERE ref_under = '$treatmentid'");
        if ($refUnderQr->num_rows() > 0):
            $refunderresult = $refUnderQr->result();
            foreach ($refunderresult as $datarow):
                //$idset .= ',' . $datarow->id;
                if ($temidset == ""):
                    $temidset = $datarow->id;
                else:
                    $temidset .= ',' . $datarow->id;
                endif;
            endforeach;

            $refUnderQr2 = $this->db->query("SELECT id FROM reference WHERE ref_under IN ($temidset)");
            if ($refUnderQr2->num_rows() > 0):
                $refunderresult2 = $refUnderQr2->result();
                foreach ($refunderresult2 as $datarow):
                    //$idset .= ',' . $datarow->id;
                    if ($temidset2 == ""):
                        $temidset2 = $datarow->id;
                    else:
                        $temidset2 .= ',' . $datarow->id;
                    endif;
                endforeach;

                $refUnderQr3 = $this->db->query("SELECT id FROM reference WHERE ref_under IN ($temidset2)");
                if ($refUnderQr3->num_rows() > 0):
                    $refunderresult3 = $refUnderQr3->result();
                    foreach ($refunderresult3 as $datarow):
                        //$idset .= ',' . $datarow->id;
                        if ($temidset3 == ""):
                            $temidset3 = $datarow->id;
                        else:
                            $temidset3 .= ',' . $datarow->id;
                        endif;
                    endforeach;
                    $searchidset = $temidset3;
                else:
                    $searchidset = $temidset2;
                endif;
            else:
                $searchidset = $temidset;
            endif;
        else:
            $searchidset = $treatmentid;
        endif;

        $presmasterQr = $this->db->query("SELECT * FROM presmaster WHERE (datetime BETWEEN '$startdate' AND '$enddate') AND id_reference IN ($searchidset)");
        $prescriptionList = $presmasterQr->result();
        if (sizeof($prescriptionList)):
            foreach ($prescriptionList as $rows):
                $Pid = $rows->patient_id;
                $pationtQry = $this->db->query("SELECT * from user where id = '$Pid'");
                $tablebody .= "<tr class='gradeX'>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $rows->pres_id . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->first_name . ' '. $pationtQry->row()->last_name  . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->email . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->mobile . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->address . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->blood_group . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->sex . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->age . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $rows->reference_value . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . date('d-m-Y h:i A', strtotime($rows->datetime)) . "</a></td>";
                $tablebody .= "</tr>";
            endforeach;
            echo $tablebody;
        else:
            $tablebody = "<tr><td colspan='10'>no data found </td></tr>";
            echo $tablebody;
        endif;
    }

    function getSearchDataOthers() {

        $others = $this->input->post('other');
        $from = $_POST['from'];
        $to = $_POST['to'];
        $data['from'] = $from;
        $data['to'] = $to;
        $datestart = new DateTime($from);
        $startdate = $datestart->format('Y-m-d');
        $startdate = $startdate . " 00:00:00";
        $dateto = new DateTime($to);
        $enddate = $dateto->format('Y-m-d');
        $enddate = $enddate . " 23:59:59";

        $tablebody = "";


        $presmasterQr = $this->db->query("SELECT * FROM presmaster WHERE (datetime BETWEEN '$startdate' AND '$enddate') AND ref_others LIKE '%$others%' ");
        $prescriptionList = $presmasterQr->result();
        if (sizeof($prescriptionList)):
            foreach ($prescriptionList as $rows):
                $Pid = $rows->patient_id;
                $pationtQry = $this->db->query("SELECT * from user where id = '$Pid'");
                $tablebody .= "<tr class='gradeX'>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $rows->pres_id . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->first_name . ' '. $pationtQry->row()->last_name . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->email . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->mobile . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->address . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->blood_group . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->sex . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $pationtQry->row()->age . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . $rows->reference_value . "</a></td>";
                $tablebody .= "<td><a href=". site_url('research/research/researchdetails?id=' . $rows->id) .">" . date('d-m-Y h:i A', strtotime($rows->datetime)) . "</a></td>";
                $tablebody .= "</tr>";
            endforeach;
            echo $tablebody;
        else:
            $tablebody = "<tr><td colspan='10'>no data found </td></tr>";
            echo $tablebody;
        endif;
    }

    function researchdetails() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $idPrescription = $this->input->get('id'); //pressmaster id
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Research Details";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "research";
            
            $presmasterQry = $this->db->query("select * from presmaster where id='$idPrescription'");
            $data['presmasterData'] = $presmasterQry->result();

            $followupQry = $this->db->query("select * from presmaster where pres_under='$idPrescription'");
            $data['followupPresData'] = $followupQry->result();

            $patientId = $presmasterQry->row()->patient_id;
            $patientQr = $this->db->query("SELECT * FROM user WHERE id='$patientId'");
            $data['patientlist'] = $patientQr->row();
            $data['idPrescription'] = $idPrescription;
            
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('research/researchdetails', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

}
