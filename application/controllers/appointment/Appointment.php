<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('appointment_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Appointment";
            $data['active_menu'] = "appointment";
            $data['active_sub_menu'] = "appointmentlist";
            $data['patientlist'] = $this->appointment_model->getData('user', 'user_role', 4);
            $data['doctorlist'] = $this->appointment_model->getData('user', 'user_role', 256);
            $query = $this->db->query("SELECT * FROM appointment WHERE status='approved'");
            $data['appointmentlist'] = $query->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('appointment/appointmentlist', $data);
            $this->load->view('footer', $data);
            $this->load->view('appointment/appointment_script', $data);
        else:
            redirect('home');
        endif;
    }

    public function requestedappointment() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Requested Appointment";
            $data['active_menu'] = "appointment";
            $data['active_sub_menu'] = "requestedappointment";
            $data['patientlist'] = $this->appointment_model->getData('user', 'user_role', 4);
            $data['doctorlist'] = $this->appointment_model->getData('user', 'user_role', 256);
            $query = $this->db->query("SELECT * FROM appointment WHERE status='pending'");
            $data['appointmentlist'] = $query->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('appointment/requestedappointment', $data);
            $this->load->view('footer', $data);
            $this->load->view('appointment/appointment_script', $data);
        else:
            redirect('home');
        endif;
    }

    public function addAppointment() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):
            $apDate = $this->input->post('appointdate');
            $apTime = $this->input->post('appointtime');
            $dateTime = $apDate . " " . $apTime;
            $timestamp = strtotime($dateTime);
            $formatDate = date("Y-m-d H:i:s", $timestamp);
//            $currentDateTime = '03-07-2015 14:15:00';
//            $newDateTime = date('d-m-Y h:i:s A', strtotime($currentDateTime));
//            echo $newDateTime;
//            exit();
            $patientId = $this->input->post('patientid');
            $doctorId = $this->input->post('doctorid');
//            $doctorName = $this->session->userdata('username');
//            $result = $this->db->query("SELECT id FROM user WHERE username='$doctorName' AND user_role='256'");
//            $doctorID = $result->row()->id;

            $appointmentData = array(
                'date' => $formatDate,
                'patient_id' => $patientId,
                'doctor_id' => $doctorId,
                'status' => 'pending'
            );
            $status = $this->db->insert('appointment', $appointmentData);
            if ($status):
                $this->session->set_userdata('successfull', 'Appointment Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Appointment Information Add Failed.');
            endif;
            redirect('appointment/appointment');
        else:
            redirect('home');
        endif;
    }

    public function editRequestedAppointment() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):

            $apId = $this->input->post('apIdEdit');
            $apDate = $this->input->post('appointDateEdit');
            $apTime = $this->input->post('appointTimeEdit');
            $dateTime = $apDate . " " . $apTime;
            $timestamp = strtotime($dateTime);
            $formatDate = date("Y-m-d H:i:s", $timestamp);

            $patientId = $this->input->post('patientIdEdit');
            $doctorId = $this->input->post('doctorIdEdit');
            $status = $this->input->post('statusEdit');

            $requestAppointmentData = array(
                'date' => $formatDate,
                'patient_id' => $patientId,
                'doctor_id' => $doctorId,
                'status' => $status
            );
            $this->db->where('id', $apId);
            $status = $this->db->update('appointment', $requestAppointmentData);
            if ($status):
                $this->session->set_userdata('successfull', 'Requested Appointment  Edit Successfully.');
            else:
                $this->session->set_userdata('failed', 'Requested Appointment  Edit Failed.');
            endif;
            redirect('appointment/appointment/requestedappointment');
        else:
            redirect('home');
        endif;
    }

    public function editAppointment() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):

            $apId = $this->input->post('apIdEdit');
            $apDate = $this->input->post('appointDateEdit');
            $apTime = $this->input->post('appointTimeEdit');
            $dateTime = $apDate . " " . $apTime;
            $timestamp = strtotime($dateTime);
            $formatDate = date("Y-m-d H:i:s", $timestamp);

            $patientId = $this->input->post('patientIdEdit');
            $doctorId = $this->input->post('doctorIdEdit');
            $status = $this->input->post('statusEdit');

            $requestAppointmentData = array(
                'date' => $formatDate,
                'patient_id' => $patientId,
                'doctor_id' => $doctorId,
                'status' => $status
            );
            $this->db->where('id', $apId);
            $status = $this->db->update('appointment', $requestAppointmentData);
            if ($status):
                $this->session->set_userdata('successfull', 'Appointment Edit Successfully.');
            else:
                $this->session->set_userdata('failed', 'Appointment Edit Failed.');
            endif;
            redirect('appointment/appointment');
        else:
            redirect('home');
        endif;
    }

    public function deleteAppointment() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):

            $id_delete = $this->input->post('id_delete');
            $this->db->where('id', $id_delete);
            $query = $this->db->delete('appointment');

            if ($query):
                $this->session->set_userdata('successfull', 'Appointment Information Delete Successfully.');
            else:
                $this->session->set_userdata('failed', 'Appointment Information Delete Failed.');
            endif;
            redirect('appointment/appointment');
        else:
            redirect('home');
        endif;
    }

    public function deleteRequestedAppointment() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):

            $id_delete = $this->input->post('id_delete');
            $this->db->where('id', $id_delete);
            $query = $this->db->delete('appointment');

            if ($query):
                $this->session->set_userdata('successfull', 'Appointment Information Delete Successfully.');
            else:
                $this->session->set_userdata('failed', 'Appointment Information Delete Failed.');
            endif;
            redirect('appointment/appointment/requestedappointment');
        else:
            redirect('home');
        endif;
    }

    public function checkAppointment() {
        $dateTime = $this->input->post('dateTime');
        $timestamp = strtotime($dateTime);
        $formatDate = date("Y-m-d H:i:s", $timestamp);
        $query=$this->db->query("SELECT * FROM appointment WHERE date='$formatDate' AND status='approved'");
        if($query->num_rows()>0):
             echo "booked";
            else:
            echo "free";
        endif;
       
    }
    
    public function checkAppointmentEdit() {
        $dateTime = $this->input->post('dateTime');
        $timestamp = strtotime($dateTime);
        $formatDate = date("Y-m-d H:i:s", $timestamp);
        $query=$this->db->query("SELECT * FROM appointment WHERE date='$formatDate' AND status='approved'");
        if($query->num_rows()>0):
             echo "booked";
            else:
            echo "free";
        endif;
       
    }

}
