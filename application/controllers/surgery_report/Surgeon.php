<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Surgeon extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Surgeon";
            $data['active_menu'] = "settings";
            $data['active_sub_menu'] = "surgeon";
            $query = $this->db->query("SELECT * FROM ortho_surgeon");
            $data['surgeonlist'] = $query->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/surgeon', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addSurgeon() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Surgeon";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgeon";

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_surgeon', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addSurgeonData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $surgeonName = $this->input->post('surgeonname');
            $email = $this->input->post('email');
            $hospitalName = $this->input->post('hospitalname');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $status = $this->input->post('status');

            $actDate = $year . "-" . $month . "-" . $day;


            $caseData = array(
                'name' => $surgeonName,
                'email' => $email,
                'hospital' => $hospitalName,
                'date' => $actDate,
                'status' => $status
            );
            $status = $this->db->insert('ortho_surgeon', $caseData);
            if ($status):
                $this->session->set_userdata('successfull', 'Surgeon Information Add Successfully.');
                redirect('surgery_report/Surgeon');
            else:
                $this->session->set_userdata('failed', 'Surgeon Information Add Failed.');
                redirect('surgery_report/Surgeon/addSurgeon');
            endif;

        else:
            redirect('home');
        endif;
    }

    public function deleteSurgeonData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $id_delete = $this->input->post('id_delete');
            $this->db->where('id', $id_delete);
            $query = $this->db->delete('ortho_surgeon');

            if ($query):
                $this->session->set_userdata('successfull', 'Surgeon Information Delete Successfully.');
            else:
                $this->session->set_userdata('failed', 'Surgeon Information Delete Failed.');
            endif;
            redirect('surgery_report/Surgeon');

        else:
            redirect('home');
        endif;
    }

    public function editSurgeonData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $idEdit = $this->input->post('idEdit');
            $surgeonName = $this->input->post('surgeonname');
            $email = $this->input->post('email');
            $hospitalName = $this->input->post('hospitalname');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $status = $this->input->post('status');
            $actDate = $year . "-" . $month . "-" . $day;

            $caseData = array(
                'name' => $surgeonName,
                'email' => $email,
                'hospital' => $hospitalName,
                'date' => $actDate,
                'status' => $status
            );
            $this->db->where('id', $idEdit);
            $status = $this->db->update('ortho_surgeon', $caseData);

            if ($status):
                $this->session->set_userdata('successfull', 'Surgeon Information Edit Successfully.');
            else:
                $this->session->set_userdata('failed', 'Surgeon Information Edit Failed.');
            endif;
            redirect('surgery_report/Surgeon');

        else:
            redirect('home');
        endif;
    }
    
     function checkSurgeonUse() {
        $surgeon_id = $this->input->post('surgeon_id');
        $query = $this->db->query("SELECT * FROM `ortho_surgery` WHERE `surgeon_id` LIKE '%$surgeon_id%'");
   
        if (sizeof($query->result())):
            echo "exits";
        else:
            echo "not exits";
        endif;
    }

}
