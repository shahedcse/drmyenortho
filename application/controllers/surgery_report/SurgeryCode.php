<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SurgeryCode extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Surgical Code";
            $data['active_menu'] = "settings";
            $data['active_sub_menu'] = "surgery_code";
            $query = $this->db->query("SELECT * FROM ortho_code");
            $data['codelist'] = $query->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/surgery_code', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addCode() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Code";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_code";

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_code', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addCodeData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $codeNumber = $this->input->post('codenumber');
            $type = $this->input->post('type');
            $description = $this->input->post('description');
            
            $caseData = array(
                'code_no' => $codeNumber,
                'type' => $type,
                'description' => $description
              
            );
            $status = $this->db->insert('ortho_code', $caseData);
            if ($status):
                $this->session->set_userdata('successfull', 'Surgical Code Information Add Successfully.');
                redirect('surgery_report/SurgeryCode');
            else:
                $this->session->set_userdata('failed', 'Surgical Code Information Add Failed.');
                redirect('surgery_report/SurgeryCode/addCode');
            endif;

        else:
            redirect('home');
        endif;
    }

    public function deleteSurgeryCodeData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $id_delete = $this->input->post('id_delete');
            $this->db->where('id', $id_delete);
            $query = $this->db->delete('ortho_code');

            if ($query):
                $this->session->set_userdata('successfull', 'Surgery Code Information Delete Successfully.');
            else:
                $this->session->set_userdata('failed', 'Surgery Code Information Delete Failed.');
            endif;
            redirect('surgery_report/SurgeryCode');

        else:
            redirect('home');
        endif;
    }

    public function editSurgeryCodeData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $idEdit = $this->input->post('idEdit');
            $codenumber = $this->input->post('codenumber');
            $type= $this->input->post('type');
            $description = $this->input->post('description');
            

            $caseData = array(
                'code_no' => $codenumber,
                'type' => $type,
                'description' => $description
              
            );
            $this->db->where('id', $idEdit);
            $status = $this->db->update('ortho_code', $caseData);

            if ($status):
                $this->session->set_userdata('successfull', 'Surgery Code Information Edit Successfully.');
            else:
                $this->session->set_userdata('failed', 'Surgery Code Information Edit Failed.');
            endif;
            redirect('surgery_report/SurgeryCode');

        else:
            redirect('home');
        endif;
    }

}
