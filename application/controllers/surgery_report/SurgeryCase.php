<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SurgeryCase extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('patient_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Surgical Case";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";

            $query = $this->db->query("SELECT `orcase`.`id`,`orcase`.`case_num`,`orcase`.`patient_id`,`orcase`.`input_date`,`orsur`.`date` AS `surgerydate` FROM ortho_case AS `orcase` LEFT JOIN ortho_surgery AS `orsur` ON `orcase`.`id`=`orsur`.`case_id` GROUP BY `orcase`.`id`");
            $data['surgerycase'] = $query->result();

            $data['patientlist'] = $this->patient_model->patientList();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/surgery_case', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addCase() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Case";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";
            $data['patientlist'] = $this->patient_model->patientList();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_case', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function editCase() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Edit Case";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";
            $id = $this->input->get('id');

            $data['case_id'] = $id;
            $query = $this->db->query("SELECT * FROM ortho_case WHERE id='$id'");
            $data['orthocase'] = $query->row();
            $queryallsurgery = $this->db->query("SELECT * FROM ortho_surgery WHERE case_id='$id'");
            $data['resultallsurgery'] = $queryallsurgery->result();

            $patient_id = $query->row()->patient_id;
            $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
            $data['patientlist'] = $query2->row();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_case', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addSurgery() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Surgery";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";

            $query = $this->db->query("SELECT * FROM ortho_surgeon WHERE status='1'");
            $data['surgeonlist'] = $query->result();
            $query2 = $this->db->query("SELECT * FROM ortho_case ORDER BY id DESC LIMIT 1");
            $data['orthocase'] = $query2->row();
            $query3 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='surgery'");
            $data['codenum'] = $query3->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_surgery', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function editSurgery() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Edit Surgery";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";
            $id_surgery = $this->input->get("id");
            $data['id_surgery'] = $id_surgery;
            $querysurgerycase = $this->db->query("SELECT * FROM ortho_surgery WHERE id='$id_surgery'");
            $data['surgerycase'] = $querysurgerycase->row();
            $case_id = $querysurgerycase->row()->case_id;
            $query = $this->db->query("SELECT * FROM ortho_surgeon WHERE status='1'");
            $data['surgeonlist'] = $query->result();
            $query2 = $this->db->query("SELECT * FROM ortho_case WHERE id='$case_id'");
            $data['orthocase'] = $query2->row();
            $query3 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='surgery'");
            $data['codenum'] = $query3->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_surgery', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addAnotherSurgery() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Surgery";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";
            $id = $this->input->get('id');
            $query = $this->db->query("SELECT * FROM ortho_surgeon WHERE status='1'");
            $data['surgeonlist'] = $query->result();
            $query2 = $this->db->query("SELECT * FROM ortho_case WHERE id='$id'");
            $data['orthocase'] = $query2->row();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_another_surgery', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addCaseData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $caseNumber = $this->input->post('casenumber');
            $patientName = $this->input->post('patientname');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');

            $unkwn = $this->input->post('unknown');
            if ($unkwn == "unknown"):
                $injuryDate = '';
            else:
                $injuryDate = $year . "-" . $month . "-" . $day;
            endif;

            $caseData = array(
                'case_num' => $caseNumber,
                'patient_id' => $patientName,
                'date' => $injuryDate,
                'input_date' => date("y-m-d")
            );
            $status = $this->db->insert('ortho_case', $caseData);
            if ($status):
                $this->session->set_userdata('successfull', 'Patient Case Information Add Successfully.');
                redirect('surgery_report/SurgeryCase/addSurgery');
            else:
                $this->session->set_userdata('failed', 'Patient Case Information Add Failed.');
                redirect('surgery_report/SurgeryCase/addCase');
            endif;

        else:
            redirect('home');
        endif;
    }

    public function addSurgeryData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $surgeryDate = $year . "-" . $month . "-" . $day;

            $size = $this->input->post('testcountdata');

            if ($size == ''):
                $surgeonName = $this->input->post('surgeonval');
            else:
                $data = "";
                for ($i = 1; $i <= $size; $i++) {
                    $data1 = $this->input->post($i);
                    $data .= "," . $data1;
                }
                $surgeonName = substr($data, 1);
            endif;

            $antibiotic = (int) $this->input->post('antibiotic');
            $unknown = (int) $this->input->post('unknown');
            $antibiotic = $antibiotic + $unknown;
            $antibiotics_after_hours = $this->input->post('antibiotics_after_hours');
            $antibiotics_after_days = $this->input->post('antibiotics_after_days');

            if ($antibiotics_after_hours == ""):
                $durationInjury = $antibiotics_after_days * 24;
            else:
                $durationInjury = $antibiotics_after_hours;
            endif;

            $antibiotics_name = $this->input->post('antibiotics_name');
            $antibiotics_coverage_hours = $this->input->post('antibiotics_coverage_hours');
            $antibiotics_coverage_days = $this->input->post('antibiotics_coverage_days');

            if ($antibiotics_coverage_hours == ""):
                $durationCoverage = $antibiotics_coverage_days * 24;
            else:
                $durationCoverage = $antibiotics_coverage_hours;
            endif;
            $comments = $this->input->post('comments');
            $case_id = $this->input->post('case_id');

            $query = $this->db->query("SELECT MAX(`surgery_id`) AS `id`  FROM ortho_surgery WHERE case_id='$case_id'");
            $surgery_id = $query->row()->id;
            If ($surgery_id == ""):
                $surgery_id = 1;
            else:
                $surgery_id = $surgery_id + 1;
            endif;
            $caseData = array(
                'surgery_id' => $surgery_id,
                'case_id' => $case_id,
                'date' => $surgeryDate,
                'surgeon_id' => $surgeonName,
                'antibiotics' => $antibiotic,
                'antibiotics_after_hours' => $durationInjury,
                'antibiotics_name' => $antibiotics_name,
                'antibiotics_coverage' => $durationCoverage,
                'comments' => $comments
            );
            $status = $this->db->insert('ortho_surgery', $caseData);
            if ($status):
                $this->session->set_userdata('successfull', 'Surgery Information Add Successfully.');
                redirect('surgery_report/SurgeryCase/addFracture');
            else:
                $this->session->set_userdata('failed', 'Surgery Information Add Failed.');
                redirect('surgery_report/SurgeryCase/addCase');
            endif;

        else:
            redirect('home');
        endif;
    }

    public function editSurgeryData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $id_surgery = $this->input->post('id_surgery');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $surgeryDate = $year . "-" . $month . "-" . $day;

            $size = $this->input->post('testcountdata');

            if ($size == ''):
                $surgeonName = $this->input->post('surgeonval');
            else:
                $data = "";
                for ($i = 1; $i <= $size; $i++) {
                    $data1 = $this->input->post($i);
                    $data .= "," . $data1;
                }
                $surgeonName = substr($data, 1);
            endif;

            $antibiotic = (int) $this->input->post('antibiotic');
            $unknown = (int) $this->input->post('unknown');
            $antibiotic = $antibiotic + $unknown;
            $antibiotics_after_hours = $this->input->post('antibiotics_after_hours');
            $antibiotics_after_days = $this->input->post('antibiotics_after_days');

            if ($antibiotics_after_hours == ""):
                $durationInjury = $antibiotics_after_days * 24;
            else:
                $durationInjury = $antibiotics_after_hours;
            endif;

            $antibiotics_name = $this->input->post('antibiotics_name');
            $antibiotics_coverage_hours = $this->input->post('antibiotics_coverage_hours');
            $antibiotics_coverage_days = $this->input->post('antibiotics_coverage_days');

            if ($antibiotics_coverage_hours == ""):
                $durationCoverage = $antibiotics_coverage_days * 24;
            else:
                $durationCoverage = $antibiotics_coverage_hours;
            endif;
            $comments = $this->input->post('comments');
            $case_id = $this->input->post('case_id');

            $query = $this->db->query("SELECT MAX(`surgery_id`) AS `id`  FROM ortho_surgery WHERE case_id='$case_id'");
            $surgery_id = $query->row()->id;
            If ($surgery_id == ""):
                $surgery_id = 1;
            else:
                $surgery_id = $surgery_id + 1;
            endif;
            $caseData = array(
                'date' => $surgeryDate,
                'surgeon_id' => $surgeonName,
                'antibiotics' => $antibiotic,
                'antibiotics_after_hours' => $durationInjury,
                'antibiotics_name' => $antibiotics_name,
                'antibiotics_coverage' => $durationCoverage,
                'comments' => $comments
            );
            $this->db->where('id', $id_surgery);
            $status = $this->db->update('ortho_surgery', $caseData);
            if ($status):
                $this->session->set_userdata('successfull', 'Surgery Information Add Successfully.');
                redirect('surgery_report/SurgeryCase');
            else:
                $this->session->set_userdata('failed', 'Surgery Information Add Failed.');
                redirect('surgery_report/SurgeryCase');
            endif;

        else:
            redirect('home');
        endif;
    }

    public function addAnotherSurgeryData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $surgeryDate = $year . "-" . $month . "-" . $day;

            $size = $this->input->post('testcountdata');

            if ($size == ''):
                $surgeonName = $this->input->post('surgeonval');
            else:
                $data = "";
                for ($i = 1; $i <= $size; $i++) {
                    $data1 = $this->input->post($i);
                    $data .= "," . $data1;
                }
                $surgeonName = substr($data, 1);
            endif;

            $antibiotic = (int) $this->input->post('antibiotic');
            $unknown = (int) $this->input->post('unknown');
            $antibiotic = $antibiotic + $unknown;
            $antibiotics_after_hours = $this->input->post('antibiotics_after_hours');
            $antibiotics_after_days = $this->input->post('antibiotics_after_days');

            if ($antibiotics_after_hours == ""):
                $durationInjury = $antibiotics_after_days * 24;
            else:
                $durationInjury = $antibiotics_after_hours;
            endif;

            $antibiotics_name = $this->input->post('antibiotics_name');
            $antibiotics_coverage_hours = $this->input->post('antibiotics_coverage_hours');
            $antibiotics_coverage_days = $this->input->post('antibiotics_coverage_days');

            if ($antibiotics_coverage_hours == ""):
                $durationCoverage = $antibiotics_coverage_days * 24;
            else:
                $durationCoverage = $antibiotics_coverage_hours;
            endif;
            $comments = $this->input->post('comments');
            $case_id = $this->input->post('case_id');

            $query = $this->db->query("SELECT MAX(`surgery_id`) AS `id`  FROM ortho_surgery WHERE case_id='$case_id'");
            $surgery_id = $query->row()->id;
            If ($surgery_id == ""):
                $surgery_id = 1;
            else:
                $surgery_id = $surgery_id + 1;
            endif;
            $caseData = array(
                'surgery_id' => $surgery_id,
                'case_id' => $case_id,
                'date' => $surgeryDate,
                'surgeon_id' => $surgeonName,
                'antibiotics' => $antibiotic,
                'antibiotics_after_hours' => $durationInjury,
                'antibiotics_name' => $antibiotics_name,
                'antibiotics_coverage' => $durationCoverage,
                'comments' => $comments
            );
            $status = $this->db->insert('ortho_surgery', $caseData);
            if ($status):
                $this->session->set_userdata('successfull', 'Surgery Information Add Successfully.');
                redirect('surgery_report/SurgeryCase');
            else:
                $this->session->set_userdata('failed', 'Surgery Information Add Failed.');
                redirect('surgery_report/SurgeryCase');
            endif;

        else:
            redirect('home');
        endif;
    }

    public function addFracture() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Fracture";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";
            $query2 = $this->db->query("SELECT * FROM ortho_case ORDER BY id DESC LIMIT 1");
            $data['orthocase'] = $query2->row();
            $query3 = $this->db->query("SELECT * FROM ortho_surgery ORDER BY id DESC LIMIT 1");
            $data['orthosurgery'] = $query3->row();
            $query4 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='fracture'");
            $data['codenum'] = $query4->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_fracture', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function editFracture() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Edit Fracture";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";
            $id_fracture = $this->input->get('id');
            $data['id_fracture'] = $id_fracture;
            $queryallfracture = $this->db->query("SELECT * FROM ortho_fracture Where id='$id_fracture'");
            $data['orthofracture'] = $queryallfracture->row();

            $queryallimgfracture = $this->db->query("SELECT * FROM ortho_image_fracture Where fracture_id='$id_fracture'");
            $data['orthoimgfracture'] = $queryallimgfracture->row();
            $querysurgeryid = $this->db->query("SELECT surgery_id FROM ortho_fracture Where id='$id_fracture'");
            $surgery_id = $querysurgeryid->row()->surgery_id;
            $querycaseid = $this->db->query("SELECT case_id FROM ortho_surgery Where id='$surgery_id'");
            $case_id = $querycaseid->row()->case_id;
            $query2 = $this->db->query("SELECT * FROM ortho_case Where id='$case_id'");
            $data['orthocase'] = $query2->row();
            $query3 = $this->db->query("SELECT * FROM ortho_surgery Where case_id='$case_id'");
            $data['orthosurgery'] = $query3->row();
            $query4 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='fracture'");
            $data['codenum'] = $query4->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_fracture', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addAnotherFracture() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Fracture";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";
            $id = $this->input->get('id');
            $caseidQr = $this->db->query("SELECT case_id FROM ortho_surgery WHERE id = '$id'");
            $caseid = $caseidQr->row()->case_id;
            $data['surgeryid'] = $id;
            $query2 = $this->db->query("SELECT * FROM ortho_case WHERE id='$caseid'");
            $data['orthocase'] = $query2->row();
            $query3 = $this->db->query("SELECT * FROM ortho_surgery WHERE case_id='$caseid'");
            $data['orthosurgery'] = $query3->row();
            $query4 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='fracture'");
            $data['codenum'] = $query4->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_another_fracture', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addFractureData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $surgery_id = $this->input->post('surgery_id');
            $fractureside = $this->input->post('fractureside');
            $surgicalapp = $this->input->post('surgicalapp');

            $locfracArr = array();

            $proximal = $this->input->post('proximal');
            $middle = $this->input->post('middle');
            $distal = $this->input->post('distal');
            $segmental = $this->input->post('segmental');
            $femoralneck = $this->input->post('femoralneck');
            $intertrochanteric = $this->input->post('intertrochanteric');
            $subtrochanteric = $this->input->post('subtrochanteric');

            if ($proximal != ''):
                $locfracArr[] = $proximal;
            endif;
            if ($middle != ''):
                $locfracArr[] = $middle;
            endif;
            if ($distal != ''):
                $locfracArr[] = $distal;
            endif;
            if ($segmental != ''):
                $locfracArr[] = $segmental;
            endif;
            if ($femoralneck != ''):
                $locfracArr[] = $femoralneck;
            endif;
            if ($intertrochanteric != ''):
                $locfracArr[] = $intertrochanteric;
            endif;
            if ($subtrochanteric != ''):
                $locfracArr[] = $subtrochanteric;
            endif;


            $locfrac = implode(',', $locfracArr);
            $typefrac = $this->input->post('typefrac');
            if ($typefrac == "closed"):
                $timeDebridement = '';
                $timeSkinClouser = '';
                $woundclosuremethod = '';
            else:
                $timeDebridementHour = $this->input->post('timeDebridementHour');
                $timeDebridementDay = $this->input->post('timeDebridementDay');
                if ($timeDebridementHour == ''):
                    $timeDebridement = $timeDebridementDay * 24;
                else:
                    $timeDebridement = $timeDebridementHour;
                endif;
                $timeSkinClouserDay = $this->input->post('timeSkinClouserDay');
                $timeSkinClouser = $timeSkinClouserDay * 24;

                $woundclosuremethodarr = array();
                $primary = $this->input->post('primary');
                $skingraft = $this->input->post('skingraft');
                $musicflap = $this->input->post('musicflap');
                $femoralneck = $this->input->post('femoralneck');
                $secondary = $this->input->post('secondary');
                $other = $this->input->post('other');

                if ($other == ''):
                    $otherval = '';
                else:
                    $otherval = $this->input->post('otherval');
                endif;

                if ($primary != ''):
                    $woundclosuremethodarr[] = $primary;
                endif;
                if ($skingraft != ''):
                    $woundclosuremethodarr[] = $skingraft;
                endif;
                if ($musicflap != ''):
                    $woundclosuremethodarr[] = $musicflap;
                endif;

                if ($secondary != ''):
                    $woundclosuremethodarr[] = $secondary;
                endif;
                if ($other != ''):
                    $woundclosuremethodarr[] = $other;
                    $woundclosuremethodarr[] = $otherval;
                endif;
                $woundclosuremethod = implode(',', $woundclosuremethodarr);


            endif;
            $fracstabilityarr = array();
            $stable = $this->input->post('stable');
            $unstableposteriormedialfragment = $this->input->post('unstableposteriormedialfragment');
            $unstablelateralfemurwall = $this->input->post('unstablelateralfemurwall');
            if ($stable != ''):
                $fracstabilityarr[] = $stable;
            endif;
            if ($unstableposteriormedialfragment != ''):
                $fracstabilityarr[] = $unstableposteriormedialfragment;
            endif;
            if ($unstablelateralfemurwall != ''):
                $fracstabilityarr[] = $unstablelateralfemurwall;
            endif;
            $fracstability = implode(',', $fracstabilityarr);



            $nonunion = $this->input->post('nonunion');
            if ($nonunion == ''):
                $nonunion = 5;
            endif;
            $preimplant = $this->input->post('preimplant');
            if ($preimplant == ''):
                $preimplant = 5;
            endif;


            $preimplanttypearr = array();
            $exFixation = $this->input->post('externalfixation');
            $plate = $this->input->post('plate');
            $imnail = $this->input->post('imnail');
            $wire = $this->input->post('wire');
            if ($exFixation != ''):
                $preimplanttypearr[] = $exFixation;
            endif;
            if ($plate != ''):
                $preimplanttypearr[] = $plate;
            endif;
            if ($imnail != ''):
                $preimplanttypearr[] = $imnail;
            endif;
            if ($wire != ''):
                $preimplanttypearr[] = $wire;
            endif;
            $preimplanttype = implode(',', $preimplanttypearr);

            if ($exFixation == ''):
                $timeExFixationDay = $this->input->post('timeExFixationDay');
                $timeRemovalExFixationDay = $this->input->post('timeRemovalExFixationDay');
            else:
                $timeExFixationDay = $this->input->post('timeExFixationDay') * 24;
                $timeRemovalExFixationDay = $this->input->post('timeRemovalExFixationDay') * 24;
            endif;
            $reaming = $this->input->post('reaming');
            $fracreduc = $this->input->post('fracreduc');
            if ($fracreduc == ''):
                $fracreduc = 5;
            endif;

            $comments = $this->input->post('comments');


            $typeNail = $this->input->post('typenail');
            if ($typeNail == "fin hip nail"):
                $lengthNail = '';
                $diameterNail = '';
                $patientweight = '';
            elseif ($typeNail == "pediatric fin nail"):
                $lengthNail = $this->input->post('length_nail');
                $diameterNail = $this->input->post('diameter_nail');
                $patientweight = $this->input->post('patientweight');
            else:
                $lengthNail = $this->input->post('length_nail');
                $diameterNail = $this->input->post('diameter_nail');
                $patientweight = '';

            endif;
            $screwnail = $this->input->post('screwnail');
            if ($screwnail == ''):
                $screwnail = 5;
            endif;
            $proximal25 = $this->input->post('proximal25');
            $proximal30 = $this->input->post('proximal30');
            $proximal35 = $this->input->post('proximal35');
            $proximal40 = $this->input->post('proximal40');
            $proximal45 = $this->input->post('proximal45');
            $proximal50 = $this->input->post('proximal50');
            $proximal55 = $this->input->post('proximal55');
            $proximal60 = $this->input->post('proximal60');
            $proximal65 = $this->input->post('proximal65');
            $proximal70 = $this->input->post('proximal70');
            $proximal75 = $this->input->post('proximal75');

            $distal25 = $this->input->post('distal25');
            $distal30 = $this->input->post('distal30');
            $distal35 = $this->input->post('distal35');
            $distal40 = $this->input->post('distal40');
            $distal45 = $this->input->post('distal45');
            $distal50 = $this->input->post('distal50');
            $distal55 = $this->input->post('distal55');
            $distal60 = $this->input->post('distal60');
            $distal65 = $this->input->post('distal65');
            $distal70 = $this->input->post('distal70');
            $distal75 = $this->input->post('distal75');

            $csqty60 = $this->input->post('csqty60');
            $csqty65 = $this->input->post('csqty65');
            $csqty70 = $this->input->post('csqty70');
            $csqty75 = $this->input->post('csqty75');
            $csqty80 = $this->input->post('csqty80');
            $csqty85 = $this->input->post('csqty85');
            $csqty90 = $this->input->post('csqty90');
            $csqty95 = $this->input->post('csqty95');
            $csqty100 = $this->input->post('csqty100');
            $csqty105 = $this->input->post('csqty105');
            $csqty110 = $this->input->post('csqty110');
            $csqty115 = $this->input->post('csqty115');

            $shcpqty60 = $this->input->post('shcpqty60');
            $shcpqty65 = $this->input->post('shcpqty65');
            $shcpqty70 = $this->input->post('shcpqty70');
            $shcpqty75 = $this->input->post('shcpqty75');
            $shcpqty80 = $this->input->post('shcpqty80');
            $shcpqty85 = $this->input->post('shcpqty85');
            $shcpqty90 = $this->input->post('shcpqty90');
            $shcpqty95 = $this->input->post('shcpqty95');

            $shccqty30 = $this->input->post('shccqty30');
            $shccqty35 = $this->input->post('shccqty35');
            $shccqty40 = $this->input->post('shccqty40');
            $shccqty45 = $this->input->post('shccqty45');



            $platesused = $this->input->post('platesused');

            if ($platesused == ''):
                $platesused = 5;
            endif;
            if ($platesused == 1):
                $rpu = $this->input->post('rpu');
                if ($rpu == ''):
                    $rpu = 5;
                endif;
                $rc = $this->input->post('rc');
                if ($rc == ''):
                    $rc = 5;
                endif;
                $us = $this->input->post('us');
                if ($us == ''):
                    $us = 5;
                endif;
                $hp3 = $this->input->post('3hp');
                if ($hp3 == ''):
                    $hp3 = 5;
                endif;
                $h2hp = $this->input->post('h2hp');
                if ($h2hp == ''):
                    $h2hp = 5;
                endif;
                $h3hp = $this->input->post('h3hp');
                if ($h3hp == ''):
                    $h3hp = 5;
                endif;
                $h4hp = $this->input->post('h4hp');
                if ($h4hp == ''):
                    $h4hp = 5;
                endif;
            elseif ($platesused == 0):
                $rpu = 0;
                $rc = 0;
                $us = 0;
                $hp3 = 0;
                $h2hp = 0;
                $h3hp = 0;
                $h4hp = 0;
            else:
                $rpu = 5;
                $rc = 5;
                $us = 5;
                $hp3 = 5;
                $h2hp = 5;
                $h3hp = 5;
                $h4hp = 5;
            endif;

//            $query = $this->db->query("SELECT MAX(`fracture_id`) AS `id`  FROM ortho_fracture WHERE surgery_id='$surgery_id'");
            $query = $this->db->query("SELECT count(*) as `fracture_id` FROM ortho_fracture JOIN ortho_surgery ON ortho_surgery.id = ortho_fracture.surgery_id WHERE ortho_surgery.surgery_id = '$surgery_id'");
            $fracture_id = $query->row()->fracture_id;
            If ($fracture_id == ""):
                $fracture_id = 1;
            else:
                $fracture_id = $fracture_id + 1;
            endif;
            $caseData = array(
                'fracture_id' => $fracture_id,
                'surgery_id' => $surgery_id,
                'fracture_side' => $fractureside,
                'surgical_approch' => $surgicalapp,
                'fracture_location' => $locfrac,
                'fracture_type' => $typefrac,
                'fracture_stability' => $fracstability,
                'time_injury_debrid' => $timeDebridement,
                'time_injury_skin' => $timeSkinClouser,
                'wound_closure_method' => $woundclosuremethod,
                'nonunion' => $nonunion,
                'previous_implant_used' => $preimplant,
                'previous_implant_type' => $preimplanttype,
                'ext_fix_duration' => $timeExFixationDay,
                'remvoe_ext_fix_sign' => $timeRemovalExFixationDay,
                'method_of_reaming' => $reaming,
                'fracture_reduction' => $fracreduc,
                'comments' => $comments,
                'type_of_nail_used' => $typeNail,
                'length_of_nail' => $lengthNail,
                'diameter_of_nail' => $diameterNail,
                'patient_weight' => $patientweight,
                'screw_with_nail' => $screwnail,
                'sis_25_prox' => $proximal25,
                'sis_30_prox' => $proximal30,
                'sis_35_prox' => $proximal35,
                'sis_40_prox' => $proximal40,
                'sis_45_prox' => $proximal45,
                'sis_50_prox' => $proximal50,
                'sis_55_prox' => $proximal55,
                'sis_60_prox' => $proximal60,
                'sis_65_prox' => $proximal65,
                'sis_70_prox' => $proximal70,
                'sis_75_prox' => $proximal75,
                'sis_25_dist' => $distal25,
                'sis_30_dist' => $distal30,
                'sis_35_dist' => $distal35,
                'sis_40_dist' => $distal40,
                'sis_45_dist' => $distal45,
                'sis_50_dist' => $distal50,
                'sis_55_dist' => $distal55,
                'sis_60_dist' => $distal60,
                'sis_65_dist' => $distal65,
                'sis_70_dist' => $distal70,
                'sis_75_dist' => $distal75,
                'cs_60' => $csqty60,
                'cs_65' => $csqty65,
                'cs_70' => $csqty70,
                'cs_75' => $csqty75,
                'cs_80' => $csqty80,
                'cs_85' => $csqty85,
                'cs_90' => $csqty90,
                'cs_95' => $csqty95,
                'cs_100' => $csqty100,
                'cs_105' => $csqty105,
                'cs_110' => $csqty110,
                'cs_115' => $csqty115,
                'shc_pis_60' => $shcpqty60,
                'shc_pis_65' => $shcpqty65,
                'shc_pis_70' => $shcpqty70,
                'shc_pis_75' => $shcpqty75,
                'shc_pis_80' => $shcpqty80,
                'shc_pis_85' => $shcpqty85,
                'shc_pis_90' => $shcpqty90,
                'shc_pis_95' => $shcpqty95,
                'shc_sc_30' => $shccqty30,
                'shc_sc_35' => $shccqty35,
                'shc_sc_40' => $shccqty40,
                'shc_sc_45' => $shccqty45,
                'plate_used' => $platesused,
                'rod_plate' => $rpu,
                'rod_connector' => $rc,
                'unicort_screw' => $us,
                'hole_plate3' => $hp3,
                'hv_2hole' => $h2hp,
                'hv_3hole' => $h3hp,
                'hv_4hole' => $h4hp
            );
            $status = $this->db->insert('ortho_fracture', $caseData);

            $queryfractureid = $this->db->query("SELECT * FROM ortho_fracture WHERE fracture_id='$fracture_id' AND surgery_id='$surgery_id'");
            $imgfractureid = $queryfractureid->row()->id;


            $opCount = $this->input->post('opCount');
            $date_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $day = $this->input->post('day' . $i);
                $month = $this->input->post('month' . $i);
                $year = $this->input->post('year' . $i);
                $inputdate = $year . "-" . $month . "-" . $day;
                $date_array[] = $inputdate;
            }
            $date = implode(',', $date_array);

            $image_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $opType = $this->input->post('operation' . $i);
                $image_array[] = $opType;
            }
            $images = implode(',', $image_array);


            $name_array = array();
            $count = count($_FILES['userfile']['size']);
            foreach ($_FILES as $key => $value)
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['userfile']['name'] = $value['name'][$s];
                    $_FILES['userfile']['type'] = $value['type'][$s];
                    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['userfile']['error'] = $value['error'][$s];
                    $_FILES['userfile']['size'] = $value['size'][$s];
                    $config['upload_path'] = './assets/uploads/xray/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width'] = '2000';
                    $config['max_height'] = '2000';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $data = $this->upload->data();
                    $name_array[] = $data['file_name'];
                }
            $names = implode(',', $name_array);
            $xRayImagedata = array(
                'fracture_id' => $imgfractureid,
                'date' => $date,
                'image_name' => $names,
                'image_type' => $images
            );
            $status = $this->db->insert('ortho_image_fracture', $xRayImagedata);
            if ($status):
                $this->session->set_userdata('successfull', 'Fracture Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Fracture Information Add Failed.');
            endif;
            redirect('surgery_report/SurgeryCase');

        else:
            redirect('home');
        endif;
    }

    public function editFractureData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $id_fracture = $this->input->post('id_fracture');
            $surgery_id = $this->input->post('surgery_id');
            $fractureside = $this->input->post('fractureside');
            $surgicalapp = $this->input->post('surgicalapp');

            $locfracArr = array();

            $proximal = $this->input->post('proximal');
            $middle = $this->input->post('middle');
            $distal = $this->input->post('distal');
            $segmental = $this->input->post('segmental');
            $femoralneck = $this->input->post('femoralneck');
            $intertrochanteric = $this->input->post('intertrochanteric');
            $subtrochanteric = $this->input->post('subtrochanteric');

            if ($proximal != ''):
                $locfracArr[] = $proximal;
            endif;
            if ($middle != ''):
                $locfracArr[] = $middle;
            endif;
            if ($distal != ''):
                $locfracArr[] = $distal;
            endif;
            if ($segmental != ''):
                $locfracArr[] = $segmental;
            endif;
            if ($femoralneck != ''):
                $locfracArr[] = $femoralneck;
            endif;
            if ($intertrochanteric != ''):
                $locfracArr[] = $intertrochanteric;
            endif;
            if ($subtrochanteric != ''):
                $locfracArr[] = $subtrochanteric;
            endif;


            $locfrac = implode(',', $locfracArr);
            $typefrac = $this->input->post('typefrac');
            if ($typefrac == "closed"):
                $timeDebridement = '';
                $timeSkinClouser = '';
                $woundclosuremethod = '';
            else:
                $timeDebridementHour = $this->input->post('timeDebridementHour');
                $timeDebridementDay = $this->input->post('timeDebridementDay');
                if ($timeDebridementHour == ''):
                    $timeDebridement = $timeDebridementDay * 24;
                else:
                    $timeDebridement = $timeDebridementHour;
                endif;
                $timeSkinClouserDay = $this->input->post('timeSkinClouserDay');
                $timeSkinClouser = $timeSkinClouserDay * 24;

                $woundclosuremethodarr = array();
                $primary = $this->input->post('primary');
                $skingraft = $this->input->post('skingraft');
                $musicflap = $this->input->post('musicflap');
                $femoralneck = $this->input->post('femoralneck');
                $secondary = $this->input->post('secondary');
                $other = $this->input->post('other');

                if ($other == ''):
                    $otherval = '';
                else:
                    $otherval = $this->input->post('otherval');
                endif;
                if ($primary != ''):
                    $woundclosuremethodarr[] = $primary;
                endif;
                if ($skingraft != ''):
                    $woundclosuremethodarr[] = $skingraft;
                endif;
                if ($musicflap != ''):
                    $woundclosuremethodarr[] = $musicflap;
                endif;

                if ($secondary != ''):
                    $woundclosuremethodarr[] = $secondary;
                endif;
                if ($other != ''):
                    $woundclosuremethodarr[] = $other;
                    $woundclosuremethodarr[] = $otherval;
                endif;
                $woundclosuremethod = implode(',', $woundclosuremethodarr);


            endif;
            $fracstabilityarr = array();
            $stable = $this->input->post('stable');
            $unstableposteriormedialfragment = $this->input->post('unstableposteriormedialfragment');
            $unstablelateralfemurwall = $this->input->post('unstablelateralfemurwall');
            if ($stable != ''):
                $fracstabilityarr[] = $stable;
            endif;
            if ($unstableposteriormedialfragment != ''):
                $fracstabilityarr[] = $unstableposteriormedialfragment;
            endif;
            if ($unstablelateralfemurwall != ''):
                $fracstabilityarr[] = $unstablelateralfemurwall;
            endif;
            $fracstability = implode(',', $fracstabilityarr);



            $nonunion = $this->input->post('nonunion');
            if ($nonunion == ''):
                $nonunion = 5;
            endif;
            $preimplant = $this->input->post('preimplant');
            if ($preimplant == ''):
                $preimplant = 5;
            endif;



            $preimplanttypearr = array();
            $exFixation = $this->input->post('externalfixation');
            $plate = $this->input->post('plate');
            $imnail = $this->input->post('imnail');
            $wire = $this->input->post('wire');
            if ($exFixation != ''):
                $preimplanttypearr[] = $exFixation;
            endif;
            if ($plate != ''):
                $preimplanttypearr[] = $plate;
            endif;
            if ($imnail != ''):
                $preimplanttypearr[] = $imnail;
            endif;
            if ($wire != ''):
                $preimplanttypearr[] = $wire;
            endif;
            $preimplanttype = implode(',', $preimplanttypearr);

            if ($exFixation == ''):
                $timeExFixationDay = $this->input->post('timeExFixationDay');
                $timeRemovalExFixationDay = $this->input->post('timeRemovalExFixationDay');
            else:
                $timeExFixationDay = $this->input->post('timeExFixationDay') * 24;
                $timeRemovalExFixationDay = $this->input->post('timeRemovalExFixationDay') * 24;
            endif;
            $reaming = $this->input->post('reaming');
            $fracreduc = $this->input->post('fracreduc');
            if ($fracreduc == ''):
                $fracreduc = 5;
            endif;
            $comments = $this->input->post('comments');


            $typeNail = $this->input->post('typenail');
            if ($typeNail == "fin hip nail"):
                $lengthNail = '';
                $diameterNail = '';
                $patientweight = '';
            elseif ($typeNail == "pediatric fin nail"):
                $lengthNail = $this->input->post('length_nail');
                $diameterNail = $this->input->post('diameter_nail');
                $patientweight = $this->input->post('patientweight');
            else:
                $lengthNail = $this->input->post('length_nail');
                $diameterNail = $this->input->post('diameter_nail');
                $patientweight = '';

            endif;
            $screwnail = $this->input->post('screwnail');
            if ($screwnail == ''):
                $screwnail = 5;
            endif;
            $proximal25 = $this->input->post('proximal25');
            $proximal30 = $this->input->post('proximal30');
            $proximal35 = $this->input->post('proximal35');
            $proximal40 = $this->input->post('proximal40');
            $proximal45 = $this->input->post('proximal45');
            $proximal50 = $this->input->post('proximal50');
            $proximal55 = $this->input->post('proximal55');
            $proximal60 = $this->input->post('proximal60');
            $proximal65 = $this->input->post('proximal65');
            $proximal70 = $this->input->post('proximal70');
            $proximal75 = $this->input->post('proximal75');

            $distal25 = $this->input->post('distal25');
            $distal30 = $this->input->post('distal30');
            $distal35 = $this->input->post('distal35');
            $distal40 = $this->input->post('distal40');
            $distal45 = $this->input->post('distal45');
            $distal50 = $this->input->post('distal50');
            $distal55 = $this->input->post('distal55');
            $distal60 = $this->input->post('distal60');
            $distal65 = $this->input->post('distal65');
            $distal70 = $this->input->post('distal70');
            $distal75 = $this->input->post('distal75');

            $csqty60 = $this->input->post('csqty60');
            $csqty65 = $this->input->post('csqty65');
            $csqty70 = $this->input->post('csqty70');
            $csqty75 = $this->input->post('csqty75');
            $csqty80 = $this->input->post('csqty80');
            $csqty85 = $this->input->post('csqty85');
            $csqty90 = $this->input->post('csqty90');
            $csqty95 = $this->input->post('csqty95');
            $csqty100 = $this->input->post('csqty100');
            $csqty105 = $this->input->post('csqty105');
            $csqty110 = $this->input->post('csqty110');
            $csqty115 = $this->input->post('csqty115');

            $shcpqty60 = $this->input->post('shcpqty60');
            $shcpqty65 = $this->input->post('shcpqty65');
            $shcpqty70 = $this->input->post('shcpqty70');
            $shcpqty75 = $this->input->post('shcpqty75');
            $shcpqty80 = $this->input->post('shcpqty80');
            $shcpqty85 = $this->input->post('shcpqty85');
            $shcpqty90 = $this->input->post('shcpqty90');
            $shcpqty95 = $this->input->post('shcpqty95');

            $shccqty30 = $this->input->post('shccqty30');
            $shccqty35 = $this->input->post('shccqty35');
            $shccqty40 = $this->input->post('shccqty40');
            $shccqty45 = $this->input->post('shccqty45');



            $platesused = $this->input->post('platesused');

            if ($platesused == ''):
                $platesused = 5;
            endif;
            if ($platesused == 1):
                $rpu = $this->input->post('rpu');
                if ($rpu == ''):
                    $rpu = 5;
                endif;
                $rc = $this->input->post('rc');
                if ($rc == ''):
                    $rc = 5;
                endif;
                $us = $this->input->post('us');
                if ($us == ''):
                    $us = 5;
                endif;
                $hp3 = $this->input->post('3hp');
                if ($hp3 == ''):
                    $hp3 = 5;
                endif;
                $h2hp = $this->input->post('h2hp');
                if ($h2hp == ''):
                    $h2hp = 5;
                endif;
                $h3hp = $this->input->post('h3hp');
                if ($h3hp == ''):
                    $h3hp = 5;
                endif;
                $h4hp = $this->input->post('h4hp');
                if ($h4hp == ''):
                    $h4hp = 5;
                endif;
            elseif ($platesused == 0):
                $rpu = 0;
                $rc = 0;
                $us = 0;
                $hp3 = 0;
                $h2hp = 0;
                $h3hp = 0;
                $h4hp = 0;
            else:
                $rpu = 5;
                $rc = 5;
                $us = 5;
                $hp3 = 5;
                $h2hp = 5;
                $h3hp = 5;
                $h4hp = 5;
            endif;

//            $query = $this->db->query("SELECT MAX(`fracture_id`) AS `id`  FROM ortho_fracture WHERE surgery_id='$surgery_id'");
            $query = $this->db->query("SELECT count(*) as `fracture_id` FROM ortho_fracture JOIN ortho_surgery ON ortho_surgery.id = ortho_fracture.surgery_id WHERE ortho_surgery.surgery_id = '$surgery_id'");
            $fracture_id = $query->row()->fracture_id;
            If ($fracture_id == ""):
                $fracture_id = 1;
            else:
                $fracture_id = $fracture_id + 1;
            endif;
            $caseData = array(
                'fracture_side' => $fractureside,
                'surgical_approch' => $surgicalapp,
                'fracture_location' => $locfrac,
                'fracture_type' => $typefrac,
                'fracture_stability' => $fracstability,
                'time_injury_debrid' => $timeDebridement,
                'time_injury_skin' => $timeSkinClouser,
                'wound_closure_method' => $woundclosuremethod,
                'nonunion' => $nonunion,
                'previous_implant_used' => $preimplant,
                'previous_implant_type' => $preimplanttype,
                'ext_fix_duration' => $timeExFixationDay,
                'remvoe_ext_fix_sign' => $timeRemovalExFixationDay,
                'method_of_reaming' => $reaming,
                'fracture_reduction' => $fracreduc,
                'comments' => $comments,
                'type_of_nail_used' => $typeNail,
                'length_of_nail' => $lengthNail,
                'diameter_of_nail' => $diameterNail,
                'patient_weight' => $patientweight,
                'screw_with_nail' => $screwnail,
                'sis_25_prox' => $proximal25,
                'sis_30_prox' => $proximal30,
                'sis_35_prox' => $proximal35,
                'sis_40_prox' => $proximal40,
                'sis_45_prox' => $proximal45,
                'sis_50_prox' => $proximal50,
                'sis_55_prox' => $proximal55,
                'sis_60_prox' => $proximal60,
                'sis_65_prox' => $proximal65,
                'sis_70_prox' => $proximal70,
                'sis_75_prox' => $proximal75,
                'sis_25_dist' => $distal25,
                'sis_30_dist' => $distal30,
                'sis_35_dist' => $distal35,
                'sis_40_dist' => $distal40,
                'sis_45_dist' => $distal45,
                'sis_50_dist' => $distal50,
                'sis_55_dist' => $distal55,
                'sis_60_dist' => $distal60,
                'sis_65_dist' => $distal65,
                'sis_70_dist' => $distal70,
                'sis_75_dist' => $distal75,
                'cs_60' => $csqty60,
                'cs_65' => $csqty65,
                'cs_70' => $csqty70,
                'cs_75' => $csqty75,
                'cs_80' => $csqty80,
                'cs_85' => $csqty85,
                'cs_90' => $csqty90,
                'cs_95' => $csqty95,
                'cs_100' => $csqty100,
                'cs_105' => $csqty105,
                'cs_110' => $csqty110,
                'cs_115' => $csqty115,
                'shc_pis_60' => $shcpqty60,
                'shc_pis_65' => $shcpqty65,
                'shc_pis_70' => $shcpqty70,
                'shc_pis_75' => $shcpqty75,
                'shc_pis_80' => $shcpqty80,
                'shc_pis_85' => $shcpqty85,
                'shc_pis_90' => $shcpqty90,
                'shc_pis_95' => $shcpqty95,
                'shc_sc_30' => $shccqty30,
                'shc_sc_35' => $shccqty35,
                'shc_sc_40' => $shccqty40,
                'shc_sc_45' => $shccqty45,
                'plate_used' => $platesused,
                'rod_plate' => $rpu,
                'rod_connector' => $rc,
                'unicort_screw' => $us,
                'hole_plate3' => $hp3,
                'hv_2hole' => $h2hp,
                'hv_3hole' => $h3hp,
                'hv_4hole' => $h4hp
            );

            $this->db->where('id', $id_fracture);
            $status = $this->db->update('ortho_fracture', $caseData);

            $queryfractureid = $this->db->query("SELECT * FROM ortho_fracture WHERE fracture_id='$fracture_id' AND surgery_id='$surgery_id'");
            $imgfractureid = $queryfractureid->row()->id;


            $opCount = $this->input->post('opCount');
            $tCount = $this->input->post('tCount');
            $date_array = array();
            for ($i = 1; $i <= $tCount; $i++) {
                $day = $this->input->post('day' . $i);
                $month = $this->input->post('month' . $i);
                $year = $this->input->post('year' . $i);
                if($day!="" && $month!="" && $year!=""):
                   $inputdate = $year . "-" . $month . "-" . $day;
                $date_array[] = $inputdate;  
                endif;
               
            }
            $date = implode(',', $date_array);

            $image_array = array();
            for ($i = 1; $i <= $tCount; $i++) {
                $opType = $this->input->post('operation' . $i);
                if($opType!=""):
                     $image_array[] = $opType;
                endif;
               
            }
            $images = implode(',', $image_array);


            $name_array = array();
            $count = count($_FILES['userfile']['size']);

            if ($opCount > $count):

                for ($i = 1; $i <= $tCount; $i++) {
                    $checkname = $this->input->post('xrayimgname' . $i);
                    if ($checkname == ""):
                        continue;
                    else:
                        $name_array[] = $checkname;
                    endif;
                }
            endif;


            foreach ($_FILES as $key => $value)
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['userfile']['name'] = $value['name'][$s];
                    $_FILES['userfile']['type'] = $value['type'][$s];
                    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['userfile']['error'] = $value['error'][$s];
                    $_FILES['userfile']['size'] = $value['size'][$s];
                    $config['upload_path'] = './assets/uploads/xray/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width'] = '2000';
                    $config['max_height'] = '2000';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $data = $this->upload->data();

//                    if ($data['file_name'] == ""):
//                        $name_array[] = $this->input->post('xrayimgname' . ($s + 1));
//                    else:
                    $name_array[] = $data['file_name'];
//                    endif;
                }
//   echo "opCount--" . $opCount . "count--" . $count."<br/>";
//            print_r($name_array);
//            exit();
            $names = implode(',', $name_array);

            $xRayImagedata = array(
//                'fracture_id' => $imgfractureid,
                'date' => $date,
                'image_name' => $names,
                'image_type' => $images
            );
            $this->db->where('fracture_id', $id_fracture);
            $status = $this->db->update('ortho_image_fracture', $xRayImagedata);
            if ($status):
                $this->session->set_userdata('successfull', 'Fracture Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Fracture Information Add Failed.');
            endif;
            redirect('surgery_report/SurgeryCase');

        else:
            redirect('home');
        endif;
    }

    public function addAnotherFractureData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $surgery_id = $this->input->post('surgery_id');
            $fractureside = $this->input->post('fractureside');
            $surgicalapp = $this->input->post('surgicalapp');

            $locfracArr = array();
            $proximal = $this->input->post('proximal');
            $middle = $this->input->post('middle');
            $distal = $this->input->post('distal');
            $segmental = $this->input->post('segmental');
            $femoralneck = $this->input->post('femoralneck');
            $intertrochanteric = $this->input->post('intertrochanteric');
            $subtrochanteric = $this->input->post('subtrochanteric');

            if ($proximal != ''):
                $locfracArr[] = $proximal;
            endif;
            if ($middle != ''):
                $locfracArr[] = $middle;
            endif;
            if ($distal != ''):
                $locfracArr[] = $distal;
            endif;
            if ($segmental != ''):
                $locfracArr[] = $segmental;
            endif;
            if ($femoralneck != ''):
                $locfracArr[] = $femoralneck;
            endif;
            if ($intertrochanteric != ''):
                $locfracArr[] = $intertrochanteric;
            endif;
            if ($subtrochanteric != ''):
                $locfracArr[] = $subtrochanteric;
            endif;


            $locfrac = implode(',', $locfracArr);
            $typefrac = $this->input->post('typefrac');
            if ($typefrac == "closed"):
                $timeDebridement = '';
                $timeSkinClouser = '';
                $woundclosuremethod = '';
            else:
                $timeDebridementHour = $this->input->post('timeDebridementHour');
                $timeDebridementDay = $this->input->post('timeDebridementDay');
                if ($timeDebridementHour == ''):
                    $timeDebridement = $timeDebridementDay * 24;
                else:
                    $timeDebridement = $timeDebridementHour;
                endif;
                $timeSkinClouserDay = $this->input->post('timeSkinClouserDay');
                $timeSkinClouser = $timeSkinClouserDay * 24;

                $woundclosuremethodarr = array();
                $primary = $this->input->post('primary');
                $skingraft = $this->input->post('skingraft');
                $musicflap = $this->input->post('musicflap');
                $femoralneck = $this->input->post('femoralneck');
                $secondary = $this->input->post('secondary');
                $other = $this->input->post('other');
                if ($other == ''):
                    $otherval = '';
                else:
                    $otherval = $this->input->post('otherval');
                endif;

                if ($primary != ''):
                    $woundclosuremethodarr[] = $primary;
                endif;
                if ($skingraft != ''):
                    $woundclosuremethodarr[] = $skingraft;
                endif;
                if ($musicflap != ''):
                    $woundclosuremethodarr[] = $musicflap;
                endif;

                if ($secondary != ''):
                    $woundclosuremethodarr[] = $secondary;
                endif;
                if ($other != ''):
                    $woundclosuremethodarr[] = $other;
                    $woundclosuremethodarr[] = $otherval;
                endif;
                $woundclosuremethod = implode(',', $woundclosuremethodarr);


            endif;
            $fracstabilityarr = array();
            $stable = $this->input->post('stable');
            $unstableposteriormedialfragment = $this->input->post('unstableposteriormedialfragment');
            $unstablelateralfemurwall = $this->input->post('unstablelateralfemurwall');
            if ($stable != ''):
                $fracstabilityarr[] = $stable;
            endif;
            if ($unstableposteriormedialfragment != ''):
                $fracstabilityarr[] = $unstableposteriormedialfragment;
            endif;
            if ($unstablelateralfemurwall != ''):
                $fracstabilityarr[] = $unstablelateralfemurwall;
            endif;
            $fracstability = implode(',', $fracstabilityarr);



            $nonunion = $this->input->post('nonunion');
            if ($nonunion == ''):
                $nonunion = 5;
            endif;
            $preimplant = $this->input->post('preimplant');
            if ($preimplant == ''):
                $preimplant = 5;
            endif;



            $preimplanttypearr = array();
            $exFixation = $this->input->post('externalfixation');
            $plate = $this->input->post('plate');
            $imnail = $this->input->post('imnail');
            $wire = $this->input->post('wire');
            if ($exFixation != ''):
                $preimplanttypearr[] = $exFixation;
            endif;
            if ($plate != ''):
                $preimplanttypearr[] = $plate;
            endif;
            if ($imnail != ''):
                $preimplanttypearr[] = $imnail;
            endif;
            if ($wire != ''):
                $preimplanttypearr[] = $wire;
            endif;
            $preimplanttype = implode(',', $preimplanttypearr);

            if ($exFixation == ''):
                $timeExFixationDay = $this->input->post('timeExFixationDay');
                $timeRemovalExFixationDay = $this->input->post('timeRemovalExFixationDay');
            else:
                $timeExFixationDay = $this->input->post('timeExFixationDay') * 24;
                $timeRemovalExFixationDay = $this->input->post('timeRemovalExFixationDay') * 24;
            endif;
            $reaming = $this->input->post('reaming');
            $fracreduc = $this->input->post('fracreduc');
            if ($fracreduc == ''):
                $fracreduc = 5;
            endif;
            $comments = $this->input->post('comments');


            $typeNail = $this->input->post('typenail');
            if ($typeNail == "fin hip nail"):
                $lengthNail = '';
                $diameterNail = '';
                $patientweight = '';
            elseif ($typeNail == "pediatric fin nail"):
                $lengthNail = $this->input->post('length_nail');
                $diameterNail = $this->input->post('diameter_nail');
                $patientweight = $this->input->post('patientweight');
            else:
                $lengthNail = $this->input->post('length_nail');
                $diameterNail = $this->input->post('diameter_nail');
                $patientweight = '';

            endif;

            $screwnail = $this->input->post('screwnail');
            if ($screwnail == ''):
                $screwnail = 5;
            endif;
            $proximal25 = $this->input->post('proximal25');
            $proximal30 = $this->input->post('proximal30');
            $proximal35 = $this->input->post('proximal35');
            $proximal40 = $this->input->post('proximal40');
            $proximal45 = $this->input->post('proximal45');
            $proximal50 = $this->input->post('proximal50');
            $proximal55 = $this->input->post('proximal55');
            $proximal60 = $this->input->post('proximal60');
            $proximal65 = $this->input->post('proximal65');
            $proximal70 = $this->input->post('proximal70');
            $proximal75 = $this->input->post('proximal75');

            $distal25 = $this->input->post('distal25');
            $distal30 = $this->input->post('distal30');
            $distal35 = $this->input->post('distal35');
            $distal40 = $this->input->post('distal40');
            $distal45 = $this->input->post('distal45');
            $distal50 = $this->input->post('distal50');
            $distal55 = $this->input->post('distal55');
            $distal60 = $this->input->post('distal60');
            $distal65 = $this->input->post('distal65');
            $distal70 = $this->input->post('distal70');
            $distal75 = $this->input->post('distal75');

            $csqty60 = $this->input->post('csqty60');
            $csqty65 = $this->input->post('csqty65');
            $csqty70 = $this->input->post('csqty70');
            $csqty75 = $this->input->post('csqty75');
            $csqty80 = $this->input->post('csqty80');
            $csqty85 = $this->input->post('csqty85');
            $csqty90 = $this->input->post('csqty90');
            $csqty95 = $this->input->post('csqty95');
            $csqty100 = $this->input->post('csqty100');
            $csqty105 = $this->input->post('csqty105');
            $csqty110 = $this->input->post('csqty110');
            $csqty115 = $this->input->post('csqty115');

            $shcpqty60 = $this->input->post('shcpqty60');
            $shcpqty65 = $this->input->post('shcpqty65');
            $shcpqty70 = $this->input->post('shcpqty70');
            $shcpqty75 = $this->input->post('shcpqty75');
            $shcpqty80 = $this->input->post('shcpqty80');
            $shcpqty85 = $this->input->post('shcpqty85');
            $shcpqty90 = $this->input->post('shcpqty90');
            $shcpqty95 = $this->input->post('shcpqty95');

            $shccqty30 = $this->input->post('shccqty30');
            $shccqty35 = $this->input->post('shccqty35');
            $shccqty40 = $this->input->post('shccqty40');
            $shccqty45 = $this->input->post('shccqty45');



            $platesused = $this->input->post('platesused');
            if ($platesused == ''):
                $platesused = 5;
            endif;
            if ($platesused == 1):
                $rpu = $this->input->post('rpu');
                if ($rpu == ''):
                    $rpu = 5;
                endif;
                $rc = $this->input->post('rc');
                if ($rc == ''):
                    $rc = 5;
                endif;
                $us = $this->input->post('us');
                if ($us == ''):
                    $us = 5;
                endif;
                $hp3 = $this->input->post('3hp');
                if ($hp3 == ''):
                    $hp3 = 5;
                endif;
                $h2hp = $this->input->post('h2hp');
                if ($h2hp == ''):
                    $h2hp = 5;
                endif;
                $h3hp = $this->input->post('h3hp');
                if ($h3hp == ''):
                    $h3hp = 5;
                endif;
                $h4hp = $this->input->post('h4hp');
                if ($h4hp == ''):
                    $h4hp = 5;
                endif;
            elseif ($platesused == 0):
                $rpu = 0;
                $rc = 0;
                $us = 0;
                $hp3 = 0;
                $h2hp = 0;
                $h3hp = 0;
                $h4hp = 0;
            else:
                $rpu = 5;
                $rc = 5;
                $us = 5;
                $hp3 = 5;
                $h2hp = 5;
                $h3hp = 5;
                $h4hp = 5;
            endif;

            $query = $this->db->query("SELECT MAX(`fracture_id`) AS `id`  FROM ortho_fracture WHERE surgery_id='$surgery_id'");
            $fracture_id = $query->row()->id;
            If ($fracture_id == ""):
                $fracture_id = 1;
            else:
                $fracture_id = $fracture_id + 1;
            endif;
            $caseData = array(
                'fracture_id' => $fracture_id,
                'surgery_id' => $surgery_id,
                'fracture_side' => $fractureside,
                'surgical_approch' => $surgicalapp,
                'fracture_location' => $locfrac,
                'fracture_type' => $typefrac,
                'fracture_stability' => $fracstability,
                'time_injury_debrid' => $timeDebridement,
                'time_injury_skin' => $timeSkinClouser,
                'wound_closure_method' => $woundclosuremethod,
                'nonunion' => $nonunion,
                'previous_implant_used' => $preimplant,
                'previous_implant_type' => $preimplanttype,
                'ext_fix_duration' => $timeExFixationDay,
                'remvoe_ext_fix_sign' => $timeRemovalExFixationDay,
                'method_of_reaming' => $reaming,
                'fracture_reduction' => $fracreduc,
                'comments' => $comments,
                'type_of_nail_used' => $typeNail,
                'length_of_nail' => $lengthNail,
                'diameter_of_nail' => $diameterNail,
                'patient_weight' => $patientweight,
                'screw_with_nail' => $screwnail,
                'sis_25_prox' => $proximal25,
                'sis_30_prox' => $proximal30,
                'sis_35_prox' => $proximal35,
                'sis_40_prox' => $proximal40,
                'sis_45_prox' => $proximal45,
                'sis_50_prox' => $proximal50,
                'sis_55_prox' => $proximal55,
                'sis_60_prox' => $proximal60,
                'sis_65_prox' => $proximal65,
                'sis_70_prox' => $proximal70,
                'sis_75_prox' => $proximal75,
                'sis_25_dist' => $distal25,
                'sis_30_dist' => $distal30,
                'sis_35_dist' => $distal35,
                'sis_40_dist' => $distal40,
                'sis_45_dist' => $distal45,
                'sis_50_dist' => $distal50,
                'sis_55_dist' => $distal55,
                'sis_60_dist' => $distal60,
                'sis_65_dist' => $distal65,
                'sis_70_dist' => $distal70,
                'sis_75_dist' => $distal75,
                'cs_60' => $csqty60,
                'cs_65' => $csqty65,
                'cs_70' => $csqty70,
                'cs_75' => $csqty75,
                'cs_80' => $csqty80,
                'cs_85' => $csqty85,
                'cs_90' => $csqty90,
                'cs_95' => $csqty95,
                'cs_100' => $csqty100,
                'cs_105' => $csqty105,
                'cs_110' => $csqty110,
                'cs_115' => $csqty115,
                'shc_pis_60' => $shcpqty60,
                'shc_pis_65' => $shcpqty65,
                'shc_pis_70' => $shcpqty70,
                'shc_pis_75' => $shcpqty75,
                'shc_pis_80' => $shcpqty80,
                'shc_pis_85' => $shcpqty85,
                'shc_pis_90' => $shcpqty90,
                'shc_pis_95' => $shcpqty95,
                'shc_sc_30' => $shccqty30,
                'shc_sc_35' => $shccqty35,
                'shc_sc_40' => $shccqty40,
                'shc_sc_45' => $shccqty45,
                'plate_used' => $platesused,
                'rod_plate' => $rpu,
                'rod_connector' => $rc,
                'unicort_screw' => $us,
                'hole_plate3' => $hp3,
                'hv_2hole' => $h2hp,
                'hv_3hole' => $h3hp,
                'hv_4hole' => $h4hp
            );
            $status = $this->db->insert('ortho_fracture', $caseData);

            $queryfractureid = $this->db->query("SELECT * FROM ortho_fracture WHERE fracture_id='$fracture_id' AND surgery_id='$surgery_id'");
            $imgfractureid = $queryfractureid->row()->id;

            $opCount = $this->input->post('opCount');
            $date_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $day = $this->input->post('day' . $i);
                $month = $this->input->post('month' . $i);
                $year = $this->input->post('year' . $i);
                $inputdate = $year . "-" . $month . "-" . $day;
                $date_array[] = $inputdate;
            }
            $date = implode(',', $date_array);

            $image_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $opType = $this->input->post('operation' . $i);
                $image_array[] = $opType;
            }
            $images = implode(',', $image_array);


            $name_array = array();
            $count = count($_FILES['userfile']['size']);


            foreach ($_FILES as $key => $value)
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['userfile']['name'] = $value['name'][$s];
                    $_FILES['userfile']['type'] = $value['type'][$s];
                    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['userfile']['error'] = $value['error'][$s];
                    $_FILES['userfile']['size'] = $value['size'][$s];
                    $config['upload_path'] = './assets/uploads/xray/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width'] = '2000';
                    $config['max_height'] = '2000';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $data = $this->upload->data();
                    $name_array[] = $data['file_name'];
                }
            $names = implode(',', $name_array);
            $xRayImagedata = array(
                'fracture_id' => $imgfractureid,
                'date' => $date,
                'image_name' => $names,
                'image_type' => $images
            );
            $status = $this->db->insert('ortho_image_fracture', $xRayImagedata);
            if ($status):
                $this->session->set_userdata('successfull', 'Fracture Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Fracture Information Add Failed.');
            endif;
            redirect('surgery_report/SurgeryCase');

        else:
            redirect('home');
        endif;
    }

    function getPatientData() {
        $patient_id = $this->input->post('patient_id');
        $query = $this->patient_model->getSelectData("user", "id", $patient_id);

        if (sizeof($query)):
            echo json_encode($query);
        else:
            echo "No data found";
        endif;
    }

    public function addFractureXrayImage() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $opCount = $this->input->post('opCount');
            $date_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $day = $this->input->post('day' . $i);
                $month = $this->input->post('month' . $i);
                $year = $this->input->post('year' . $i);
                $inputdate = $year . "-" . $month . "-" . $day;
                $date_array[] = $inputdate;
            }
            $date = implode(',', $date_array);

            $image_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $opType = $this->input->post('operation' . $i);
                $image_array[] = $opType;
            }
            $images = implode(',', $image_array);


            $name_array = array();
            $count = count($_FILES['userfile']['size']);
            foreach ($_FILES as $key => $value)
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['userfile']['name'] = $value['name'][$s];
                    $_FILES['userfile']['type'] = $value['type'][$s];
                    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['userfile']['error'] = $value['error'][$s];
                    $_FILES['userfile']['size'] = $value['size'][$s];
                    $config['upload_path'] = './assets/uploads/xray/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '100';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '768';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $data = $this->upload->data();
                    $name_array[] = $data['file_name'];
                }
            $names = implode(',', $name_array);
            $xRayImagedata = array(
                'fracture_id' => 1,
                'date' => $date,
                'image_name' => $names,
                'image_type' => $images
            );
            $status = $this->db->insert('ortho_image_fracture', $xRayImagedata);
            if ($status):
                $this->session->set_userdata('successfull', 'Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Information Add Failed.');
            endif;
            redirect('surgery_report/SurgeryCase/addFracture');

        else:
            redirect('home');
        endif;
    }

    public function addFollowup() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Add Followup";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";

            $id_fracture = $this->input->get('id');
            $data['fractureid'] = $id_fracture;

            $queryallimgfracture = $this->db->query("SELECT * FROM ortho_image_fracture Where fracture_id='$id_fracture'");
            $data['orthoimgfracture'] = $queryallimgfracture->row();

            $surgeryidQr = $this->db->query("SELECT surgery_id FROM ortho_fracture WHERE id = '$id_fracture'");
            $surgeryid = $surgeryidQr->row()->surgery_id;

            $caseidQr = $this->db->query("SELECT case_id FROM ortho_surgery WHERE id = '$surgeryid'");
            $caseid = $caseidQr->row()->case_id;
            $query2 = $this->db->query("SELECT * FROM ortho_case WHERE id='$caseid'");
            $data['orthocase'] = $query2->row();

            $query4 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='followup'");
            $data['codenum'] = $query4->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/add_followup', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function editFollowup() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Edit Followup";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "surgery_case";

            $id_followup = $this->input->get('id');
            $data['id_followup'] = $id_followup;
            $queryallfollowup = $this->db->query("SELECT * FROM ortho_followup WHERE id = '$id_followup'");
            $data['orthofollowup'] = $queryallfollowup->row();

            $id_fracture = $queryallfollowup->row()->fracture_id;
            $data['fractureid'] = $id_fracture;

            $queryallimgfracture = $this->db->query("SELECT * FROM ortho_image_fracture Where fracture_id='$id_fracture'");
            $data['orthoimgfracture'] = $queryallimgfracture->row();

            $queryallimgfollowup = $this->db->query("SELECT * FROM ortho_image_followup Where followup_id='$id_followup'");
            $data['orthoimgfollowup'] = $queryallimgfollowup->row();

            $surgeryidQr = $this->db->query("SELECT surgery_id FROM ortho_fracture WHERE id = '$id_fracture'");
            $surgeryid = $surgeryidQr->row()->surgery_id;

            $caseidQr = $this->db->query("SELECT case_id FROM ortho_surgery WHERE id = '$surgeryid'");
            $caseid = $caseidQr->row()->case_id;
            $query2 = $this->db->query("SELECT * FROM ortho_case WHERE id='$caseid'");
            $data['orthocase'] = $query2->row();

            $query4 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='followup'");
            $data['codenum'] = $query4->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_followup', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addFollowupData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $date = $year . "-" . $month . "-" . $day;

            $infection = $this->input->post('infection');
            if ($infection == ''):
                $infection = 5;
            endif;
            $infection_typearr = array();
            if ($infection == 1):
                $infectionwound = $this->input->post('infectionwound');
                $infectiondepth = $this->input->post('infectiondepth');
                if ($infectionwound == ''):
                    $infectionwound = 5;
                endif;
                if ($infectiondepth == ''):
                    $infectiondepth = 5;
                endif;

                $infectionduration = $this->input->post('infectionduration');
                $osteomyelitis = $this->input->post('osteomyelitis');
                $amputation = $this->input->post('amputation');
                if ($osteomyelitis != ''):
                    $infection_typearr[] = $osteomyelitis;
                endif;
                if ($amputation != ''):
                    $infection_typearr[] = $amputation;
                endif;
                $infection_type = implode(',', $infection_typearr);
            else:
                $infectionwound = 5;
                $infectiondepth = 5;
                $infectionduration = '';
                $osteomyelitis = '';
                $amputation = '';
                $infection_type = '';
            endif;

            $partialweight = $this->input->post('partialweight');
            if ($partialweight == ''):
                $partialweight = 5;
            endif;

            $painlessweight = $this->input->post('painlessweight');
            if ($painlessweight == ''):
                $painlessweight = 5;
            endif;

            $healingxray = $this->input->post('healingxray');
            if ($healingxray == ''):
                $healingxray = 5;
            endif;
            $kneeflexion = $this->input->post('kneeflexion');
            if ($kneeflexion == ''):
                $kneeflexion = 5;
            endif;
            $screwbreakage = $this->input->post('screwbreakage');
            if ($screwbreakage == ''):
                $screwbreakage = 5;
            endif;
            $screwloosening = $this->input->post('screwloosening');
            if ($screwloosening == ''):
                $screwloosening = 5;
            endif;
            $nailbreakage = $this->input->post('nailbreakage');
            if ($nailbreakage == ''):
                $nailbreakage = 5;
            endif;
            $nailloosening = $this->input->post('nailloosening');
            if ($nailloosening == ''):
                $nailloosening = 5;
            endif;
            $deformity = $this->input->post('deformity');
            if ($deformity == ''):
                $deformity = 5;
            endif;

            if ($deformity == 1):
                $deformity_alignment = $this->input->post('alignment');
                $rotation = $this->input->post('rotation');
                if ($rotation == ''):
                    $rotation = 5;
                endif;
            else:
                $deformity_alignment = '';
                $rotation = 5;
            endif;

            $repeatsurgery = $this->input->post('repeatsurgery');
            if ($repeatsurgery == ''):
                $repeatsurgery = 5;
            endif;

            $infectionrepeat = $this->input->post('infectionrepeat');
            $deformityrepeat = $this->input->post('deformityrepeat');
            $nonunionrepeat = $this->input->post('nonunionrepeat');
            $repeatsurgeryarr = array();
            if ($repeatsurgery == 1):
                if ($infectionrepeat != ''):
                    $repeatsurgeryarr[] = $infectionrepeat;
                endif;
                if ($deformityrepeat != ''):
                    $repeatsurgeryarr[] = $deformityrepeat;
                endif;
                if ($nonunionrepeat != ''):
                    $repeatsurgeryarr[] = $nonunionrepeat;
                endif;
                $repeatsurgerycause = implode(',', $repeatsurgeryarr);
            else:
                $repeatsurgerycause = '';
            endif;


            $repeatsurgerytypearr = array();
            $dynamize = $this->input->post('dynamize');
            $exchangenail = $this->input->post('exchangenail');
            $iliacrestbonegraft = $this->input->post('iliacrestbonegraft');
            $other = $this->input->post('other');
            if ($other == ''):
                $otherval = '';
            else:
                $otherval = $this->input->post('otherval');
            endif;

            if ($nonunionrepeat != ''):
                if ($dynamize != ''):
                    $repeatsurgerytypearr[] = $dynamize;
                endif;
                if ($exchangenail != ''):
                    $repeatsurgerytypearr[] = $exchangenail;
                endif;
                if ($iliacrestbonegraft != ''):
                    $repeatsurgerytypearr[] = $iliacrestbonegraft;
                endif;
                if ($other != ''):
                    $repeatsurgerytypearr[] = $other;
                    $repeatsurgerytypearr[] = $otherval;
                endif;
                $repeatsurgerytype = implode(',', $repeatsurgerytypearr);
            else:
                $repeatsurgerytype = '';
            endif;
            $comments = $this->input->post('comments');

            $fracture_id = $this->input->post('fracture_id');
            $query = $this->db->query("SELECT MAX(`followup_id`) AS `id`  FROM ortho_followup WHERE fracture_id='$fracture_id'");
            $followup_id = $query->row()->id;
            If ($followup_id == ""):
                $followup_id = 1;
            else:
                $followup_id = $followup_id + 1;
            endif;
            $caseData = array(
                'followup_id' => $followup_id,
                'fracture_id' => $fracture_id,
                'date' => $date,
                'infection' => $infection,
                'incision_wound' => $infectionwound,
                'infection_depth' => $infectiondepth,
                'infection_duration' => $infectionduration,
                'infection_type' => $infection_type,
                'partial_weight_bear' => $partialweight,
                'painless_full_weight_bear' => $painlessweight,
                'healing_by_xray' => $healingxray,
                'knee_flex_greater_90deg' => $kneeflexion,
                'screw_breakage' => $screwbreakage,
                'screw_loosen' => $screwloosening,
                'nail_breakage' => $nailbreakage,
                'nail_loosen' => $nailloosening,
                'deformity' => $deformity,
                'deformity_alignment' => $deformity_alignment,
                'deformity_rotaion' => $rotation,
                'repeat_surgery' => $repeatsurgery,
                'repeat_surgery_cause' => $repeatsurgerycause,
                'repeat_surgery_type' => $repeatsurgerytype,
                'comments' => $comments
            );
            $status = $this->db->insert('ortho_followup', $caseData);

            $queryfollowupid = $this->db->query("SELECT * FROM ortho_followup WHERE followup_id='$followup_id' AND fracture_id='$fracture_id'");
            $imgfollowupid = $queryfollowupid->row()->id;

            $opCount = $this->input->post('opCount');

            $date_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $day = $this->input->post('day' . $i);
                $month = $this->input->post('month' . $i);
                $year = $this->input->post('year' . $i);
                $inputdate = $year . "-" . $month . "-" . $day;
                $date_array[] = $inputdate;
            }
            $date = implode(',', $date_array);



            $image_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $opType = $this->input->post('operation' . $i);
                $image_array[] = $opType;
            }
            $images = implode(',', $image_array);


            $name_array = array();
            $count = count($_FILES['userfile']['size']);
            foreach ($_FILES as $key => $value)
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['userfile']['name'] = $value['name'][$s];
                    $_FILES['userfile']['type'] = $value['type'][$s];
                    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['userfile']['error'] = $value['error'][$s];
                    $_FILES['userfile']['size'] = $value['size'][$s];
                    $config['upload_path'] = './assets/uploads/xray/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width'] = '2000';
                    $config['max_height'] = '2000';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $data = $this->upload->data();
                    $name_array[] = $data['file_name'];
                }
            $names = implode(',', $name_array);
            $xRayImagedata = array(
                'followup_id' => $imgfollowupid,
                'date' => $date,
                'image_name' => $names,
                'image_type' => $images
            );
            $status = $this->db->insert('ortho_image_followup', $xRayImagedata);
            if ($status):
                $this->session->set_userdata('successfull', 'Followup Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Followup Information Add Failed.');
            endif;
            redirect('surgery_report/SurgeryCase');

        else:
            redirect('home');
        endif;
    }

    public function editFollowupData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):


            $id_followup = $this->input->post('id_followup');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $date = $year . "-" . $month . "-" . $day;

            $infection = $this->input->post('infection');
            if ($infection == ''):
                $infection = 5;
            endif;
            $infection_typearr = array();
            if ($infection == 1):
                $infectionwound = $this->input->post('infectionwound');
                $infectiondepth = $this->input->post('infectiondepth');
                if ($infectionwound == ''):
                    $infectionwound = 5;
                endif;
                if ($infectiondepth == ''):
                    $infectiondepth = 5;
                endif;

                $infectionduration = $this->input->post('infectionduration');
                $osteomyelitis = $this->input->post('osteomyelitis');
                $amputation = $this->input->post('amputation');
                if ($osteomyelitis != ''):
                    $infection_typearr[] = $osteomyelitis;
                endif;
                if ($amputation != ''):
                    $infection_typearr[] = $amputation;
                endif;
                $infection_type = implode(',', $infection_typearr);
            else:
                $infectionwound = 5;
                $infectiondepth = 5;
                $infectionduration = '';
                $osteomyelitis = '';
                $amputation = '';
                $infection_type = '';
            endif;

            $partialweight = $this->input->post('partialweight');
            if ($partialweight == ''):
                $partialweight = 5;
            endif;

            $painlessweight = $this->input->post('painlessweight');
            if ($painlessweight == ''):
                $painlessweight = 5;
            endif;

            $healingxray = $this->input->post('healingxray');
            if ($healingxray == ''):
                $healingxray = 5;
            endif;
            $kneeflexion = $this->input->post('kneeflexion');
            if ($kneeflexion == ''):
                $kneeflexion = 5;
            endif;
            $screwbreakage = $this->input->post('screwbreakage');
            if ($screwbreakage == ''):
                $screwbreakage = 5;
            endif;
            $screwloosening = $this->input->post('screwloosening');
            if ($screwloosening == ''):
                $screwloosening = 5;
            endif;
            $nailbreakage = $this->input->post('nailbreakage');
            if ($nailbreakage == ''):
                $nailbreakage = 5;
            endif;
            $nailloosening = $this->input->post('nailloosening');
            if ($nailloosening == ''):
                $nailloosening = 5;
            endif;
            $deformity = $this->input->post('deformity');
            if ($deformity == ''):
                $deformity = 5;
            endif;

            if ($deformity == 1):
                $deformity_alignment = $this->input->post('alignment');
                $rotation = $this->input->post('rotation');
                if ($rotation == ''):
                    $rotation = 5;
                endif;
            else:
                $deformity_alignment = '';
                $rotation = 5;
            endif;

            $repeatsurgery = $this->input->post('repeatsurgery');
            if ($repeatsurgery == ''):
                $repeatsurgery = 5;
            endif;
            $infectionrepeat = $this->input->post('infectionrepeat');
            $deformityrepeat = $this->input->post('deformityrepeat');
            $nonunionrepeat = $this->input->post('nonunionrepeat');
            $repeatsurgeryarr = array();
            if ($repeatsurgery == 1):
                if ($infectionrepeat != ''):
                    $repeatsurgeryarr[] = $infectionrepeat;
                endif;
                if ($deformityrepeat != ''):
                    $repeatsurgeryarr[] = $deformityrepeat;
                endif;
                if ($nonunionrepeat != ''):
                    $repeatsurgeryarr[] = $nonunionrepeat;
                endif;
                $repeatsurgerycause = implode(',', $repeatsurgeryarr);
            else:
                $repeatsurgerycause = '';
            endif;


            $repeatsurgerytypearr = array();
            $dynamize = $this->input->post('dynamize');
            $exchangenail = $this->input->post('exchangenail');
            $iliacrestbonegraft = $this->input->post('iliacrestbonegraft');
            $other = $this->input->post('other');
            if ($other == ''):
                $otherval = '';
            else:
                $otherval = $this->input->post('otherval');
            endif;

            if ($nonunionrepeat != ''):
                if ($dynamize != ''):
                    $repeatsurgerytypearr[] = $dynamize;
                endif;
                if ($exchangenail != ''):
                    $repeatsurgerytypearr[] = $exchangenail;
                endif;
                if ($iliacrestbonegraft != ''):
                    $repeatsurgerytypearr[] = $iliacrestbonegraft;
                endif;
                if ($other != ''):
                    $repeatsurgerytypearr[] = $other;
                    $repeatsurgerytypearr[] = $otherval;
                endif;
                $repeatsurgerytype = implode(',', $repeatsurgerytypearr);
            else:
                $repeatsurgerytype = '';
            endif;
            $comments = $this->input->post('comments');

            $fracture_id = $this->input->post('fracture_id');
            $query = $this->db->query("SELECT MAX(`followup_id`) AS `id`  FROM ortho_followup WHERE fracture_id='$fracture_id'");
            $followup_id = $query->row()->id;
            If ($followup_id == ""):
                $followup_id = 1;
            else:
                $followup_id = $followup_id + 1;
            endif;
            $caseData = array(
                'date' => $date,
                'infection' => $infection,
                'incision_wound' => $infectionwound,
                'infection_depth' => $infectiondepth,
                'infection_duration' => $infectionduration,
                'infection_type' => $infection_type,
                'partial_weight_bear' => $partialweight,
                'painless_full_weight_bear' => $painlessweight,
                'healing_by_xray' => $healingxray,
                'knee_flex_greater_90deg' => $kneeflexion,
                'screw_breakage' => $screwbreakage,
                'screw_loosen' => $screwloosening,
                'nail_breakage' => $nailbreakage,
                'nail_loosen' => $nailloosening,
                'deformity' => $deformity,
                'deformity_alignment' => $deformity_alignment,
                'deformity_rotaion' => $rotation,
                'repeat_surgery' => $repeatsurgery,
                'repeat_surgery_cause' => $repeatsurgerycause,
                'repeat_surgery_type' => $repeatsurgerytype,
                'comments' => $comments
            );

            $this->db->where('id', $id_followup);
            $status = $this->db->update('ortho_followup', $caseData);

            $queryfollowupid = $this->db->query("SELECT * FROM ortho_followup WHERE followup_id='$followup_id' AND fracture_id='$fracture_id'");
            $imgfollowupid = $queryfollowupid->row()->id;

            $opCount = $this->input->post('opCount');
             $tCount = $this->input->post('tCount');
            $date_array = array();
//            for ($i = 1; $i <= $opCount; $i++) {
//                $day = $this->input->post('day' . $i);
//                $month = $this->input->post('month' . $i);
//                $year = $this->input->post('year' . $i);
//                $inputdate = $year . "-" . $month . "-" . $day;
//                $date_array[] = $inputdate;
//            }
//            $date = implode(',', $date_array);
//
//            $image_array = array();
//            for ($i = 1; $i <= $opCount; $i++) {
//                $opType = $this->input->post('operation' . $i);
//                $image_array[] = $opType;
//            }
//            $images = implode(',', $image_array);
//
//
//            $name_array = array();
//            $count = count($_FILES['userfile']['size']);
             for ($i = 1; $i <= $tCount; $i++) {
                $day = $this->input->post('day' . $i);
                $month = $this->input->post('month' . $i);
                $year = $this->input->post('year' . $i);
                if($day!="" && $month!="" && $year!=""):
                   $inputdate = $year . "-" . $month . "-" . $day;
                $date_array[] = $inputdate;  
                endif;
               
            }
            $date = implode(',', $date_array);

            $image_array = array();
            for ($i = 1; $i <= $tCount; $i++) {
                $opType = $this->input->post('operation' . $i);
                if($opType!=""):
                     $image_array[] = $opType;
                endif;
               
            }
            $images = implode(',', $image_array);


            $name_array = array();
            $count = count($_FILES['userfile']['size']);

            if ($opCount > $count):

                for ($i = 1; $i <= $tCount; $i++) {
                    $checkname = $this->input->post('xrayimgname' . $i);
                    if ($checkname == ""):
                        continue;
                    else:
                        $name_array[] = $checkname;
                    endif;
                }
            endif;
            
            
            
            foreach ($_FILES as $key => $value)
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['userfile']['name'] = $value['name'][$s];
                    $_FILES['userfile']['type'] = $value['type'][$s];
                    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['userfile']['error'] = $value['error'][$s];
                    $_FILES['userfile']['size'] = $value['size'][$s];
                    $config['upload_path'] = './assets/uploads/xray/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width'] = '2000';
                    $config['max_height'] = '2000';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $data = $this->upload->data();
//                    if ($data['file_name'] == ""):
//                        $name_array[] = $this->input->post('xrayimgname' . ($s + 1));
//                    else:
                        $name_array[] = $data['file_name'];
//                    endif;
                }
            $names = implode(',', $name_array);
            $xRayImagedata = array(
                'date' => $date,
                'image_name' => $names,
                'image_type' => $images
            );
            $this->db->where('followup_id', $id_followup);
            $status = $this->db->update('ortho_image_followup', $xRayImagedata);
            if ($status):
                $this->session->set_userdata('successfull', 'Followup Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Followup Information Add Failed.');
            endif;
            redirect('surgery_report/SurgeryCase');

        else:
            redirect('home');
        endif;
    }

    function deleteCaseData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $id_case = $this->input->post('id_delete');

            $this->db->where('id', $id_case);
            $status = $this->db->delete('ortho_case');

            $queryIdSurgery = $this->db->query("SELECT id FROM ortho_surgery WHERE case_id='$id_case'");
            $id_surgery = $queryIdSurgery->result();
            $this->db->where('case_id', $id_case);
            $status = $this->db->delete('ortho_surgery');

            if (sizeof($id_surgery) > 0):
                foreach ($id_surgery as $datarowsurgery):
                    $surgery_id = $datarowsurgery->id;
                    $queryIdFracture = $this->db->query("SELECT id FROM ortho_fracture WHERE surgery_id='$surgery_id'");
                    $id_fracture = $queryIdFracture->result();
                    $this->db->where('surgery_id', $surgery_id);
                    $status = $this->db->delete('ortho_fracture');
                    if (sizeof($id_fracture) > 0):
                        foreach ($id_fracture as $datarowfracture):
                            $fracture_id = $datarowfracture->id;
                            $this->db->where('fracture_id', $fracture_id);
                            $status = $this->db->delete('ortho_image_fracture');
                            $queryIdFollowup = $this->db->query("SELECT id FROM ortho_followup WHERE fracture_id='$fracture_id'");
                            $id_followup = $queryIdFollowup->result();
                            $this->db->where('fracture_id', $fracture_id);
                            $status = $this->db->delete('ortho_followup');

                            if (sizeof($id_followup) > 0):
                                foreach ($id_followup as $datarowfollowup):
                                    $followup_id = $datarowfollowup->id;
                                    $this->db->where('followup_id', $followup_id);
                                    $status = $this->db->delete('ortho_image_followup');

                                endforeach;
                            endif;
                        endforeach;
                    endif;
                endforeach;
            endif;



            if ($status):
                $this->session->set_userdata('successfull', 'Case Information Delete Successfully.');
            else:
                $this->session->set_userdata('failed', 'Case Information Delete Failed.');
            endif;
            redirect('surgery_report/SurgeryCase');

        else:
            redirect('home');
        endif;
    }

}
