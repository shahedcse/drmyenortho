<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Research extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS-Research";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "research";
            $queryallorthocase = $this->db->query("SELECT * FROM ortho_case");
            $data['allorthocase'] = $queryallorthocase->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/research', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    function getParameter() {
        $category = $_POST['category'];
        $query = $this->db->query("SHOW COLUMNS FROM $category");
        $result = $query->result();
        if (sizeof($result) > 0) :
            echo '<option value="">--Select Parameter--</option>';
            foreach ($result as $data_row) :
                if ($data_row->Field == 'id'):
                    continue;
                elseif ($data_row->Field == 'case_id'):
                    continue;
                elseif ($data_row->Field == 'surgery_id'):
                    continue;
                elseif ($data_row->Field == 'fracture_id'):
                    continue;
                elseif ($data_row->Field == 'followup_id'):
                    continue;
                elseif ($data_row->Field == 'infection_type'):
                    continue;
                elseif ($data_row->Field == 'repeat_surgery_cause'):
                    continue;
                elseif ($data_row->Field == 'repeat_surgery_type'):
                    continue;

                elseif (substr($data_row->Field, 0, 3) == 'sis' || substr($data_row->Field, 0, 3) == 'cs_' || substr($data_row->Field, 0, 3) == 'shc' || substr($data_row->Field, 0, 3) == 'rod' || substr($data_row->Field, 0, 3) == 'uni' || substr($data_row->Field, 0, 3) == 'hol' || substr($data_row->Field, 0, 3) == 'hv_'):
                    continue;
                elseif ($data_row->Field == 'surgeon_id'):
                    echo '<option value="' . $data_row->Field . '">' . "Surgeon Name" . '</option>';
                elseif ($data_row->Field == 'antibiotics'):
                    echo '<option value="' . $data_row->Field . '">' . "Antibiotics Used" . '</option>';
                elseif ($data_row->Field == 'antibiotics_after_hours'):
                    echo '<option value="' . $data_row->Field . '">' . "How long from time of injury" . '</option>';
                elseif ($data_row->Field == 'antibiotics_name'):
                    echo '<option value="' . $data_row->Field . '">' . "Antibiotics Name" . '</option>';
                elseif ($data_row->Field == 'antibiotics_coverage'):
                    echo '<option value="' . $data_row->Field . '">' . "Duration Of Antibiotic Coverage" . '</option>';
                elseif ($data_row->Field == 'comments'):
                    echo '<option value="' . $data_row->Field . '">' . "Comments" . '</option>';
                elseif ($data_row->Field == 'case_num'):
                    echo '<option value="' . $data_row->Field . '">' . "Case Number" . '</option>';
                elseif ($data_row->Field == 'patient_id'):
                    echo '<option value="' . $data_row->Field . '">' . "Patient Name" . '</option>';
                elseif ($data_row->Field == 'date' && $category == 'ortho_case'):
                    echo '<option value="' . $data_row->Field . '">' . "Injury Date" . '</option>';
                elseif ($data_row->Field == 'date' && $category == 'ortho_surgery'):
                    echo '<option value="' . $data_row->Field . '">' . "Surgery Date" . '</option>';
                elseif ($data_row->Field == 'date' && $category == 'ortho_followup'):
                    echo '<option value="' . $data_row->Field . '">' . "Followup Date" . '</option>';
                elseif ($data_row->Field == 'input_date'):
                    echo '<option value="' . $data_row->Field . '">' . "Input Date" . '</option>';

                elseif ($data_row->Field == 'fracture_side'):
                    echo '<option value="' . $data_row->Field . '">' . "Fracture Side" . '</option>';
                elseif ($data_row->Field == 'surgical_approch'):
                    echo '<option value="' . $data_row->Field . '">' . "Surgical Approch" . '</option>';
                elseif ($data_row->Field == 'fracture_location'):
                    echo '<option value="' . $data_row->Field . '">' . "Fracture Location" . '</option>';
                elseif ($data_row->Field == 'fracture_type'):
                    echo '<option value="' . $data_row->Field . '">' . "Fracture Type" . '</option>';
                elseif ($data_row->Field == 'fracture_stability'):
                    echo '<option value="' . $data_row->Field . '">' . "Fracture Stability" . '</option>';
                elseif ($data_row->Field == 'time_injury_debrid'):
                    echo '<option value="' . $data_row->Field . '">' . "Time From Injury To Debridement" . '</option>';
                elseif ($data_row->Field == 'time_injury_skin'):
                    echo '<option value="' . $data_row->Field . '">' . "Time From Injury To Skin Clouser" . '</option>';
                elseif ($data_row->Field == 'wound_closure_method'):
                    echo '<option value="' . $data_row->Field . '">' . "Method Of Wound Closure" . '</option>';
                elseif ($data_row->Field == 'nonunion'):
                    echo '<option value="' . $data_row->Field . '">' . "Nonunion" . '</option>';
                elseif ($data_row->Field == 'previous_implant_used'):
                    echo '<option value="' . $data_row->Field . '">' . "Previous Implant Used" . '</option>';
                elseif ($data_row->Field == 'previous_implant_type'):
                    echo '<option value="' . $data_row->Field . '">' . "Previous Implant Type" . '</option>';
                elseif ($data_row->Field == 'ext_fix_duration'):
                    echo '<option value="' . $data_row->Field . '">' . "How Long Was The External Fixation In Place" . '</option>';
                elseif ($data_row->Field == 'remvoe_ext_fix_sign'):
                    echo '<option value="' . $data_row->Field . '">' . "Time Between Removal Of External Fixation And SIGN" . '</option>';
                elseif ($data_row->Field == 'method_of_reaming'):
                    echo '<option value="' . $data_row->Field . '">' . "Method Of Reaming" . '</option>';
                elseif ($data_row->Field == 'fracture_reduction'):
                    echo '<option value="' . $data_row->Field . '">' . "Fracture Reduction" . '</option>';
                elseif ($data_row->Field == 'type_of_nail_used'):
                    echo '<option value="' . $data_row->Field . '">' . "Type Of Nail Used" . '</option>';
                elseif ($data_row->Field == 'length_of_nail'):
                    echo '<option value="' . $data_row->Field . '">' . "Length Of Nail" . '</option>';
                elseif ($data_row->Field == 'diameter_of_nail'):
                    echo '<option value="' . $data_row->Field . '">' . "Diameter Of Nail" . '</option>';

                elseif ($data_row->Field == 'patient_weight'):
                    echo '<option value="' . $data_row->Field . '">' . "Patient Weight" . '</option>';
                elseif ($data_row->Field == 'screw_with_nail'):
                    echo '<option value="' . $data_row->Field . '">' . "Screw With Nail" . '</option>';
                elseif ($data_row->Field == 'plate_used'):
                    echo '<option value="' . $data_row->Field . '">' . "Plate Used" . '</option>';
                else:
                    echo '<option value="' . $data_row->Field . '">' . $data_row->Field . '</option>';
                endif;
            endforeach;
        else :
            echo '<option value="">--no data found--</option>';
        endif;
    }

    function getSearchData() {
        $category = $_POST['category'];
        $parameter = $_POST['parameter'];
        $parametervalue = $_POST['parametervalue'];
        $result = [];

//********************************start ortho case here*************************************//
        if ($category == "ortho_case"):

            if ($parameter == "date" || $parameter == "input_date"):
                $from = $_POST['from'];
                $to = $_POST['to'];

                $date = new DateTime($from);
                $startdate = $date->format('Y-m-d');
                $date = new DateTime($to);
                $enddate = $date->format('Y-m-d');

                $queryallcase = $this->db->query("SELECT * FROM $category WHERE $parameter BETWEEN '$startdate' AND '$enddate'");
                $result = $queryallcase->result();

            elseif ($parameter == 'patient_id'):
                $queryalluser = $this->db->query("SELECT * FROM user WHERE username LIKE '%$parametervalue%'");
                $alluserresult = $queryalluser->result();
                if (sizeof($alluserresult)):
                    foreach ($alluserresult as $datarow):
                        $patient_id = $datarow->id;
                        $queryallcase = $this->db->query("SELECT * FROM ortho_case WHERE patient_id='$patient_id'");
                        $allcaseresult = $queryallcase->result();
                        if (sizeof($allcaseresult) > 0):
                            foreach ($allcaseresult as $data_row):
                                $id = $data_row->id;
                                $queryallsurgery = $this->db->query("SELECT * FROM ortho_surgery WHERE case_id='$id'");

                                $patient_id = $queryallcase->row()->patient_id;
                                $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
                                if ($query2->num_rows() > 0):
                                    $patient_name = $query2->row()->username;
                                else:
                                    $patient_name = '';
                                endif;


                                echo '<tr>
                    <td>' . $data_row->case_num . '</td>               
                  <td>' . $patient_name . '</td>
                    <td> <a href = "' .
                                site_url('surgery_report/Research/editCaseView/?id=' . $id) . '">
                 <button class = "btn btn-primary btn-xs"><i class = "fa fa-edit"></i>&nbsp;
                    View</button></a>
                   </td>
                    </tr>';
                            endforeach;
//                        else:
//                            echo '<tr>
//                    <td colspan="3">No data found</td>                             
//                    </tr>';

                        endif;

                    endforeach;
                endif;

            else:
                $queryallcase = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$parametervalue%'");
                $result = $queryallcase->result();
            endif;

            if (sizeof($result) > 0):
                foreach ($result as $data_row):
                    $id = $data_row->id;
                    $queryallsurgery = $this->db->query("SELECT * FROM ortho_surgery WHERE case_id='$id'");

                    $patient_id = $queryallcase->row()->patient_id;
                    $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
                    if ($query2->num_rows() > 0):
                        $patient_name = $query2->row()->username;
                    else:
                        $patient_name = '';
                    endif;


                    echo '<tr>
                    <td>' . $data_row->case_num . '</td>               
                  <td>' . $patient_name . '</td>
                    <td> <a href = "' .
                    site_url('surgery_report/Research/editCaseView/?id=' . $id) . '">
                 <button class = "btn btn-primary btn-xs"><i class = "fa fa-edit"></i>&nbsp;
                    View</button></a>
                   </td>
                    </tr>';
                endforeach;
            else:
//                echo '<tr>
//                    <td colspan="3">No data found</td>                             
//                    </tr>';

            endif;
        endif;

//********************************end ortho case here*************************************//
//********************************start ortho surgery here*************************************//       
        if ($category == "ortho_surgery"):

            if ($parameter == "date"):
                $from = $_POST['from'];
                $to = $_POST['to'];

                $date = new DateTime($from);
                $startdate = $date->format('Y-m-d');
                $date = new DateTime($to);
                $enddate = $date->format('Y-m-d');

                $queryallsurgery = $this->db->query("SELECT * FROM $category WHERE $parameter BETWEEN '$startdate' AND '$enddate'");
                $result = $queryallsurgery->result();

            elseif ($parameter == "surgeon_id"):
                $queryallsurgeon = $this->db->query("SELECT * FROM ortho_surgeon WHERE name LIKE '%$parametervalue%'");
                $allsurgeonresult = $queryallsurgeon->result();
                if (sizeof($allsurgeonresult)):
                    foreach ($allsurgeonresult as $datarow):
                        $surgeon_id = $datarow->id;
                        $queryallsurgery = $this->db->query("SELECT * FROM ortho_surgery WHERE surgeon_id LIKE '%$surgeon_id%'");
                        $allsurgeryresult = $queryallsurgery->result();
                        if (sizeof($allsurgeryresult) > 0):
                            foreach ($allsurgeryresult as $data_row):
                                $id = $data_row->case_id;
                                $queryallcase = $this->db->query("SELECT * FROM ortho_case WHERE id='$id'");
                                $allcaseresult = $queryallcase->result();

                                if (sizeof($allcaseresult) > 0):
                                    foreach ($allcaseresult as $data_row):
                                        $patient_id = $queryallcase->row()->patient_id;
                                        $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
                                        $patient_name = $query2->row()->username;

                                        echo '<tr>
                    <td>' . $data_row->case_num . '</td>               
                  <td>' . $patient_name . '</td>
                    <td> <a href = "' .
                                        site_url('surgery_report/Research/editCaseView/?id=' . $id) . '">
                 <button class = "btn btn-primary btn-xs"><i class = "fa fa-edit"></i>&nbsp;
                    View</button></a>
                   </td>
                    </tr>';
                                    endforeach;
                                endif;
                            endforeach;
//                        else:
//                            echo '<tr>
//                   <td colspan="3">No data found</td>   
//                    </tr>';

                        endif;

                    endforeach;
                endif;
            elseif ($parameter == "antibiotics") :
                $antibiotic = $_POST['antibiotic'];
                $queryallsurgery = $this->db->query("SELECT * FROM $category WHERE $parameter = '$antibiotic'");
                $result = $queryallsurgery->result();
            elseif ($parameter == "antibiotics_after_hours" || $parameter == "antibiotics_coverage") :
                $queryallsurgery = $this->db->query("SELECT * FROM $category WHERE $parameter <= '$parametervalue'");
                $result = $queryallsurgery->result();
            else:
                $queryallsurgery = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$parametervalue%'");
                $result = $queryallsurgery->result();
            endif;

//            print_r($result);
//            exit();

            if (sizeof($result) > 0):
                foreach ($result as $data_row):
                    $id = $data_row->case_id;
                    $queryallcase = $this->db->query("SELECT * FROM ortho_case WHERE id='$id'");
                    $result = $queryallcase->result();

                    if (sizeof($result) > 0):
                        foreach ($result as $data_row):
                            $patient_id = $queryallcase->row()->patient_id;
                            $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
                            $patient_name = $query2->row()->username;

                            echo '<tr>
                    <td>' . $data_row->case_num . '</td>               
                  <td>' . $patient_name . '</td>
                    <td> <a href = "' .
                            site_url('surgery_report/Research/editCaseView/?id=' . $id) . '">
                 <button class = "btn btn-primary btn-xs"><i class = "fa fa-edit"></i>&nbsp;
                    View</button></a>
                   </td>
                    </tr>';
                        endforeach;
                    endif;
                endforeach;
//            else:
//                echo '<tr>
//                   <td colspan="3">No data found</td>   
//                    </tr>';

            endif;
        endif;
//********************************end ortho surgery here*************************************// 
//********************************start ortho fracture here*************************************// 
        if ($category == "ortho_fracture"):

            if ($parameter == "fracture_side"):
                $fractureside = $_POST['fractureside'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$fractureside'");
                $result = $queryallfracture->result();
            elseif ($parameter == "surgical_approch"):
                $surgicalapp = $_POST['surgicalapp'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$surgicalapp'");
                $result = $queryallfracture->result();
            elseif ($parameter == "fracture_type"):
                $typefrac = $_POST['typefrac'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$typefrac'");
                $result = $queryallfracture->result();
            elseif ($parameter == "time_injury_debrid" || $parameter == "time_injury_skin" || $parameter == "ext_fix_duration" || $parameter == "remvoe_ext_fix_sign") :
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter <= '$parametervalue'");
                $result = $queryallfracture->result();
            elseif ($parameter == "nonunion" || $parameter == "previous_implant_used" || $parameter == "screw_with_nail" || $parameter == "plate_used") :
                $antibiotic = $_POST['antibiotic'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$antibiotic'");
                $result = $queryallfracture->result();
            elseif ($parameter == "method_of_reaming"):
                $reaming = $_POST['reaming'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$reaming'");
                $result = $queryallfracture->result();
            elseif ($parameter == "fracture_reduction"):
                $fracreduc = $_POST['fracreduc'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$fracreduc'");
                $result = $queryallfracture->result();
            elseif ($parameter == "length_of_nail"):
                $length_nail = $_POST['length_nail'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$length_nail'");
                $result = $queryallfracture->result();
            elseif ($parameter == "diameter_of_nail"):
                $diameter_nail = $_POST['diameter_nail'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter = '$diameter_nail'");
                $result = $queryallfracture->result();
            elseif ($parameter == "fracture_location"):
                $locfrac = $_POST['locfrac'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$locfrac%'");
                $result = $queryallfracture->result();
            elseif ($parameter == "fracture_stability"):
                $stafrac = $_POST['stafrac'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$stafrac%'");
                $result = $queryallfracture->result();
            elseif ($parameter == "wound_closure_method"):
                $woundclosure = $_POST['woundclosure'];
                if ($woundclosure == "other"):
                    $woundclosure = $parametervalue;
                endif;
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$woundclosure%'");
                $result = $queryallfracture->result();
            elseif ($parameter == "previous_implant_type"):
                $previousimplanttype = $_POST['previousimplanttype'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$previousimplanttype%'");
                $result = $queryallfracture->result();
            elseif ($parameter == "type_of_nail_used"):
                $typenail = $_POST['typenail'];
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$typenail%'");
                $result = $queryallfracture->result();
            else:
                $queryallfracture = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$parametervalue%'");
                $result = $queryallfracture->result();
            endif;


            if (sizeof($result) > 0):
                foreach ($result as $data_row):
                    $id = $data_row->surgery_id;
                    $queryallsurgery = $this->db->query("SELECT * FROM ortho_surgery WHERE id='$id'");
                    $result = $queryallsurgery->result();

                    if (sizeof($result) > 0):
                        foreach ($result as $data_row):
                            $id = $data_row->case_id;
                            $queryallcase = $this->db->query("SELECT * FROM ortho_case WHERE id='$id'");
                            $result = $queryallcase->result();

                            if (sizeof($result) > 0):
                                foreach ($result as $data_row):
                                    $patient_id = $queryallcase->row()->patient_id;
                                    $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
                                    $patient_name = $query2->row()->username;

                                    echo '<tr>
                    <td>' . $data_row->case_num . '</td>               
                  <td>' . $patient_name . '</td>
                    <td> <a href = "' .
                                    site_url('surgery_report/Research/editCaseView/?id=' . $id) . '">
                 <button class = "btn btn-primary btn-xs"><i class = "fa fa-edit"></i>&nbsp;
                    View</button></a>
                   </td>
                    </tr>';
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                endforeach;
            else:
                echo '<tr>
                  <td colspan="3">No data found</td>   
                    </tr>';

            endif;
        endif;

        //********************************end ortho fracture here*************************************//
        //********************************start ortho followup here*************************************// 

        if ($category == "ortho_followup"):


            if ($parameter == "date"):
                $from = $_POST['from'];
                $to = $_POST['to'];

                $date = new DateTime($from);
                $startdate = $date->format('Y-m-d');
                $date = new DateTime($to);
                $enddate = $date->format('Y-m-d');

                $queryallfollowup = $this->db->query("SELECT * FROM $category WHERE $parameter BETWEEN '$startdate' AND '$enddate'");
                $result = $queryallfollowup->result();

            elseif ($parameter == "infection" || $parameter == "incision_wound" || $parameter == "infection_depth" || $parameter == "partial_weight_bear" || $parameter == "painless_full_weight_bear" || $parameter == "healing_by_xray" || $parameter == "knee_flex_greater_90deg" || $parameter == "screw_breakage" || $parameter == "screw_loosen" || $parameter == "nail_breakage" || $parameter == "nail_loosen" || $parameter == "deformity" || $parameter == "deformity_rotaion" || $parameter == "repeat_surgery") :
                $antibiotic = $_POST['antibiotic'];
                $queryallfollowup = $this->db->query("SELECT * FROM $category WHERE $parameter = '$antibiotic'");
                $result = $queryallfollowup->result();

            elseif ($parameter == "infection_duration") :
                $queryallfollowup = $this->db->query("SELECT * FROM $category WHERE $parameter <= '$parametervalue'");
                $result = $queryallfollowup->result();
            elseif ($parameter == "deformity_alignment") :
                $alignment = $_POST['alignment'];
                $queryallfollowup = $this->db->query("SELECT * FROM $category WHERE $parameter = '$alignment'");
                $result = $queryallfollowup->result();
            else:
                $queryallfollowup = $this->db->query("SELECT * FROM $category WHERE $parameter LIKE '%$parametervalue%'");
                $result = $queryallfollowup->result();
            endif;





            if (sizeof($result) > 0):
                foreach ($result as $data_row):
                    $id = $data_row->fracture_id;
                    $queryallfracture = $this->db->query("SELECT * FROM ortho_fracture WHERE id='$id'");
                    $result = $queryallfracture->result();

                    if (sizeof($result) > 0):
                        foreach ($result as $data_row):
                            $id = $data_row->surgery_id;
                            $queryallsurgery = $this->db->query("SELECT * FROM ortho_surgery WHERE id='$id'");
                            $result = $queryallsurgery->result();

                            if (sizeof($result) > 0):
                                foreach ($result as $data_row):
                                    $id = $data_row->case_id;
                                    $queryallcase = $this->db->query("SELECT * FROM ortho_case WHERE id='$id'");
                                    $result = $queryallcase->result();

                                    if (sizeof($result) > 0):
                                        foreach ($result as $data_row):
                                            $patient_id = $queryallcase->row()->patient_id;
                                            $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
                                            $patient_name = $query2->row()->username;

                                            echo '<tr>
                    <td>' . $data_row->case_num . '</td>               
                  <td>' . $patient_name . '</td>
                    <td> <a href = "' .
                                            site_url('surgery_report/Research/editCaseView/?id=' . $id) . '">
                 <button class = "btn btn-primary btn-xs"><i class = "fa fa-edit"></i>&nbsp;
                    View</button></a>
                   </td>
                    </tr>';
                                        endforeach;
                                    endif;
                                endforeach;
                            endif;
                        endforeach;
                    else:
                        echo '<tr>
                  <td colspan="3">No data found</td>   
                    </tr>';
                    endif;
                endforeach;
            else:
                echo '<tr>
                  <td colspan="3">No data found</td>   
                    </tr>';
            endif;
        endif;
    }

    public function editCaseView() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "View Case";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "research";
            $id = $this->input->get('id');

            $data['case_id'] = $id;
            $query = $this->db->query("SELECT * FROM ortho_case WHERE id='$id'");
            $data['orthocase'] = $query->row();
            $queryallsurgery = $this->db->query("SELECT * FROM ortho_surgery WHERE case_id='$id'");
            $data['resultallsurgery'] = $queryallsurgery->result();

            $patient_id = $query->row()->patient_id;
            $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
            $data['patientlist'] = $query2->row();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_case_view', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function editSurgeryView() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "View Surgery";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "research";
            $id_surgery = $this->input->get("id");
            $data['id_surgery'] = $id_surgery;
            $querysurgerycase = $this->db->query("SELECT * FROM ortho_surgery WHERE id='$id_surgery'");
            $data['surgerycase'] = $querysurgerycase->row();
            $case_id = $querysurgerycase->row()->case_id;
            $query = $this->db->query("SELECT * FROM ortho_surgeon WHERE status='1'");
            $data['surgeonlist'] = $query->result();
            $query2 = $this->db->query("SELECT * FROM ortho_case WHERE id='$case_id'");
            $data['orthocase'] = $query2->row();
            $query3 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='surgery'");
            $data['codenum'] = $query3->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_surgery_view', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function editFractureView() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "View Fracture";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "research";
            $id_fracture = $this->input->get('id');
            $data['id_fracture'] = $id_fracture;
            $queryallfracture = $this->db->query("SELECT * FROM ortho_fracture Where id='$id_fracture'");
            $data['orthofracture'] = $queryallfracture->row();

            $queryallimgfracture = $this->db->query("SELECT * FROM ortho_image_fracture Where fracture_id='$id_fracture'");
            $data['orthoimgfracture'] = $queryallimgfracture->row();
            $querysurgeryid = $this->db->query("SELECT surgery_id FROM ortho_fracture Where id='$id_fracture'");
            $surgery_id = $querysurgeryid->row()->surgery_id;
            $querycaseid = $this->db->query("SELECT case_id FROM ortho_surgery Where id='$surgery_id'");
            $case_id = $querycaseid->row()->case_id;
            $query2 = $this->db->query("SELECT * FROM ortho_case Where id='$case_id'");
            $data['orthocase'] = $query2->row();
            $query3 = $this->db->query("SELECT * FROM ortho_surgery Where case_id='$case_id'");
            $data['orthosurgery'] = $query3->row();
            $query4 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='fracture'");
            $data['codenum'] = $query4->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_fracture_view', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function editFollowupView() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "View Followup";
            $data['active_menu'] = "surgery";
            $data['active_sub_menu'] = "research";

            $id_followup = $this->input->get('id');
            $data['id_followup'] = $id_followup;
            $queryallfollowup = $this->db->query("SELECT * FROM ortho_followup WHERE id = '$id_followup'");
            $data['orthofollowup'] = $queryallfollowup->row();

            $id_fracture = $queryallfollowup->row()->fracture_id;
            $data['fractureid'] = $id_fracture;

            $queryallimgfracture = $this->db->query("SELECT * FROM ortho_image_fracture Where fracture_id='$id_fracture'");
            $data['orthoimgfracture'] = $queryallimgfracture->row();

            $queryallimgfollowup = $this->db->query("SELECT * FROM ortho_image_followup Where followup_id='$id_followup'");
            $data['orthoimgfollowup'] = $queryallimgfollowup->row();

            $surgeryidQr = $this->db->query("SELECT surgery_id FROM ortho_fracture WHERE id = '$id_fracture'");
            $surgeryid = $surgeryidQr->row()->surgery_id;

            $caseidQr = $this->db->query("SELECT case_id FROM ortho_surgery WHERE id = '$surgeryid'");
            $caseid = $caseidQr->row()->case_id;
            $query2 = $this->db->query("SELECT * FROM ortho_case WHERE id='$caseid'");
            $data['orthocase'] = $query2->row();

            $query4 = $this->db->query("SELECT code_no FROM ortho_code WHERE type='followup'");
            $data['codenum'] = $query4->result();

            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery_report/edit_followup_view', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

}
