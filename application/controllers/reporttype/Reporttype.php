<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reporttype extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Test_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Report Type";
            $data['active_menu'] = "settings";
            $data['active_sub_menu'] = "reporttype";
            $reportQry = $this->db->query('SELECT * from report_type');
            $data['reportlist'] = $reportQry->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('reporttype/reporttype', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function addReporttype() {
        $report_typeData = array(
            'reporttype' => $this->input->post('reporttype'),
        );
        $status = $this->db->insert('report_type', $report_typeData);
        if ($status):
            $this->session->set_userdata('successfull', 'Report Type Information Add Successfully.');
        else:
            $this->session->set_userdata('failed', 'Report Type Information Add Failed.');
        endif;
        redirect('reporttype/reporttype');
    }

    public function delete_reporttype() {
        $rId = $this->input->post('rid');
        $this->db->where('id', $rId);
        $status = $this->db->delete('report_type');
        if ($status):
            $this->session->set_userdata('successfull', 'Report Type Information Delete Successfully.');
        else:
            $this->session->set_userdata('failed', 'Report Type Information Delete Failed.');
        endif;
        redirect('reporttype/reporttype');
    }

}
