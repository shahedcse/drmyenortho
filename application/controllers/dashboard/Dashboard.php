<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Dashboard";
            $data['active_menu'] = "dashboard";
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('dashboard/dashboard', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

}
