<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('patient_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Patient";
            $data['active_menu'] = "patient";
            $data['patientlist'] = $this->patient_model->patientList();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('patient/patient', $data);
            $this->load->view('footer', $data);
            $this->load->view('patient/patient_script', $data);
        else:
            redirect('home');
        endif;
    }

    public function addPatient() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

//            $patientName = $this->input->post('username');
//            $password = $this->input->post('password');
            $fName = $this->input->post('fname');
            $lName = $this->input->post('lname');
            $sex = $this->input->post('sex');
            $bloodGroup = $this->input->post('bloodgroup');
//            $originalDate = $this->input->post('birthday');
//            $birthDay = date("Y-m-d", strtotime($originalDate));
//            $day = $this->input->post('birthday');
//            $month = $this->input->post('birthmonth');
//            $year = $this->input->post('birthyear');
//            $birthDay = $year . "-" . $month . "-" . $day;
            $age = $this->input->post('age');
            $mobile = $this->input->post('mobile');
            $email = $this->input->post('email');
            $address = $this->input->post('address');
            $description = $this->input->post('description');

            $config['file_name'] = hash('sha1', $mobile);
            $config['overwrite'] = false;

            $config['upload_path'] = './assets/uploads/profile/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '2000';
            $config['max_width'] = '2000';
            $config['max_height'] = '2000';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // $this->upload->set_allowed_types('*');
            $data['upload_data'] = '';

            if (!$this->upload->do_upload('userfile')) {
                $error = $this->upload->display_errors();
//                $this->session->set_userdata("failed", "Image Upload Failed. " . $error);
                $patientdata = array(
                    'username' => $mobile,
                    'password' => $mobile,
                    'first_name' => $fName,
                    'last_name' => $lName,
                    'address' => $address,
                    'mobile' => $mobile,
                    'age' => $age,
                    'email' => $email,
                    'user_role' => '4',
                    'sex' => $sex,
                    'blood_group' => $bloodGroup,
//                    'date_of_birth' => $birthDay,
                    'image_name' => "default.jpg",
                    'description' => $description,
                    'created_date' => date('Y-m-d H:i:s'),
                    'status' => '1'
                );
                $status = $this->db->insert('user', $patientdata);
                if ($status):
                    $this->session->set_userdata('successfull', 'Patient Information Add Successfully.Image Upload Failed.' . $error);
                else:
                    $this->session->set_userdata('failed', 'Patient Information Add Failed.Image Upload Failed.' . $error);
                endif;
                redirect('patient/patient');
            } else {
                $data['upload_data'] = $this->upload->data();
                $upload_data = $data['upload_data'];
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload->upload_path . $this->upload->file_name;
                $config['create_thumb'] = FALSE;
                $config['new_image'] = 'thumb_' . $data['upload_data']['file_name'];
                $config['maintain_ratio'] = FALSE;
                $config['quality'] = '100%';
                $config['width'] = 250;
                $config['height'] = 200;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                if (!$this->image_lib->resize()) {
                    $error = $this->image_lib->display_errors();
                    $this->session->set_userdata('failed', 'Image Resize Failed.' . $error);
                    redirect('patient/patient');
                }
                $patientdata = array(
                    'username' => $mobile,
                    'password' => $mobile,
                    'first_name' => $fName,
                    'last_name' => $lName,
                    'address' => $address,
                    'mobile' => $mobile,
                    'age' => $age,
                    'email' => $email,
                    'user_role' => '4',
                    'sex' => $sex,
                    'blood_group' => $bloodGroup,
//                    'date_of_birth' => $birthDay,
                    'image_name' => 'thumb_' . $data['upload_data']['file_name'],
                    'description' => $description,
                    'created_date' => date('Y-m-d H:i:s'),
                    'status' => '1'
                );

                if ($data['upload_data']['file_name'] != "default.jpg"):
                    unlink('./assets/uploads/profile/' . $data['upload_data']['file_name']);
                endif;


                $status = $this->db->insert('user', $patientdata);
                if ($status):
                    $this->session->set_userdata('successfull', 'Patient Information Add Successfully.Image Uploaded Successfully.');
                else:
                    $this->session->set_userdata('failed', 'Patient Information Add Failed.Image Uploaded Successfully.');
                endif;
                redirect('patient/patient');
            }

        else:
            redirect('home');
        endif;
    }

    public function editPatient() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $patientId = $this->input->post('id_edit');
            $patientName = $this->input->post('username_edit');
            $password = $this->input->post('password_edit');
            $fName = $this->input->post('fname_edit');
            $lName = $this->input->post('lname_edit');
            $sex = $this->input->post('sex_edit');
            $bloodGroup = $this->input->post('bloodgroup_edit');
            $age = $this->input->post('age_edit');
//            $originalDate = $this->input->post('birthday_edit');
//            $birthDay = date("Y-m-d", strtotime($originalDate));
//            $day = $this->input->post('birthday_edit');
//            $month = $this->input->post('birthmonth_edit');
//            $year = $this->input->post('birthyear_edit');
//            $birthDay = $year . "-" . $month . "-" . $day;
            $mobile = $this->input->post('mobile_edit');
            $email = $this->input->post('email_edit');
            $address = $this->input->post('address_edit');
            $description = $this->input->post('description_edit');


            $patientdata = array(
                'username' => $patientName,
                'password' => $password,
                'first_name' => $fName,
                'last_name' => $lName,
                'address' => $address,
                'mobile' => $mobile,
                'age' => $age,
                'email' => $email,
                'user_role' => '4',
                'sex' => $sex,
                'blood_group' => $bloodGroup,
//                'date_of_birth' => $birthDay,
                'description' => $description
            );
            $this->db->where('id', $patientId);
            $status = $this->db->update('user', $patientdata);
            if ($status):
                $this->session->set_userdata('successfull', 'Patient Information Edit Successfully.');
            else:
                $this->session->set_userdata('failed', 'Patient Information Edit Failed.');
            endif;
            redirect('patient/patient');
        else:
            redirect('home');
        endif;
    }

    function checkUniqueUser() {
        $username = $_POST['username'];
        $userQuery = $this->db->query("SELECT username FROM user WHERE username = '$username'");
        if ($userQuery->num_rows() > 0):
            echo 'booked';
        else:
            echo 'free';
        endif;
    }

    function getPatientData() {
        $patient_id = $this->input->post('patient_id');
        $query = $this->patient_model->getSelectData("user", "id", $patient_id);

        if (sizeof($query)):
            echo json_encode($query);
        else:
            echo "No data found";
        endif;
    }

    function checkPatientUse() {
        $patient_id = $this->input->post('patient_id');
        $query = $this->patient_model->getSelectData("presmaster", "patient_id", $patient_id);
        
        if (sizeof($query)):
            echo "exits";
        else:
            echo "not exits";
        endif;
    }

    function deletePatient() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $id_delete = $this->input->post('id_delete');
            $queryImageName = $this->db->query("SELECT image_name FROM user WHERE id='$id_delete'");
            $imageName = $queryImageName->row()->image_name;
            if ($imageName != "default.jpg"):
                unlink('./assets/uploads/profile/' . $imageName);
            endif;
            $this->db->where('id', $id_delete);
            $query = $this->db->delete('user');

            $this->db->where('patient_id', $id_delete);
            $this->db->delete('appointment');

            if ($query):
                $this->session->set_userdata('successfull', 'Patient Information Delete Successfully.');
            else:
                $this->session->set_userdata('failed', 'Patient Information Delete Failed.');
            endif;
            redirect('patient/patient');
        else:
            redirect('home');
        endif;
    }

    function changeProfilePicture() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $patient_id = $this->input->post('id_img');
            $imageName = $this->input->post('name_img');

            $config['file_name'] = hash('sha1', $imageName);
            $config['overwrite'] = true;

            $config['upload_path'] = './assets/uploads/profile/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '2000';
            $config['max_width'] = '2000';
            $config['max_height'] = '2000';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            // $this->upload->set_allowed_types('*');
            $data['upload_data'] = '';

            if (!$this->upload->do_upload('userfile_edit')) {
                $error = $this->upload->display_errors();
                $this->session->set_userdata('failed', 'Image Upload Failed.' . $error);
                redirect('patient/patient');
            } else {
                $data['upload_data'] = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload->upload_path . $this->upload->file_name;
                $config['create_thumb'] = FALSE;
                $config['new_image'] = 'thumb_' . $data['upload_data']['file_name'];
                $config['maintain_ratio'] = FALSE;
                $config['quality'] = '100%';
                $config['width'] = 250;
                $config['height'] = 200;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                if (!$this->image_lib->resize()) {
                    $error = $this->image_lib->display_errors();
                    $this->session->set_userdata('failed', 'Image Resize Failed.' . $error);
                    redirect('patient/patient');
                }

                $upload_data = $data['upload_data'];
                $patientdata = array(
                    'image_name' => 'thumb_' . $data['upload_data']['file_name']
                );

                if ($data['upload_data']['file_name'] != "default.jpg"):
                    unlink('./assets/uploads/profile/' . $data['upload_data']['file_name']);
                endif;


                $this->db->where('id', $patient_id);
                $status = $this->db->update('user', $patientdata);
                if ($status):
                    $this->session->set_userdata('successfull', 'Image Upload Successfully.');
                    redirect('patient/patient');

                else:
                    $this->session->set_userdata('failed', 'Image Upload Failed.');
                    redirect('patient/patient');
                endif;
            }
        else:
            redirect('home');
        endif;
    }

}
