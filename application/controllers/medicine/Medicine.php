<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Medicine extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Medicine_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Medicine";
            $data['active_menu'] = "settings";
            $data['active_sub_menu'] = "medicine";
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('medicine/medicine', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function getMedicineList() {
        $table = 'medicine';
        $primaryKey = 'id';
        $columns = array(
            array('db' => 'manufacturer_name', 'dt' => 0),
            array('db' => 'brand_name', 'dt' => 1),
            array('db' => 'generic_name', 'dt' => 2),
            array('db' => 'strength', 'dt' => 3),
            array('db' => 'formulation', 'dt' => 4),
            array('db' => 'dosages', 'dt' => 5),
            array('db' => 'notes', 'dt' => 6),
            array('db' => 'id', 'dt' => 7,
                'formatter' => function ($rowvalue, $row) {
                    return '<a  href="#"  onclick="showDeleteModal(' . $rowvalue . ')">
      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o">&nbsp;Delete</i></button></a>&nbsp;<a href="#"  onclick="showEditModal(\'' . $row[7] . "," . $row[0] . "," . $row[1] . "," . $row[2] . "," . $row[3] . "," . $row[4] ."," . $row[5] ."," . $row[6] . '\')">
         <button class="btn btn-primary btn-xs"><i class="fa fa-edit">&nbsp;Edit</i></button></a>';
                })
        );
        $this->load->database();
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $this->load->library('SSP');

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    public function addMedicine() {
        $medicineData = array(
            'manufacturer_name' => $this->input->post('manufacturer_name'),
            'brand_name' => $this->input->post('brand_name'),
            'generic_name' => $this->input->post('generic_name'),
            'strength' => $this->input->post('strength'),
            'formulation' => $this->input->post('formulation'),
            'dosages' => $this->input->post('dosages'),
            'notes' => $this->input->post('notes')
        );
        $status = $this->db->insert('medicine', $medicineData);
        if ($status):
            $this->session->set_userdata('successfull', 'Medicine Information Add Successfully.');
        else:
            $this->session->set_userdata('failed', 'Medicine Information Add Failed.');
        endif;
        redirect('medicine/medicine');
    }

    public function editMedicine() {
        $mId = $this->input->post('medicine_id');
        $medicineData = array(
            'manufacturer_name' => $this->input->post('manufacturer_name'),
            'brand_name' => $this->input->post('brand_name'),
            'generic_name' => $this->input->post('generic_name'),
            'strength' => $this->input->post('strength'),
            'formulation' => $this->input->post('formulation'),
            'dosages' => $this->input->post('dosages'),
            'notes' => $this->input->post('notes')
        );
        #$status = $this->db->insert('medicine', $medicineData);
        $this->db->where('id', $mId);
        $status = $this->db->update('medicine', $medicineData);
        if ($status):
            $this->session->set_userdata('successfull', 'Medicine Information Edit Successfully.');
        else:
            $this->session->set_userdata('failed', 'Medicine Information Edit Failed.');
        endif;
        redirect('medicine/medicine');
    }

    public function deleteMedicine() {
        $mId = $this->input->post('medicine_id');
        $this->db->where('id', $mId);
        $status = $this->db->delete('medicine');
        if ($status):
            $this->session->set_userdata('successfull', 'Medicine Information Delete Successfully.');
        else:
            $this->session->set_userdata('failed', 'Medicine Information Delete Failed.');
        endif;
        redirect('medicine/medicine');
    }

}
