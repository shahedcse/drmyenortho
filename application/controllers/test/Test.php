<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Test_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Test";
            $data['active_menu'] = "settings";
            $data['active_sub_menu'] = "test";
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('test/test', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function getTestList() {
        $table = 'test';
        $primaryKey = 'id';
        $columns = array(
            array('db' => 'id', 'dt' => 0),
            array('db' => 'test_name', 'dt' => 1),
            array('db' => 'id', 'dt' => 2,
                'formatter' => function ($rowvalue, $row) {
            return '<a  href="#"  onclick="showDeleteModal(' . $rowvalue . ')">
      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o">&nbsp;Delete</i></button></a>&nbsp;<a href="#"  onclick="showEditModal(\'' . $row[2] . "-," . $row[0] . "-," . $row[1] . '\')">
         <button class="btn btn-primary btn-xs"><i class="fa fa-edit">&nbsp;Edit</i></button></a>';
        })
        );
        $this->load->database();
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $this->load->library('SSP');

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    public function addTest() {
        $testData = array(
            'test_name' => $this->input->post('test_name'),
            'status' => 1
        );
        $status = $this->db->insert('test', $testData);
        if ($status):
            $this->session->set_userdata('successfull', 'Test Information Add Successfully.');
        else:
            $this->session->set_userdata('failed', 'Test Information Add Failed.');
        endif;
        redirect('test/test');
    }

    public function editTest() {
        $mId = $this->input->post('id');
        $testData = array(
            'test_name' => $this->input->post('test_name')
        );
        $this->db->where('id', $mId);
        $status = $this->db->update('test', $testData);
        if ($status):
            $this->session->set_userdata('successfull', 'Test Information Edit Successfully.');
        else:
            $this->session->set_userdata('failed', 'Test Information Edit Failed.');
        endif;
        redirect('test/test');
    }

    public function deleteTest() {
        $mId = $this->input->post('testid');
        $this->db->where('id', $mId);
        $status = $this->db->delete('test');
        if ($status):
            $this->session->set_userdata('successfull', 'Test Information Delete Successfully.');
        else:
            $this->session->set_userdata('failed', 'Test Information Delete Failed.');
        endif;
        redirect('test/test');
    }

}
