<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prescription extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('prescription_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Prescription";
            $data['active_menu'] = "prescription";
            #$data['roledata'] = $this->doctor_model->roleDetails();
            $data['prescriptionList'] = $this->prescription_model->getpressMasterData('presmaster');
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('prescription/prescription', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    function addprescription() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Add Prescription";
            $data['active_menu'] = "prescription";
            $data['testlist'] = $this->prescription_model->getAllData('test');
            $referenceQry = $this->db->query("select * from reference where ref_under = '1'");
            $data['referencelist'] = $referenceQry->result();
            $data['patientlist'] = $this->prescription_model->getData('user', 'user_role', 4);
            $data['medicinelist'] = $this->prescription_model->getAllData('medicine');
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('prescription/addprescription', $data);
            $this->load->view('prescription/pres_script');
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function addPrescriptiondata() {
        $doctorUsername = $this->session->userdata('username');

        $dayInjury = $this->input->post('dayInjury');
        $monthInjury = $this->input->post('monthInjury');
        $yearInjury = $this->input->post('yearInjury');

        $unkwnInjury = $this->input->post('unknownInjury');
        if ($unkwnInjury == "unknown"):
            $injuryDate = '';
        else:
            $injuryDate = $yearInjury . "-" . $monthInjury . "-" . $dayInjury;
        endif;

        $daySurgery = $this->input->post('daySurgery');
        $monthSurgery = $this->input->post('monthSurgery');
        $yearSurgery = $this->input->post('yearSurgery');

        $unkwnSurgery = $this->input->post('unknownSurgery');
        if ($unkwnSurgery == "unknown"):
            $surgeryDate = '';
        else:
            $surgeryDate = $yearSurgery . "-" . $monthSurgery . "-" . $daySurgery;
        endif;
//        $advice = $this->input->post('advice');
        $alladvice = $this->input->post('alladvice');
//        $alladvice = $advice . $staticadvice;
        $queryData = $this->db->query("SELECT id FROM user WHERE username = '$doctorUsername'");
        if ($queryData->num_rows() > 0):
            $doctorId = $queryData->row()->id;
        endif;
        $patientidData1 = array(
            'patient_id' => $this->input->post('patientid'),
            'doctor_id' => $doctorId,
            'pres_id' => $this->input->post('prescriptionIdinform'),
            'injury_date' => $injuryDate,
            'surgery_date' => $surgeryDate,
            'history' => $this->input->post('history'),
            'additional_info' => $this->input->post('additionalinformation'),
            'o_e_nerve' => $this->input->post('nerve_injury'),
            'o_e_nerve_pre' => $this->input->post('nerve_injury_input'),
            'o_e_vascular' => $this->input->post('vascular_injury'),
            'o_e_vascular_pre' => $this->input->post('vascular_injury_input'),
            'o_e' => $this->input->post('others'),
            'diagnosis' => $this->input->post('diagnosis'),
            'advice' => $alladvice,
            'id_reference' => $this->input->post('lastupdtedataid'),
            'reference_value' => $this->input->post('referencetextvalue'),
            'ref_others' => $this->input->post('referenceothervalue'),
            'datetime' => date('Y-m-d H:i:s')
        );
        $status = $this->db->insert('presmaster', $patientidData1);


        $medicinecountdata = $this->input->post('medicinecountdata');
        $presmasterqueryData = $this->db->query("SELECT id FROM presmaster WHERE id = (SELECT MAX(id) FROM presmaster)");
        if ($presmasterqueryData->num_rows() > 0):
            $presmasterId = $presmasterqueryData->row()->id;
        endif;
        for ($i = 1; $i <= $medicinecountdata; $i++):
            $medicine = $this->input->post('medicinename' . $i);
            if ($medicine != "") {
                $medicineName[$i] = $this->input->post('medicinename' . $i);
                $getMedicine = explode('-,', $medicineName[$i]);
                $patientidData2 = array(
                    'pres_master_id' => $presmasterId,
                    'medicine_name' => $getMedicine[0],
                    'procedure' => $getMedicine[1],
                    'timing' => $this->input->post('datetime')
                );
                $status = $this->db->insert('pres_medication', $patientidData2);
            }
        endfor;

        // image add start 
        $presid = $this->input->post('prescriptionIdinform');
        $querypres_id = $this->db->query("SELECT * FROM presmaster WHERE pres_id='$presid'");
        $pres_id = $querypres_id->row()->id;

        $opCount = $this->input->post('opCount');
        $date_array = array();
        for ($i = 1; $i <= $opCount; $i++) {
            $day = $this->input->post('day' . $i);
            $month = $this->input->post('month' . $i);
            $year = $this->input->post('year' . $i);
            $inputdate = $year . "-" . $month . "-" . $day;
            $date_array[] = $inputdate;
        }
        $date = implode(',', $date_array);

        $image_array = array();
        for ($i = 1; $i <= $opCount; $i++) {
            $opType = $this->input->post('operation' . $i);
            $image_array[] = $opType;
        }
        $images = implode(',', $image_array);

        $name_array = array();
        if (isset($_FILES['userfile']['size'])):
            $count = count($_FILES['userfile']['size']);
        else:
            $count = 0;
        endif;

        foreach ($_FILES as $key => $value)
            for ($s = 0; $s <= $count - 1; $s++) {
                $_FILES['userfile']['name'] = $value['name'][$s];
                $_FILES['userfile']['type'] = $value['type'][$s];
                $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                $_FILES['userfile']['error'] = $value['error'][$s];
                $_FILES['userfile']['size'] = $value['size'][$s];
                $config['upload_path'] = './assets/uploads/xray/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2000';
                $config['max_width'] = '2000';
                $config['max_height'] = '2000';
                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $data = $this->upload->data();
                $name_array[] = $data['file_name'];
            }
        $names = implode(',', $name_array);
        $xRayImagedata = array(
            'pres_id' => $pres_id,
            'date' => $date,
            'image_name' => $names,
            'image_type' => $images
        );
        $status = $this->db->insert('pres_image', $xRayImagedata);

        //image add end 

        $testcountdata = $this->input->post('testcountdata');
        for ($i = 1; $i <= $testcountdata; $i++):
            $testName = $this->input->post('medicaltestname' . $i);
            if ($testName != "") {
                $testname[$i] = $this->input->post('medicaltestname' . $i);
                $testquery = $this->db->query("SELECT * FROM test WHERE test_name='$testname[$i]'");
                if ($testquery->num_rows() > 0):

                else:
                    $testData3 = array(
                        'test_name' => $testname[$i],
                        'status' => 1
                    );
                    $status = $this->db->insert('test', $testData3);
                endif;
                $patientidData3 = array(
                    'pres_master_id' => $presmasterId,
                    'test_name' => $testname[$i],
                    'description' => 'advice'
                );
                $status = $this->db->insert('pres_testadvised', $patientidData3);
            }
        endfor;
        if ($status):
            $this->session->set_userdata('successfull', 'Prescription Information Add Successfully.');
        else:
            $this->session->set_userdata('failed', 'Prescription Information Add Failed.');
        endif;
        redirect('prescription/prescription');
    }

    function delete_prescription() {
        $id_delete = $this->input->post('prescriptionid');
        $this->db->where('id', $id_delete);
        $query = $this->db->delete('presmaster');

        $this->db->where('pres_master_id', $id_delete);
        $query = $this->db->delete('pres_medication');

        $this->db->where('pres_master_id', $id_delete);
        $query = $this->db->delete('pres_testadvised');

        $this->db->where('pres_id', $id_delete);
        $query = $this->db->delete('pres_image');

        $followupDelQr = $this->db->query("SELECT id FROM presmaster  WHERE pres_under='$id_delete'");
        $followupId = $followupDelQr->result();
        if (sizeof($followupId)):
            foreach ($followupId as $rows):
                $followupImageDelQr = $this->db->query("SELECT id FROM pres_followup  WHERE pres_master_id='$rows->id'");
                $followupImageId = $followupImageDelQr->result();
                if (sizeof($followupImageId)):
                    foreach ($followupImageId as $imgrows):
                        $this->db->where('followup_id', $imgrows->id);
                        $query = $this->db->delete('pres_image_followup');
                    endforeach;
                endif;

                $this->db->where('pres_master_id', $rows->id);
                $query = $this->db->delete('pres_followup');

                $this->db->where('pres_master_id', $rows->id);
                $query = $this->db->delete('pres_medication');

                $this->db->where('pres_master_id', $rows->id);
                $query = $this->db->delete('pres_testadvised');
            endforeach;
        endif;

        $this->db->where('pres_under', $id_delete);
        $query = $this->db->delete('presmaster');


        if ($query):
            $this->session->set_userdata('successfull', ' Prescription Delete Successfully');
        else:
            $this->session->set_userdata('failed', ' Prescription Delete Failed');
        endif;
        redirect('prescription/prescription');
    }

    function getPatientInfo() {
        $username = $this->input->post('patientid');
        $queryResult = $this->db->query("select * from user where id = '$username'");
        $userdata = $queryResult->row();
        print (json_encode($userdata));
    }

    function getPatientCount() {
        $username = $this->input->post('patientid');
        $queryResult = $this->db->query("select COUNT(patient_id) as visitcount from presmaster where patient_id = '$username'");
        $userdata = $queryResult->row();
        print (json_encode($userdata));
    }

    function generateRandonNum() {
        $username = $this->input->post('patientid');
        $randomNumber = rand(9, 9999999);
        echo $randomNumber;
    }

    function getMedicineDesc() {
        $medicineName = $this->input->post('medicineName');
        $queryResult = $this->db->query("select * from medicine where id = '$medicineName'");
        $userdata = $queryResult->row();
        print (json_encode($userdata));
    }

    function get_prescription() {
        $id = $this->input->post('id');
        $presmasterQueryResult = $this->db->query("select * from presmaster where id = '$id'");
        $presmasterData = $presmasterQueryResult->row();

        $presmasterId = $presmasterQueryResult->row()->id;
        $patientId = $presmasterQueryResult->row()->patient_id;

        $userQueryResult = $this->db->query("select * from user where id = '$patientId'");
        $userData = $userQueryResult->row();

        $patientCountquery = $this->db->query("select COUNT(patient_id) as visitcount from presmaster where patient_id = '$patientId'");
        $patientCountdata = $patientCountquery->row();

        $MedicationQry = $this->db->query("select * from pres_medication where pres_master_id = '$presmasterId'");
        $MedicationData = $MedicationQry->result();

        $medicineviewdata = "";
        if (sizeof($MedicationData)):
            $medicinecount = 0;
            foreach ($MedicationData as $datarow):
                $medicinecount++;
                $medicineviewdata .= "<div><p style='margin: 0'>" . '#   ' . $datarow->medicine_name . "</p>" . "<p style='margin: 0 0 8px 15px;'>" . $datarow->procedure . '<br>' . "</p></div>";
            endforeach;
        else:
            $medicineviewdata = "No Medicine Availabe";
        endif;

        $testQry = $this->db->query("select * from pres_testadvised where pres_master_id = '$presmasterId'");
        $testData = $testQry->result();

        $testviewdata = "";
        if (sizeof($testData)):
            $testcount = 0;
            foreach ($testData as $datarow):
                $testcount++;
                $testviewdata .= "<div><p style='margin: 0 0 8px 0'>" . '#   ' . $datarow->test_name . "</p></div>";
            endforeach;
        else:
            $testviewdata = "No test available!";
        endif;

        $presImageQueryResult = $this->db->query("select * from pres_image where pres_id = '$id'");
        $presImageData = $presImageQueryResult->row();
        $outputdata = array(
            'presmasterdata' => $presmasterData,
            'userdata' => $userData,
            'patientCountdata' => $patientCountdata,
            'presMedicationData' => $medicineviewdata,
            'testadvice' => $testviewdata,
            'presImageData' => $presImageData
        );
        print (json_encode($outputdata));
    }

    function getFirstReference1() {
        $counter = 0;
        $refimagedata = "";
        $underrefimagedata = "";
        $id = $this->input->post('id');
        if (is_int((int) $id)):
            $getrefimgQr = $this->db->query("SELECT image_name FROM reference WHERE id = '$id'");
            $refimagename = $getrefimgQr->row()->image_name;
            $refimagedata .= '<img src="' . site_url('assets/img/reference_image/' . $refimagename) . '" class="img-rounded" alt="" width="auto" height="50">';

            $getFstQry = $this->db->query("SELECT * FROM reference where ref_under = '$id'");
            if ($getFstQry->num_rows() > 0):
                $fstQryresult = $getFstQry->result();
                foreach ($fstQryresult as $datarow):
                    $getImgQry = $this->db->query("SELECT * FROM reference where ref_under = '$datarow->id '");
                    if ($getImgQry->num_rows() > 0):
                        $counter++;
                    endif;
                endforeach;
                if ($counter > 0):
                    $underrefimagedata .= '<select name="" id="reference3id" class="form-control" onchange="return getReferunder2(this.value);">';
                    if ($getFstQry->num_rows() > 0):
                        $fstQryresult = $getFstQry->result();
                        $underrefimagedata .= '<option value="">Select</option>';
                        foreach ($fstQryresult as $datarow):
                            $underrefimagedata .= '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option>';
                        endforeach;
                    else:
                        $underrefimagedata .= '<option value = "">no data found</option>';
                    endif;
                    $underrefimagedata .= '</select>';
                else:
                    $fstQryresult = $getFstQry->result();
                    if (sizeof($fstQryresult) > 0):
                        foreach ($fstQryresult as $datarow):
                            $underrefimagedata .= "<div style='border:1px solid black;margin:2px;' class='col-lg-4'><span onclick='selectimg();'><img src='" . site_url('assets/img/reference_image/' . $datarow->image_name) . "' class='img-rounded'     width='110'  height='auto'>" . $datarow->ref_name . "</span></div>";
                        endforeach;
                    endif;
                endif;
            else:
                $underrefimagedata .= '';
            endif;
            $outputdata = array(
                'refimagedata' => $refimagedata,
                'underrefimagedata' => $underrefimagedata
            );
            echo json_encode($outputdata);
        else:
            echo '';
        endif;
    }

    function getFirstReference2() {
        $counter = 0;
        $refimagedata = "";
        $underrefimagedata = "";
        $id = $this->input->post('id');
        if (is_int((int) $id)):
            $getrefimgQr = $this->db->query("SELECT image_name FROM reference WHERE id = '$id'");
            $refimagename = $getrefimgQr->row()->image_name;
            $refimagedata .= '<img src="' . site_url('assets/img/reference_image/' . $refimagename) . '" class="img-rounded" alt="" width="auto" height="50">';

            $getFstQry = $this->db->query("SELECT * FROM reference where ref_under = '$id'");
            if ($getFstQry->num_rows() > 0):
                $fstQryresult = $getFstQry->result();
                foreach ($fstQryresult as $datarow):
                    $getImgQry = $this->db->query("SELECT * FROM reference where ref_under = '$datarow->id '");
                    if ($getImgQry->num_rows() > 0):
                        $counter++;
                    endif;
                endforeach;
                if ($counter > 0):
                    $underrefimagedata .= '<select name="" id="reference3id" class="form-control" onchange="return getReferunder3(this.value);">';
                    if ($getFstQry->num_rows() > 0):
                        $fstQryresult = $getFstQry->result();
                        $underrefimagedata .= '<option value="">Select</option>';
                        foreach ($fstQryresult as $datarow):
                            $underrefimagedata .= '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option>';
                        endforeach;
                    else:
                        $underrefimagedata .= '<option value = "">no data found</option>';
                    endif;
                    $underrefimagedata .= '</select>';
                else:
                    $imgcount = 0;
                    $fstQryresult = $getFstQry->result();
                    if (sizeof($fstQryresult) > 0):
                        foreach ($fstQryresult as $datarow):
                            $imgcount++;
                            $underrefimagedata .= "<div id='div" . $imgcount . "' style='border:1px solid blue;width:150px; height:150px;margin:3px;' class='col-lg-3'><span onclick='selectimg3(this.id);' id='$imgcount" . '#' . $datarow->id . '#' . $datarow->ref_name . "'><img src='" . site_url('assets/img/reference_image/' . $datarow->image_name) . "' class='img-rounded'     width='110'  height='auto'>" . $datarow->ref_name . "</span></div>";
                        endforeach;
                        $underrefimagedata .="<input type='hidden' id='totalimgcount3' value='$imgcount'>";
                    endif;
                endif;
            else:
                $underrefimagedata .= '';
            endif;
            $outputdata = array(
                'refimagedata' => $refimagedata,
                'underrefimagedata' => $underrefimagedata
            );
            echo json_encode($outputdata);
        else:
            echo '';
        endif;
    }

    function getFirstReference3() {

        $counter = 0;
        $refimagedata = "";
        $underrefimagedata = "";
        $id = $this->input->post('id');
        if (is_int((int) $id)):
            $getrefimgQr = $this->db->query("SELECT image_name FROM reference WHERE id = '$id'");
            $refimagename = $getrefimgQr->row()->image_name;
            $refimagedata .= '<img src="' . site_url('assets/img/reference_image/' . $refimagename) . '" class="img-rounded" alt="" width="auto" height="50">';

            $getFstQry = $this->db->query("SELECT * FROM reference where ref_under = '$id'");
            if ($getFstQry->num_rows() > 0):
                $fstQryresult = $getFstQry->result();
                foreach ($fstQryresult as $datarow):
                    $getImgQry = $this->db->query("SELECT * FROM reference where ref_under = '$datarow->id '");
                    if ($getImgQry->num_rows() > 0):
                        $counter++;
                    endif;
                endforeach;
                if ($counter > 0):
                    $underrefimagedata .= '<select name="" id="reference3id" class="form-control" onchange="return getReferunder4(this.value);">';
                    if ($getFstQry->num_rows() > 0):
                        $fstQryresult = $getFstQry->result();
                        $underrefimagedata .= '<option value="">Select</option>';
                        foreach ($fstQryresult as $datarow):
                            $underrefimagedata .= '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option>';
                        endforeach;
                    else:
                        $underrefimagedata .= '<option value = "">no data found</option>';
                    endif;
                    $underrefimagedata .= '</select>';
                else:
                    $imgcount = 0;
                    $fstQryresult = $getFstQry->result();
                    if (sizeof($fstQryresult) > 0):
                        foreach ($fstQryresult as $datarow):
                            $imgcount++;
                            $underrefimagedata .= "<div id='div" . $imgcount . "' style='border:1px solid blue;width:150px; height:150px;margin:3px;' class='col-lg-3'><span onclick='selectimg4(this.id);' id='$imgcount" . '#' . $datarow->id . '#' . $datarow->ref_name . "'><img src='" . site_url('assets/img/reference_image/' . $datarow->image_name) . "' class='img-rounded'     width='110'  height='auto'>" . $datarow->ref_name . "</span></div>";
                        endforeach;
                        $underrefimagedata .="<input type='hidden' id='totalimgcount4' value='$imgcount'>";
                    endif;
                endif;
            else:
                $underrefimagedata .= '';
            endif;
            $outputdata = array(
                'refimagedata' => $refimagedata,
                'underrefimagedata' => $underrefimagedata
            );
            echo json_encode($outputdata);
        else:
            echo '';
        endif;
    }

    function addReport($id) {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['idAddreport'] = $id; //pressmaster id
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Add Report";
            $data['active_menu'] = "prescription";
            $reporttypeQry = $this->db->query("select * from report_type");
            $data['reportType'] = $reporttypeQry->result();
            $reportQry = $this->db->query("select * from report where pres_master_id = '$id'");
            $data['reportList'] = $reportQry->result();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('prescription/addReport', $data);
            $this->load->view('prescription/pres_script');
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function add_report() {
        $id = $this->input->post('prescriptionid'); //pressmaster id
        $reportQry = $this->db->query("select * from presmaster where id = '$id'");
        $patient_id = $reportQry->row()->patient_id; //patient id
        $apDate = $this->input->post('date');
        $apTime = $this->input->post('time');
        $dateTime = $apDate . " " . $apTime;
        $timestamp = strtotime($dateTime);
        $formatDate = date("Y-m-d H:i:s", $timestamp);
        $randnum = rand(0, 99);
        $uploadfile = $_FILES['picture']['name'];
        $picname = explode('.', $uploadfile);
        $picname[0] = $id;
        $picname[1] = 'png';
        $file = $picname[0] . '_' . $randnum . '.' . $picname[1];
        $userdata = array(
        );
        $reportData = array(
            'pres_master_id' => $id,
            'patient_id' => $patient_id,
            'date' => $formatDate,
            'report_type' => $this->input->post('generic_name'),
            'report_type' => $this->input->post('report_type'),
            'image_name' => $file,
            'description' => $this->input->post('description')
        );
        $status = $this->db->insert('report', $reportData);
        if ($file != "") {
            $updatestatus = $this->prescription_model->UploadImage('picture', "assets/uploads/presreport/", $file);
            if ($updatestatus == TRUE) {
                $this->session->set_userdata('failed', 'Report Information Add Failed.');
            } else {
                $this->session->set_userdata('successfull', 'Report Information Add Successfully.');
            }
            redirect('prescription/prescription/addReport/' . $id);
        } else {
            $this->session->set_userdata('failed', 'Report Information Add Failed.');
            redirect('prescription/prescription/addReport/' . $id);
        }
    }

    function delete_report() {
        $id_delete = $this->input->post('rid');

        $reportQry = $this->db->query("select * from report where id = '$id_delete'");
        $pres_master_id = $reportQry->row()->pres_master_id; //patient id

        $this->db->where('id', $id_delete);
        $query = $this->db->delete('report');

        if ($query):
            $this->session->set_userdata('successfull', ' Report Delete Successfully');
        else:
            $this->session->set_userdata('failed', ' Report Delete Failed');
        endif;
        redirect('prescription/prescription/addReport/' . $pres_master_id);
    }

    function editPrescription() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $idPrescription = $this->input->get('id'); //pressmaster id
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Prescription Information";
            $data['active_menu'] = "prescription";

            $presmasterQry = $this->db->query("select * from presmaster where id='$idPrescription'");
            $data['presmasterData'] = $presmasterQry->result();

            $followupQry = $this->db->query("select * from presmaster where pres_under='$idPrescription'");
            $data['followupPresData'] = $followupQry->result();

            $patientId = $presmasterQry->row()->patient_id;
            $patientQr = $this->db->query("SELECT * FROM user WHERE id='$patientId'");
            $data['patientlist'] = $patientQr->row();
            $data['idPrescription'] = $idPrescription;
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('prescription/edit_prescription', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function addFollowup() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Add Followup";
            $data['active_menu'] = "prescription";

            $id = $this->input->get('id');
            $data['idPrescription'] = $id;

            $presmasterQueryResult = $this->db->query("select * from presmaster where id = '$id'");
            $data['presmasterData'] = $presmasterQueryResult->row();

            $presImageQueryResult = $this->db->query("select * from pres_image where pres_id = '$id'");
            $data['presImageData'] = $presImageQueryResult->row();


            $presmasterId = $presmasterQueryResult->row()->id;
            $patientId = $presmasterQueryResult->row()->patient_id;

            $userQueryResult = $this->db->query("select * from user where id = '$patientId'");
            $data['patientlist'] = $userQueryResult->row();

            $patientCountquery = $this->db->query("select COUNT(patient_id) as visitcount from presmaster where patient_id = '$patientId'");
            $data['patientCountdata'] = $patientCountquery->row();

            $MedicationQry = $this->db->query("select * from pres_medication where pres_master_id = '$presmasterId'");
            $data['medicationData'] = $MedicationQry->row();

            $testQry = $this->db->query("select * from pres_testadvised where pres_master_id = '$presmasterId'");
            $data['testData'] = $testQry->row();
            $data['testlist'] = $this->prescription_model->getAllData('test');
            $data['medicinelist'] = $this->prescription_model->getAllData('medicine');
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('prescription/add_followup', $data);
            $this->load->view('footer', $data);
            $this->load->view('prescription/pres_script', $data);

        else:
            redirect('home');
        endif;
    }

    public function editFollowup() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Followup Information";
            $data['active_menu'] = "prescription";

            $pres_id = $this->input->get('pres_id');
            $followup_id = $this->input->get('followup_id');
            $pres_under_id = $this->input->get('pres_under_id');

            $data['idPrescription'] = $pres_id;
            $data['idFollowup'] = $followup_id;

            $presmasterQueryResult = $this->db->query("select * from presmaster where id = '$pres_id'");
            $data['presmasterData'] = $presmasterQueryResult->row();


            $presImageQueryResult = $this->db->query("select * from pres_image where pres_id = '$pres_under_id'");
            $data['presImageData'] = $presImageQueryResult->row();

            $presfollowupQueryResult = $this->db->query("select * from pres_followup where id = '$followup_id'");
            $data['presfollowupData'] = $presfollowupQueryResult->row();

            $presimagefollowupQueryResult = $this->db->query("select * from pres_image_followup where followup_id = '$followup_id'");
            $data['presimagefollowupData'] = $presimagefollowupQueryResult->row();


            $presmasterId = $presmasterQueryResult->row()->id;
            $patientId = $presmasterQueryResult->row()->patient_id;

            $userQueryResult = $this->db->query("select * from user where id = '$patientId'");
            $data['patientlist'] = $userQueryResult->row();

            $patientCountquery = $this->db->query("select COUNT(patient_id) as visitcount from presmaster where patient_id = '$patientId'");
            $data['patientCountdata'] = $patientCountquery->row();

            $MedicationQry = $this->db->query("select * from pres_medication where pres_master_id = '$pres_id'");
            $data['medicationData'] = $MedicationQry->result();

            $testQry = $this->db->query("select * from pres_testadvised where pres_master_id = '$pres_id'");
            $data['testData'] = $testQry->row();
            $data['testlist'] = $this->prescription_model->getAllData('test');
            $data['medicinelist'] = $this->prescription_model->getAllData('medicine');
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('prescription/edit_followup', $data);
            $this->load->view('prescription/pres_script', $data);
            $this->load->view('footer', $data);

        else:
            redirect('home');
        endif;
    }

    public function addFollowupData() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):

            $idPrescription = $this->input->post('idPrescription');

//            $advice = $this->input->post('advice');
            $alladvice = $this->input->post('alladvice');
//            $alladvice = $advice . $staticadvice;

            $patientidData1 = array(
                'patient_id' => $this->input->post('patient_id'),
                'doctor_id' => $this->input->post('doctor_id'),
                'pres_id' => $this->input->post('pres_id'),
                'pres_under' => $idPrescription,
                'injury_date' => $this->input->post('injury_date'),
                'surgery_date' => $this->input->post('surgery_date'),
                'history' => $this->input->post('history'),
                'additional_info' => $this->input->post('additional_info'),
                'o_e_nerve' => $this->input->post('nerve_injury'),
                'o_e_nerve_pre' => $this->input->post('nerve_injury_input'),
                'o_e_vascular' => $this->input->post('vascular_injury'),
                'o_e_vascular_pre' => $this->input->post('vascular_injury_input'),
                'o_e' => $this->input->post('others'),
                'diagnosis' => $this->input->post('diagnosis'),
                'advice' => $alladvice,
                'id_reference' => $this->input->post('id_reference'),
                'reference_value' => $this->input->post('reference_value'),
                'ref_others' => $this->input->post('ref_others'),
                'datetime' => date('Y-m-d H:i:s')
            );
            $status = $this->db->insert('presmaster', $patientidData1);


            $medicinecountdata = $this->input->post('medicinecountdata');
            $presmasterqueryData = $this->db->query("SELECT id FROM presmaster WHERE id = (SELECT MAX(id) FROM presmaster)");
            if ($presmasterqueryData->num_rows() > 0):
                $presmasterId = $presmasterqueryData->row()->id;
            endif;
            for ($i = 1; $i <= $medicinecountdata; $i++):
                $medicine = $this->input->post('medicinename' . $i);
                if ($medicine != "") {
                    $medicineName[$i] = $this->input->post('medicinename' . $i);
                    $getMedicine = explode('-,', $medicineName[$i]);
                    $patientidData2 = array(
                        'pres_master_id' => $presmasterId,
                        'medicine_name' => $getMedicine[0],
                        'procedure' => $getMedicine[1],
                        'timing' => $this->input->post('datetime')
                    );
                    $status = $this->db->insert('pres_medication', $patientidData2);
                }
            endfor;



            $testcountdata = $this->input->post('testcountdata');
            for ($i = 1; $i <= $testcountdata; $i++):
                $testName = $this->input->post('medicaltestname' . $i);
                if ($testName != "") {
                    $testname[$i] = $this->input->post('medicaltestname' . $i);
                    $patientidData3 = array(
                        'pres_master_id' => $presmasterId,
                        'test_name' => $testname[$i],
                        'description' => 'advice'
                    );
                    $status = $this->db->insert('pres_testadvised', $patientidData3);
                }
            endfor;

//            followup  info start here  
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $date = $year . "-" . $month . "-" . $day;

            $infection = $this->input->post('infection');
            if ($infection == ''):
                $infection = 5;
            endif;
            $infection_typearr = array();
            if ($infection == 1):
                $infectionwound = $this->input->post('infectionwound');
                $infectiondepth = $this->input->post('infectiondepth');
                if ($infectionwound == ''):
                    $infectionwound = 5;
                endif;
                if ($infectiondepth == ''):
                    $infectiondepth = 5;
                endif;

                $infectionduration = $this->input->post('infectionduration');
                $osteomyelitis = $this->input->post('osteomyelitis');
                $amputation = $this->input->post('amputation');
                if ($osteomyelitis != ''):
                    $infection_typearr[] = $osteomyelitis;
                endif;
                if ($amputation != ''):
                    $infection_typearr[] = $amputation;
                endif;
                $infection_type = implode(',', $infection_typearr);
            else:
                $infectionwound = 5;
                $infectiondepth = 5;
                $infectionduration = '';
                $osteomyelitis = '';
                $amputation = '';
                $infection_type = '';
            endif;

            $partialweight = $this->input->post('partialweight');
            if ($partialweight == ''):
                $partialweight = 5;
            endif;

            $painlessweight = $this->input->post('painlessweight');
            if ($painlessweight == ''):
                $painlessweight = 5;
            endif;

            $healingxray = $this->input->post('healingxray');
            if ($healingxray == ''):
                $healingxray = 5;
            endif;
            $kneeflexion = $this->input->post('kneeflexion');
            if ($kneeflexion == ''):
                $kneeflexion = 5;
            endif;
            $screwbreakage = $this->input->post('screwbreakage');
            if ($screwbreakage == ''):
                $screwbreakage = 5;
            endif;
            $screwloosening = $this->input->post('screwloosening');
            if ($screwloosening == ''):
                $screwloosening = 5;
            endif;
            $nailbreakage = $this->input->post('nailbreakage');
            if ($nailbreakage == ''):
                $nailbreakage = 5;
            endif;
            $nailloosening = $this->input->post('nailloosening');
            if ($nailloosening == ''):
                $nailloosening = 5;
            endif;
            $deformity = $this->input->post('deformity');
            if ($deformity == ''):
                $deformity = 5;
            endif;

            if ($deformity == 1):
                $deformity_alignment = $this->input->post('alignment');
                $rotation = $this->input->post('rotation');
                if ($rotation == ''):
                    $rotation = 5;
                endif;
            else:
                $deformity_alignment = '';
                $rotation = 5;
            endif;

            $repeatsurgery = $this->input->post('repeatsurgery');
            if ($repeatsurgery == ''):
                $repeatsurgery = 5;
            endif;

            $infectionrepeat = $this->input->post('infectionrepeat');
            $deformityrepeat = $this->input->post('deformityrepeat');
            $nonunionrepeat = $this->input->post('nonunionrepeat');
            $repeatsurgeryarr = array();
            if ($repeatsurgery == 1):
                if ($infectionrepeat != ''):
                    $repeatsurgeryarr[] = $infectionrepeat;
                endif;
                if ($deformityrepeat != ''):
                    $repeatsurgeryarr[] = $deformityrepeat;
                endif;
                if ($nonunionrepeat != ''):
                    $repeatsurgeryarr[] = $nonunionrepeat;
                endif;
                $repeatsurgerycause = implode(',', $repeatsurgeryarr);
            else:
                $repeatsurgerycause = '';
            endif;


            $repeatsurgerytypearr = array();
            $dynamize = $this->input->post('dynamize');
            $exchangenail = $this->input->post('exchangenail');
            $iliacrestbonegraft = $this->input->post('iliacrestbonegraft');
            $other = $this->input->post('other');
            if ($other == ''):
                $otherval = '';
            else:
                $otherval = $this->input->post('otherval');
            endif;

            if ($nonunionrepeat != ''):
                if ($dynamize != ''):
                    $repeatsurgerytypearr[] = $dynamize;
                endif;
                if ($exchangenail != ''):
                    $repeatsurgerytypearr[] = $exchangenail;
                endif;
                if ($iliacrestbonegraft != ''):
                    $repeatsurgerytypearr[] = $iliacrestbonegraft;
                endif;
                if ($other != ''):
                    $repeatsurgerytypearr[] = $other;
                    $repeatsurgerytypearr[] = $otherval;
                endif;
                $repeatsurgerytype = implode(',', $repeatsurgerytypearr);
            else:
                $repeatsurgerytype = '';
            endif;
            $comments = $this->input->post('comments');


            $query = $this->db->query("SELECT count(`pres_under`) AS `followup_id` FROM `presmaster` WHERE `pres_under`='$idPrescription'");
            $followup_id = $query->row()->followup_id;
            $query = $this->db->query("SELECT MAX(`id`) AS `id` FROM presmaster");
            $pres_master_id = $query->row()->id;

//            if ($followup_id == ""):
//                $followup_id = 1;
//            else:
//                $followup_id = $followup_id + 1;
//            endif;
            $caseData = array(
                'followup_id' => $followup_id,
                'pres_master_id' => $pres_master_id,
                'date' => $date,
                'infection' => $infection,
                'incision_wound' => $infectionwound,
                'infection_depth' => $infectiondepth,
                'infection_duration' => $infectionduration,
                'infection_type' => $infection_type,
                'partial_weight_bear' => $partialweight,
                'painless_full_weight_bear' => $painlessweight,
                'healing_by_xray' => $healingxray,
                'knee_flex_greater_90deg' => $kneeflexion,
                'screw_breakage' => $screwbreakage,
                'screw_loosen' => $screwloosening,
                'nail_breakage' => $nailbreakage,
                'nail_loosen' => $nailloosening,
                'deformity' => $deformity,
                'deformity_alignment' => $deformity_alignment,
                'deformity_rotaion' => $rotation,
                'repeat_surgery' => $repeatsurgery,
                'repeat_surgery_cause' => $repeatsurgerycause,
                'repeat_surgery_type' => $repeatsurgerytype,
                'comments' => $comments
            );
            $status = $this->db->insert('pres_followup', $caseData);

            $opCount = $this->input->post('opCount');

            $date_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $day = $this->input->post('dayfollowup' . $i);
                $month = $this->input->post('monthfollowup' . $i);
                $year = $this->input->post('yearfollowup' . $i);
                $inputdate = $year . "-" . $month . "-" . $day;
                $date_array[] = $inputdate;
            }
            $date = implode(',', $date_array);



            $image_array = array();
            for ($i = 1; $i <= $opCount; $i++) {
                $opType = $this->input->post('operation' . $i);
                $image_array[] = $opType;
            }
            $images = implode(',', $image_array);


            $name_array = array();
            if (isset($_FILES['userfile']['size'])):
                $count = count($_FILES['userfile']['size']);
            else:
                $count = 0;
            endif;
            foreach ($_FILES as $key => $value)
                for ($s = 0; $s <= $count - 1; $s++) {
                    $_FILES['userfile']['name'] = $value['name'][$s];
                    $_FILES['userfile']['type'] = $value['type'][$s];
                    $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['userfile']['error'] = $value['error'][$s];
                    $_FILES['userfile']['size'] = $value['size'][$s];
                    $config['upload_path'] = './assets/uploads/xray/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width'] = '2000';
                    $config['max_height'] = '2000';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $data = $this->upload->data();
                    $name_array[] = $data['file_name'];
                }
            $names = implode(',', $name_array);



            $query = $this->db->query("SELECT MAX(`id`)  AS `id`  FROM pres_followup");
            $followup_id = $query->row()->id;

            $xRayImagedata = array(
                'followup_id' => $followup_id,
                'date' => $date,
                'image_name' => $names,
                'image_type' => $images
            );
            $status = $this->db->insert('pres_image_followup', $xRayImagedata);
            if ($status):
                $this->session->set_userdata('successfull', 'Followup Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Followup Information Add Failed.');
            endif;
            redirect('prescription/prescription');

        else:
            redirect('home');
        endif;
    }

}
