<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChangePassword
 *
 * @author Tushar
 */
class Changepassword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('password');
    }

    public function index() {
        $data['title'] = "PMS-Change Password";
        $data['active_menu'] = "changepassword";
        $data['baseurl'] = $this->config->item('base_url');

        $this->load->view('header', $data);
        $this->load->view('sidebar', $data);
        $this->load->view('changepassword/changepassword', $data);
        $this->load->view('footer', $data);
    }

    function updatepass() {
        $tablename = 'user';
        $loginname = $this->session->userdata('username');
        $cpassword = $this->input->post('cpassword');
        $npassword = $this->input->post('npassword');
        $queryCheckPwd = $this->password->checkPassword($loginname, $tablename, $cpassword);
        if ($queryCheckPwd == TRUE) {
            $userdata = array(
                'password' => $npassword
            );
            $updatestatus = $this->password->updateuserinfo($userdata, $loginname, $tablename);
            if ($updatestatus == TRUE) {
                $this->session->set_userdata('successfull', 'Password updated successfully !!!');
            } else {
                $this->session->set_userdata('failed', 'Password updated fail. Try again !!!');
            }
            redirect('changepassword/changepassword');
        } else {
            $this->session->set_userdata('failed', 'Password do not match with your existing password, Please try again !!!');
            redirect('changepassword/changepassword');
        }
    }

}
