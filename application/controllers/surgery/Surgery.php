<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Surgery extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('patient_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(8, 16, 32, 64, 128, 256, 512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "Surgery";
            $data['active_menu'] = "surgery";
            $data['patientlist'] = $this->patient_model->patientList();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('surgery/surgery', $data);
            $this->load->view('footer', $data);
          
        else:
            redirect('home');
        endif;
    }

   
}
