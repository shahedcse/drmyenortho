<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('doctor_model');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(512))):
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Doctor";
            $data['active_menu'] = "doctor";
            $data['roledata'] = $this->doctor_model->roleDetails();
            $data['doctorlist'] = $this->doctor_model->doctorList();
            $this->load->view('header', $data);
            $this->load->view('sidebar', $data);
            $this->load->view('doctor/doctor', $data);
            $this->load->view('footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function adddoctor() {
        if (in_array($this->session->userdata('user_role'), array(512))):
            $doctordata = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'first_name' => $this->input->post('fname'),
                'last_name' => $this->input->post('lname'),
                'address' => $this->input->post('address'),
                'mobile' => $this->input->post('mobile'),
                'email' => $this->input->post('email'),
                'user_role' => '256',
                'position_id' => $this->input->post('position_id'),
                'sex' => $this->input->post('sex'),
                'created_date' => date('Y-m-d H:i:s'),
                'status' => '1'
            );
            $status = $this->db->insert('user', $doctordata);
            if ($status):
                $this->session->set_userdata('successfull', 'Doctor Information Add Successfully.');
            else:
                $this->session->set_userdata('failed', 'Doctor Information Add Failed.');
            endif;
            redirect('doctor/doctor');
        else:
            redirect('home');
        endif;
    }

    function checkUniquePin() {
        $username = $_POST['username'];
        $userQuery = $this->db->query("SELECT username FROM user WHERE username = '$username'");
        if ($userQuery->num_rows() > 0):
            echo 'booked';
        else:
            echo 'free';
        endif;
    }

}
