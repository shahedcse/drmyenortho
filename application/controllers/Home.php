<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of home
 *
 * @author sh
 */
class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->helper('common_helper');
    }

    public function index() {
        if (in_array($this->session->userdata('user_role'), array(4, 8, 16, 32, 64, 128, 256, 512))):
            redirect('dashboard/dashboard');
        else:
            $data['baseurl'] = $this->config->item('base_url');
            $data['title'] = "PMS - Login";
            $this->load->view('home', $data);
        endif;
    }

    public function login() {
        $username = mysql_real_escape_string($this->input->post('username'));
        $password = mysql_real_escape_string($this->input->post('password'));
        $login_check = $this->home_model->loginFun($username, $password);
        if ($login_check->num_rows() > 0):
            $user_role = $login_check->row()->user_role;
            $this->session->set_userdata('username', $username);
            $this->session->set_userdata('user_role', $user_role);
            $logdetails = "User: " . $username . " Login successfully";
            savelogdata("Login", $logdetails);
            redirect('dashboard/dashboard');
        else:
            $logdetails = "User: " . $username . " Your Username or Password Incorrect.";
            savelogdata("Login", $logdetails);
            $this->session->set_userdata('login_error', 'Your Username or Password Incorrect!');
            redirect('home');
        endif;
    }

    public function logout() {
        $logdetails = "User: " . $this->session->userdata('username') . " Successfully logout";
        savelogdata("Logout", $logdetails);
        $this->session->unset_userdata('username', '');
        $this->session->unset_userdata('user_role', '');
        $this->session->set_userdata('login_error', 'Successfully Logout!');
        redirect('home');
    }

}
