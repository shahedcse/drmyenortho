<?php

class Doctor_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function roleDetails() {
        $this->db->select('id, position_name');
        $this->db->where('role_master_id', '256');
        $resultqr = $this->db->get('role_details');
        return $resultqr->result();
    }

    public function doctorList() {
        $this->db->select('*');
        $this->db->where('user_role', '256');
        $this->db->where('status', '1');
        $resultqr = $this->db->get('user');
        return $resultqr->result();
    }

    function getData($tablename, $selectedrow, $selectedValue) {
        $this->db->select('*');
        $this->db->where($selectedrow, $selectedValue);
        $queryResult = $this->db->get($tablename);
        return $queryResult->result();
    }

}
