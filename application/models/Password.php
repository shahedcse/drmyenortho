<?php

class Password extends CI_Model {

    function __construct() {
        parent::__construct();
    }

      
    function checkPassword($loginname, $tablename, $cpassword) {
        $this->db->select('password');
        $this->db->where('username', $loginname);
        $queryResult = $this->db->get($tablename);
        $queryrow = $queryResult->row();
        if ($queryrow->password == $cpassword):
            return TRUE;
        else:
            return FALSE;
        endif;
    }

    function updateuserinfo($userdata, $loginname, $tablename) {
        $this->db->where('username', $loginname);
        $query = $this->db->update($tablename, $userdata);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
