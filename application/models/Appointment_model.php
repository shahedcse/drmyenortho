<?php

class Appointment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function getData($tablename, $selectedrow, $selectedValue) {
        $this->db->select('*');
        $this->db->where($selectedrow, $selectedValue);
        $queryResult = $this->db->get($tablename);
        return $queryResult->result();
    }

}
