<?php

class Patient_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function patientList() {
        $this->db->select('*');
        $this->db->where('user_role', '4');
        $this->db->where('status', '1');
        $this->db->order_by('id', 'DESC');
        $resultqr = $this->db->get('user');
        return $resultqr->result();
    }

    public function getSelectData($tableName,$rowName,$rowValue) {
        $this->db->select('*');
        $this->db->where($rowName, $rowValue);
        $resultqr = $this->db->get($tableName);
        return $resultqr->result();
    }

}
