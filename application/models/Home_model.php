<?php
class Home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function loginFun($username, $password) {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('status', '1');
        $userinfo = $this->db->get('user');
        return $userinfo;
    }

}
