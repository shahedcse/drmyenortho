<?php

class Prescription_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function getData($tablename, $selectedrow, $selectedValue) {
        $this->db->select('*');
        $this->db->where($selectedrow, $selectedValue);
        $queryResult = $this->db->get($tablename);
        return $queryResult->result();
    }

    function getAllData($tablename) {
        $this->db->select('*');
        $queryResult = $this->db->get($tablename);
        return $queryResult->result();
    }
    
    function getpressMasterData($tablename) {
        $this->db->select('*');
         $this->db->where('pres_under =', 0);
        $this->db->order_by('id', 'DESC');
        $queryResult = $this->db->get($tablename);
        return $queryResult->result();
    }

    public function UploadImage($field, $url, $name) {
        $config['upload_path'] = $url;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['overwrite'] = TRUE;
        $config['max_size'] = '10000';
        $config['max_width'] = '1524';
        $config['max_height'] = '1524';
        $config['file_name'] = $name;
        $this->load->library('upload', $config);
        $this->upload->do_upload($field);
    }

    function getimage($username) {
        $this->db->select('picture');
        $this->db->where('username', $username);
        $querypic = $this->db->get('profilepicture');
        if ($querypic->num_rows() > 0) {
            return $querypic->row();
        } else {
            return FALSE;
        }
    }

    function updateuserpic($userdata, $username) {
        $this->db->where('username', $username);
        $query = $this->db->update('users', $userdata);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
