<script type="text/javascript">

    //code for add medicine

    var medicinecount = 0;
    $("#addmedicine").click(function () {
        var medicinename = $("#medicinename").val();
        var medicinefeedinst = $("#medicinefeedinst").val();
        if (medicinename == "") {
            $("#addmedicinedescription").text("Please type a medicine name.");
            $("#addmedicinedescription").css('color', 'red');
            return false;
        }
        if (medicinefeedinst == "") {
            $("#medicineinstreq").text("Please type medicine feed instruction.");
            $("#medicineinstreq").css('color', 'red');
            return false;
        }
        medicinecount = medicinecount + 1;
        var medicinetext = "<div id='medicine" + medicinecount + "'><input type='hidden' name='medicinename" + medicinecount + "'value='" + medicinename + "-," + medicinefeedinst + "'><p style='margin: 0'><span style='color:red;cursor:pointer'; onclick='return deleteRowData(" + medicinecount + ")'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + "#  " + medicinename + "</p>" + "<p style='margin: 0 0 8px 15px;'>" + medicinefeedinst + "</p></div>";
        var medicinetextprint = "<div id='medicineprint" + medicinecount + "'><p style='margin: 0'>" + "#  " + medicinename + "</p>" + "<p style='margin: 0 0 8px 15px;'>" + medicinefeedinst + "</p></div>";
        $("#medicinedata").append(medicinetext);
        $("#medicinedataP").append(medicinetextprint);
        $("#medicinecountdata").val(medicinecount);
        $("#medicinename").val('');
        $("#medicinefeedinst").val('');
        $("#addmedicinedescription").text("");
        $("#medicineinstreq").text("");
    });
    //code for add test

    var testcount = 0;
    $("#addtest").click(function () {
        var medicaltestname = $("#testname").val();
        if (medicaltestname == "") {
            $("#addmedicinetestdescription").text("Please Select a Test Name.");
            $("#addmedicinetestdescription").css('color', 'red');
            return false;
        }
        testcount = testcount + 1;
        var testtext = "<div id='test" + testcount + "'><input type='hidden' id='medicaltestname' name='medicaltestname" + testcount + "'value='" + medicaltestname + "'><p style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteTestRowData(" + testcount + ")'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + "#  " + medicaltestname + "</p></div>";
        var testtextprint = "<div id='testprint" + testcount + "'><p style='margin: 0 0 8px 0'>" + "#  " + medicaltestname + "</p></div>";
        $("#medicaltestdatashow").append(testtext);
        $("#medicaltestdatashowP").append(testtextprint);
        $("#addmedicinetestdescription").text("");
        $("#testcountdata").val(testcount);
        $("#testname").val("").change();
    });
    var reference1count = 0;
    $("#reference1add").click(function () {
        var reference1 = $("#reference1name").val();
        reference1count = reference1count + 1;
        var reference1text = "<div id='medicine" + reference1count + "'><input type='hidden' id='reference1' name='reference1" + reference1count + "'value='" + reference1 + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deletedata(testcount)'>&nbsp;&nbsp;×&nbsp;&nbsp;</span>" + reference1count + ". " + reference1 + "</p></div>";
        //var medicinetext = "<div id='medicine" + testcount + "'><input type='hidden' name='medicaltestname" + testcount + "'value='" + medicaltestname + "'><p style='margin: 0 0 8px 0'>" + testcount + ". " + medicaltestname + "</p></div>";
        $("#reference1addshow").append(reference1text);
        $("#medicaltestdatashowPq").append(reference1text);
        $("#reference1countcountdata").val(reference1count);
    });
    var reference2count = 0;
    $("#reference2add").click(function () {
        var reference2 = $("#reference2name").val();
        reference2count = reference2count + 1;
        var reference2text = "<div id='medicine" + reference2count + "'><input type='hidden' id='reference2' name='reference2" + reference2count + "'value='" + reference2 + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deletedata(testcount)'>&nbsp;&nbsp;×&nbsp;&nbsp;</span>" + reference2count + ". " + reference2 + "</p></div>";
        //var medicinetext = "<div id='medicine" + testcount + "'><input type='hidden' name='medicaltestname" + testcount + "'value='" + medicaltestname + "'><p style='margin: 0 0 8px 0'>" + testcount + ". " + medicaltestname + "</p></div>";
        $("#reference2addshow").append(reference2text);
        $("#medicaltestdatashowPq").append(reference2text);
        $("#reference2countcountdata").val(reference2count);
    });
    var reference3count = 0;
    $("#reference3add").click(function () {
        var reference3 = $("#reference3name").val();
        reference3count = reference3count + 1;
        var reference3text = "<div id='medicine" + reference3count + "'><input type='hidden' id='reference3' name='reference3" + reference3count + "'value='" + reference3 + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deletedata(testcount)'>&nbsp;&nbsp;×&nbsp;&nbsp;</span>" + reference3count + ". " + reference3 + "</p></div>";
        //var medicinetext = "<div id='medicine" + testcount + "'><input type='hidden' name='medicaltestname" + testcount + "'value='" + medicaltestname + "'><p style='margin: 0 0 8px 0'>" + testcount + ". " + medicaltestname + "</p></div>";
        $("#reference3addshow").append(reference3text);
        $("#medicaltestdatashowPq").append(reference3text);
        $("#reference3countcountdata").val(reference3count);
    });
    var reference4count = 0;
    $("#reference4add").click(function () {
        var reference4 = $("#reference4name").val();
        reference4count = reference4count + 1;
        var reference4text = "<div id='medicine" + reference4count + "'><input type='hidden' id='reference4' name='reference4" + reference4count + "'value='" + reference4 + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deletedata(testcount)'>&nbsp;&nbsp;×&nbsp;&nbsp;</span>" + reference4count + ". " + reference4 + "</p></div>";
        //var medicinetext = "<div id='medicine" + testcount + "'><input type='hidden' name='medicaltestname" + testcount + "'value='" + medicaltestname + "'><p style='margin: 0 0 8px 0'>" + testcount + ". " + medicaltestname + "</p></div>";
        $("#reference4addshow").append(reference4text);
        $("#medicaltestdatashowPq").append(reference4text);
        $("#reference4countcountdata").val(reference4count);
    });
    var referencecount = 0;
    var referencefst = "";
    var referencescd = "";
    var referencetrd = "";
    var referencefur = "";
    $("#referenceadd").click(function () {
        var referencefst = $("#reference1name").val();
        var referencescd = $("#reference2name").val();
        var referencetrd = $("#reference3name").val();
        var referencefur = $("#reference4name").val();
        var reference4idfst = $("#reference1").val();
        var reference4idscd = $("#reference2").val();
        var reference4idtrd = $("#reference3").val();
        var reference4idfur = $("#reference4").val();

        var otherref = $("#otherref").val();

        var lastupdtedata;
        if (reference4idscd == "") {
            lastupdtedata = reference4idfst;
            // alert(lastupdtedata);
        } else if (reference4idtrd == "") {
            lastupdtedata = reference4idscd;
            //alert(lastupdtedata);
        } else if (reference4idfur == "") {
            lastupdtedata = reference4idtrd;
        } else {
            lastupdtedata = reference4idfur;
            //alert(lastupdtedata);
        }
        //alert(lastupdtedata);
        // var referencetext = referencefst + '->' + referencescd + '->' + referencetrd + '->' + referencefur;
        var referencetext;
        if (referencefur == "") {
            referencetext = referencefst + '->' + referencescd + '->' + referencetrd;
        } else {
            referencetext = referencefst + '->' + referencescd + '->' + referencetrd + '->' + referencefur;
        }

        reference4count = reference4count + 1;
        if (otherref == "" && referencefst != "") {
            var reference4text = "<div id='areatreatment" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + referencetext + "</p></div>";
            var reference5text = "<div id='diagnosis" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'>" + referencetext + "</p></div>";
            var reference4textP = "<div id='areatreatmentP" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + referencetext + "</p></div>";
            var reference5textP = "<div id='diagnosisP" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'>" + referencetext + "</p></div>";
        } else if (otherref != "" && referencefst == "") {
            var reference4text = "<div id='areatreatmentothers" + reference4count + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceOthersData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + otherref + "</p></div>";
            var reference5text = "<div id='diagnosisothers" + reference4count + "'><p>" + otherref + "</p></div>";
            var reference4textP = "<div id='areatreatmentothersP" + reference4count + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceOthersData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + otherref + "</p></div>";
            var reference5textP = "<div id='diagnosisothersP" + reference4count + "'><p>" + otherref + "</p></div>";
        } else if (otherref == "" && referencefst == "") {
            var reference4text = "";
            var reference5text = "";
            var reference4textP = "";
            var reference5textP = "";
        } else {
            var reference4text = "<div id='areatreatment" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + referencetext + "</p></div><div id='areatreatmentothers" + reference4count + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceOthersData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + otherref + "</p></div>";
            var reference5text = "<div id='diagnosis" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'>" + referencetext + "</p></div><div id='diagnosisothers" + reference4count + "'><p>" + otherref + "</p></div>";
            var reference4textP = "<div id='areatreatmentP" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + referencetext + "</p></div><div id='areatreatmentothersP" + reference4count + "'><p id ='removedata' style='margin: 0 0 8px 0'><span style='color:red;cursor:pointer'; onclick='return deleteReferenceOthersData(this.id)' id='" + reference4count + "'>&nbsp;&nbsp;<i class='fa fa-times-circle delete-icon'></i>&nbsp;&nbsp;</span>" + otherref + "</p></div>";
            var reference5textP = "<div id='diagnosisP" + reference4count + "'><input type='hidden' id='referencetext' name='referencetext" + reference4count + "'value='" + referencetext + "'><p id ='removedata' style='margin: 0 0 8px 0'>" + referencetext + "</p></div><div id='diagnosisothersP" + reference4count + "'><p>" + otherref + "</p></div>";
        }


        $("#referenceaddshow").append(reference4text);
        $("#referenceaddshow2").append(reference5text);
        $("#referenceaddshow2P").append(reference5textP);
        $("#referenceshowP").append(reference4textP);
        $("#reference4countcountdata").val(reference4count);
        $("#lastupdtedataid").val(lastupdtedata);
        $("#referencetextvalue").val(referencetext);
        $("#referenceothervalue").val(otherref);

    });

    function setInjury() {
        var dayInjury = $("#dayInjury").val();
        var monthInjury = $("#monthInjury").val();
        var yearInjury = $("#yearInjury").val();
        var dateInjury = dayInjury + "-" + monthInjury + "-" + yearInjury;
        $("#injuryshow").text(dateInjury);
        $("#injuryshowp").text(dateInjury);
    }

    function setFollowup() {
        var day = $("#day").val();
        var month = $("#month").val();
        var year = $("#year").val();
        var dateFollowup = day + "-" + month + "-" + year;
        $("#followupshow").text(dateFollowup);
//        $("#injuryshowp").text(dateFollowup);
    }

    function setSurgery() {
        var daySurgery = $("#daySurgery").val();
        var monthSurgery = $("#monthSurgery").val();
        var yearSurgery = $("#yearSurgery").val();
        var dateSurgery = daySurgery + "-" + monthSurgery + "-" + yearSurgery;
        $("#surgeryshow").text(dateSurgery);
        $("#surgeryshowp").text(dateSurgery);
    }

    $("#adviceadd").click(function () {
        var ad1chk = $("#advice1").prop('checked');
        var ad2chk = $("#advice2").prop('checked');
        var ad3chk = $("#advice3").prop('checked');
        var ad4chk = $("#advice4").prop('checked');
        var ad5chk = $("#advice5").prop('checked');
        var ad6chk = $("#advice6").prop('checked');
        var others = $("#advice").val();

        var adviceText = '';
        var allAdviceText = '';

        if (ad1chk) {
            adviceText += "<div>";
            adviceText += "<label>";
            var input = $("#advice1input").val();
            if ($("#advice1right").prop('checked') && $("#advice1left").prop('checked')) {
                adviceText += "* " + input + " দিন  ডান,বাম পায়ে ভর দিবেন না.";
                allAdviceText += "* " + input + " দিন  ডান,বাম পায়ে ভর দিবেন না.";
            }
            else if ($("#advice1right").prop('checked')) {
                adviceText += "* " + input + " দিন  ডান পায়ে ভর দিবেন না.";
                allAdviceText += "* " + input + " দিন  ডান পায়ে ভর দিবেন না.";
            }
            else if ($("#advice1left").prop('checked')) {
                adviceText += "* " + input + " দিন  বাম পায়ে ভর দিবেন না.";
                allAdviceText += "* " + input + " দিন  বাম পায়ে ভর দিবেন না.";
            } else {
                adviceText += "* " + input;
                allAdviceText += "* " + input;
            }
            adviceText += "</label>";
            adviceText += "</div>";


        } else {
            adviceText += "";
            allAdviceText += "";
        }


        if (ad2chk) {
            adviceText += "<div>";
            adviceText += "<label>";
            if ($("#advice2right").prop('checked') && $("#advice2left").prop('checked')) {
                adviceText += "* ডান,বাম পায়ে অল্প ভর দিয়ে হাঁটবেন.";
                allAdviceText += "* ডান,বাম পায়ে অল্প ভর দিয়ে হাঁটবেন.";
            }
            else if ($("#advice2right").prop('checked')) {
                adviceText += "* ডান পায়ে অল্প ভর দিয়ে হাঁটবেন.";
                allAdviceText += "* ডান পায়ে অল্প ভর দিয়ে হাঁটবেন.";
            }
            else if ($("#advice2left").prop('checked')) {
                adviceText += "* বাম পায়ে অল্প ভর দিয়ে হাঁটবেন.";
                allAdviceText += "* বাম পায়ে অল্প ভর দিয়ে হাঁটবেন.";
            } else {
                adviceText += "* ";
                allAdviceText += "* ";
            }
            adviceText += "</label>";
            adviceText += "</div>";


        } else {
            adviceText += "";
            allAdviceText += "";
        }


        if (ad3chk) {
            adviceText += "<div>";
            adviceText += "<label>";
            if ($("#advice3right").prop('checked') && $("#advice3left").prop('checked')) {
                adviceText += "* ডান,বাম পায়ে পুরো ভর দিয়ে হাঁটবেন.";
                allAdviceText += "* ডান,বাম পায়ে পুরো ভর দিয়ে হাঁটবেন.";
            }
            else if ($("#advice3right").prop('checked')) {
                adviceText += "* ডান পায়ে পুরো ভর দিয়ে হাঁটবেন.";
                allAdviceText += "* ডান পায়ে পুরো ভর দিয়ে হাঁটবেন.";
            }
            else if ($("#advice3left").prop('checked')) {
                adviceText += "* বাম পায়ে পুরো ভর দিয়ে হাঁটবেন.";
                allAdviceText += "* বাম পায়ে পুরো ভর দিয়ে হাঁটবেন.";
            } else {
                adviceText += "* ";
                allAdviceText += "* ";
            }
            adviceText += "</label>";
            adviceText += "</div>";


        } else {
            adviceText += "";
            allAdviceText += "";
        }

        if (ad4chk) {
            adviceText += "<div>";
            adviceText += "<label>";
            adviceText += "*  শেখানো মতো  ব্যায়াম করবেন.";
            allAdviceText += "*  শেখানো মতো  ব্যায়াম করবেন.";
            adviceText += "</label>";
            adviceText += "</div>";

        } else {
            adviceText += "";
            allAdviceText += "";
        }

        if (ad5chk) {
            adviceText += "<div>";
            adviceText += "<label>";
            adviceText += "*  হাতের আঙ্গুল বেশি বেশি নাড়াবেন.";
            allAdviceText += "*  হাতের আঙ্গুল বেশি বেশি নাড়াবেন.";
            adviceText += "</label>";
            adviceText += "</div>";

        } else {
            adviceText += "";
            allAdviceText += "";
        }
        if (ad6chk) {
            adviceText += "<div>";
            adviceText += "<label>";
            var input = $("#advice6input").val();
            var cmback = $('input[name=cmback]:checked').val();
            if (cmback == "1") {
                adviceText += "* " + input + " দিন পর দেখা করবেন.";
                allAdviceText += "* " + input + " দিন পর দেখা করবেন.";
            } else {
                adviceText += "* " + input + " মাস পর দেখা করবেন.";
                allAdviceText += "* " + input + " মাস পর দেখা করবেন.";
            }

            adviceText += "</label>";
            adviceText += "</div>";

        } else {
            adviceText += "";
            allAdviceText += "";
        }



        if (others != "") {
            adviceText += "* " + others;
            allAdviceText += "* " + others;
        }


        $("#adviceshown").html(adviceText);
        $("#adviceshowP").html(adviceText);

        $("#alladvice").val(allAdviceText);

    });
    function deleteRowData(rownumber) {
        var idvaldata = "medicine" + rownumber;
        document.getElementById(idvaldata).remove();
        var idvaldataprint = "medicineprint" + rownumber;
        document.getElementById(idvaldataprint).remove();
    }

    function deleteTestRowData(rownumber) {
        var idvaldata2 = "test" + rownumber;
        document.getElementById(idvaldata2).remove();
        var idvaldataprint2 = "testprint" + rownumber;
        document.getElementById(idvaldataprint2).remove();
    }

    function deleteReferenceData(val) {
        $("#areatreatment" + val).remove();
        $("#diagnosis" + val).remove();
        $("#areatreatmentP" + val).remove();
        $("#diagnosisP" + val).remove();
    }

    function deleteReferenceOthersData(val) {
        $("#areatreatmentothers" + val).remove();
        $("#diagnosisothers" + val).remove();
        $("#areatreatmentothersP" + val).remove();
        $("#diagnosisothersP" + val).remove();
    }
    $(document).ready(function () {
        setInjury();
        setSurgery();
        setFollowup();

    });

    $("#addoe").click(function () {
        var nerve_injury = $("input[name=nerve_injury]:checked", "#nerve_injury_div").val();
        var vascular_injury = $("input[name=vascular_injury]:checked", "#vascular_injury_div").val();

        if (nerve_injury == 1) {
            var nerve_injury_input = "<div>" + "Nerve Injury Presence : " + $('#nerve_injury_input').val() + "</div>";
        } else {
            var nerve_injury_input = "<div>" + "Nerve Injury Presence : No.<br>" + "</div>";
        }


        if (vascular_injury == 1) {
            var vascular_injury_input = "<div>" + "Vascular Injury Presence : " + $('#vascular_injury_input').val() + "</div>";
        } else {
            var vascular_injury_input = "<div>" + "Vascular Injury Presence : No.<br>" + "</div>";
        }

        var others = "<div>Others : " + $('#others').val() + "</div>";
        $('#oeshow').html(nerve_injury_input + vascular_injury_input + others);
        $('#oeshowP').html(nerve_injury_input + vascular_injury_input + others);

    });

</script> 
