<!--main content start-->
<style type="text/css">
    #testname2{
        height: 10px;
        padding: 5px;
    }
    .ui-menu-item{
        height: auto;
        padding: 25px;
        border: 1px solid blue;
        padding-bottom: 20px;
        margin-bottom: 20px;
        font-size: 12px;
        width: 355px;
    }
</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <form class="cmxform form-horizontal tasi-form" id="addprescriptiondata" method="POST" action="<?php echo site_url('prescription/prescription/addPrescriptiondata'); ?>"  enctype="multipart/form-data">
                <div class="col-lg-6" style="margin-bottom: 20px;">              
                    <section class="panel">
                        <header class="panel-heading">
                            Add Prescription
                        </header>
                        <div class="panel-body">
                            <?php
                            if ($this->session->userdata('successfull')):
                                echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                                $this->session->unset_userdata('successfull');
                            endif;
                            if ($this->session->userdata('failed')):
                                echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                                $this->session->unset_userdata('failed');
                            endif;
                            ?>                            
                            <div class=" form">
                                <div class="form-group">
                                    <label for="patientid" class="control-label col-lg-3"><b>Patient Name:</b><span style="color: red"> *</span></label>
                                    <div class="col-lg-8">
                                        <select class="form-control m-bot15 selectpicker" name="patientid" id="patientid" data-live-search="true" onchange="return getPatientInfo()" required disabled>
                                            <option value=""> --Select-- </option> 
                                            <?php
                                            if (sizeof($patientlist) > 0):
                                                foreach ($patientlist as $datarow):
                                                    echo '<option value="' . $datarow->id . '">' . $datarow->first_name . " " . $datarow->last_name . '</option> ';
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="day" class="control-label col-lg-3" ><b>Date Of Injury:</b></label>
                                    <div class="col-lg-8">
<!--                                                                        <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <select class="form-control m-bot15" name="dayInjury" onchange="setInjury();" id="dayInjury" required>
                                                <option selected="selected" value="">Day</option>
                                                <?php
                                                $currentDate = date('Y-m-d');
                                                $temDate = explode("-", $currentDate);
                                                $cYear = $temDate[0];
                                                $cMonth = $temDate[1];
                                                $cDay = $temDate[2];
                                                for ($day = 1; $day <= 31; $day++):
                                                    if ($day == $cDay):
                                                        ?>
                                                        <option value="<?php echo $day; ?>" selected="selected"><?php echo $day; ?></option>
                                                        <?php
                                                    else:
                                                        ?>
                                                        <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                    <?php
                                                    endif;
                                                    ?>

                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <select class="form-control m-bot15" name="monthInjury" onchange="setInjury();" id="monthInjury" required>
                                                <option selected="selected" value="">Month</option>
                                                <option value="01" <?php if ($cMonth == "01") echo 'selected'; ?>>January</option>
                                                <option value="02" <?php if ($cMonth == "02") echo 'selected'; ?>>February</option>
                                                <option value="03" <?php if ($cMonth == "03") echo 'selected'; ?>>March</option>
                                                <option value="04" <?php if ($cMonth == "04") echo 'selected'; ?>>April</option>
                                                <option value="05" <?php if ($cMonth == "05") echo 'selected'; ?>>May</option>
                                                <option value="06" <?php if ($cMonth == "06") echo 'selected'; ?>>June</option>
                                                <option value="07" <?php if ($cMonth == "07") echo 'selected'; ?>>July</option>
                                                <option value="08" <?php if ($cMonth == "08") echo 'selected'; ?>>August</option>
                                                <option value="09" <?php if ($cMonth == "09") echo 'selected'; ?>>September</option>
                                                <option value="10" <?php if ($cMonth == "10") echo 'selected'; ?>>October</option>
                                                <option value="11" <?php if ($cMonth == "11") echo 'selected'; ?>>November</option>
                                                <option value="12" <?php if ($cMonth == "12") echo 'selected'; ?>>December</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <select class="form-control m-bot15" name="yearInjury" onchange="setInjury();" id="yearInjury" required>
                                                <option selected="selected" value="">Year</option>
                                                <?php
                                                $currentyr = date('Y');
                                                for ($year = 1970; $year <= $currentyr + 15; $year++):
                                                    if ($year == $cYear):
                                                        ?>
                                                        <option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
                                                        <?php
                                                    else:
                                                        ?>
                                                        <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                    endif;
                                                    ?>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <input  id="unknownInjury" name="unknownInjury" onclick="resetDateFieldsInjury();" value="unknown"  type="checkbox"/> Unknown
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="day" class="control-label col-lg-3" ><b>Date Of Surgery:</b></label>
                                    <div class="col-lg-8">
<!--                                                                        <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <select class="form-control m-bot15" name="daySurgery" onchange="setSurgery()" id="daySurgery" required>
                                                <option selected="selected" value="">Day</option>
                                                <?php
                                                $currentDate = date('Y-m-d');
                                                $temDate = explode("-", $currentDate);
                                                $cYear = $temDate[0];
                                                $cMonth = $temDate[1];
                                                $cDay = $temDate[2];
                                                for ($day = 1; $day <= 31; $day++):
                                                    if ($day == $cDay):
                                                        ?>
                                                        <option value="<?php echo $day; ?>" selected="selected"><?php echo $day; ?></option>
                                                        <?php
                                                    else:
                                                        ?>
                                                        <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                    <?php
                                                    endif;
                                                    ?>

                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <select class="form-control m-bot15" name="monthSurgery" onchange="setSurgery()" id="monthSurgery" required>
                                                <option selected="selected" value="">Month</option>
                                                <option value="01" <?php if ($cMonth == "01") echo 'selected'; ?>>January</option>
                                                <option value="02" <?php if ($cMonth == "02") echo 'selected'; ?>>February</option>
                                                <option value="03" <?php if ($cMonth == "03") echo 'selected'; ?>>March</option>
                                                <option value="04" <?php if ($cMonth == "04") echo 'selected'; ?>>April</option>
                                                <option value="05" <?php if ($cMonth == "05") echo 'selected'; ?>>May</option>
                                                <option value="06" <?php if ($cMonth == "06") echo 'selected'; ?>>June</option>
                                                <option value="07" <?php if ($cMonth == "07") echo 'selected'; ?>>July</option>
                                                <option value="08" <?php if ($cMonth == "08") echo 'selected'; ?>>August</option>
                                                <option value="09" <?php if ($cMonth == "09") echo 'selected'; ?>>September</option>
                                                <option value="10" <?php if ($cMonth == "10") echo 'selected'; ?>>October</option>
                                                <option value="11" <?php if ($cMonth == "11") echo 'selected'; ?>>November</option>
                                                <option value="12" <?php if ($cMonth == "12") echo 'selected'; ?>>December</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <select class="form-control m-bot15" name="yearSurgery" onchange="setSurgery()" id="yearSurgery" required>
                                                <option selected="selected" value="">Year</option>
                                                <?php
                                                $currentyr = date('Y');
                                                for ($year = 1970; $year <= $currentyr + 15; $year++):
                                                    if ($year == $cYear):
                                                        ?>
                                                        <option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
                                                        <?php
                                                    else:
                                                        ?>
                                                        <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                    endif;
                                                    ?>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3" style="padding-left: 1px;padding-right: 1px">
                                            <input  id="unknownSurgery" name="unknownSurgery" onclick="resetDateFieldsSurgery();" value="unknown"  type="checkbox"/> Unknown
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="patientid" class="control-label col-lg-3"><b>Area Of Treatment:</b></label>
                                    <div class="col-lg-6">
                                        <select class="form-control" name="" id="reference1id" onchange="getReferunder(this.value)">                                               
                                            <?php
                                            if (sizeof($referencelist) > 0):
                                                echo ' <option value="">Select</option> ';
                                                foreach ($referencelist as $datarow):
                                                    echo '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option> ';
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                        <input type="hidden" id="reference1" name="reference1">
                                        <input type="hidden" id="reference1name" name="reference1name">
                                        <input type="hidden" name="reference1countcountdata" id="reference1countcountdata">
                                    </div>

                                </div>
                                <div class="form-group" id="surgeryreference2" hidden>
                                    <div class="col-lg-6 col-lg-offset-3">
                                        <div class="col-md-10">
                                            <span id="getrefdata2">
                                            </span>
                                            <input type="hidden" id="reference2" name="reference2">
                                            <input type="hidden" id="reference2name" name="reference2name">
                                            <input type="hidden" name="reference2countcountdata" id="reference2countcountdata">
                                        </div>
                                    </div>  
                                    <div class="col-lg-3" id="getrefimagedata2"></div>
                                </div> 
                                <div class="form-group" id="surgeryreference3" hidden>
                                    <!--                                    <label for="patientid" class="control-label col-lg-3"><b>Surgery Reference3:</b></label>-->
                                    <div class="col-lg-9 col-lg-offset-3">

                                        <span id="getrefdata3"></span> 

                                        <input type="hidden" id="reference3" name="reference3">
                                        <input type="hidden" id="reference3name" name="reference3name">
                                        <input type="hidden" name="reference3countcountdata" id="reference3countcountdata">

                                    </div>

                                </div>

                                <div class="form-group" id="surgeryreference4" hidden>
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-9">
                                        <span id="getrefdata4">

                                        </span> 
                                        <input type="hidden" id="reference4name" name="reference4name">
                                    </div>
                                    <div class="col-lg-3" id="getrefimagedata4">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="control-label col-lg-3"> </div>                                  
                                    <div class="col-lg-1"><b>Others</b></div>
                                    <div class="col-lg-5">
                                        <input  type="text" id="otherref" name="otherref">
                                    </div>
                                </div>

                                <div class="form-group" id="">
                                    <label for="patientid" class="control-label col-lg-3"></label>
                                    <div class="col-lg-8">
                                        <div class="col-md-2">
                                            <input type="hidden" id="reference4" name="reference4">
                                            <input type="hidden" id="reference4name" name="reference4name">
                                            <input type="hidden" name="reference4countcountdata" id="reference4countcountdata">
                                            <input type="hidden" name="lastupdtedataid" id="lastupdtedataid">
                                            <input type="hidden" name="referencetextvalue" id="referencetextvalue">
                                            <input type="hidden" name="referenceothervalue" id="referenceothervalue">
                                            <button class="btn btn-primary" id="referenceadd" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                        </div>
                                    </div>    
                                </div>
                                <div class="form-group ">
                                    <label for="history" class="control-label col-lg-3"><b>History:</b></label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="history" id="history" rows="5"  onkeyup="historyvalue()"></textarea>
                                    </div>
                                </div> 
                                <div class="form-group ">
                                    <label for="additionalinformation" class="control-label col-lg-3"><b>Additional Information:</b></label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" name="additionalinformation" id="additionalinformation" rows="5" onkeyup="additionalvalue()"></textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="history" class="control-label col-lg-3"><b>O/E:</b></label>
                                    <!--                                                                   <div class="col-md-8">
                                                                                                            <textarea class="form-control" name="oe" id="oe" rows="5"  onkeyup="oevalue()"></textarea>
                                                                                                        </div>-->
                                    <div class="col-lg-8">
                                        <label for="nerve_injury" class="control-label col-lg-7" style="text-align: left;" ><b>Nerve Injury Presence?</b></label>
                                        <div class="col-lg-5" id="nerve_injury_div">                             
                                            <input name="nerve_injury" id="nerve_injury" value="1" type="radio" onclick="hideNerveInjury(this.value);"  /> Yes &nbsp;&nbsp;
                                            <input name="nerve_injury" id="nerve_injury" value="0" type="radio"  onclick="hideNerveInjury(this.value);" /> No
                                            <input name="nerve_injury_input"  id="nerve_injury_input"  type="text"  style="margin: 10px 0px; display: none;" />
                                        </div>

                                        <label for="vascular_injury" class="control-label col-lg-7" style="text-align: left;" ><b>Vascular Injury Presence?</b></label>
                                        <div class="col-lg-5" id="vascular_injury_div">                             
                                            <input name="vascular_injury" id="vascular_injury" value="1" type="radio" onclick="hideVascularInjury(this.value);"  /> Yes &nbsp;&nbsp;
                                            <input name="vascular_injury" id="vascular_injury" value="0" type="radio" onclick="hideVascularInjury(this.value);" /> No 
                                            <input name="vascular_injury_input"  id="vascular_injury_input"  type="text"  style="margin: 10px 0px;display: none;" />
                                        </div>

                                        <label for="vascular_injury" class="control-label col-lg-7" style="text-align: left;" ><b>Others</b></label>
                                        <div class="col-lg-5" >                                                                       
                                            <input name="others"  id="others"  type="text"  style="margin: 10px 0px;" />
                                        </div>

                                        <div class="col-md-2">
                                            <button class="btn btn-primary" id="addoe" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                        </div>

                                    </div>
                                </div>                            

                                <div class="form-group last">
                                    <label class="control-label col-md-3"><b>Medical Test:</b></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="medicaltestname" id="testname" placeholder="Type Test Name">
<!--                                            <select class="form-control m-bot15 selectpicker" name="medicaltestname" id="testname" data-live-search="true">
                                            <option value=""> --Select Medical Test-- </option> 
                                        <?php
                                        if (sizeof($testlist) > 0):
                                            foreach ($testlist as $datarow):
                                                echo '<option value="' . $datarow->test_name . '">' . $datarow->test_name . '</option> ';
                                            endforeach;
                                        endif;
                                        ?>
                                        </select>-->
                                        <input type="hidden" name="testcountdata" id="testcountdata">
                                        <span id="addmedicinetestdescription"></span>                                      
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary" id="addtest" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="diagnosis" class="control-label col-lg-3"><b>Diagnosis:</b></label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" name="diagnosis" id="diagnosis" rows="5" onkeyup="diagnosisvalue()"></textarea>
                                    </div>
                                </div> 
                                <div class="form-group last">
                                    <label class="control-label col-md-3"><b>Medicine:</b></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="medicinename" id="medicinename" onblur="medicineDetails(this.value)" placeholder="Type Medicine Name">
                                        <span id="addmedicinedescription"></span>
                                        <input type="hidden" name="medicinetabval" id="medicinetabval" value="0">
                                        <input type="hidden" name="medicinecountdata" id="medicinecountdata">
                                        <p> &nbsp; </p>
                                        <textarea class="form-control" name="medicinefeedinst" id="medicinefeedinst" placeholder="Type medicine feed instruction here.." rows="4"></textarea>
                                        <span id="medicineinstreq"></span>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary" id="addmedicine" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="advice" class="control-label col-lg-3"><b>Advice:</b></label>
                                    <div class="col-md-8">
                                        <div><span id="span1"><input type="checkbox" id="advice1" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>১.</label>&nbsp;&nbsp;<select class="input-small" name="advice1input"  id="advice1input">
                                                    <option value="">Select</option>                                                                                                                                                                                                 
                                                    <option value="১">১</option>
                                                    <option value="২">২</option>
                                                    <option value="৩">৩</option>
                                                    <option value="৪">৪</option>
                                                    <option value="৫">৫</option>
                                                    <option value="৬">৬</option>
                                                    <option value="৭">৭</option>
                                                    <option value="৮">৮</option>
                                                    <option value="৯">৯</option>
                                                    <option value="১০">১০</option>                                                                                                                                                                                                 
                                                    <option value="১১">১১</option>
                                                    <option value="১২">১২</option>
                                                    <option value="১৩">১৩</option>
                                                    <option value="১৪">১৪</option>
                                                    <option value="১৫">১৫</option>
                                                    <option value="১৬">১৬</option>
                                                    <option value="১৭">১৭</option>
                                                    <option value="১৮">১৮</option>
                                                    <option value="১৯">১৯</option>
                                                    <option value="২০">২০</option> 
                                                    <option value="২১">২১</option>
                                                    <option value="২২">২২</option>
                                                    <option value="২৩">২৩</option>
                                                    <option value="২৪">২৪</option>
                                                    <option value="২৫">২৫</option>
                                                    <option value="২৬">২৬</option>
                                                    <option value="২৭">২৭</option>
                                                    <option value="২৮">২৮</option>
                                                    <option value="২৯">২৯</option>
                                                    <option value="৩০">৩০</option>  
                                                    <option value="৩১">৩১</option>
                                                    <option value="৩২">৩২</option>
                                                    <option value="৩৩">৩৩</option>
                                                    <option value="৩৪">৩৪</option>
                                                    <option value="৩৫">৩৫</option>
                                                    <option value="৩৬">৩৬</option>
                                                    <option value="৩৭">৩৭</option>
                                                    <option value="৩৮">৩৮</option>
                                                    <option value="৩৯">৩৯</option>
                                                    <option value="৪০">৪০</option>                                                                                                                                                                                                 
                                                    <option value="৪১">৪১</option>
                                                    <option value="৪২">৪২</option>
                                                    <option value="৪৩">৪৩</option>
                                                    <option value="৪৪">৪৪</option>
                                                    <option value="৪৫">৪৫</option>
                                                    <option value="৪৬">৪৬</option>
                                                    <option value="৪৭">৪৭</option>
                                                    <option value="৪৮">৪৮</option>
                                                    <option value="৪৯">৪৯</option>
                                                    <option value="৫০">৫০</option>                                                                                                                                                                                                 
                                                    <option value="৫১">৫১</option>
                                                    <option value="৫২">৫২</option>
                                                    <option value="৫৩">৫৩</option>
                                                    <option value="৫৪">৫৪</option>
                                                    <option value="৫৫">৫৫</option>
                                                    <option value="৫৬">৫৬</option>
                                                    <option value="৫৭">৫৭</option>
                                                    <option value="৫৮">৫৮</option>
                                                    <option value="৫৯">৫৯</option>
                                                    <option value="৬০">৬০</option> 
                                                    <option value="৬১">৬১</option>
                                                    <option value="৬২">৬২</option>
                                                    <option value="৬৩">৬৩</option>
                                                    <option value="৬৪">৬৪</option>
                                                    <option value="৬৫">৬৫</option>
                                                    <option value="৬৬">৬৬</option>
                                                    <option value="৬৭">৬৭</option>
                                                    <option value="৬৮">৬৮</option>
                                                    <option value="৬৯">৬৯</option>
                                                    <option value="৭০">৭০</option>  
                                                    <option value="৭১">৭১</option>
                                                    <option value="৭২">৭২</option>
                                                    <option value="৭৩">৭৩</option>
                                                    <option value="৭৪">৭৪</option>
                                                    <option value="৭৫">৭৫</option>
                                                    <option value="৭৬">৭৬</option>
                                                    <option value="৭৭">৭৭</option>
                                                    <option value="৭৮">৭৮</option>
                                                    <option value="৭৯">৭৯</option>
                                                    <option value="৮০">৮০</option>                                                                                                                                                                                                 
                                                    <option value="৮১">৮১</option>
                                                    <option value="৮২">৮২</option>
                                                    <option value="৮৩">৮৩</option>
                                                    <option value="৮৪">৮৪</option>
                                                    <option value="৮৫">৮৫</option>
                                                    <option value="৮৬">৮৬</option>
                                                    <option value="৮৭">৮৭</option>
                                                    <option value="৮৮">৮৮</option>
                                                    <option value="৮৯">৮৯</option>
                                                    <option value="৯০">৯০</option>                                                                                                                                                                                                 
                                                    <option value="৯১">৯১</option>
                                                    <option value="৯২">৯২</option>
                                                    <option value="৯৩">৯৩</option>
                                                    <option value="৯৪">৯৪</option>
                                                    <option value="৯৫">৯৫</option>
                                                    <option value="৯৬">৯৬</option>
                                                    <option value="৯৭">৯৭</option>
                                                    <option value="৯৮">৯৮</option>
                                                    <option value="৯৯">৯৯</option>
                                                </select>&nbsp;&nbsp;<label>দিন  ডান</label>&nbsp;<input type="checkbox" id="advice1right" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;<label>, বাম</label>&nbsp;<input type="checkbox" id="advice1left"  onclick="controlAdvice(this.id);">&nbsp;<label>পায়ে ভর দিবেন না.</label></span></div>
                                        <div><span><input type="checkbox" id="advice2" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>২.</label>&nbsp;&nbsp;<label>ডান</label>&nbsp;<input type="checkbox" id="advice2right" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;<label>, বাম</label>&nbsp;<input type="checkbox" id="advice2left" onclick="controlAdvice(this.id);">&nbsp;<label>পায়ে অল্প ভর দিয়ে হাঁটবেন.</label></span></div>
                                        <div><span><input type="checkbox" id="advice3" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৩.</label>&nbsp;&nbsp;<label>ডান</label>&nbsp;<input type="checkbox" id="advice3right" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;<label>, বাম</label>&nbsp;<input type="checkbox" id="advice3left" onclick="controlAdvice(this.id);">&nbsp;<label>পায়ে পুরো ভর দিয়ে হাঁটবেন.</label></span></div>
                                        <div><span><input type="checkbox" id="advice4" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৪.</label>&nbsp;&nbsp;<label id="label4">শেখানো মতো  ব্যায়াম করবেন.</label></span></div>
                                        <div><span><input type="checkbox" id="advice5" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৫.</label>&nbsp;&nbsp;<label id="label5">হাতের আঙ্গুল বেশি বেশি নাড়াবেন.</label></span></div>
                                        <div><span><input type="checkbox" id="advice6" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৬.</label>&nbsp;&nbsp;<select class="input-small" name="advice6input"  id="advice6input">
                                                    <option value="">Select</option>                                                                                                                                                                                                 
                                                    <option value="১">১</option>
                                                    <option value="২">২</option>
                                                    <option value="৩">৩</option>
                                                    <option value="৪">৪</option>
                                                    <option value="৫">৫</option>
                                                    <option value="৬">৬</option>
                                                    <option value="৭">৭</option>
                                                    <option value="৮">৮</option>
                                                    <option value="৯">৯</option>
                                                    <option value="১০">১০</option>                                                                                                                                                                                                 
                                                    <option value="১১">১১</option>
                                                    <option value="১২">১২</option>
                                                    <option value="১৩">১৩</option>
                                                    <option value="১৪">১৪</option>
                                                    <option value="১৫">১৫</option>
                                                    <option value="১৬">১৬</option>
                                                    <option value="১৭">১৭</option>
                                                    <option value="১৮">১৮</option>
                                                    <option value="১৯">১৯</option>
                                                    <option value="২০">২০</option> 
                                                    <option value="২১">২১</option>
                                                    <option value="২২">২২</option>
                                                    <option value="২৩">২৩</option>
                                                    <option value="২৪">২৪</option>
                                                    <option value="২৫">২৫</option>
                                                    <option value="২৬">২৬</option>
                                                    <option value="২৭">২৭</option>
                                                    <option value="২৮">২৮</option>
                                                    <option value="২৯">২৯</option>
                                                    <option value="৩০">৩০</option>  
                                                    <option value="৩১">৩১</option>
                                                    <option value="৩২">৩২</option>
                                                    <option value="৩৩">৩৩</option>
                                                    <option value="৩৪">৩৪</option>
                                                    <option value="৩৫">৩৫</option>
                                                    <option value="৩৬">৩৬</option>
                                                    <option value="৩৭">৩৭</option>
                                                    <option value="৩৮">৩৮</option>
                                                    <option value="৩৯">৩৯</option>
                                                    <option value="৪০">৪০</option>                                                                                                                                                                                                 
                                                    <option value="৪১">৪১</option>
                                                    <option value="৪২">৪২</option>
                                                    <option value="৪৩">৪৩</option>
                                                    <option value="৪৪">৪৪</option>
                                                    <option value="৪৫">৪৫</option>
                                                    <option value="৪৬">৪৬</option>
                                                    <option value="৪৭">৪৭</option>
                                                    <option value="৪৮">৪৮</option>
                                                    <option value="৪৯">৪৯</option>
                                                    <option value="৫০">৫০</option>                                                                                                                                                                                                 
                                                    <option value="৫১">৫১</option>
                                                    <option value="৫২">৫২</option>
                                                    <option value="৫৩">৫৩</option>
                                                    <option value="৫৪">৫৪</option>
                                                    <option value="৫৫">৫৫</option>
                                                    <option value="৫৬">৫৬</option>
                                                    <option value="৫৭">৫৭</option>
                                                    <option value="৫৮">৫৮</option>
                                                    <option value="৫৯">৫৯</option>
                                                    <option value="৬০">৬০</option> 
                                                    <option value="৬১">৬১</option>
                                                    <option value="৬২">৬২</option>
                                                    <option value="৬৩">৬৩</option>
                                                    <option value="৬৪">৬৪</option>
                                                    <option value="৬৫">৬৫</option>
                                                    <option value="৬৬">৬৬</option>
                                                    <option value="৬৭">৬৭</option>
                                                    <option value="৬৮">৬৮</option>
                                                    <option value="৬৯">৬৯</option>
                                                    <option value="৭০">৭০</option>  
                                                    <option value="৭১">৭১</option>
                                                    <option value="৭২">৭২</option>
                                                    <option value="৭৩">৭৩</option>
                                                    <option value="৭৪">৭৪</option>
                                                    <option value="৭৫">৭৫</option>
                                                    <option value="৭৬">৭৬</option>
                                                    <option value="৭৭">৭৭</option>
                                                    <option value="৭৮">৭৮</option>
                                                    <option value="৭৯">৭৯</option>
                                                    <option value="৮০">৮০</option>                                                                                                                                                                                                 
                                                    <option value="৮১">৮১</option>
                                                    <option value="৮২">৮২</option>
                                                    <option value="৮৩">৮৩</option>
                                                    <option value="৮৪">৮৪</option>
                                                    <option value="৮৫">৮৫</option>
                                                    <option value="৮৬">৮৬</option>
                                                    <option value="৮৭">৮৭</option>
                                                    <option value="৮৮">৮৮</option>
                                                    <option value="৮৯">৮৯</option>
                                                    <option value="৯০">৯০</option>                                                                                                                                                                                                 
                                                    <option value="৯১">৯১</option>
                                                    <option value="৯২">৯২</option>
                                                    <option value="৯৩">৯৩</option>
                                                    <option value="৯৪">৯৪</option>
                                                    <option value="৯৫">৯৫</option>
                                                    <option value="৯৬">৯৬</option>
                                                    <option value="৯৭">৯৭</option>
                                                    <option value="৯৮">৯৮</option>
                                                    <option value="৯৯">৯৯</option>
                                                </select>&nbsp;&nbsp;<input name="cmback" id="cmback" class="cb" value="1" type="radio" onclick="controlAdvice(this.id);" /><label> দিন </label>&nbsp;&nbsp;<input name="cmback" id="cmback" class="cb" value="0" type="radio" onclick="controlAdvice(this.id);" /><label>মাস</label> &nbsp;<label id="label6">পর দেখা করবেন.</label></span></div>
                                        <textarea class="form-control" name="advice" id="advice" rows="5"></textarea>
                                        <input type="hidden" name="datetime" value="<?php echo date('j  F,  Y'); ?>">
                                        <input type="hidden" name="visitcountinform" id="visitcountinform">
                                        <input type="hidden" name="prescriptionIdinform" id="prescriptionIdinform">
                                        <input type="hidden" name="alladvice" id="alladvice">
                                        <br>
                                        <button class="btn btn-primary" id="adviceadd" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                    </div>
                                </div> 

                                <h6 class="panel-heading" >
                                    Prescription Images
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="xRayImageForm" name="xRayImageForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureXrayImage');                                                                   ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="display: block;"  id="img">
                                    <div class="col-lg-11 col-lg-offset-1" id="xrayimagearea">

                                    </div>
                                </div>

                                <div class="form-group" style="padding-top: 10px;">
                                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-lg-9 col-md-9  col-sm-9 col-xs-9">
                                        <button onclick="addnewxrayimage()" class="btn "  style="background: #21BBC7" type="button" >&nbsp;&nbsp;+ Add Image To This Prescription&nbsp;&nbsp;</button>                               
                                    </div>
                                </div>
                                <!--                                        end formgroup-->

                                <hr/> 


                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-lg-6">              
                    <section class="panel">
                        <!--                        <div id="column2">
                                                    <div contenteditable="true">-->
                        <header class="panel-heading">
                            Prescription Overview
                        </header>
                        <div class="panel-body" id="">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <b>ID : <span id="prescriptionId"></span></b> 
                                    <p style="margin: 0px;"><span id="fullname"></span> <span id="birthday"></span></p>
                                    <p style="margin: 0px;"><span id="address"></span></p> 
                                </div>
                                <div class="col-lg-6">
                                    Date : <?php echo date('j  F,  Y'); ?>
                                </div>
                            </div>                        
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <p style="margin-top: 50px;"><b>Date Of Injury :</b></p> 
                                    <p style="margin: 0px;" id="injuryshow"></p>
                                    <p style="margin-top: 10px;"><b>Date Of Surgery :</b></p> 
                                    <p style="margin: 0px;" id="surgeryshow"></p>


                                    <p style="margin-top: 10px;"><b>History :</b></p> 
                                    <p style="margin: 0px;" id="historyshow"></p>
                                    <p style="margin-top: 10px;"><b>Additional Information :</b></p> 
                                    <p style="margin: 0px;" id="additionalinformationshow"></p>

                                    <p style="margin-top: 10px;"><b>O/E :</b></p> 
                                    <p style="margin: 0px;" id="oeshow"><b></b></p>

                                    <p style="margin-top: 10px;"><b>Area Of Treatment:</b></p>
<!--                                    <p style="margin: 0px;" id="reference1addshow"></p>
                                    <p style="margin: 0px;" id="reference2addshow"></p>
                                    <p style="margin: 0px;" id="reference3addshow"></p>
                                    <p style="margin: 0px;" id="reference4addshow"></p>-->
                                    <p style="margin: 0px;" id="referenceaddshow"></p>

                                    <p style="margin-top: 10px;"><b>Test Advised :</b></p>
                                    <p style="margin: 0px;" id="medicaltestdatashow"></p>
                                    <p style="margin-top: 10px;"><b>Diagnosis :</b></p>
                                    <p style="margin: 0px;" id="referenceaddshow2"></p>
                                    <p style="margin: 0px;" id="diagnosisshow"></p>
                                </div>
                                <div class="col-lg-6">
                                    <p style="margin-top: 50px;"><b>Rx :</b></p> 
                                    <div id="medicinedata">

                                    </div>                               
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6">
                                    <p style="margin-top: 20px;"><b>Advice :</b></p>
                                    <p style="margin: 0px;" id="adviceshown"></p>  
                                    <p style="margin: 0px;" id="adviceshow"></p>                              
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-4 col-lg-8" style="margin-top: 30px; margin-bottom: 20px;">
                                <button class="btn btn-primary" onclick="Clickheretoprint('#printprescription')"  type="submit">&nbsp;&nbsp;Save & Print &nbsp;&nbsp;</button>
                                <button class="btn btn-primary" type="submit">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
                                <button class="btn btn-primary" type="reset">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
                            </div>
                        </div>
                        <!--                            </div>
                                                </div>-->
                    </section>
                </div>
            </form>            
        </div>
    </section>
</section>
<!--main content end-->
<section class="page" style="display: none" id="printprescription">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-body">
                <div class="col-lg-12">              
                    <section class="panel">
                        <div class="panel-body" id="">
                            <style>
                                #printtd {
                                    width: 50%;
                                    height: auto;
                                    text-align: left;
                                    padding: 5px 10px 10px 50px;
                                    border: hidden;
                                }
                                #bottompaddingrow {
                                    display: none;
                                }
                                @media print {
                                    #panelheading {
                                        display: none !important;
                                    }
                                    #img {
                                        display: none !important;
                                    }
                                    #bottompaddingrow {
                                        display: block;
                                    }
                                    table {
                                        font-size: 12px;
                                    }
                                    @page { size: portrait; margin: 0; }
                                }
                            </style>
                            <table style="width: 100%;">
                                <tr id="bottompaddingrow">
                                    <td style="padding-bottom: 190px;"></td>
                                    <td style="padding-bottom: 190px;"></td>
                                </tr>
                                <tr>
                                    <td id="printtd" style="padding-bottom: 0px"><b>ID : </b><span id="prescriptionIdP"></span></td>
                                    <td id="printtd">Date : <?php echo date('j  F,  Y'); ?></td>
                                </tr>
                                <tr>
                                    <td id="printtd" style=" padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="fullnameP"></span> <span id="birthdayP"></span></p></td>
                                    <td id="printtd"></td>
                                </tr>
                                <tr>
                                    <td id="printtd" style=" padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="addressP"></span></p></td>
                                    <td id="printtd"> </td>
                                </tr>
                                <tr>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>Date Of Injury :</b></p> 
                                        <p style="margin: 0px;" id="injuryshowp"></p>
                                    </td> 
                                    <td id="printtd" rowspan="5" style="vertical-align: top">
                                        <p style="margin-top: 0px;"><b>Rx :</b></p> 
                                        <div id="medicinedataP"></div>      
                                    </td>
                                </tr>
                                <tr>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>Date Of Surgery :</b></p> 
                                        <p style="margin: 0px;" id="surgeryshowp"></p>
                                    </td>                                     
                                </tr>

                                <tr>                                                                                               
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>History :</b></p> 
                                        <p style="margin: 0px;" id="historyshowP"></p>
                                    </td>

                                </tr>
                                <tr>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>Additional Information :</b></p> 
                                        <p style="margin: 0px;" id="additionalinformationshowP"></p>
                                    </td>

                                </tr>
                                <tr>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>O/E :</b></p> 
                                        <p style="margin: 0px;" id="oeshowP"></p>
                                    </td>

                                </tr>
                                <tr>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>Area Of Treatment:</b></p>
                                        <p style="margin: 0px;" id="referenceshowP"></p>
                                    </td>

                                </tr>
                                <tr>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>Test Advised :</b></p>
                                        <p style="margin: 0px;" id="medicaltestdatashowP"></p>
                                    </td>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>Advice:</b></p>
                                        <p style="margin: 0px;" id="adviceshowP"></p> 
                                    </td>

                                </tr>
                                <tr>
                                    <td id="printtd">
                                        <p style="margin: 0px; padding: 0"><b>Diagnosis :</b></p>
                                        <p style="margin: 0px;" id="referenceaddshow2P"></p>
                                        <p style="margin: 0px;" id="diagnosisshowP"></p>
                                    </td>

                                </tr>                                        
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>
<link href="<?php echo $baseurl; ?>assets/css/jquery-ui.css" rel="stylesheet">


<script type="text/javascript">
    var mlimit = 15;
    var availableTags = new Array();
    var medicineIdList = new Array();
    $(function () {
<?php
if (sizeof($medicinelist) > 0):
    foreach ($medicinelist as $datarow):
        ?>
                availableTags.push("<?php echo $datarow->formulation . ' - ' . $datarow->brand_name . ' - ' . $datarow->strength; ?>");
                medicineIdList.push("<?php echo $datarow->id; ?>");
        <?php
    endforeach;
endif;
?>
        $("#medicinename").autocomplete({
            //source: availableTags
            source: function (request, response) {
                var results = $.ui.autocomplete.filter(availableTags, request.term);
                response(results.slice(0, mlimit));
            }
        });
    });
</script>
<script type="text/javascript">
    var limit = 15;
    var availableTest = new Array();
    var testList = new Array();
    $(function () {
<?php
if (sizeof($testlist) > 0):
    foreach ($testlist as $datarow):
        ?>
                availableTest.push("<?php echo $datarow->test_name; ?>");
                testList.push("<?php echo $datarow->id; ?>");
        <?php
    endforeach;
endif;
?>
        $("#testname").autocomplete({
            source: function (request, response) {
                var results = $.ui.autocomplete.filter(availableTest, request.term);
                response(results.slice(0, limit));
            }
        });
    });
</script>
<script type="text/javascript">
    
    $( document ).ready(function() {
     document.getElementById('patientid').disabled = false;
});

    function getPatientInfo() {
        var patientid = $("#patientid").val();
        $.ajax({type: "POST",
            url: "<?php echo site_url('prescription/prescription/getPatientInfo'); ?>",
            data: 'patientid=' + patientid,
            success: function (data) {
                var ob = JSON.parse(data);
                $("#fullname").text(ob.first_name + ' ' + ob.last_name);
                $("#fullnameP").text(ob.first_name + ' ' + ob.last_name);
                $("#birthday").text(' , ' + ob.age + ' years');
                $("#birthdayP").text(' , ' + ob.age + ' years');
                $("#address").text(ob.address);
                $("#addressP").text(ob.address);
                $("#mobile").val(ob.mobile);
            }
        });
//        $.ajax({
//            type: "POST",
//            url: "<?php //echo site_url('prescription/prescription/getPatientCount');                             ?>",
//            data: 'patientid=' + patientid,
//            success: function (data) {
//                var ob = JSON.parse(data);
//                var visitcount = parseInt(ob.visitcount) + 1
//                $("#visitcount").text(visitcount);
//                //$("#visitcount").text(ob.visitcount);
//                $("#visitcountP").text(visitcount);
//                $("#visitcountinform").val(visitcount);
//            }
//        });
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/generateRandonNum'); ?>",
            data: 'patientid=' + patientid,
            success: function (data) {
                var ob = JSON.parse(data);
                $("#prescriptionId").text('R' + data);
                $("#prescriptionIdP").text('R' + data);
                $("#prescriptionIdinform").val('R' + data);
            }
        });
    }

    function medicineDetails(medicinename) {
        //alert(medicinename);
        var medicineIndex = availableTags.indexOf(medicinename);
        var medicineId = medicineIdList[medicineIndex];
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getMedicinedesc'); ?>",
            data: 'medicineName=' + medicineId,
            success: function (data) {
                var destext = "";
                var dosages;
                try {
                    var ob = JSON.parse(data);
                    destext = ob.manufacturer_name + ", " + ob.generic_name + ", " + ob.strength;

                    var d = ob.dosages;
                    var n = ob.notes;
                    if (d == null && n == null) {

                    } else {
                        if (d == null) {
                            d = "";
                        }
                        if (n == null) {
                            n = "";
                        }
                        dosages = d + "\n" + n;
                        $("#medicinefeedinst").val(dosages);
                    }

                    $("#medicinetabval").val(1);
                } catch (e) {
                    destext = "No description available for this medicine.";
                    $("#medicinetabval").val(0);
                }
                $("#addmedicinedescription").text(destext);
            }
        });
    }

    function getReferunder(id) {
        var curr = id.split(',');
        $("#reference1").val(curr[0]);
        $("#reference1name").val(curr[1]);
        $("#reference2name").val("");
        $("#reference3name").val("");
        $("#reference4name").val("");

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getFirstReference1'); ?>",
            data: 'id=' + id,
            success: function (data) {
                var arrData = JSON.parse(data);
                var underrefimagedata = arrData.underrefimagedata;
                $("#surgeryreference2").show();
                $("#getrefdata2").show();

                $("#getrefimagedata2").hide();
                $("#surgeryreference3").hide();
                $("#getrefdata3").hide();
                $("#surgeryreference4").hide();
                $("#getrefdata4").hide();
                document.getElementById('getrefdata2').innerHTML = underrefimagedata;
            }
        });
    }

    function getReferunder2(id) {
        var curr = id.split(',');
        $("#reference2").val(curr[0]);
        $("#reference2name").val(curr[1]);
        $("#reference3name").val("");
        $("#reference4name").val("");
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getFirstReference2'); ?>",
            data: 'id=' + id,
            success: function (data) {

                var arrData = JSON.parse(data);
                var underrefimagedata = arrData.underrefimagedata;
                var refimagedata = arrData.refimagedata;

                $("#surgeryreference3").show();
                $("#getrefdata3").show();
                $("#getrefimagedata2").show();
                $("#getrefimagedata3").hide();
                $("#surgeryreference4").hide();
                $("#getrefdata4").hide();

                if (underrefimagedata.indexOf("<select") >= 0) {
                    underrefimagedata = '<div class="col-lg-7">' + underrefimagedata + '</div>' + '<div class="col-lg-4 col-lg-offset-1" id="getrefimagedata3"></div>';
                }


                document.getElementById('getrefdata3').innerHTML = underrefimagedata;
                document.getElementById('getrefimagedata2').innerHTML = refimagedata;


            }
        });
    }

    function getReferunder3(id) {
        var curr = id.split(',');
        $("#reference3").val(curr[0]);
        $("#reference3name").val(curr[1]);
        $("#reference4name").val("");
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getFirstReference3'); ?>",
            data: 'id=' + id,
            success: function (data) {
                var arrData = JSON.parse(data);
                var underrefimagedata = arrData.underrefimagedata;
                var refimagedata = arrData.refimagedata;

                $("#surgeryreference4").show();
                $("#getrefdata4").show();
                $("#getrefimagedata3").show();

                document.getElementById('getrefdata4').innerHTML = underrefimagedata;
                document.getElementById('getrefimagedata3').innerHTML = refimagedata;


            }
        });
    }

    function getReferunder4(id) {
        var curr = id.split(',');
        $("#reference4").val(curr[0]);
        $("#reference4name").val(curr[1]);
    }

    function historyvalue() {
        var historyval = $("#history");
        var val = historyval.val().replace(/\n/g, '<br/>');
        $("#historyshow").html(val);
        $("#historyshowP").html(val);
    }

    function additionalvalue() {
        var additionalinformationval = $("#additionalinformation");
        var val = additionalinformationval.val().replace(/\n/g, '<br/>');
        $("#additionalinformationshow").html(val);
        $("#additionalinformationshowP").html(val);
    }

    function oevalue() {
        var oeval = $("#oe");
        var val = oeval.val().replace(/\n/g, '<br/>');
        $("#oeshow").html(val);
        $("#oeshowP").html(val);
    }

    function medicaltestvalue(medicaltestdata) {
        $("#medicaltestdatashow").text(medicaltestdata);
        $("#medicaltestdatashowP").text(medicaltestdata);
    }

    function diagnosisvalue() {
        var diagnosisval = $("#diagnosis");
        var val = diagnosisval.val().replace(/\n/g, '<br/>');
        $("#diagnosisshow").html(val);
        $("#diagnosisshowP").html(val);
    }

    function advicevalue() {
        var adviceval = $("#advice");
        var val = adviceval.val().replace(/\n/g, '<br/>');
        $("#adviceshow").html(val);
        $("#adviceshowP").html(val);
    }

    function resetDateFieldsInjury() {
        if ($("#unknownInjury").prop("checked") == true) {
            $('#dayInjury').prop("required", "");
            $('#monthInjury').prop("required", "");
            $('#yearInjury').prop("required", "");
            $('#dayInjury').prop('selectedIndex', 0);
            $('#monthInjury').prop('selectedIndex', 0);
            $('#yearInjury').prop('selectedIndex', 0);
            setInjury();
        } else {
            $('#dayInjury').prop("required", "required");
            $('#monthInjury').prop("required", "required");
            $('#yearInjury').prop("required", "required");
        }
    }

    function resetDateFieldsSurgery() {
        if ($("#unknownSurgery").prop("checked") == true) {
            $('#daySurgery').prop("required", "");
            $('#monthSurgery').prop("required", "");
            $('#yearSurgery').prop("required", "");
            $('#daySurgery').prop('selectedIndex', 0);
            $('#monthSurgery').prop('selectedIndex', 0);
            $('#yearSurgery').prop('selectedIndex', 0);
            setSurgery();
        } else {
            $('#daySurgery').prop("required", "required");
            $('#monthSurgery').prop("required", "required");
            $('#yearSurgery').prop("required", "required");
        }
    }

    function selectimg4(val) {

        var data = val.split("#");

        var id = data[0];
        var ref_id = data[1];
        var ref = data[2];
        $("#reference4").val(ref_id);
        $("#reference4name").val(ref);
        $("#div" + id).css('border-color', 'red');
        var i;
        var len = $("#totalimgcount4").val();
        for (i = 1; i <= len; i++) {
            if (i == id)
                continue;
            $("#div" + i).css('border-color', 'blue');
        }
    }

    function selectimg3(val) {


        var data = val.split("#");
        var id = data[0];
        var ref_id = data[1];
        var ref = data[2];
        $("#reference3").val(ref_id);
        $("#reference3name").val(ref);
        $("#div" + id).css('border-color', 'red');
        var i;
        var len = $("#totalimgcount3").val();
        for (i = 1; i <= len; i++) {
            if (i == id)
                continue;
            $("#div" + i).css('border-color', 'blue');
        }
    }

    function hideNerveInjury(value) {
        if (value == "1") {
            $("#nerve_injury_input").show();
        }
        else {
            $("#nerve_injury_input").hide();
        }
    }

    function hideVascularInjury(value) {
        if (value == "1") {
            $("#vascular_injury_input").show();
        }
        else {
            $("#vascular_injury_input").hide();
        }
    }

    //For print
    function Clickheretoprint(elem) {

        Popup(jQuery(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', '');
        mywindow.document.write('<html><title></title>');
        //  mywindow.document.write('<link rel="stylesheet" href="<?php //echo $baseurl                               ?>assets/css/bootstrap.min.css" type="text/css" />');
        mywindow.document.write('<head></head><body><style type="text/css">');
        mywindow.document.write('table{margin-left:12px;margin-right:0px;margin-top:1px;margin-bottom:0px}');
        mywindow.document.write('table thead, tr, th, table tbody, tr, td { border: none; width:auto}');
        mywindow.document.write('<style type="text/css">body{ margin-left:100px;margin-right:0px;margin-top:1px;margin-bottom:0px } </style></body>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.document.close();
        mywindow.print();
    }

    var count = 0;
    function addnewxrayimage() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();


        count++;
        var newxrayimagediv = "";
        var selectday = "";
        var selectimg = "";
        var selectop = "";
        var imgnum = "";
        var selectmonth = "";
        var selectyear = "";
        var btndelete = "";

        newxrayimagediv = "<div class='col-lg-5' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + count + "'>";
        selectimg = "<div class ='col-md-4 col-md-offset-1'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '' alt = '' /> \n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
 <span class = 'btn btn-white btn-file'>\n\
<span class = 'fileupload-new'> <i class = 'fa fa-paper-clip' > </i> Select image</span>\n\
 <span class = 'fileupload-exists'> <i class = 'fa fa-undo'> </i> Change</span> \n\
<input type = 'file' multiple name='userfile[]' id='userfile" + count + "' class = 'default' required/> </span> \n\
<input type = 'hidden' name='opCount' value ='" + count + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

        imgnum = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #</span>\n\
</select>\n\
</div>";

        selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = 'operation" + count + "'id = 'operation' required>\n\
            <option selected = 'selected' value = '' >Pre/Post</option>\n\
            <option value = '1'> Pre-Op </option>\n\
<option value = '0'> Post-Op </option>\n\
</select>\n\
</div>";

        selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='day" + count + "' id='day" + count + "' required><option selected='selected' value=''>Day</option>";
        for (var day = 1; day <= 31; day++) {
            if (day == dd) {
                selectday += "<option value='" + day + "' selected = 'selected'>" + day + "</option>";
            } else {
                selectday += "<option value='" + day + "'>" + day + "</option>";
            }
        }
        selectday += "</select>";
        selectday += "</div>";
        var months = ['Month', 'Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        var mondata = "<option selected = 'selected' value = '' >Month</option>";
        for (var m = 1; m < 13; m++) {
            if (m == mm) {
                mondata += "<option selected = 'selected'  value= '" + m + "'>" + months[m] + "</option>";
            } else {
                mondata += "<option value= '" + m + "'>" + months[m] + "</option>";
            }

        }

        selectmonth = "<div class = 'col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'month" + count + "' id = 'month' required>" + mondata + "</select></div>";


        var d = new Date();
        var n = d.getFullYear();
        selectyear = "<div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='year" + count + "' id='year" + count + "' required><option selected='selected' value=''>Year</option>";
        for (var year = 1970; year <= n + 10; year++) {
            if (year == yyyy) {
                selectyear += "<option value='" + year + "' selected>" + year + "</option>";
            } else {
                selectyear += "<option value='" + year + "'>" + year + "</option>";
            }

        }
        selectyear += "</select>";
        selectyear += "</div></div>";

        btndelete = "<div class='col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5'><a href='javascript:deleteDiv(" + count + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";

        newxrayimagediv += selectimg;

        newxrayimagediv += selectop;
        newxrayimagediv += selectday;
        newxrayimagediv += selectmonth;
        newxrayimagediv += selectyear;
        newxrayimagediv += btndelete;
        newxrayimagediv += "</div>";
        $("#xrayimagearea").append(newxrayimagediv);
        $('#userfile' + count).trigger('click');

    }

    function deleteDiv(v) {
        count--;
        $("#xrayimagediv" + v).remove();
    }

    function controlAdvice(val) {

        var check = $("#" + val).prop('checked');
        switch (val) {
            case 'advice1':
                if (check) {
//                    $("#" + val + "input").prop('disabled', false);
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $("#" + val + "input").val('');
                    $("#" + val + "right").prop('checked', false);
                    $("#" + val + "left").prop('checked', false);

//                    $("#" + val + "input").prop('disabled', true);
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice2':
                if (check) {
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $("#" + val + "right").prop('checked', false);
                    $("#" + val + "left").prop('checked', false);
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice3':
                if (check) {
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $("#" + val + "right").prop('checked', false);
                    $("#" + val + "left").prop('checked', false);
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice4':

                break;
            case 'advice5':
                break;
            case 'advice6':
                if (check) {
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $(".cb").prop('checked', false); 
                          $("#" + val + "input").val('');
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice1right':
            case 'advice1left':
                if (check) {
                    $("#advice1").prop('checked', true);

                } else {

                }
                break;
            case 'advice2right':
            case 'advice2left':
                if (check) {
                    $("#advice2").prop('checked', true);

                } else {

                }
                break;
            case 'advice3right':
            case 'advice3left':
                if (check) {
                    $("#advice3").prop('checked', true);
                } else {

                }
                break;
            case 'cmback':
                if (check) {
                    $("#advice6").prop('checked', true);
                } else {
                    $("#advice6").prop('checked', true);
                }
                break;


            default :
        }
    }


</script>

<script src="<?php echo $baseurl; ?>assets/assets/summernote/dist/summernote.min.js"></script>
