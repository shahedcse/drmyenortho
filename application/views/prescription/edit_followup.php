<style type="text/css">
    #testname2{
        height: 10px;
        padding: 5px;
    }
    .ui-menu-item{
        height: auto;
        padding: 25px;
        border: 1px solid blue;
        padding-bottom: 20px;
        margin-bottom: 20px;
        font-size: 12px;
        width: 355px;
    }

    #printtd {
        width: 50%;
        height: auto;
        text-align: left;
        padding: 10px 10px 13px 50px;
        border: hidden;
    }
</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">   

                <section class="panel">
                    <header class="panel-heading ">                  
                        Followup Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  


                        <div class="row" id="followupprint">
                            <div class=" form">
                                <form class="cmxform form-horizontal tasi-form" id="addprescriptiondata" method="POST" action="<?php echo site_url('prescription/Prescription/addFollowupData'); ?>"  enctype="multipart/form-data">

                                    <section class="panel">

                                        <div class="panel-body">

                                            <style>
                                                #printtd {
                                                    width: 50%;
                                                    height: auto;
                                                    text-align: left;
                                                    padding: 5px 10px 10px 50px;
                                                    border: hidden;
                                                }
                                                #bottompaddingrow {
                                                    display: none;
                                                }
                                                @media print
                                                { 
                                                    #followupimagediv {
                                                        display: none !important;
                                                    }
                                                    #presimagediv {
                                                        display: none !important;
                                                    }
                                                    #followupdatadiv {
                                                        display: none !important;
                                                    }
                                                    #bottompaddingrow {
                                                        display: block;
                                                    }
                                                    table {
                                                        font-size: 12px;
                                                    }
                                                    @page { size: portrait; margin: 0; }
                                                }
                                            </style>
                                            <table style="width: 100%;">
                                                <tr id="bottompaddingrow">
                                                    <td style="padding-bottom: 190px;border: hidden;"></td>
                                                    <td style="padding-bottom: 190px;border: hidden;"></td>
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd" style="padding-bottom: 0px"><b>ID : </b><span id="prescriptionIdP"><?php echo $presmasterData->pres_id;
                        ?></span>&nbsp;&nbsp;</td>
                                                    <td id="printtd">Date : <?php echo date('j  F,  Y'); ?></td>
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd" style=" padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="fullnameP"><?php echo $patientlist->first_name . ' ' . $patientlist->last_name ?></span> <span id="birthdayP"><?php echo' , ' . $patientlist->age . ' years'; ?></span></p></td>
                                                    <td id="printtd"></td>
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd" style=" padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="addressP"><?php echo $patientlist->address; ?></span></p></td>
                                                    <td id="printtd"> </td>
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Date Of Followup</b></p> 
                                                        <p style="margin: 0px;" id="inshowP"> <?php
                                                            $databaseDate = $presfollowupData->date;
                                                            $originalDate = explode("-", $databaseDate);
                                                            $yearFollowup = $originalDate[0];
                                                            $monthFollowup = $originalDate[1];

                                                            $dayFollowup = $originalDate[2];
                                                            echo $dayFollowup . "-" . $monthFollowup . "-" . $yearFollowup;
                                                            ?></p>
                                                    </td> 
                                                    <td id="printtd" rowspan="5" style="vertical-align: top">
                                                        <p style="margin-top: 0px;"><b>Rx :</b></p> 
                                                        <div id="medicinedataP"><?php
                                                            if (sizeof($medicationData)):
                                                                foreach ($medicationData as $datarow):
                                                                    echo "<div><p style='margin: 0'>" . '#   ' . $datarow->medicine_name . "</p>" . "<p style='margin: 0 0 8px 15px;'>" . $datarow->procedure . '<br>' . "</p></div>";
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </div>      
                                                    </td> 
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Date Of Injury :</b></p> 
                                                        <p style="margin: 0px;" id="inshowP"><?php
                                                            $injuryDate = $presmasterData->injury_date;
                                                            $arrInjuryDate = explode("-", $injuryDate);
                                                            $year = $arrInjuryDate[0];
                                                            $month = $arrInjuryDate[1];
                                                            $day = $arrInjuryDate[2];
                                                            echo $day . "-" . $month . "-" . $year;
                                                            ?></p>
                                                    </td> 
                                                </tr>
                                                <tr style="border: hidden;">                                      
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Date Of Surgery :</b></p> 
                                                        <p style="margin: 0px;" id="surshowP"><?php
                                                            $injuryDate = $presmasterData->surgery_date;
                                                            $arrSurgeryDate = explode("-", $injuryDate);
                                                            $year = $arrSurgeryDate[0];
                                                            $month = $arrSurgeryDate[1];
                                                            $day = $arrSurgeryDate[2];
                                                            echo $day . "-" . $month . "-" . $year;
                                                            ?></p>
                                                    </td> 
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>History :</b></p> 
                                                        <p style="margin: 0px;" id="historyshowP"><?php echo $presmasterData->history;
                                                            ?></p>
                                                    </td>                                                   
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Additional Information :</b></p> 
                                                        <p style="margin: 0px;" id="additionalinformationshowP"><?php echo $presmasterData->additional_info;
                                                            ?></p>
                                                    </td>

                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>O/E :</b></p> 
                                                        <p style="margin: 0px;" id="oeshowP"><?php
                                                            $nerve_injury = $presmasterData->o_e_nerve;
                                                            $vascular_injury = $presmasterData->o_e_vascular;

                                                            if ($nerve_injury == 1) {
                                                                $nerve_injury_input = "<div>" . "Nerve Injury Presence : " . $presmasterData->o_e_nerve_pre . "</div>";
                                                            } else {
                                                                $nerve_injury_input = "<div>" . "Nerve Injury Presence : No.<br>" . "</div>";
                                                            }


                                                            if ($vascular_injury == 1) {
                                                                $vascular_injury_input = "<div>" . "Vascular Injury Presence : " . $presmasterData->o_e_vascular_pre . "</div>";
                                                            } else {
                                                                $vascular_injury_input = "<div>" . "Vascular Injury Presence : No.<br>" . "</div>";
                                                            }

                                                            $others = "<div>Others : " . $presmasterData->o_e . "</div>";

                                                            echo $nerve_injury_input . $vascular_injury_input . $others;
                                                            ?>  

                                                        </p>
                                                    </td>
                                                    <td id="printtd"></td>

                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Area Of Treatment :</b></p>
                                                        <div style="margin: 0px;" id="surgeryshowP"><?php echo $presmasterData->reference_value; ?></div>
                                                          <div style="margin: 0px;" ><?php echo $presmasterData->ref_others; ?></div>
                                                    </td>
                                                    <td id="printtd"></td>
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Test Advised :</b></p>
                                                        <div style="margin: 0px;" id="medicaltestdatashowP"><?php
                                                            if (sizeof($testData) > 0):
                                                                echo $testData->test_name;
                                                            else: echo '';
                                                            endif;
                                                            ?></div>

                                                    </td>
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Advice :</b></p>
                                                        <p style="margin: 0px;" id="adviceshowP"> <?php
                                                            $alladvice = $presmasterData->advice;
                                                            if ($alladvice != "") {
                                                                $sepadvice = explode("*", $alladvice);
                                                                $len = sizeof($sepadvice);
                                                                for ($i = 1; $i < $len; $i++) {
                                                                    $advicetext = "<div><label> * " . $sepadvice[$i] . " </label></div>";
                                                                    echo $advicetext;
                                                                }
                                                            }
                                                            ?></p> 
                                                    </td>
                                                </tr>
                                                <tr style="border: hidden;">
                                                    <td id="printtd">
                                                        <p style="margin: 0px; padding: 0"><b>Diagnosis :</b></p>
                                                        <div style="margin: 0px;" id="surgeryshowP"><?php echo $presmasterData->reference_value; ?></div>
                                                          <div style="margin: 0px;" ><?php echo $presmasterData->ref_others; ?></div>
                                                        <p style="margin: 0px;" id="diagnosisshowP"><?php echo $presmasterData->diagnosis; ?></p>
                                                    </td>
                                                    <td id="printtd"></td>
                                                </tr>                                        
                                            </table>


                                            <!--                                                follow up data add from here-->
                                            <div id="followupdatadiv">
                                                <hr>
                                                <div class="form-group" id="infectiondiv">
                                                    <label for="infection" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Infection</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->infection == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>
                                                        <?php
                                                        if ($presfollowupData->infection == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?>
                                                    </div>

                                                </div>
                                                <div class="form-group" id="infectionwound" style="display: none">
                                                    <label for="infectionwound" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Incision of the wound?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->incision_wound == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>  
                                                        <?php
                                                        if ($presfollowupData->incision_wound == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 

                                                    </div>
                                                </div>
                                                <div class="form-group" id="infectiondepth" style="display: none">
                                                    <label for="infectiondepth" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Infection Depth</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->infection_depth == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>  
                                                        <?php
                                                        if ($presfollowupData->infection_depth == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 

                                                    </div>
                                                </div>


                                                <div class="form-group" id="infectionduration" style="display: none">
                                                    <label for="infectionduration" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Duration Of Infection</label>
                                                    <div class="col-lg-2 col-md-2  col-sm-2 col-xs-5">
                                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $presfollowupData->infection_duration . " Weeks" . '</label>'; ?>                                         
                                                    </div>                                                                                          
                                                </div>
                                                <div class="form-group" id="infectiontype" style="display: none">                                   
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2"> 

                                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $presfollowupData->infection_type . '</label>'; ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="partialweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Partial weight bearing?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->partial_weight_bear == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>  
                                                        <?php
                                                        if ($presfollowupData->partial_weight_bear == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="painlessweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Painless full weight bearing?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->painless_full_weight_bear == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?> 
                                                        <?php
                                                        if ($presfollowupData->painless_full_weight_bear == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="healingxray" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Healing by x-ray?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->healing_by_xray == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?> 
                                                        <?php
                                                        if ($presfollowupData->healing_by_xray == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="kneeflexion" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Knee flexion greater than 90 degrees?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->knee_flex_greater_90deg == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?> 
                                                        <?php
                                                        if ($presfollowupData->knee_flex_greater_90deg == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="screwbreakage" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Screw breakage?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->screw_breakage == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>  
                                                        <?php
                                                        if ($presfollowupData->screw_breakage == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="screwloosening" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Screw loosening?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->screw_loosen == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>  
                                                        <?php
                                                        if ($presfollowupData->screw_loosen == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nailbreakage" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nail breakage?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->nail_breakage == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?> 
                                                        <?php
                                                        if ($presfollowupData->nail_breakage == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nailloosening" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nail loosening?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->nail_loosen == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>  
                                                        <?php
                                                        if ($presfollowupData->nail_loosen == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 
                                                    </div>
                                                </div>

                                                <div class="form-group" id="deformitydiv">
                                                    <label for="deformity" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Deformity? </label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                                        <?php
                                                        if ($presfollowupData->deformity == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?>  
                                                        <?php
                                                        if ($presfollowupData->deformity == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="alignment" style="display: none">  
                                                    <label for="alignment" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-1" >Alignment?</label>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">                                                                  
                                                        <?php
                                                        if ($presfollowupData->deformity_alignment == "10 varus"):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Over 10 degrees varus" . '</label>';
                                                        endif;
                                                        ?> 
                                                        <?php
                                                        if ($presfollowupData->deformity_alignment == "20 varus"):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Over 20 degrees varus" . '</label>';
                                                        endif;
                                                        ?>


                                                        <?php
                                                        if ($presfollowupData->deformity_alignment == "10 valgus"):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . " Over 10 degrees valgus" . '</label>';
                                                        endif;
                                                        ?>
                                                        <?php
                                                        if ($presfollowupData->deformity_alignment == "20 valgus"):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . " Over 20 degrees valgus" . '</label>';
                                                        endif;
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="rotation" style="display: none">   
                                                    <label for="rotation" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-1" >Rotation?</label>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">                                                                  
                                                        <?php
                                                        if ($presfollowupData->deformity_rotaion == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Over 30 degrees" . '</label>';
                                                        endif;
                                                        ?> 
                                                    </div>
                                                </div>

                                                <div class="form-group" id="repeatsurgerydiv">  
                                                    <label for="repeatsurgery" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Repeat surgery?</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                                                                  
                                                        <?php
                                                        if ($presfollowupData->repeat_surgery == 1):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                                        endif;
                                                        ?> 
                                                        <?php
                                                        if ($presfollowupData->repeat_surgery == 0):
                                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                                        endif;
                                                        ?> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="surgerycause" style="display: none">  

                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2 ">                                                                  
                                                        <?php
                                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $presfollowupData->repeat_surgery_cause . '</label>';
                                                        ?> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="surgerytype" style="display: none">  

                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2 ">                                                                  
                                                        <?php
                                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $presfollowupData->repeat_surgery_type . '</label>';
                                                        ?> 
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label for="comments" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Comments</label>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $presfollowupData->comments . '</label>'; ?>
                                                    </div>
                                                </div>
                                            </div>


                                            <div id="followupimagediv">
                                                <h6 class="panel-heading" style="background: #21BBC7">
                                                    Followup Images
                                                </h6>
                                                <!--                        <div class="form">
                                                                            <form class="form-horizontal table-bordered" id="xRayImageForm" name="xRayImageForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureXrayImage');                                                                      ?>" enctype="multipart/form-data">-->
                                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                                    <div class="col-lg-11 col-lg-offset-1" id="xrayimagearea">

                                                    </div>
                                                </div>
                                            </div>


                                            <div id="presimagediv">
                                                <h6 class="panel-heading" >
                                                    Prescription Images
                                                </h6>                              
                                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                                    <div class="col-lg-11 col-lg-offset-2" id="xrayimageareaprescription">
                                                    </div>
                                                </div>             
                                            </div>


                                        </div>
                                    </section>
                                </form>
                            </div>
                        </div> 
                    </div>
                </section>

                <div id="buttondiv" style="text-align: right; margin: 0 32px 40px 32px;">
                    <button class="btn btn-primary" style="" onclick="Clickheretoprint('#followupprint')"  type="submit">&nbsp;&nbsp;Print Followup&nbsp;&nbsp;</button>
                </div>  

            </div>
        </div>
        <!--        end surgical cases-->                                                          
    </section>              
</div>
</div>
</section>
</section>
<!--main content end-->
<script src="<?php echo $baseurl; ?>assets/assets/fancybox/source/jquery.fancybox.js"></script>
<script>


                        //image code begins here
                        var allimgname = '';
                        var allimgtype = '';
                        var allimgdate = '';
                        var length;
                        var i;
                        var sepimgname, sepimgtype, sepimgdate, sepyearmonthday, yearpres, monthpres, daypres;

                        var imgname = '';
                        var imgtype = '';
                        var imgdate = '';
                        var count = 0;
                        var counttotal = 0;
                        var selectedif, selectedelse, selectedday, selectedmonth, selectedyear, selectedop;

                        var allimgpres = "<?php echo sizeof($presImageData); ?>";

                        if (allimgpres > 0) {
                            allimgname = "<?php echo $presImageData->image_name; ?>";
                            allimgtype = "<?php echo $presImageData->image_type; ?>";
                            allimgdate = "<?php echo $presImageData->date; ?>";

                            sepimgname = allimgname.split(",");
                            sepimgtype = allimgtype.split(",");
                            sepimgdate = allimgdate.split(",");

                            length = '<?php echo substr_count($presImageData->image_name, ",") ?>';

                            if (allimgname != "") {
                                for (i = 0; i <= length; i++) {
                                    imgname = sepimgname[i];
                                    imgtype = sepimgtype[i];
                                    imgdate = sepimgdate[i];
                                    if (imgdate == '') {
                                        yearpres = '';
                                        monthpres = '';
                                        daypres = '';
                                    } else {
                                        sepyearmonthday = imgdate.split("-");
                                        yearpres = sepyearmonthday[0];
                                        monthpres = sepyearmonthday[1];
                                        daypres = sepyearmonthday[2];
                                    }

                                    var loc = "<?php echo $baseurl . "assets/uploads/xray/"; ?>" + imgname;

                                    if (imgtype == 1) {
                                        selectedif = 'pre-op';
                                        selectedelse = '';
                                    } else if (imgtype == '') {
                                        selectedelse = 'selected';
                                        selectedif = '';
                                    } else {
                                        selectedif = 'post-op';
                                    }

                                    //dynamic div start
                                    count++;
                                    counttotal++;
                                    var newxrayimagediv = "";
                                    var selectday = "";
                                    var selectimg = "";
                                    var selectop = "";
                                    var imgnum = "";
                                    var selectmonth = "";
                                    var selectyear = "";
                                    var btndelete = "";

                                    newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + count + "'>";
                                    selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
<a class='fancybox' rel='group' href='" + loc + "'><img src = '" + loc + "' alt = '' /></a>\n\
<input type = 'hidden' name='xrayimgname" + count + "' value ='" + imgname + "'/>\n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
<input type = 'hidden' name='opCount' id='opCount" + count + "' value ='" + count + "'class = 'default'/> </span>\n\
<input type = 'hidden' name='tCount'  id='tCount" + count + "' value ='" + count + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

                                    imgnum = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #" + count + "</span>\n\
</select>\n\
</div>";


                                    selectop = "<div class = 'col-lg-12'>\n\
            <span style='color:white'>Operation  #" + selectedif + "</span>\n\
</div>";

                                    selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-12 space'><span style='color:white'>Date #" + daypres + "-" + monthpres + "-" + yearpres + "</span></div>";
//            selectmonth = "<div class = 'col-lg-2 space'><span style='color:white'>" + monthfrac + "</span></div>";
//            selectyear = "<div class='col-lg-2 space'><span style='color:white'>" + yearfrac + "</span></div>";
                                    selectyear += "</div></div>";

//                            btndelete = "<div class='col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5'><a href='javascript:deleteDiv(" + count + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";

                                    newxrayimagediv += selectimg;
                                    newxrayimagediv += imgnum;
                                    newxrayimagediv += selectop;
                                    newxrayimagediv += selectday;
                                    newxrayimagediv += selectmonth;
                                    newxrayimagediv += selectyear;
//                            newxrayimagediv += btndelete;
                                    newxrayimagediv += "</div>";
                                    $("#xrayimageareaprescription").append(newxrayimagediv);
                                    //dynamic div end
                                    $("#month" + count).val(monthpres);
                                }
                            }

                        }
                        //image code end here

                        //For print
                        function Clickheretoprint(elem) {
                            Popup(jQuery(elem).html());
                        }

                        function Popup(data) {
                            var mywindow = window.open('', '');
                            mywindow.document.write('<html><title></title>');
                            //mywindow.document.write('<link rel="stylesheet" href="<?php //echo $baseurl      ?>assets/css/bootstrap.min.css" type="text/css" />');
                            mywindow.document.write('<head></head><body><style type="text/css">');
                            mywindow.document.write('table{margin-left:12px;margin-right:0px;margin-top:1px;margin-bottom:0px}');
                            mywindow.document.write('table thead, tr, th, table tbody, tr, td { border: none; width:auto}');
                            mywindow.document.write('<style type="text/css">body{ margin-left:100px;margin-right:0px;margin-top:1px;margin-bottom:0px } </style></body>');
                            mywindow.document.write(data);
                            mywindow.document.write('</body></html>');
                            mywindow.document.close();
                            mywindow.print();
                        }

</script>

<link href="<?php echo $baseurl; ?>assets/css/jquery-ui.css" rel="stylesheet">

<script>

    var allimgname = '';
    var allimgtype = '';
    var allimgdate = '';
    var length;
    var i;
    var sepimgname, sepimgtype, sepimgdate, sepyearmonthday, yearfrac, monthfrac, dayfrac;
    var imgtype = '';
    var imgname = '';
    var imgdate = '';
    var countfrac = 0;
    var countfollowup = 0;
    var selectedif, selectedelse, selectedday, selectedmonth, selectedyear;

    var allimgfollowup = "<?php echo sizeof($presimagefollowupData); ?>";

    if (allimgfollowup > 0) {
        allimgname = "<?php echo $presimagefollowupData->image_name; ?>";
        allimgtype = "<?php echo $presimagefollowupData->image_type; ?>";
        allimgdate = "<?php echo $presimagefollowupData->date; ?>";

        sepimgname = allimgname.split(",");
        sepimgtype = allimgtype.split(",");
        sepimgdate = allimgdate.split(",");

        length = '<?php echo substr_count($presimagefollowupData->image_name, ",") ?>';
        if (allimgname != "") {
            for (i = 0; i <= length; i++) {
                imgname = sepimgname[i];
                imgtype = sepimgtype[i];
                imgdate = sepimgdate[i];

                if (imgdate == '') {
                    yearfrac = '';
                    monthfrac = '';
                    dayfrac = '';
                } else {
                    sepyearmonthday = imgdate.split("-");
                    yearfrac = sepyearmonthday[0];
                    monthfrac = sepyearmonthday[1];
                    dayfrac = sepyearmonthday[2];
                }


                var loc = "<?php echo $baseurl . "assets/uploads/xray/"; ?>" + imgname;
                if (imgtype == 1) {
                    selectedif = 'pre-op';
                    selectedelse = '';
                } else if (imgtype == '') {
                    selectedelse = 'selected';
                    selectedif = '';
                } else {
                    selectedif = 'post-op';
                }
                //dynamic div start
                countfollowup++;
                var newxrayimagediv = "";
                var selectday = "";
                var selectimg = "";
                var selectop = "";
                var selectmonth = "";
                var selectyear = "";
                var btndelete = "";

                newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + countfollowup + "'>";
                selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
<a class='fancybox' rel='group' href='" + loc + "'><img src = '" + loc + "' alt = '' /></a>\n\
 <input type = 'hidden' name='xrayimgname" + countfollowup + "' value ='" + imgname + "'/>\n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
<input type = 'hidden' name='opCount' value ='" + countfollowup + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

                imgnumfollowup = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #" + countfollowup + "</span>\n\
</select>\n\
</div>";

                selectop = "<div class = 'col-lg-12'>\n\
            <span style='color:white'>Operation  #" + selectedif + "</span>\n\
</div>";

                selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-12 space'><span style='color:white'>Date #" + dayfrac + "-" + monthfrac + "-" + yearfrac + "</span></div>";
//            selectmonth = "<div class = 'col-lg-2 space'><span style='color:white'>" + monthfrac + "</span></div>";
//            selectyear = "<div class='col-lg-2 space'><span style='color:white'>" + yearfrac + "</span></div>";
                selectyear += "</div>";



                newxrayimagediv += selectimg;
                newxrayimagediv += imgnumfollowup;
                newxrayimagediv += selectop;
                newxrayimagediv += selectday;
                newxrayimagediv += selectmonth;
                newxrayimagediv += selectyear;

                newxrayimagediv += "</div>";
                $("#xrayimagearea").append(newxrayimagediv);
                //dynamic div end
                $("#month" + countfollowup).val(monthfrac);

            }
        }

    }


    //For print
    /*
     function Clickheretoprint(elem) {
     
     Popup(jQuery(elem).html());
     }
     
     function Popup(data) {
     var mywindow = window.open('', '');
     mywindow.document.write('<html><title></title>');
     //mywindow.document.write('<link rel="stylesheet" href="<?php //echo $baseurl      ?>assets/css/bootstrap.min.css" type="text/css" />');
     mywindow.document.write('<head></head><body><style type="text/css">');
     mywindow.document.write('table{margin-left:12px;margin-right:0px;margin-top:1px;margin-bottom:0px}');
     mywindow.document.write('table thead, tr, th, table tbody, tr, td { border: 1px solid #000; width:auto}');
     mywindow.document.write('<style type="text/css">body{ margin-left:100px;margin-right:0px;margin-top:1px;margin-bottom:0px } </style></body>');
     mywindow.document.write(data);
     mywindow.document.write('</body></html>');
     mywindow.document.close();
     mywindow.print();
     } */






////////////////////////////////////



    function hideInfection(val) {
        if (val == "1") {
            $("#infectionwound").show();
            $("#infectiondepth").show();
            $("#infectionduration").show();
            $("#infectiontype").show();
        } else {

            $("#infectionwound").hide();
            $("#infectiondepth").hide();
            $("#infectionduration").hide();
            $("#infectiontype").hide();

        }
    }

    function hideDeformity(val) {
        if (val == "1") {
            $("#alignment").show();
            $("#rotation").show();

        } else {

            $("#alignment").hide();
            $("#rotation").hide();


        }
    }


    function hideRepeatSurgery(val) {
        if (val == "1") {
            $("#surgerycause").show();
            $("#surgerytype").hide();

        } else {
//            document.getElementById('alignment1').checked = false;
//            document.getElementById('alignment2').checked = false;
//            document.getElementById('alignment3').checked = false;
//            document.getElementById('alignment4').checked = false;
//            $('#rotation1').prop('checked', false);
            $("#surgerycause").hide();
            $("#surgerytype").hide();
        }
    }


    function hideRepeatType(val) {
        if (document.getElementById('nonunionrepeat').checked) {
            $("#surgerytype").show();
        } else {
//            document.getElementById('alignment1').checked = false;
//            document.getElementById('alignment2').checked = false;
//            document.getElementById('alignment3').checked = false;
//            document.getElementById('alignment4').checked = false;
//            $('#rotation1').prop('checked', false);        
            $("#surgerytype").hide();
        }
    }

    function hideOther() {
        if (document.getElementById('other').checked) {
            $("#otherval").show();
        } else {
//            document.getElementById('alignment1').checked = false;
//            document.getElementById('alignment2').checked = false;
//            document.getElementById('alignment3').checked = false;
//            document.getElementById('alignment4').checked = false;
//            $('#rotation1').prop('checked', false);        
            $("#otherval").hide();
        }
    }


    $(document).ready(function () {
        var monthval = '<?php echo $monthFollowup; ?>';

        $("#month").val(monthval);
        hideInfection("<?php echo $presfollowupData->infection; ?>");


        hideDeformity("<?php echo $presfollowupData->deformity; ?>");

        hideRepeatSurgery("<?php echo $presfollowupData->repeat_surgery; ?>");

        //Repeat surgery cause

        var allsurgerycause = "<?php echo $presfollowupData->repeat_surgery_cause; ?>";
        var length = "<?php echo substr_count($presfollowupData->repeat_surgery_cause, ",") ?>";
        var sepsurgerycause = allsurgerycause.split(",");
        var surgerycause = '';
        var i;
        for (i = 0; i <= length; i++) {
            surgerycause = sepsurgerycause[i];
            if (surgerycause == "nonunion") {
                $("#surgerytype").show();
            } else {
                $("#surgerytype").hide();
            }

        }

//        hideRepeatType($("#nonunionrepeat").val());

    });

    $(function () {
        //    fancybox
        jQuery(".fancybox").fancybox();
    });
</script>

<script src="<?php echo $baseurl; ?>assets/assets/summernote/dist/summernote.min.js"></script>