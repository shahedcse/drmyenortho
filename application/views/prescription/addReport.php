<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">              
                <section class="panel">
                    <header class="panel-heading">
                        Report List
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Well done !!!  </strong>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oh snap !!!  </strong>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>Date</th>
                                        <th>Report Type</th>                                        
                                        <th>Images</th>
                                        <th>Description</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    if (sizeof($reportList > 0)):
                                        foreach ($reportList as $report):
                                            ?>
                                            <tr class="gradeX">
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $report->date; ?></td>
                                                <td><?php echo $report->report_type; ?></td>
                                                <td>
                                        <figcaption style="padding: 0px; margin: 0px;">
                                            <a class="fancybox" rel="group" href="<?php echo $baseurl . "/assets/uploads/presreport/" . $report->image_name; ?>"><p class="pull thumb p-thumb"><img class="avatar" style="width: 30px; height: 30px; border-radius: 50%" src="<?php echo $baseurl . "/assets/uploads/presreport/" . $report->image_name; ?>" alt=""></p></a>
                                        </figcaption>
                                        </td>
                                        <div class="modal fade" id="fullReportImage-<?php echo $report->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <figure>
                                                    <img style="margin-left: 20px; width: 500px; height: 500px;" src="<?php echo $baseurl . "/assets/uploads/presreport/" . $report->image_name; ?>" alt="">
                                                </figure>
                                            </div>
                                        </div>
                                        <td><?php echo $report->description; ?></td>
                                        <td><a href="#deleteModal-<?php echo $report->id; ?>"data-toggle="modal" ><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i>&nbsp;Delete</button></a></td>
                                        </tr>
                                        <div class="modal fade" id="deleteModal-<?php echo $report->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <form class="cmxform form-horizontal tasi-form" id="signupForm2" method="post" action="<?php echo site_url('prescription/prescription/delete_report'); ?>" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Delete Report</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="panel-body">
                                                                <p>Do You Want to Delete Report Data ???</p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer" >
                                                            <input type="hidden" name="rid" value="<?php echo $report->id; ?>">
                                                            <button class="btn btn-danger" type="submit">Delete</button>
                                                            <button style="margin-right: 40%" data-dismiss="modal" class="btn btn-danger" type="button">Close</button>                                                            
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row" style="padding-bottom: 20px">
            <div class="col-sm-12">              
                <section class="panel">
                    <header class="panel-heading">
                        Add Report
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('prescription/prescription/add_report'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px">
                                    <label for="appointdate" class="control-label col-lg-3">Date</label>
                                    <div class="col-lg-3">
                                        <input class="form-control form-control-inline input-medium default-date-picker" autocomplete="off" data-date-format="dd-mm-yyyy"  size="16" type="text"   id="appointdate" name="appointdate"  value="<?php echo date('d-m-Y'); ?>" required/>
                                    </div>
                                    <label for="appointtime" class="control-label col-md-1">Time</label>
                                    <div class="col-md-3">
                                        <div class="input-group bootstrap-timepicker">
                                            <input type="text" class="form-control timepicker-default" id="appointtime" autocomplete="off" name="appointtime" value="<?php echo date("l jS \of F Y h:i:s A"); ?>" required>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="patientname" class="control-label col-lg-3">Report Type</label>
                                    <div class="col-lg-7">
                                        <select class="form-control" name="report_type">
                                            <option value="">Select Report Type</option>
                                            <?php
                                            if (sizeof($reportType) > 0):
                                                foreach ($reportType as $typereport):
                                                    echo '<option value = "' . $typereport->reporttype . '">' . $typereport->reporttype . '</option>';
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </div>                           
                                <div class="form-group">
                                    <label for="userfile" class="control-label col-lg-3">Image</label>
                                    <div class="col-lg-7">
                                        <input class="form-control" id="picture" name="picture" type="file"  />
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="description" class="control-label col-lg-3">Description</label>
                                        <div class="col-lg-7">
                                            <textarea class="form-control" id="description" name="description"></textarea>
                                            <input type="hidden" name="prescriptionid" value="<?php echo $idAddreport ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-9">
                                        <button class="btn btn-success" type="submit">&nbsp;&nbsp;Save & Continue&nbsp;&nbsp;</button>                               
                                    </div>
                                </div>
                            </form>
                        </div> 
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<!--main content end-->
<script src="<?php echo $baseurl; ?>assets/assets/fancybox/source/jquery.fancybox.js"></script>
<script>

    $(function() {
        $('#appointdate').datepicker({
        });
    });
    $(function() {
        $('#appointtime').timepicker({
        });
    });
    $(function() {
        //    fancybox
        jQuery(".fancybox").fancybox();
    });
</script>
<script>
    $(document).ready(function() {
        $('.img-zoom').hover(function() {
            $(this).addClass('transition');

        }, function() {
            $(this).removeClass('transition');
        });
    });
</script>