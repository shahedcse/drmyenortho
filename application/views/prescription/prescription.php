<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">              
                <section class="panel">
                    <header class="panel-heading">
                        Prescription List
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Well done !!!  </strong>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oh snap !!!  </strong>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <span class="tools pull-right">
                                <a href="<?php echo site_url('prescription/prescription/addprescription'); ?>">
                                    <button class="btn btn-success" >
                                        Add prescription&nbsp;&nbsp;<i class="fa fa-plus"></i>
                                    </button>
                                </a>                        
                            </span>
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Prescription Id</th>
                                        <th>Patient Name</th>                                                                          
                                        <th>Area Of Treatment</th>
                                        <th>Date</th>                                       
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (sizeof($prescriptionList) > 0):
                                        foreach ($prescriptionList as $datarow):
                                            $id = $datarow->id;
                                            ?>
                                            <tr class="gradeX">
                                                <td><a href="<?php                                               
                                                    echo site_url('prescription/Prescription/editPrescription?id=' . $id);
                                                    ?>"><?php echo $datarow->pres_id; ?></a></td>
                                                <td>
                                                   <a href="<?php                                               
                                                    echo site_url('prescription/Prescription/editPrescription?id=' . $id);
                                                    ?>"> <?php
                                                    $Pid = $datarow->patient_id;
                                                    $pationtQry = $this->db->query("SELECT * from user where id = '$Pid'");
                                                    echo $pationtQry->row()->first_name." ".$pationtQry->row()->last_name;
                                                    ?></a>
                                                </td>                                                
                                                
                                                <td><a href="<?php                                               
                                                    echo site_url('prescription/Prescription/editPrescription?id=' . $id);
                                                    ?>"><?php echo $datarow->reference_value; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('prescription/Prescription/editPrescription?id=' . $id);
                                                    ?>"><?php echo date('d-m-Y h:i A', strtotime($datarow->datetime)); ?></a></td>
                                                <td>
                                              
                                                    <a href="#deleteModal-<?php echo $datarow->id; ?>"data-toggle="modal" ><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i>&nbsp;Delete</button></a>
                                                </td>
                                            </tr> 
                                        <div class="modal fade" id="deleteModal-<?php echo $datarow->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <form class="cmxform form-horizontal tasi-form" id="signupForm2" method="post" action="<?php echo site_url('prescription/prescription/delete_prescription'); ?>" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Delete Prescription</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="panel-body">
                                                                <p>Do You Want to Delete Prescription Data ???</p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer" >
                                                            <input type="hidden" name="prescriptionid" value="<?php echo $datarow->id; ?>">
                                                            <button class="btn btn-danger" type="submit">Delete</button>
                                                            <button style="margin-right: 40%" data-dismiss="modal" class="btn btn-danger" type="button">Close</button>                                                            
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="getReport-<?php echo $datarow->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title" style="text-align: center;">Add Report Information</h4>
                                                    </div>
                                                    <div class="modal-body" style="overflow-y: scroll; max-height: 480px;">
                                                        <div class="form">
                                                            <form class="cmxform form-horizontal tasi-form" id="addAppointmentForm" method="POST" action="<?php echo site_url('prescription/prescription/add_report'); ?>">
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <label for="appointdate" class="control-label col-lg-3">Date</label>
                                                                        <div class="col-lg-4">
                                                                            <input class="form-control form-control-inline input-medium default-date-picker" data-date-format="dd-mm-yyyy"  size="16" type="text"   id="date" name="date" onchange="checkAppointmentAdd();"  required/>
                                                                        </div>
                                                                        <label for="appointtime" class="control-label col-md-1">Time</label>
                                                                        <div class="col-md-4">
                                                                            <div class="input-group bootstrap-timepicker">
                                                                                <input type="text" class="form-control timepicker-default" id="time" name="time" onchange="checkAppointmentAdd();" required>
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <label for="patientname" class="control-label col-lg-3">Report Type</label>
                                                                        <div class="col-lg-9">
                                                                            <select class="form-control" name="report_type">
                                                                                <option value="">Select Report Type</option>
                                                                                <option value="xray">Xray</option>
                                                                                <option value="blood_test">Blood Test</option>
                                                                            </select>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <label for="userfile" class="control-label col-lg-3">Image</label>
                                                                        <div class="col-lg-9">
                                                                            <input class="form-control" id="userfile" name="userfile" type="file"  />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <label for="description" class="control-label col-lg-3">Description</label>
                                                                        <div class="col-lg-9">
                                                                            <textarea class="form-control" id="description" name="description"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-lg-offset-3 col-lg-9">
                                                                        <button class="btn btn-success" type="submit"  >&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>  
                                                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>          
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                        
                                                                                                                  
                    </div>
                </section>
            </div>
        </div>
        <section class="page" id="printprescription" hidden>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel-body">
                        <div class="col-lg-12">              
                            <section class="panel">
                                <div style="" class="panel-body" id="">
                                    <style>
                                        #printtd {
                                            width: 50%;
                                            height: auto;
                                            text-align: left;
                                            padding: 10px 10px 13px 50px;
                                            border: hidden;
                                        }
                                    </style>
                                    <table style="border: hidden; width: 100%; border: 1px solid red; padding: 0px; font-size: 13px;">
                                        <tr style="border: hidden;">
                                            <td style="padding-bottom: 50px;border: hidden;"></td>
                                            <td style="padding-bottom: 50px;border: hidden;"></td>
                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd" style="padding-bottom: 0px"><b>ID : </b><span id="prescriptionIdP"></span>&nbsp;&nbsp;<b> Visit No: </b><span id="visitcountP"></span></td>
                                            <td id="printtd">Date : <?php echo date('j  F,  Y'); ?></td>
                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd" style=" padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="fullnameP"></span> <span id="birthdayP"></span></p></td>
                                            <td id="printtd"></td>
                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd" style=" padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="addressP"></span></p></td>
                                            <td id="printtd"> </td>
                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd">
                                                <p style="margin-top: 0px;"><b>History :</b></p> 
                                                <p style="margin: 0px;" id="historyshowP"></p>
                                            </td>
                                            <td id="printtd" rowspan="5" style="vertical-align: top">
                                                <p style="margin-top: 0px;"><b>Rx :</b></p> 
                                                <div id="medicinedataP"></div>      
                                            </td>
                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd">
                                                <p style="margin-top: 0px;"><b>Additional Information :</b></p> 
                                                <p style="margin: 0px;" id="additionalinformationshowP"></p>
                                            </td>

                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd">
                                                <p style="margin-top: 0px;"><b>O/E :</b></p> 
                                                <p style="margin: 0px;" id="oeshowP"></p>
                                            </td>

                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd">
                                                <p style="margin-top: 0px;"><b>Surgery Reference :</b></p>
                                                <div style="margin: 0px;" id="surgeryshowP"></div>
                                            </td>
                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd">
                                                <p style="margin-top: 0px;"><b>Test Advised :</b></p>
                                                <div style="margin: 0px;" id="medicaltestdatashowP"></div>
                                            </td>
                                        </tr>
                                        <tr style="border: hidden;">
                                            <td id="printtd">
                                                <p style="margin-top: 0px;"><b>Diagnosis :</b></p>
                                                <p style="margin: 0px;" id="diagnosisshowP"></p>
                                            </td>
                                            <td id="printtd">
                                                <p style="margin-top: 0px;"><b>Advice :</b></p>
                                                <p style="margin: 0px;" id="adviceshowP"></p> 
                                            </td>
                                        </tr>                                        
                                    </table>                                    
                                </div>                                
                            </section>                                                                        
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="buttondiv" style="text-align: right; margin: 0 32px 40px 32px; border: 1px solid #23527c" hidden>
            <button class="btn btn-primary" style="" onclick="Clickheretoprint('#printprescription')"  type="submit">&nbsp;&nbsp;Print Prescription&nbsp;&nbsp;</button>
        </div>
    </section>
</section>

<!--main content end-->
<script>
    
  
    function getPriscription(id) {
        $("#printprescription").show();
        $("#buttondiv").show();
        $("#medicaltestdatashowP").text("");
        $("#medicinedataP").text("");
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/get_prescription'); ?>",
            data: 'id=' + id,
            success: function (data) {
                var response_json_data = JSON.parse(data);

                var presmasterdata = response_json_data.presmasterdata;
                var userdata = response_json_data.userdata;
                var patientCountdata = response_json_data.patientCountdata;

                var presMedicationData = response_json_data.presMedicationData;
                var testadvice = response_json_data.testadvice;

                $("#prescriptionIdP").text(presmasterdata.pres_id);

                $("#fullnameP").text(userdata.first_name + ' ' + userdata.last_name);
                $("#birthdayP").text(' , ' + userdata.age + ' years');
                $("#addressP").text(userdata.address);

                $("#visitcountP").text(patientCountdata.visitcount);

                $("#historyshowP").text(presmasterdata.history);
                $("#additionalinformationshowP").text(presmasterdata.additional_info);
                $("#oeshowP").text(presmasterdata.o_e);
                $("#surgeryshowP").text(presmasterdata.reference_value);
                $("#diagnosisshowP").text(presmasterdata.diagnosis);
                $("#adviceshowP").text(presmasterdata.advice);

                $("#medicinedataP").append(presMedicationData);

                $("#medicaltestdatashowP").append(testadvice);

            }
        });
    }

    //For print
    function Clickheretoprint(elem) {
        Popup(jQuery(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', '');
        mywindow.document.write('<html><title></title>');
        mywindow.document.write('<link rel="stylesheet" href="<?php echo $baseurl ?>assets/css/bootstrap.min.css" type="text/css" />');
        mywindow.document.write('<head></head><body><style type="text/css">');
        mywindow.document.write('table{margin-left:12px;margin-right:0px;margin-top:1px;margin-bottom:0px}');
        mywindow.document.write('table thead, tr, th, table tbody, tr, td { border: 1px solid #000; width:auto}');
        mywindow.document.write('<style type="text/css">body{ margin-left:100px;margin-right:0px;margin-top:1px;margin-bottom:0px } </style></body>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.document.close();
        mywindow.print();
    }
</script>
<script type="text/javascript">
    /*   var today_date = "<?php //echo date("Y-m-d");       ?>";
     $('#appointdate').datetimepicker({
     dayOfWeekStart: 1,
     lang: 'en',
     disabledDates: ['1986-01-08', '1986-01-09', '1986-01-10'],
     startDate: today_date,
     timepicker: true
     }); */

    $(function () {
        $('#appointdate').datepicker({
        });
    });
    $(function () {
        $('#appointtime').timepicker({
        });
    });
    $(function () {
        $('#appointDateEdit').datepicker({
        });
    });
    $(function () {
        $('#appointTimeEdit').timepicker({
        });
    });
    
    
    
    
    


</script>