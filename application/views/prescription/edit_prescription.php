<style>
    a:link {
        text-decoration: underline;
    }

    a:visited {
        text-decoration: underline;
    }

    a:hover {
        text-decoration: underline;
    }

    a:active {
        text-decoration: underline;
    }

</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Prescription Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->                    
                        <div class="form">
                            <form class="form-horizontal table-bordered"  id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">                                
                                <div class="form-group" style="margin: 0;padding: 0">
                                    <label for="address" class="control-label col-lg-3 " style="font-weight: bold;color: #167F52;text-align: left"><h5>Patient Information</h5></label>
                                </div>
                                <hr/>
                                <div class="form-group" style="margin: 0;padding: 0">
                                    <label for="address" class="control-label col-lg-3 " style="font-weight: bold;color: #167F52;text-align: left">Patient Name :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->first_name . " " . $patientlist->last_name; ?></span></label>
                                    <label for="address" class="control-label col-lg-3 " style="font-weight: bold;color: #167F52;text-align: left">Address :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->address ?></span></label>
                                    <label for="address" class="control-label col-lg-3 " style="font-weight: bold;color: #167F52;text-align: left">Gender :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->sex ?></span></label>
                                    <label for="address" class="control-label col-lg-3 " style="font-weight: bold;color: #167F52;text-align: left">Phone :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->mobile ?></span></label>
                                    <label for="address" class="control-label col-lg-3 " style="font-weight: bold;color: #167F52;text-align: left">Email :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->email ?></span></label>
                                </div> 
                                <hr/>
                            </form>
                        </div>  
                        <br>
                        <div class="form">
                            <form class="form-horizontal table-bordered"  id="addPatientForm" method="POST" action="#" enctype="multipart/form-data">
                                <div id="allprescription">
                                    <?php
                                    if (sizeof($presmasterData)):
                                        foreach ($presmasterData as $row):
                                            $id = $row->id;
                                            ?>
                                            <div class="form-group" style="margin: 0;padding: 0">
                                                <label for="allprescription" class="control-label col-lg-3" style="font-weight: bold;text-align: left">Prescription :&nbsp;&nbsp;<a href="#" onclick="getPriscription(<?php echo $id ?>)" style="color: #000">
                                                        <?php echo date('d-m-Y h:i A', strtotime($row->datetime)); ?> </a></label>
                                            </div> 

                                            <?php
                                            if (sizeof($followupPresData)):
                                                foreach ($followupPresData as $row):
//                                                    $followup_id = $row->id;
                                                    $id_followup = $row->id;
                                                    $queryFollowupData = $this->db->query("SELECT * FROM pres_followup WHERE pres_master_id='$id_followup'");
                                                    if (sizeof($queryFollowupData->result())):
                                                        foreach ($queryFollowupData->result() as $row):
                                                            $followup_id = $row->id;
                                                            ?>
                                                            <div class="form-group" style="margin: 0;padding: 0">
                                                                <label for="allfollowup" class="control-label col-lg-3 col-lg-offset-2" style="font-weight: bold;text-align: left">Followup <?php echo $row->followup_id; ?> :&nbsp;&nbsp;<a href="<?php echo site_url('prescription/Prescription/editFollowup?followup_id=' . $followup_id . '&pres_id=' . $id_followup . '&pres_under_id=' . $id); ?>" onclick="#" style="color: #000">
                                                                        <?php echo $row->date; ?> </a></label>
                                                            </div>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                            <div class="form-group"  style="margin: 0;padding: 0" >
                                                <label for="addfollowup" class="control-label col-lg-3 col-lg-offset-2" style="text-align: left"  > <a href='<?php
                                                    echo site_url('prescription/Prescription/addFollowup/?id=' . $id)
                                                    ?>' style="color: #000">+Add Followup&nbsp;&nbsp;</a></label>
                                            </div> 
                                            <?php
                                        endforeach;
                                        ?>
                                        <?php
                                    endif;
                                    ?>
                                </div>                                     
                            </form>
                        </div>
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>


        <section class="page" id="printprescription" hidden>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel-body">
                        <div class="col-lg-12">              
                            <section class="panel">
                                <div style="" class="panel-body">
                                    <style>
                                        #printtd {
                                            width: 50%;
                                            height: auto;
                                            text-align: left;
                                            padding: 5px 10px 10px 50px;
                                            border: hidden;
                                        }
                                        #bottompaddingrow {
                                            display: none;
                                        }
                                        @media print {
                                            #panelheading {
                                                display: none !important;
                                            }
                                            #img {
                                                display: none !important;
                                            }
                                            #bottompaddingrow {
                                                display: block;
                                            }
                                            table {
                                                font-size: 12px;
                                            }
                                            @page { size: portrait; margin: 0; }
                                        }
                                    </style>
                                    <table style="width: 100%;">
                                        <tr id="bottompaddingrow">
                                            <td style="padding-bottom: 190px;"></td>
                                            <td style="padding-bottom: 190px;"></td>
                                        </tr>
                                        <tr>
                                            <td id="printtd" style="padding-bottom: 0px"><b>ID : </b><span id="prescriptionIdP"></span></td>
                                            <td id="printtd">Date : <span id="prescriptionDate"></span></td>
                                        </tr>
                                        <tr>
                                            <td id="printtd" style="padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="fullnameP"></span> <span id="birthdayP"></span></p></td>
                                            <td id="printtd"></td>
                                        </tr>
                                        <tr>
                                            <td id="printtd" style="padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="addressP"></span></p></td>
                                            <td id="printtd"> </td>
                                        </tr>
                                        <tr>
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>Date Of Injury :</b></p> 
                                                <p style="margin: 0px;" id="inshowP"></p>
                                            </td> 
                                            <td id="printtd" rowspan="5" style="vertical-align: top">
                                                <p style="margin-top: 0px;"><b>Rx :</b></p> 
                                                <div id="medicinedataP"></div>      
                                            </td>
                                        </tr>
                                        <tr>                                      
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>Date Of Surgery :</b></p> 
                                                <p style="margin: 0px;" id="surshowP"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>History :</b></p> 
                                                <p style="margin: 0px;" id="historyshowP"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>Additional Information :</b></p> 
                                                <p style="margin: 0px;" id="additionalinformationshowP"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>O/E :</b></p> 
                                                <p style="margin: 0px;" id="oeshowP"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>Area Of treatment :</b></p>
                                                <div style="margin: 0px;" id="surgeryshowP"></div>
                                                <div style="margin: 0px;" id="othershowP"></div>
                                            </td> 
                                            <td id="printtd"> </td>
                                        </tr>
                                        <tr>
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>Test Advised :</b></p>
                                                <div style="margin: 0px;" id="medicaltestdatashowP"></div>
                                            </td>
                                            <td id="printtd"> </td>

                                        </tr>
                                        <tr>
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>Diagnosis :</b></p>
                                                <p style="margin: 0px;" id="diagnosisshowP"></p>
                                            </td>   
                                            <td id="printtd">
                                                <p style="margin: 0px; padding: 0"><b>Advice :</b></p>
                                                <p style="margin: 0px;" id="adviceshowP"></p> 
                                            </td>
                                        </tr>     
                                    </table>
                                    <h6 class="panel-heading"  id="panelheading">
                                        Prescription Images
                                    </h6>                              
                                    <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                        <div class="col-lg-11 col-lg-offset-2" id="xrayimageareaprescription">
                                        </div>
                                    </div> 
                                </div>                                
                            </section>                                                                        
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <div id="buttondiv" style="text-align: right; margin: 0 32px 40px 32px;" hidden>
            <button class="btn btn-primary" style="" onclick="Clickheretoprint('#printprescription')"  type="submit">&nbsp;&nbsp;Print Prescription&nbsp;&nbsp;</button>
        </div>   

    </section>
</section>
<!--main content end-->
<script src="<?php echo $baseurl; ?>assets/assets/fancybox/source/jquery.fancybox.js"></script>
<script>

                var countAddBtn = 0;
                function getPriscription(id) {
                    countAddBtn++;
                    $("#printprescription").show();
                    $("#buttondiv").show();
                    $("#medicaltestdatashowP").text("");
                    $("#medicinedataP").text("");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('prescription/prescription/get_prescription'); ?>",
                        data: 'id=' + id,
                        success: function (data) {
                            var response_json_data = JSON.parse(data);
                            var presmasterdata = response_json_data.presmasterdata;
                            var userdata = response_json_data.userdata;
                            var patientCountdata = response_json_data.patientCountdata;
                            var presMedicationData = response_json_data.presMedicationData;
                            var testadvice = response_json_data.testadvice;
                            var presImageData = response_json_data.presImageData;

                            $("#prescriptionIdP").text(presmasterdata.pres_id);                        
                            $("#prescriptionDate").text(formatAMPM(presmasterdata.datetime));
                            $("#fullnameP").text(userdata.first_name + ' ' + userdata.last_name);
                            $("#birthdayP").text(' , ' + userdata.age + ' years');
                            $("#addressP").text(userdata.address);
                            $("#historyshowP").text(presmasterdata.history);
                            $("#additionalinformationshowP").text(presmasterdata.additional_info);

                            var nerve_injury = presmasterdata.o_e_nerve;
                            var vascular_injury = presmasterdata.o_e_vascular;

                            if (nerve_injury == 1) {
                                var nerve_injury_input = "<div>" + "Nerve Injury Presence : " + presmasterdata.o_e_nerve_pre + "</div>";
                            } else {
                                var nerve_injury_input = "<div>" + "Nerve Injury Presence : No.<br>" + "</div>";
                            }


                            if (vascular_injury == 1) {
                                var vascular_injury_input = "<div>" + "Vascular Injury Presence : " + presmasterdata.o_e_vascular_pre + "</div>";
                            } else {
                                var vascular_injury_input = "<div>" + "Vascular Injury Presence : No.<br>" + "</div>";
                            }

                            var others = "<div>Others : " + presmasterdata.o_e + "</div>";

                            $('#oeshowP').html(nerve_injury_input + vascular_injury_input + others);




                            var injuryDate = presmasterdata.injury_date;
                            var arrInjuryDate = injuryDate.split("-");
                            var year = arrInjuryDate[0];
                            var month = arrInjuryDate[1];
                            var day = arrInjuryDate[2];
                            var formattedInjuryDate = day + "-" + month + "-" + year;

                            var surgeryDate = presmasterdata.surgery_date;
                            var arrSurgeryDate = surgeryDate.split("-");
                            var year = arrSurgeryDate[0];
                            var month = arrSurgeryDate[1];
                            var day = arrSurgeryDate[2];
                            var formattedSurgeryDate = day + "-" + month + "-" + year;

                            $("#inshowP").text(formattedInjuryDate);
                            $("#surshowP").text(formattedSurgeryDate);

                            $("#surgeryshowP").text(presmasterdata.reference_value);
                            $("#othershowP").text(presmasterdata.ref_others);

                            $("#diagnosisshowP").html(presmasterdata.reference_value + "<br>" + presmasterdata.diagnosis);
                            var alladvice = presmasterdata.advice;

                            if (countAddBtn < 2) {
                                if (alladvice != "") {
                                    var sepadvice = alladvice.split("*");
                                    var len = sepadvice.length;
                                    for (var i = 1; i < len; i++) {
                                        var advicetext = "<div><label> * " + sepadvice[i] + " </label></div>";
                                        $("#adviceshowP").append(advicetext);
                                    }
                                }

                            }

                            $("#medicinedataP").append(presMedicationData);
                            $("#medicaltestdatashowP").append(testadvice);

                            //image code begins here
                            var allimgname = '';
                            var allimgtype = '';
                            var allimgdate = '';
                            var length;
                            var i;
                            var sepimgname, sepimgtype, sepimgdate, sepyearmonthday, yearpres, monthpres, daypres;

                            var imgname = '';
                            var imgtype = '';
                            var imgdate = '';
                            var count = 0;
                            var counttotal = 0;
                            var selectedif, selectedelse, selectedday, selectedmonth, selectedyear, selectedop;

                            var allimgpres = Object.keys(presImageData).length;

                            if (allimgpres > 0 && countAddBtn < 2) {
                                allimgname = presImageData.image_name;
                                allimgtype = presImageData.image_type;
                                allimgdate = presImageData.date;

                                sepimgname = allimgname.split(",");
                                sepimgtype = allimgtype.split(",");
                                sepimgdate = allimgdate.split(",");

                                var data = presImageData.image_name;
                                var regex = /,/igm;
                                var countimagename = data.match(regex);
                                length = (countimagename) ? countimagename.length : 0;





                                if (allimgname != "") {
                                    for (i = 0; i <= length; i++) {
                                        imgname = sepimgname[i];
                                        imgtype = sepimgtype[i];
                                        imgdate = sepimgdate[i];

                                        sepyearmonthday = imgdate.split("-");
                                        yearpres = sepyearmonthday[0];
                                        monthpres = sepyearmonthday[1];
                                        daypres = sepyearmonthday[2];
                                        var loc = "<?php echo $baseurl . "assets/uploads/xray/"; ?>" + imgname;

                                        if (imgtype == 1) {
                                            selectedop = '';
                                            selectedif = 'selected';
                                            selectedelse = '';
                                        } else if (imgtype == '') {
                                            selectedop = 'selected';
                                            selectedif = '';
                                            selectedelse = '';
                                        } else {
                                            selectedop = '';
                                            selectedelse = 'selected';
                                            selectedif = '';
                                        }

                                        //dynamic div start
                                        count++;
                                        counttotal++;
                                        var newxrayimagediv = "";
                                        var selectday = "";
                                        var selectimg = "";
                                        var selectop = "";
                                        var imgnum = "";
                                        var selectmonth = "";
                                        var selectyear = "";
                                        var btndelete = "";

                                        newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + count + "'>";
                                        selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
<a class='fancybox' rel='group' href='" + loc + "'><img src = '" + loc + "' alt = '' /></a>\n\
<input type = 'hidden' name='xrayimgname" + count + "' value ='" + imgname + "'/>\n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
<input type = 'hidden' name='opCount' id='opCount" + count + "' value ='" + count + "'class = 'default'/> </span>\n\
<input type = 'hidden' name='tCount'  id='tCount" + count + "' value ='" + count + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

                                        imgnum = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #" + count + "</span>\n\
</select>\n\
</div>";

                                        selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = 'operation" + count + "'id = 'operation' required>\n\
            <option selected = 'selected' value = '' " + selectedop + " >Pre/Post</option>\n\
            <option value = '1'" + selectedif + "> Pre-Op </option>\n\
<option value = '0' " + selectedelse + "> Post-Op </option>\n\
</select>\n\
</div>";

                                        selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4 col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='day" + count + "' id='day" + count + "' required><option selected='selected' value=''>Day</option>";
                                        for (var day = 1; day <= 31; day++) {
                                            if (day == daypres) {
                                                selectedday = 'selected';
                                                selectday += "<option value='" + day + "'" + selectedday + ">" + day + "</option>";
                                            } else {
                                                selectday += "<option value='" + day + "'>" + day + "</option>";
                                            }
                                        }
                                        selectday += "</select>";
                                        selectday += "</div>";

                                        selectmonth = "<div class = 'col-lg-4 col-md-4 col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'month" + count + "' id = 'month" + count + "' required>\n\
                                                                <option selected = 'selected' value = '' >Month</option>\n\
                                                                <option value = '1'> January </option>\n\
                                                                <option value = '2'> February </option>\n\
                                                                <option value = '3'> March </option>\n\
                                                                <option value = '4'> April </option>\n\
                                                                <option value = '5'> May </option>\n\
                                                                <option value = '6'> June </option>\n\
                                                                <option value = '7'> July </option>\n\
                                                                <option value = '8'> August </option>\n\
                                                                <option value = '9'> September </option>\n\
                                                                <option value = '10'> October </option>\n\
                                                                <option value = '11'> November </option>\n\
                                                                <option value = '12'> December </option>\n\
                                                                </select>\n\
                                                                </div>";


                                        var d = new Date();
                                        var n = d.getFullYear();
                                        selectyear = "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='year" + count + "' id='year" + count + "' required><option selected='selected' value=''>Year</option>";
                                        for (var year = 1970; year <= n + 10; year++) {
                                            if (year == yearpres) {
                                                selectedyear = 'selected';
                                                selectyear += "<option value='" + year + "' " + selectedyear + ">" + year + "</option>";
                                            } else {
                                                selectyear += "<option value='" + year + "'>" + year + "</option>";
                                            }
                                        }
                                        selectyear += "</select>";
                                        selectyear += "</div></div>";

//                            btndelete = "<div class='col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5'><a href='javascript:deleteDiv(" + count + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";

                                        newxrayimagediv += selectimg;
                                        newxrayimagediv += imgnum;
                                        newxrayimagediv += selectop;
                                        newxrayimagediv += selectday;
                                        newxrayimagediv += selectmonth;
                                        newxrayimagediv += selectyear;
//                            newxrayimagediv += btndelete;
                                        newxrayimagediv += "</div>";
                                        $("#xrayimageareaprescription").append(newxrayimagediv);
                                        //dynamic div end
                                        $("#month" + count).val(monthpres);
                                    }
                                }

                            }
                            //image code end here


                        }
                    }
                    );
                }






                //For print
                function Clickheretoprint(elem) {

                    Popup(jQuery(elem).html());
                }

                function Popup(data) {
                    var mywindow = window.open('', '');
                    mywindow.document.write('<html><title></title>');
                    //mywindow.document.write('<link rel="stylesheet" href="<?php //echo $baseurl      ?>assets/css/bootstrap.min.css" type="text/css" />');
                    mywindow.document.write('<head></head><body><style type="text/css">');
                    mywindow.document.write('table{margin-left:12px;margin-right:0px;margin-top:1px;margin-bottom:0px}');
                    mywindow.document.write('table thead, tr, th, table tbody, tr, td { border: none; width:auto}');
                    mywindow.document.write('<style type="text/css">body{ margin-left:100px;margin-right:0px;margin-top:1px;margin-bottom:0px } </style></body>');
                    mywindow.document.write(data);
                    mywindow.document.write('</body></html>');
                    mywindow.document.close();
                    mywindow.print();
                }



                $(function () {
                    //    fancybox
                    jQuery(".fancybox").fancybox();
                });

                function formatAMPM(date) {
                    var t = date.split(/[- :]/);
                    var newdate = new Date(t[0], t[1], t[2], t[3], t[4], t[5]);
                    var hours = newdate.getHours();
                    var minutes = newdate.getMinutes();
                    var ampm = hours >= 12 ? 'PM' : 'AM';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0' + minutes : minutes;
                    var dateval = newdate.getDate();
                    var monval = newdate.getMonth();
                    if (dateval < 10) {
                        dateval = "0" + dateval;
                    }
                    if (monval < 10) {
                        monval = "0" + monval;
                    }
                    if (hours < 10) {
                        hours = "0" + hours;
                    }
                    var strTime = dateval + "-" + monval + "-" + newdate.getFullYear() + " " + hours + ':' + minutes + ' ' + ampm;
                    return strTime;
                }
</script>



