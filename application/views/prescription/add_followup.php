
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Enter Followup Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  


                        <div class="row">
                            <div class=" form">
                                <form class="cmxform form-horizontal tasi-form" id="addprescriptiondata" method="POST" action="<?php echo site_url('prescription/Prescription/addFollowupData'); ?>"  enctype="multipart/form-data">
                                    <div class="col-lg-6" >              
                                        <section class="panel">
                                            <header class="panel-heading">
                                                Add Prescription
                                            </header>
                                            <div class="panel-body">
                                                <?php
                                                if ($this->session->userdata('successfull')):
                                                    echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                                                    $this->session->unset_userdata('successfull');
                                                endif;
                                                if ($this->session->userdata('failed')):
                                                    echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                                                    $this->session->unset_userdata('failed');
                                                endif;
                                                ?>                            

                                                <div class="form-group">
                                                    <label for="birthday" class="control-label col-lg-3" ><b>Date Of Followup</b></label>
                                                    <div class="col-lg-8">

                                                        <input type="hidden" name="idPrescription" value="<?php echo $idPrescription; ?>" >
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <select class="form-control m-bot15" name="day" id="day" onchange="setFollowup();" >
                                                                <option selected="selected" value="">Day</option>
                                                                <?php
                                                                $currentDate = date('Y-m-d');
                                                                $temDate = explode("-", $currentDate);
                                                                $cYear = $temDate[0];
                                                                $cMonth = $temDate[1];
                                                                $cDay = $temDate[2];
                                                                for ($day = 1; $day <= 31; $day++):
                                                                    if ($day == $cDay):
                                                                        ?>
                                                                        <option value="<?php echo $day; ?>" selected="selected"><?php echo $day; ?></option>
                                                                        <?php
                                                                    else:
                                                                        ?>
                                                                        <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                                    <?php
                                                                    endif;
                                                                    ?>

                                                                    <?php
                                                                endfor;
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <select class="form-control m-bot15" name="month" id="month"  onchange="setFollowup();" >
                                                                <option selected="selected" value="">Month</option>
                                                                <option value="01" <?php if ($cMonth == "01") echo 'selected'; ?>>January</option>
                                                                <option value="02" <?php if ($cMonth == "02") echo 'selected'; ?>>February</option>
                                                                <option value="03" <?php if ($cMonth == "03") echo 'selected'; ?>>March</option>
                                                                <option value="04" <?php if ($cMonth == "04") echo 'selected'; ?>>April</option>
                                                                <option value="05" <?php if ($cMonth == "05") echo 'selected'; ?>>May</option>
                                                                <option value="06" <?php if ($cMonth == "06") echo 'selected'; ?>>June</option>
                                                                <option value="07" <?php if ($cMonth == "07") echo 'selected'; ?>>July</option>
                                                                <option value="08" <?php if ($cMonth == "08") echo 'selected'; ?>>August</option>
                                                                <option value="09" <?php if ($cMonth == "09") echo 'selected'; ?>>September</option>
                                                                <option value="10" <?php if ($cMonth == "10") echo 'selected'; ?>>October</option>
                                                                <option value="11" <?php if ($cMonth == "11") echo 'selected'; ?>>November</option>
                                                                <option value="12" <?php if ($cMonth == "12") echo 'selected'; ?>>December</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <select class="form-control m-bot15" name="year" id="year"  onchange="setFollowup();" >
                                                                <option selected="selected" value="">Year</option>
                                                                <?php
                                                                $currentyr = date('Y');
                                                                for ($year = 1970; $year <= $currentyr + 15; $year++):
                                                                    if ($year == $cYear):
                                                                        ?>
                                                                        <option value="<?php echo $year; ?>" selected="selected"><?php echo $year; ?></option>
                                                                        <?php
                                                                    else:
                                                                        ?>
                                                                        <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                                    <?php
                                                                    endif;
                                                                    ?>
                                                                    <?php
                                                                endfor;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"><b>Medical Test:</b></label>
                                                    <div class="col-md-8">
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control" name="medicaltestname" id="testname" placeholder="Type Test Name">
                <!--                                            <select class="form-control m-bot15 selectpicker" name="medicaltestname" id="testname" data-live-search="true">
                                                                <option value=""> --Select Medical Test-- </option> 
                                                            <?php
                                                            if (sizeof($testlist) > 0):
                                                                foreach ($testlist as $datarow):
                                                                    echo '<option value="' . $datarow->test_name . '">' . $datarow->test_name . '</option> ';
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                            </select>-->
                                                            <input type="hidden" name="testcountdata" id="testcountdata">
                                                            <span id="addmedicinetestdescription"></span>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-primary" id="addtest" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="diagnosis" class="control-label col-lg-3"><b>Diagnosis:</b></label>
                                                    <div class="col-lg-8">
                                                        <textarea class="form-control" name="diagnosis" id="diagnosis" rows="5" onkeyup="diagnosisvalue()"></textarea>
                                                    </div>
                                                </div> 
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"><b>Medicine:</b></label>
                                                    <div class="col-md-8">
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control" name="medicinename" id="medicinename" onblur="medicineDetails(this.value)" placeholder="Type Medicine Name">
                                                            <span id="addmedicinedescription"></span>
                                                            <input type="hidden" name="medicinetabval" id="medicinetabval" value="0">
                                                            <input type="hidden" name="medicinecountdata" id="medicinecountdata">
                                                            <p> &nbsp; </p>
                                                            <textarea class="form-control" name="medicinefeedinst" id="medicinefeedinst" placeholder="Type medicine feed instruction here.." rows="4"></textarea>
                                                            <span id="medicineinstreq"></span>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-primary" id="addmedicine" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="advice" class="control-label col-lg-3"><b>Advice:</b></label>
                                                    <div class="col-md-8">
                                                        <div><span id="span1"><input type="checkbox" id="advice1" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>১.</label>&nbsp;&nbsp;<select class="input-small" name="advice1input"  id="advice1input">
                                                                    <option value="">Select</option>                                                                                                                                                                                                 
                                                                    <option value="১">১</option>
                                                                    <option value="২">২</option>
                                                                    <option value="৩">৩</option>
                                                                    <option value="৪">৪</option>
                                                                    <option value="৫">৫</option>
                                                                    <option value="৬">৬</option>
                                                                    <option value="৭">৭</option>
                                                                    <option value="৮">৮</option>
                                                                    <option value="৯">৯</option>
                                                                    <option value="১০">১০</option>                                                                                                                                                                                                 
                                                                    <option value="১১">১১</option>
                                                                    <option value="১২">১২</option>
                                                                    <option value="১৩">১৩</option>
                                                                    <option value="১৪">১৪</option>
                                                                    <option value="১৫">১৫</option>
                                                                    <option value="১৬">১৬</option>
                                                                    <option value="১৭">১৭</option>
                                                                    <option value="১৮">১৮</option>
                                                                    <option value="১৯">১৯</option>
                                                                    <option value="২০">২০</option> 
                                                                    <option value="২১">২১</option>
                                                                    <option value="২২">২২</option>
                                                                    <option value="২৩">২৩</option>
                                                                    <option value="২৪">২৪</option>
                                                                    <option value="২৫">২৫</option>
                                                                    <option value="২৬">২৬</option>
                                                                    <option value="২৭">২৭</option>
                                                                    <option value="২৮">২৮</option>
                                                                    <option value="২৯">২৯</option>
                                                                    <option value="৩০">৩০</option>  
                                                                    <option value="৩১">৩১</option>
                                                                    <option value="৩২">৩২</option>
                                                                    <option value="৩৩">৩৩</option>
                                                                    <option value="৩৪">৩৪</option>
                                                                    <option value="৩৫">৩৫</option>
                                                                    <option value="৩৬">৩৬</option>
                                                                    <option value="৩৭">৩৭</option>
                                                                    <option value="৩৮">৩৮</option>
                                                                    <option value="৩৯">৩৯</option>
                                                                    <option value="৪০">৪০</option>                                                                                                                                                                                                 
                                                                    <option value="৪১">৪১</option>
                                                                    <option value="৪২">৪২</option>
                                                                    <option value="৪৩">৪৩</option>
                                                                    <option value="৪৪">৪৪</option>
                                                                    <option value="৪৫">৪৫</option>
                                                                    <option value="৪৬">৪৬</option>
                                                                    <option value="৪৭">৪৭</option>
                                                                    <option value="৪৮">৪৮</option>
                                                                    <option value="৪৯">৪৯</option>
                                                                    <option value="৫০">৫০</option>                                                                                                                                                                                                 
                                                                    <option value="৫১">৫১</option>
                                                                    <option value="৫২">৫২</option>
                                                                    <option value="৫৩">৫৩</option>
                                                                    <option value="৫৪">৫৪</option>
                                                                    <option value="৫৫">৫৫</option>
                                                                    <option value="৫৬">৫৬</option>
                                                                    <option value="৫৭">৫৭</option>
                                                                    <option value="৫৮">৫৮</option>
                                                                    <option value="৫৯">৫৯</option>
                                                                    <option value="৬০">৬০</option> 
                                                                    <option value="৬১">৬১</option>
                                                                    <option value="৬২">৬২</option>
                                                                    <option value="৬৩">৬৩</option>
                                                                    <option value="৬৪">৬৪</option>
                                                                    <option value="৬৫">৬৫</option>
                                                                    <option value="৬৬">৬৬</option>
                                                                    <option value="৬৭">৬৭</option>
                                                                    <option value="৬৮">৬৮</option>
                                                                    <option value="৬৯">৬৯</option>
                                                                    <option value="৭০">৭০</option>  
                                                                    <option value="৭১">৭১</option>
                                                                    <option value="৭২">৭২</option>
                                                                    <option value="৭৩">৭৩</option>
                                                                    <option value="৭৪">৭৪</option>
                                                                    <option value="৭৫">৭৫</option>
                                                                    <option value="৭৬">৭৬</option>
                                                                    <option value="৭৭">৭৭</option>
                                                                    <option value="৭৮">৭৮</option>
                                                                    <option value="৭৯">৭৯</option>
                                                                    <option value="৮০">৮০</option>                                                                                                                                                                                                 
                                                                    <option value="৮১">৮১</option>
                                                                    <option value="৮২">৮২</option>
                                                                    <option value="৮৩">৮৩</option>
                                                                    <option value="৮৪">৮৪</option>
                                                                    <option value="৮৫">৮৫</option>
                                                                    <option value="৮৬">৮৬</option>
                                                                    <option value="৮৭">৮৭</option>
                                                                    <option value="৮৮">৮৮</option>
                                                                    <option value="৮৯">৮৯</option>
                                                                    <option value="৯০">৯০</option>                                                                                                                                                                                                 
                                                                    <option value="৯১">৯১</option>
                                                                    <option value="৯২">৯২</option>
                                                                    <option value="৯৩">৯৩</option>
                                                                    <option value="৯৪">৯৪</option>
                                                                    <option value="৯৫">৯৫</option>
                                                                    <option value="৯৬">৯৬</option>
                                                                    <option value="৯৭">৯৭</option>
                                                                    <option value="৯৮">৯৮</option>
                                                                    <option value="৯৯">৯৯</option>
                                                                </select>&nbsp;&nbsp;<label>দিন  ডান</label>&nbsp;<input type="checkbox" id="advice1right" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;<label>, বাম</label>&nbsp;<input type="checkbox" id="advice1left"  onclick="controlAdvice(this.id);">&nbsp;<label>পায়ে ভর দিবেন না.</label></span></div>
                                                        <div><span><input type="checkbox" id="advice2" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>২.</label>&nbsp;&nbsp;<label>ডান</label>&nbsp;<input type="checkbox" id="advice2right" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;<label>, বাম</label>&nbsp;<input type="checkbox" id="advice2left" onclick="controlAdvice(this.id);">&nbsp;<label>পায়ে অল্প ভর দিয়ে হাঁটবেন.</label></span></div>
                                                        <div><span><input type="checkbox" id="advice3" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৩.</label>&nbsp;&nbsp;<label>ডান</label>&nbsp;<input type="checkbox" id="advice3right" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;<label>, বাম</label>&nbsp;<input type="checkbox" id="advice3left" onclick="controlAdvice(this.id);">&nbsp;<label>পায়ে পুরো ভর দিয়ে হাঁটবেন.</label></span></div>
                                                        <div><span><input type="checkbox" id="advice4" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৪.</label>&nbsp;&nbsp;<label id="label4">শেখানো মতো  ব্যায়াম করবেন.</label</span></div>
                                                        <div><span><input type="checkbox" id="advice5" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৫.</label>&nbsp;&nbsp;<label id="label5">হাতের আঙ্গুল বেশি বেশি নাড়াবেন.</label></span></div>
                                                        <div><span><input type="checkbox" id="advice6" onclick="controlAdvice(this.id);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>৬.</label>&nbsp;&nbsp;<select class="input-small" name="advice6input"  id="advice6input">
                                                    <option value="">Select</option>                                                                                                                                                                                                 
                                                    <option value="১">১</option>
                                                    <option value="২">২</option>
                                                    <option value="৩">৩</option>
                                                    <option value="৪">৪</option>
                                                    <option value="৫">৫</option>
                                                    <option value="৬">৬</option>
                                                    <option value="৭">৭</option>
                                                    <option value="৮">৮</option>
                                                    <option value="৯">৯</option>
                                                    <option value="১০">১০</option>                                                                                                                                                                                                 
                                                    <option value="১১">১১</option>
                                                    <option value="১২">১২</option>
                                                    <option value="১৩">১৩</option>
                                                    <option value="১৪">১৪</option>
                                                    <option value="১৫">১৫</option>
                                                    <option value="১৬">১৬</option>
                                                    <option value="১৭">১৭</option>
                                                    <option value="১৮">১৮</option>
                                                    <option value="১৯">১৯</option>
                                                    <option value="২০">২০</option> 
                                                    <option value="২১">২১</option>
                                                    <option value="২২">২২</option>
                                                    <option value="২৩">২৩</option>
                                                    <option value="২৪">২৪</option>
                                                    <option value="২৫">২৫</option>
                                                    <option value="২৬">২৬</option>
                                                    <option value="২৭">২৭</option>
                                                    <option value="২৮">২৮</option>
                                                    <option value="২৯">২৯</option>
                                                    <option value="৩০">৩০</option>  
                                                    <option value="৩১">৩১</option>
                                                    <option value="৩২">৩২</option>
                                                    <option value="৩৩">৩৩</option>
                                                    <option value="৩৪">৩৪</option>
                                                    <option value="৩৫">৩৫</option>
                                                    <option value="৩৬">৩৬</option>
                                                    <option value="৩৭">৩৭</option>
                                                    <option value="৩৮">৩৮</option>
                                                    <option value="৩৯">৩৯</option>
                                                    <option value="৪০">৪০</option>                                                                                                                                                                                                 
                                                    <option value="৪১">৪১</option>
                                                    <option value="৪২">৪২</option>
                                                    <option value="৪৩">৪৩</option>
                                                    <option value="৪৪">৪৪</option>
                                                    <option value="৪৫">৪৫</option>
                                                    <option value="৪৬">৪৬</option>
                                                    <option value="৪৭">৪৭</option>
                                                    <option value="৪৮">৪৮</option>
                                                    <option value="৪৯">৪৯</option>
                                                    <option value="৫০">৫০</option>                                                                                                                                                                                                 
                                                    <option value="৫১">৫১</option>
                                                    <option value="৫২">৫২</option>
                                                    <option value="৫৩">৫৩</option>
                                                    <option value="৫৪">৫৪</option>
                                                    <option value="৫৫">৫৫</option>
                                                    <option value="৫৬">৫৬</option>
                                                    <option value="৫৭">৫৭</option>
                                                    <option value="৫৮">৫৮</option>
                                                    <option value="৫৯">৫৯</option>
                                                    <option value="৬০">৬০</option> 
                                                    <option value="৬১">৬১</option>
                                                    <option value="৬২">৬২</option>
                                                    <option value="৬৩">৬৩</option>
                                                    <option value="৬৪">৬৪</option>
                                                    <option value="৬৫">৬৫</option>
                                                    <option value="৬৬">৬৬</option>
                                                    <option value="৬৭">৬৭</option>
                                                    <option value="৬৮">৬৮</option>
                                                    <option value="৬৯">৬৯</option>
                                                    <option value="৭০">৭০</option>  
                                                    <option value="৭১">৭১</option>
                                                    <option value="৭২">৭২</option>
                                                    <option value="৭৩">৭৩</option>
                                                    <option value="৭৪">৭৪</option>
                                                    <option value="৭৫">৭৫</option>
                                                    <option value="৭৬">৭৬</option>
                                                    <option value="৭৭">৭৭</option>
                                                    <option value="৭৮">৭৮</option>
                                                    <option value="৭৯">৭৯</option>
                                                    <option value="৮০">৮০</option>                                                                                                                                                                                                 
                                                    <option value="৮১">৮১</option>
                                                    <option value="৮২">৮২</option>
                                                    <option value="৮৩">৮৩</option>
                                                    <option value="৮৪">৮৪</option>
                                                    <option value="৮৫">৮৫</option>
                                                    <option value="৮৬">৮৬</option>
                                                    <option value="৮৭">৮৭</option>
                                                    <option value="৮৮">৮৮</option>
                                                    <option value="৮৯">৮৯</option>
                                                    <option value="৯০">৯০</option>                                                                                                                                                                                                 
                                                    <option value="৯১">৯১</option>
                                                    <option value="৯২">৯২</option>
                                                    <option value="৯৩">৯৩</option>
                                                    <option value="৯৪">৯৪</option>
                                                    <option value="৯৫">৯৫</option>
                                                    <option value="৯৬">৯৬</option>
                                                    <option value="৯৭">৯৭</option>
                                                    <option value="৯৮">৯৮</option>
                                                    <option value="৯৯">৯৯</option>
                                                </select>&nbsp;&nbsp;<input name="cmback" id="cmback" class="cb" value="1" type="radio" onclick="controlAdvice(this.id);" /><label> দিন </label>&nbsp;&nbsp;<input name="cmback" id="cmback" class="cb" value="0" type="radio" onclick="controlAdvice(this.id);" /><label>মাস</label> &nbsp;<label id="label6">পর দেখা করবেন.</label></span></div>
                                                        <textarea class="form-control" name="advice" id="advice" rows="5"></textarea>
                                                        <input type="hidden" name="datetime" value="<?php echo date('j  F,  Y'); ?>">
                                                        <input type="hidden" name="visitcountinform" id="visitcountinform">
                                                        <input type="hidden" name="prescriptionIdinform" id="prescriptionIdinform">
                                                        <input type="hidden" name="alladvice" id="alladvice">
                                                        <br>
                                                        <button class="btn btn-primary" id="adviceadd" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                                    </div>
                                                </div> 
                                                <hr/> 

                                                <div class="form-group">
                                                    <label for="infection" class="control-label col-lg-3" >Infection</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="infection" id="infection" value="1" type="radio" onclick="hideInfection(this.value)"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="infection" id="infection" value="0" type="radio" onclick="hideInfection(this.value)"  /> No &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                                <div class="form-group" id="infectionwound" style="display: none">
                                                    <label for="infectionwound" class="control-label col-lg-3" >Incision of the wound?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="infectionwound" id="infectionwound1" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="infectionwound" id="infectionwound2" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </div>
                                                <div class="form-group" id="infectiondepth" style="display: none">
                                                    <label for="infectiondepth" class="control-label col-lg-3" >Infection Depth</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="infectiondepth" id="infectiondepth1" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="infectiondepth" id="infectiondepth2" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </div>


                                                <div class="form-group" id="infectionduration" style="display: none">
                                                    <label for="infectionduration" class="control-label col-lg-3" >Duration Of Infection</label>
                                                    <div class="col-lg-2">
                                                        <input class=" form-control" id="infectduration" name="infectionduration"  type="text" />Weeks                                          
                                                    </div>                                                                                          
                                                </div>
                                                <div class="form-group" id="infectiontype" style="display: none">                                   
                                                    <div class="col-lg-8 col-lg-offset-2">                                                                  
                                                        <input id="osteomyelitis" name="osteomyelitis" value="osteomyelitis"  type="checkbox"/> Osteomyelitis
                                                        <input id="amputation" name="amputation" value="amputation"  type="checkbox"/> Amputation
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="partialweight" class="control-label col-lg-3" >Partial weight bearing?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="partialweight" id="partialweight" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="partialweight" id="partialweight" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="painlessweight" class="control-label col-lg-3" >Painless full weight bearing?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="painlessweight" id="painlessweight" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="painlessweight" id="painlessweight" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="healingxray" class="control-label col-lg-3" >Healing by x-ray?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="healingxray" id="healingxray" value="1" type="radio"   /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="healingxray" id="healingxray" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="kneeflexion" class="control-label col-lg-3" >Knee flexion greater than 90 degrees?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="kneeflexion" id="kneeflexion" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="kneeflexion" id="kneeflexion" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="screwbreakage" class="control-label col-lg-3" >Screw breakage?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="screwbreakage" id="screwbreakage" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="screwbreakage" id="screwbreakage" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="screwloosening" class="control-label col-lg-3" >Screw loosening?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="screwloosening" id="screwloosening" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="screwloosening" id="screwloosening" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nailbreakage" class="control-label col-lg-3" >Nail breakage?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="nailbreakage" id="nailbreakage" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="nailbreakage" id="nailbreakage" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nailloosening" class="control-label col-lg-3" >Nail loosening?</label>
                                                    <div class="col-lg-8">                             
                                                        <input name="nailloosening" id="nailloosening" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="nailloosening" id="nailloosening" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div class="form-group">
                                                    <label for="deformity" class="control-label col-lg-3" >Deformity? </label>
                                                    <div class="col-lg-8">                             
                                                        <input name="deformity" id="deformity" value="1" type="radio"  onclick="hideDeformity(this.value)"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="deformity" id="deformity" value="0" type="radio"  onclick="hideDeformity(this.value)"  /> No &nbsp;&nbsp;&nbsp;(none over 10 degrees)
                                                    </div>
                                                </div>
                                                <div class="form-group" id="alignment" style="display: none">  
                                                    <label for="alignment" class="control-label col-lg-3 col-lg-offset-1" >Alignment?</label>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-11 col-lg-offset-2">                                                                  
                                                        <input id="alignment1" name="alignment" value="10 varus"  type="radio" /> Over 10 degrees varus  &nbsp;&nbsp;&nbsp; 
                                                        <input id="alignment2" name="alignment" value="20 varus"  type="radio"/> Over 20 degrees varus  &nbsp;&nbsp;&nbsp;

                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-11 col-lg-offset-2">                                                                                                       
                                                        <input id="alignment3" name="alignment" value="10 valgus"  type="radio"/> Over 10 degrees valgus &nbsp;
                                                        <input id="alignment4" name="alignment" value="20 valgus"  type="radio"/> Over 20 degrees valgus  &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                                <div class="form-group" id="rotation" style="display: none">   
                                                    <label for="rotation" class="control-label col-lg-3 col-lg-offset-1" >Rotation?</label>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-11 col-lg-offset-2">                                                                  
                                                        <input id="rotation1" name="rotation" value="1"  type="checkbox" /> Over 30 degrees
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div class="form-group">  
                                                    <label for="repeatsurgery" class="control-label col-lg-3" >Repeat surgery?</label>
                                                    <div class="col-lg-8 ">                                                                  
                                                        <input name="repeatsurgery" id="repeatsurgery1" value="1" onclick="hideRepeatSurgery(this.value)" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                                        <input name="repeatsurgery" id="repeatsurgery2" value="0" onclick="hideRepeatSurgery(this.value)" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                                <div class="form-group" id="surgerycause" style="display: none">  
                                                    <label for="repeatsurgery" class="control-label col-lg-3 col-lg-offset-2" style="font-style: italic;color: #000;text-align: left" >Check all That apply</label>                                  
                                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-11 col-lg-offset-2 ">                                                                  
                                                        <input name="infectionrepeat" id="infectionrepeat" value="infection" type="checkbox"    /> For Infection &nbsp;&nbsp;&nbsp;
                                                        <input name="deformityrepeat" id="deformityrepeat" value="deformity" type="checkbox"  /> For Deformity &nbsp;&nbsp;&nbsp;
                                                        <input name="nonunionrepeat" id="nonunionrepeat" onclick="hideRepeatType(this.value)" value="nonunion" type="checkbox"  /> For Non-Union &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                </div>
                                                <div class="form-group" id="surgerytype" style="display: none">  
                                                    <label for="repeatsurgery" class="control-label col-lg-3 col-lg-offset-2" style="font-style: italic;color: #000;text-align: left" >Check all That apply</label>                                  
                                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-11 col-lg-offset-2 ">                                                                  
                                                        <input name="dynamize" id="dynamize" value="dynamize" type="checkbox"    /> Dynamize &nbsp;&nbsp;&nbsp;
                                                        <input name="exchangenail" id="exchangenail" value="exchangenail" type="checkbox"  /> Exchange Nail &nbsp;&nbsp;&nbsp;
                                                        <input name="iliacrestbonegraft" id="iliacrestbonegraft" value="iliacrestbonegraft" type="checkbox"  /> Iliac Crest Bone Graft &nbsp;&nbsp;&nbsp;
                                                        <input name="other" id="other" value="other" onclick="hideOther();" type="checkbox"  /> Other &nbsp;&nbsp;&nbsp;
                                                        <input name="otherval" id="otherval"  type="text" style="display: none"  /> 
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div class="form-group ">
                                                    <label for="comments" class="control-label col-lg-3" >Comments</label>
                                                    <div class="col-lg-8">
                                                        <textarea class="form-control" id="comments" name="comments" ></textarea>
                                                    </div>
                                                </div>
                                                <hr/>

                                                <h6 class="panel-heading">
                                                    Followup Images
                                                </h6>
                                                <!--                        <div class="form">
                                                                            <form class="form-horizontal table-bordered" id="xRayImageForm" name="xRayImageForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureXrayImage');                                                                                ?>" enctype="multipart/form-data">-->
                                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                                    <div class="col-lg-11 col-lg-offset-2" id="xrayimageareafollowup">

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-9">
                                                        <button onclick="addnewxrayimagefollowup()" class="btn "  style="background: #21BBC7" type="button" >&nbsp;&nbsp;+ Add An X-Ray Image To This Followup&nbsp;&nbsp;</button>                               
                                                    </div>
                                                </div>
                                                <hr/>
                                                <h6 class="panel-heading" >
                                                    Prescription Images
                                                </h6>                              
                                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                                    <div class="col-lg-11 col-lg-offset-2" id="xrayimageareaprescription">
                                                    </div>
                                                </div>                                                           
                                                <hr/>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-9">
                                                        <button class="btn btn-primary" onclick="Clickheretoprint('#printprescription')"  type="submit">&nbsp;&nbsp;Save & Print &nbsp;&nbsp;</button>
                                                        <button class="btn btn-primary" type="submit">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
                                                        <button class="btn btn-primary" type="reset">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
                                                        <!--                                                    <button class="btn "  style="background: #21BBC7" type="submit">Save & Continue</button>
                                                                                                            <a href="<?php //echo site_url('prescription/Prescription');                                 ?>"> <button class="btn "  style="background: #21BBC7;color: #000" type="button">Cancel</button></a> -->
                                                    </div>
                                                </div>   


                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-lg-6">              
                                        <section class="panel">                                  
                                            <header class="panel-heading">
                                                Prescription Overview
                                            </header>
                                            <div class="panel-body">


                                                <input type="hidden" name="patient_id" value="<?php echo $presmasterData->patient_id ?>" >
                                                <input type="hidden" name="doctor_id" value="<?php echo $presmasterData->doctor_id ?>" >

                                                <input type="hidden" name="pres_id" value="<?php echo $presmasterData->pres_id ?>" >
                                                <input type="hidden" name="injury_date" value="<?php echo $presmasterData->injury_date ?>" >
                                                <input type="hidden" name="surgery_date" value="<?php echo $presmasterData->surgery_date ?>" >
                                                <input type="hidden" name="id_reference" value="<?php echo $presmasterData->id_reference ?>" >
                                                <input type="hidden" name="reference_value" value="<?php echo $presmasterData->reference_value ?>" >
                                                <input type="hidden" name="ref_others" value="<?php echo $presmasterData->ref_others ?>" >


                                                <input type="hidden" name="history" value="<?php echo $presmasterData->history ?>" >
                                                <input type="hidden" name="additional_info" value="<?php echo $presmasterData->additional_info ?>" >
                                                <input type="hidden" name="nerve_injury" value="<?php echo $presmasterData->o_e_nerve ?>" >
                                                <input type="hidden" name="nerve_injury_input" value="<?php echo $presmasterData->o_e_nerve_pre ?>" >
                                                <input type="hidden" name="vascular_injury" value="<?php echo $presmasterData->o_e_vascular ?>" >
                                                <input type="hidden" name="vascular_injury_input" value="<?php echo $presmasterData->o_e_vascular_pre ?>" >
                                                <input type="hidden" name="others" value="<?php echo $presmasterData->o_e ?>" />

                                                <div class="col-lg-12" id="printprescription">
                                                    <section class="panel">
                                                        <div style="" class="panel-body">
                                                            <style>
                                                                #printtd {
                                                                    width: 50%;
                                                                    height: auto;
                                                                    text-align: left;
                                                                    padding: 5px 10px 10px 50px;
                                                                    border: hidden;
                                                                }
                                                                #bottompaddingrow {
                                                                    display: none;
                                                                }
                                                                @media print {
                                                                    #panelheading {
                                                                        display: none !important;
                                                                    }
                                                                    #img {
                                                                        display: none !important;
                                                                    }
                                                                    #bottompaddingrow {
                                                                        display: block;
                                                                    }
                                                                    table {
                                                                        font-size: 12px;
                                                                    }
                                                                    @page { size: portrait; margin: 0; }
                                                                }
                                                            </style>
                                                            <table style="width: 100%;">
                                                                <tr id="bottompaddingrow">
                                                                    <td style="padding-bottom: 190px;"></td>
                                                                    <td style="padding-bottom: 190px;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd" style="padding-bottom: 0px"><b>ID : </b><span id="prescriptionId"><?php echo $presmasterData->pres_id ?></td>
                                                                    <td id="printtd">Date : <?php echo date('j  F,  Y'); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd" style="padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="fullname"><?php echo $patientlist->first_name . " " . $patientlist->last_name . "," ?> </span> <span id="birthday"><?php echo $patientlist->age . ' years'; ?> </span></p></td>
                                                                    <td id="printtd"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd" style="padding-top: 0px; padding-bottom: 0px"><p style="margin: 0px;"><span id="address"><?php echo $patientlist->address; ?></span></td>
                                                                    <td id="printtd"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Date Of Followup :</b></p> 
                                                                        <p style="margin: 0px;" id="followupshow"></p>
                                                                    </td> 
                                                                    <td id="printtd" rowspan="5" style="vertical-align: top">
                                                                        <p style="margin-top: 0px;"><b>Rx :</b></p> 
                                                                        <div id="medicinedata"></div>      
                                                                    </td>
                                                                </tr>
                                                                <tr>                                      
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Date Of Injury :</b></p> 
                                                                        <p style="margin: 0px;"><?php
                                                                            $date = new DateTime($presmasterData->injury_date);
                                                                            $injurydate = $date->format('d-m-Y');
                                                                            echo $injurydate;
                                                                            ?> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>                                      
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Date Of Surgery :</b></p> 
                                                                        <p style="margin: 0px;" ><?php
                                                                            $date = new DateTime($presmasterData->surgery_date);
                                                                            $surgerydate = $date->format('d-m-Y');
                                                                            echo $surgerydate;
                                                                            ?></p>
                                                                    </td>
                                                                </tr>



                                                                <tr>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>History :</b></p> 
                                                                        <p style="margin: 0px;" id="historyshow"><?php echo $presmasterData->history ?></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Additional Information :</b></p> 
                                                                        <p style="margin: 0px;" id="additionalinformationshow"><?php echo $presmasterData->additional_info ?></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>O/E :</b></p> 
                                                                        <p style="margin: 0px;" id="oeshow">
                                                                            <?php
                                                                            $nerve_injury = $presmasterData->o_e_nerve;
                                                                            $vascular_injury = $presmasterData->o_e_vascular;

                                                                            if ($nerve_injury == 1) {
                                                                                $nerve_injury_input = "<div>" . "Nerve Injury Presence : " . $presmasterData->o_e_nerve_pre . "</div>";
                                                                            } else {
                                                                                $nerve_injury_input = "<div>" . "Nerve Injury Presence : No.<br>" . "</div>";
                                                                            }


                                                                            if ($vascular_injury == 1) {
                                                                                $vascular_injury_input = "<div>" . "Vascular Injury Presence : " . $presmasterData->o_e_vascular_pre . "</div>";
                                                                            } else {
                                                                                $vascular_injury_input = "<div>" . "Vascular Injury Presence : No.<br>" . "</div>";
                                                                            }

                                                                            $others = "<div>Others : " . $presmasterData->o_e . "</div>";

                                                                            echo $nerve_injury_input . $vascular_injury_input . $others;
                                                                            ?></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Area Of treatment :</b></p>
                                                                        <div style="margin: 0px;"><?php echo $presmasterData->reference_value ?></div>
                                                                          <div style="margin: 0px;"><?php echo $presmasterData->ref_others ?></div>
                                                                        <p style="margin: 0px;" id="referenceaddshow"></p>
                                                                    </td> 
                                                                    <td id="printtd"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Test Advised :</b></p>
                                                                        <div style="margin: 0px;" id="medicaltestdatashow"></div>
                                                                    </td>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Advice :</b></p>
                                                                        <p style="margin: 0px;" id="adviceshown"></p>  
                                                                        <p style="margin: 0px;" id="adviceshow"></p> 
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="printtd">
                                                                        <p style="margin: 0px; padding: 0"><b>Diagnosis :</b></p>
                                                                        <p style="margin: 0px;" id="referenceaddshow2"><?php echo $presmasterData->reference_value ?></p>
                                                                         <p style="margin: 0px;"><?php echo $presmasterData->ref_others ?></p>
                                                                        <p style="margin: 0px;" id="diagnosisshow"></p>
                                                                    </td>   
                                                                    <td id="printtd"> </td>
                                                                </tr> 

                                                            </table>
                                                        </div>
                                                    </section>
                                                </div>
                                                <!--end here-->






                                            </div>
                                        </section>
                                    </div>
                                </form>
                            </div>
                        </div> 
                    </div>
            </div>
        </div>
        <!--        end surgical cases-->                                                          
    </section>              
</div>
</div>
</section>
</section>
<!--main content end-->
<script src="<?php echo $baseurl; ?>assets/assets/fancybox/source/jquery.fancybox.js"></script>
<script>
// followup x-ray image begin here

                                                            var countfollowup = 0;
                                                            function addnewxrayimagefollowup() {
                                                                var today = new Date();
                                                                var dd = today.getDate();
                                                                var mm = today.getMonth() + 1; //January is 0!
                                                                var yyyy = today.getFullYear();


                                                                countfollowup++;
                                                                var newxrayimagediv = "";
                                                                var selectday = "";
                                                                var selectimg = "";
                                                                var selectop = "";
                                                                var selectmonth = "";
                                                                var selectyear = "";
                                                                var btndelete = "";
                                                                var imgnum = "";
                                                                newxrayimagediv = "<div class='col-lg-5' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + countfollowup + "'>";
                                                                selectimg = "<div class ='col-md-4 col-md-offset-1'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '' alt = '' /> \n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
 <span class = 'btn btn-white btn-file'>\n\
<span class = 'fileupload-new'> <i class = 'fa fa-paper-clip' > </i> Select image</span>\n\
 <span class = 'fileupload-exists'> <i class = 'fa fa-undo'> </i> Change</span> \n\
<input type = 'file' multiple name='userfile[]' id='userfile" + countfollowup + "' class = 'default' /> </span> \n\
<input type = 'hidden' name='opCount' value ='" + countfollowup + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";
                                                                imgnum = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #</span>\n\
</select>\n\
</div>";

                                                                selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = 'operation" + countfollowup + "'id = 'operation'  >\n\
            <option selected = 'selected' value = '' >Pre/Post</option>\n\
            <option value = '1'> Pre-Op </option>\n\
<option value = '0'> Post-Op </option>\n\
</select>\n\
</div>";
                                                                selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='dayfollowup" + countfollowup + "' id='day" + countfollowup + "' ><option selected='selected' value=''>Day</option>";
                                                                for (var day = 1; day <= 31; day++) {
                                                                    if (day == dd) {
                                                                        selectday += "<option value='" + day + "' selected = 'selected'>" + day + "</option>";
                                                                    } else {
                                                                        selectday += "<option value='" + day + "'>" + day + "</option>";
                                                                    }
                                                                }
                                                                selectday += "</select>";
                                                                selectday += "</div>";

                                                                var months = ['Month', 'Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
                                                                var mondata = "<option selected = 'selected' value = '' >Month</option>";
                                                                for (var m = 1; m < 13; m++) {
                                                                    if (m == mm) {
                                                                        mondata += "<option selected = 'selected'  value= '" + m + "'>" + months[m] + "</option>";
                                                                    } else {
                                                                        mondata += "<option value= '" + m + "'>" + months[m] + "</option>";
                                                                    }

                                                                }

                                                                selectmonth = "<div class = 'col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'monthfollowup" + countfollowup + "' id = 'month' >" + mondata + "</select></div>";
                                                                var d = new Date();
                                                                var n = d.getFullYear();
                                                                selectyear = "<div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='yearfollowup" + countfollowup + "' id='year" + countfollowup + "' ><option selected='selected' value=''>Year</option>";
                                                                for (var year = 1970; year <= n + 10; year++) {
                                                                    if (year == yyyy) {
                                                                        selectyear += "<option value='" + year + "' selected>" + year + "</option>";
                                                                    } else {
                                                                        selectyear += "<option value='" + year + "'>" + year + "</option>";
                                                                    }
                                                                }
                                                                selectyear += "</select>";
                                                                selectyear += "</div></div>";
                                                                btndelete = "<div class='col-lg-offset-2 col-lg-8'><a href='javascript:deleteDiv(" + countfollowup + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";
                                                                newxrayimagediv += selectimg;
                                                                newxrayimagediv += imgnum;
                                                                newxrayimagediv += selectop;
                                                                newxrayimagediv += selectday;
                                                                newxrayimagediv += selectmonth;
                                                                newxrayimagediv += selectyear;
                                                                newxrayimagediv += btndelete;
                                                                newxrayimagediv += "</div>";
                                                                $("#xrayimageareafollowup").append(newxrayimagediv);
                                                                $('#userfile' + countfollowup).trigger('click');
                                                            }

                                                            function deleteDivFollowup(v) {
                                                                countfollowup--;
                                                                $("#xrayimagediv" + v).remove();
                                                            }

                                                            function deleteDiv(v) {
                                                                count--;
                                                                $("#xrayimagediv" + v).remove();
                                                            }

                                                            function hideInfection(val) {

                                                                if (val == "1") {
                                                                    $("#infectionwound").show();
                                                                    $("#infectiondepth").show();
                                                                    $("#infectionduration").show();
                                                                    $("#infectiontype").show();
                                                                } else {
                                                                    document.getElementById('infectionwound1').checked = false;
                                                                    document.getElementById('infectionwound2').checked = false;
                                                                    document.getElementById('infectiondepth1').checked = false;
                                                                    document.getElementById('infectiondepth2').checked = false;
                                                                    $("#infectduration").val("");
                                                                    $('#osteomyelitis').prop('checked', false);
                                                                    $('#amputation').prop('checked', false);
                                                                    $("#infectionwound").hide();
                                                                    $("#infectiondepth").hide();
                                                                    $("#infectionduration").hide();
                                                                    $("#infectiontype").hide();

                                                                }
                                                            }

                                                            function hideDeformity(val) {
                                                                if (val == "1") {
                                                                    $("#alignment").show();
                                                                    $("#rotation").show();

                                                                } else {
                                                                    document.getElementById('alignment1').checked = false;
                                                                    document.getElementById('alignment2').checked = false;
                                                                    document.getElementById('alignment3').checked = false;
                                                                    document.getElementById('alignment4').checked = false;
                                                                    $('#rotation1').prop('checked', false);
                                                                    $("#alignment").hide();
                                                                    $("#rotation").hide();
                                                                }
                                                            }

                                                            function hideRepeatSurgery(val) {
                                                                if (val == "1") {
                                                                    $("#surgerycause").show();
                                                                    $("#surgerytype").hide();

                                                                } else {

                                                                    $('#infectionrepeat').prop('checked', false);
                                                                    $('#deformityrepeat').prop('checked', false);
                                                                    $('#nonunionrepeat').prop('checked', false);
                                                                    $('#dynamize').prop('checked', false);
                                                                    $('#exchangenail').prop('checked', false);
                                                                    $('#iliacrestbonegraft').prop('checked', false);
                                                                    $('#other').prop('checked', false);
                                                                    $('#otherval').val('');

                                                                    $("#surgerycause").hide();
                                                                    $("#surgerytype").hide();
                                                                }
                                                            }

                                                            function hideRepeatType(val) {
                                                                if (document.getElementById('nonunionrepeat').checked) {
                                                                    $("#surgerytype").show();
                                                                } else {
                                                                    $('#dynamize').prop('checked', false);
                                                                    $('#exchangenail').prop('checked', false);
                                                                    $('#iliacrestbonegraft').prop('checked', false);
                                                                    $('#other').prop('checked', false);
                                                                    $('#otherval').val('');
                                                                    $("#surgerytype").hide();
                                                                }
                                                            }

                                                            function hideOther() {
                                                                if (document.getElementById('other').checked) {
                                                                    $("#otherval").show();
                                                                } else {
                                                                    $('#otherval').val('');
                                                                    $("#otherval").hide();
                                                                }
                                                            }

                                                            $(function () {
                                                                //    fancybox
                                                                jQuery(".fancybox").fancybox();
                                                            });

                                                            function getPriscription(id) {
                                                                $("#printprescription").show();
                                                                $("#buttondiv").show();
                                                                $("#medicaltestdatashowP").text("");
                                                                $("#medicinedataP").text("");
                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "<?php echo site_url('prescription/prescription/get_prescription'); ?>",
                                                                    data: 'id=' + id,
                                                                    success: function (data) {
                                                                        var response_json_data = JSON.parse(data);

                                                                        var presmasterdata = response_json_data.presmasterdata;
                                                                        var userdata = response_json_data.userdata;
                                                                        var patientCountdata = response_json_data.patientCountdata;

                                                                        var presMedicationData = response_json_data.presMedicationData;
                                                                        var testadvice = response_json_data.testadvice;

                                                                        $("#prescriptionIdP").text(presmasterdata.pres_id);

                                                                        $("#fullnameP").text(userdata.first_name + ' ' + userdata.last_name);
                                                                        $("#birthdayP").text(' , ' + userdata.age + ' years');
                                                                        $("#addressP").text(userdata.address);



                                                                        $("#historyshowP").text(presmasterdata.history);
                                                                        $("#additionalinformationshowP").text(presmasterdata.additional_info);
                                                                        $("#oeshowP").text(presmasterdata.o_e);
                                                                        $("#surgeryshowP").text(presmasterdata.reference_value);
                                                                        $("#diagnosisshowP").text(presmasterdata.diagnosis);
                                                                        $("#adviceshowP").text(presmasterdata.advice);
                                                                        $("#medicinedataP").append(presMedicationData);
                                                                        $("#medicaltestdatashowP").append(testadvice);

                                                                    }
                                                                });
                                                            }


</script>


<link href="<?php echo $baseurl; ?>assets/css/jquery-ui.css" rel="stylesheet">


<script type="text/javascript">
    var mlimit = 15;
    var availableTags = new Array();
    var medicineIdList = new Array();
    $(function () {
<?php
if (sizeof($medicinelist) > 0):
    foreach ($medicinelist as $datarow):
        ?>
                availableTags.push("<?php echo $datarow->formulation . ' - ' . $datarow->brand_name . ' - ' . $datarow->strength; ?>");
                medicineIdList.push("<?php echo $datarow->id; ?>");
        <?php
    endforeach;
endif;
?>
        $("#medicinename").autocomplete({
            //source: availableTags
            source: function (request, response) {
                var results = $.ui.autocomplete.filter(availableTags, request.term);
                response(results.slice(0, mlimit));
            }
        });
    });
</script>
<script type="text/javascript">
    var limit = 15;
    var availableTest = new Array();
    var testList = new Array();
    $(function () {
<?php
if (sizeof($testlist) > 0):
    foreach ($testlist as $datarow):
        ?>
                availableTest.push("<?php echo $datarow->test_name; ?>");
                testList.push("<?php echo $datarow->id; ?>");
        <?php
    endforeach;
endif;
?>
        $("#testname").autocomplete({
            source: function (request, response) {
                var results = $.ui.autocomplete.filter(availableTest, request.term);
                response(results.slice(0, limit));
            }
        });
    });
</script>
<script type="text/javascript">

    function getPatientInfo() {
        var patientid = $("#patientid").val();
        $.ajax({type: "POST",
            url: "<?php echo site_url('prescription/prescription/getPatientInfo'); ?>",
            data: 'patientid=' + patientid,
            success: function (data) {
                var ob = JSON.parse(data);
                $("#fullname").text(ob.first_name + ' ' + ob.last_name);
                $("#fullnameP").text(ob.first_name + ' ' + ob.last_name);
                $("#birthday").text(' , ' + ob.age + ' years');
                $("#birthdayP").text(' , ' + ob.age + ' years');
                $("#address").text(ob.address);
                $("#addressP").text(ob.address);
                $("#mobile").val(ob.mobile);
            }
        });
//        $.ajax({
//            type: "POST",
//            url: "<?php //echo site_url('prescription/prescription/getPatientCount');                         ?>",
//            data: 'patientid=' + patientid,
//            success: function (data) {
//                var ob = JSON.parse(data);
//                var visitcount = parseInt(ob.visitcount) + 1
//                $("#visitcount").text(visitcount);
//                //$("#visitcount").text(ob.visitcount);
//                $("#visitcountP").text(visitcount);
//                $("#visitcountinform").val(visitcount);
//            }
//        });
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/generateRandonNum'); ?>",
            data: 'patientid=' + patientid,
            success: function (data) {
                var ob = JSON.parse(data);
                $("#prescriptionId").text('R' + data);
                $("#prescriptionIdP").text('R' + data);
                $("#prescriptionIdinform").val('R' + data);
            }
        });
    }

    function medicineDetails(medicinename) {
        //alert(medicinename);
        var medicineIndex = availableTags.indexOf(medicinename);
        var medicineId = medicineIdList[medicineIndex];
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getMedicinedesc'); ?>",
            data: 'medicineName=' + medicineId,
            success: function (data) {
                var destext = "";
                var dosages;
                try {
                    var ob = JSON.parse(data);
                    destext = ob.manufacturer_name + ", " + ob.generic_name + ", " + ob.strength;
                    var d = ob.dosages;
                    var n = ob.notes;
                    if (d == null && n == null) {

                    } else {
                        if (d == null) {
                            d = "";
                        }
                        if (n == null) {
                            n = "";
                        }
                        dosages = d + "\n" + n;
                        $("#medicinefeedinst").val(dosages);
                    }
                    $("#medicinetabval").val(1);
                } catch (e) {
                    destext = "No description available for this medicine.";
                    $("#medicinetabval").val(0);
                }
                $("#addmedicinedescription").text(destext);
            }
        });
    }

    function getReferunder(id) {
        var curr = id.split(',');
        $("#reference1").val(curr[0]);
        $("#reference1name").val(curr[1]);
        $("#reference2name").val("");
        $("#reference3name").val("");
        $("#reference4name").val("");

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getFirstReference1'); ?>",
            data: 'id=' + id,
            success: function (data) {
                var arrData = JSON.parse(data);
                var underrefimagedata = arrData.underrefimagedata;
                $("#surgeryreference2").show();
                $("#getrefdata2").show();

                $("#getrefimagedata2").hide();
                $("#surgeryreference3").hide();
                $("#getrefdata3").hide();
                $("#surgeryreference4").hide();
                $("#getrefdata4").hide();
                document.getElementById('getrefdata2').innerHTML = underrefimagedata;
            }
        });
    }

    function getReferunder2(id) {
        var curr = id.split(',');
        $("#reference2").val(curr[0]);
        $("#reference2name").val(curr[1]);
        $("#reference3name").val("");
        $("#reference4name").val("");
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getFirstReference2'); ?>",
            data: 'id=' + id,
            success: function (data) {

                var arrData = JSON.parse(data);
                var underrefimagedata = arrData.underrefimagedata;
                var refimagedata = arrData.refimagedata;

                $("#surgeryreference3").show();
                $("#getrefdata3").show();
                $("#getrefimagedata2").show();
                $("#getrefimagedata3").hide();
                $("#surgeryreference4").hide();
                $("#getrefdata4").hide();

                document.getElementById('getrefdata3').innerHTML = underrefimagedata;
                document.getElementById('getrefimagedata2').innerHTML = refimagedata;


            }
        });
    }

    function getReferunder3(id) {
        var curr = id.split(',');
        $("#reference3").val(curr[0]);
        $("#reference3name").val(curr[1]);
        $("#reference4name").val("");
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('prescription/prescription/getFirstReference3'); ?>",
            data: 'id=' + id,
            success: function (data) {
                var arrData = JSON.parse(data);
                var underrefimagedata = arrData.underrefimagedata;
                var refimagedata = arrData.refimagedata;

                $("#surgeryreference4").show();
                $("#getrefdata4").show();
                $("#getrefimagedata3").show();

                document.getElementById('getrefdata4').innerHTML = underrefimagedata;
                document.getElementById('getrefimagedata3').innerHTML = refimagedata;


            }
        });
    }

    function getReferunder4(id) {
        var curr = id.split(',');
        $("#reference4").val(curr[0]);
        $("#reference4name").val(curr[1]);
    }

    function historyvalue() {
        var historyval = $("#history");
        var val = historyval.val().replace(/\n/g, '<br/>');
        $("#historyshow").html(val);
        $("#historyshowP").html(val);
    }

    function additionalvalue() {
        var additionalinformationval = $("#additionalinformation");
        var val = additionalinformationval.val().replace(/\n/g, '<br/>');
        $("#additionalinformationshow").html(val);
        $("#additionalinformationshowP").html(val);
    }

    function oevalue() {
        var oeval = $("#oe");
        var val = oeval.val().replace(/\n/g, '<br/>');
        $("#oeshow").html(val);
        $("#oeshowP").html(val);
    }

    function medicaltestvalue(medicaltestdata) {
        $("#medicaltestdatashow").text(medicaltestdata);
        $("#medicaltestdatashowP").text(medicaltestdata);
    }

    function diagnosisvalue() {
        var diagnosisval = $("#diagnosis");
        var val = diagnosisval.val().replace(/\n/g, '<br/>');
        $("#diagnosisshow").html(val);
        $("#diagnosisshowP").html(val);
    }

    function advicevalue() {
        var adviceval = $("#advice");
        var val = adviceval.val().replace(/\n/g, '<br/>');
        $("#adviceshow").html(val);
        $("#adviceshowP").html(val);
    }

    function resetDateFieldsInjury() {
        if ($("#unknownInjury").prop("checked") == true) {
            $('#dayInjury').prop("", "");
            $('#monthInjury').prop("", "");
            $('#yearInjury').prop("", "");

            $('#dayInjury').prop('selectedIndex', 0);
            $('#monthInjury').prop('selectedIndex', 0);
            $('#yearInjury').prop('selectedIndex', 0);
        } else {
            $('#dayInjury').prop("", "");
            $('#monthInjury').prop("", "");
            $('#yearInjury').prop("", "");
        }
    }

    function resetDateFieldsSurgery() {
        if ($("#unknownSurgery").prop("checked") == true) {
            $('#daySurgery').prop("", "");
            $('#monthSurgery').prop("", "");
            $('#yearSurgery').prop("", "");

            $('#daySurgery').prop('selectedIndex', 0);
            $('#monthSurgery').prop('selectedIndex', 0);
            $('#yearSurgery').prop('selectedIndex', 0);
        } else {
            $('#daySurgery').prop("", "");
            $('#monthSurgery').prop("", "");
            $('#yearSurgery').prop("", "");
        }
    }

    function selectimg4(val) {


        var data = val.split("#");
        var id = data[0];
        var ref = data[1];
        $("#reference4name").val(ref);
        $("#div" + id).css('border-color', 'red');
        var i;
        var len = $("#totalimgcount4").val();
        for (i = 1; i <= len; i++) {
            if (i == id)
                continue;
            $("#div" + i).css('border-color', 'blue');
        }
    }

    function selectimg3(val) {


        var data = val.split("#");
        var id = data[0];
        var ref = data[1];
        $("#reference3name").val(ref);
        $("#div" + id).css('border-color', 'red');
        var i;
        var len = $("#totalimgcount3").val();
        for (i = 1; i <= len; i++) {
            if (i == id)
                continue;
            $("#div" + i).css('border-color', 'blue');
        }
    }

    function hideNerveInjury(value) {
        if (value == "1") {
            $("#nerve_injury_input").show();
        }
        else {
            $("#nerve_injury_input").hide();
        }
    }

    function hideVascularInjury(value) {
        if (value == "1") {
            $("#vascular_injury_input").show();
        }
        else {
            $("#vascular_injury_input").hide();
        }
    }

    //For print
    function Clickheretoprint(elem) {
        $("#rowSpaceId").show();
        Popup(jQuery(elem).html());
    }
    function Popup(data) {
        var mywindow = window.open('', '');
        mywindow.document.write('<html><title></title>');
        //  mywindow.document.write('<link rel="stylesheet" href="<?php //echo $baseurl                  ?>assets/css/bootstrap.min.css" type="text/css" />');
        mywindow.document.write('<head></head><body><style type="text/css">');
        mywindow.document.write('table{margin-left:12px;margin-right:0px;margin-top:1px;margin-bottom:0px}');
        mywindow.document.write('table thead, tr, th, table tbody, tr, td { border: none; width:auto}');
        mywindow.document.write('<style type="text/css">body{ margin-left:100px;margin-right:0px;margin-top:1px;margin-bottom:0px } </style></body>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.document.close();
        mywindow.print();
    }


    var count = 0;
    function addnewxrayimage() {

        count++;
        var newxrayimagediv = "";
        var selectday = "";
        var selectimg = "";
        var selectop = "";
        var imgnum = "";
        var selectmonth = "";
        var selectyear = "";
        var btndelete = "";

        newxrayimagediv = "<div class='col-lg-5' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + count + "'>";
        selectimg = "<div class ='col-md-4 col-md-offset-1'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '' alt = '' /> \n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
 <span class = 'btn btn-white btn-file'>\n\
<span class = 'fileupload-new'> <i class = 'fa fa-paper-clip' > </i> Select image</span>\n\
 <span class = 'fileupload-exists'> <i class = 'fa fa-undo'> </i> Change</span> \n\
<input type = 'file' multiple name='userfile[]' class = 'default' /> </span> \n\
<input type = 'hidden' name='opCount' value ='" + count + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

        imgnum = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #</span>\n\
</select>\n\
</div>";

        selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = 'operation" + count + "'id = 'operation' >\n\
            <option selected = 'selected' value = '' >Pre/Post</option>\n\
            <option value = '1'> Pre-Op </option>\n\
<option value = '0'> Post-Op </option>\n\
</select>\n\
</div>";

        selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='day" + count + "' id='day" + count + "' ><option selected='selected' value=''>Day</option>";
        for (var day = 1; day <= 31; day++) {
            selectday += "<option value='" + day + "'>" + day + "</option>";
        }
        selectday += "</select>";
        selectday += "</div>";

        selectmonth = "<div class = 'col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'month" + count + "' id = 'month' >\n\
                                                                <option selected = 'selected' value = '' >Month</option>\n\
                                                                <option value = '1'> January </option>\n\
                                                                <option value = '2'> February </option>\n\
                                                                <option value = '3'> March </option>\n\
                                                                <option value = '4'> April </option>\n\
                                                                <option value = '5'> May </option>\n\
                                                                <option value = '6'> June </option>\n\
                                                                <option value = '7'> July </option>\n\
                                                                <option value = '8'> August </option>\n\
                                                                <option value = '9'> September </option>\n\
                                                                <option value = '10'> October </option>\n\
                                                                <option value = '11'> November </option>\n\
                                                                <option value = '12'> December </option>\n\
                                                                </select>\n\
                                                                </div>";


        var d = new Date();
        var n = d.getFullYear();
        selectyear = "<div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='year" + count + "' id='year" + count + "' ><option selected='selected' value=''>Year</option>";
        for (var year = 1970; year <= n + 10; year++) {
            selectyear += "<option value='" + year + "'>" + year + "</option>";
        }
        selectyear += "</select>";
        selectyear += "</div></div>";

        btndelete = "<div class='col-lg-offset-2 col-lg-8'><a href='javascript:deleteDiv(" + count + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";

        newxrayimagediv += selectimg;

        newxrayimagediv += selectop;
        newxrayimagediv += selectday;
        newxrayimagediv += selectmonth;
        newxrayimagediv += selectyear;
        newxrayimagediv += btndelete;
        newxrayimagediv += "</div>";
        $("#xrayimagearea").append(newxrayimagediv);

    }

    function deleteDiv(v) {
        count--;
        $("#xrayimagediv" + v).remove();
    }

    function controlAdvice(val) {

        var check = $("#" + val).prop('checked');
        switch (val) {
            case 'advice1':
                if (check) {
//                    $("#" + val + "input").prop('disabled', false);
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $("#" + val + "input").val('');
                    $("#" + val + "right").prop('checked', false);
                    $("#" + val + "left").prop('checked', false);

//                    $("#" + val + "input").prop('disabled', true);
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice2':
                if (check) {
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $("#" + val + "right").prop('checked', false);
                    $("#" + val + "left").prop('checked', false);
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice3':
                if (check) {
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $("#" + val + "right").prop('checked', false);
                    $("#" + val + "left").prop('checked', false);
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice4':

                break;
            case 'advice5':
                break;
            case 'advice6':
                if (check) {
//                    $("#" + val + "right").prop('disabled', false);
//                    $("#" + val + "left").prop('disabled', false);
                } else {
                    $(".cb").prop('checked', false); 
                          $("#" + val + "input").val('');
//                    $("#" + val + "right").prop('disabled', true);
//                    $("#" + val + "left").prop('disabled', true);
                }
                break;
            case 'advice1right':
            case 'advice1left':
                if (check) {
                    $("#advice1").prop('checked', true);

                } else {

                }
                break;
            case 'advice2right':
            case 'advice2left':
                if (check) {
                    $("#advice2").prop('checked', true);

                } else {

                }
                break;
            case 'advice3right':
            case 'advice3left':
                if (check) {
                    $("#advice3").prop('checked', true);
                } else {

                }
                break;
            case 'cmback':
                if (check) {
                    $("#advice6").prop('checked', true);
                } else {
                    $("#advice6").prop('checked', true);
                }
                break;


            default :
        }
    }


    var allimgname = '';
    var allimgtype = '';
    var allimgdate = '';
    var length;
    var i;
    var sepimgname, sepimgtype, sepimgdate, sepyearmonthday, yearfrac, monthfrac, dayfrac;

    var imgname = '';
    var imgtype = '';
    var imgdate = '';
    var countfrac = 0;
    var counttotal = 0;
    var selectedif, selectedelse, selectedday, selectedmonth, selectedyear, selectedop;
    var allimgfracture = "<?php echo sizeof($presImageData); ?>";

    if (allimgfracture > 0) {
        allimgname = "<?php echo $presImageData->image_name; ?>";
        allimgtype = "<?php echo $presImageData->image_type; ?>";
        allimgdate = "<?php echo $presImageData->date; ?>";

        sepimgname = allimgname.split(",");
        sepimgtype = allimgtype.split(",");
        sepimgdate = allimgdate.split(",");

        length = '<?php echo substr_count($presImageData->image_name, ",") ?>';

        if (allimgname != "") {
            for (i = 0; i <= length; i++) {
                imgname = sepimgname[i];
                imgtype = sepimgtype[i];
                imgdate = sepimgdate[i];

                sepyearmonthday = imgdate.split("-");
                yearfrac = sepyearmonthday[0];
                monthfrac = sepyearmonthday[1];
                dayfrac = sepyearmonthday[2];
                var loc = "<?php echo $baseurl . "assets/uploads/xray/"; ?>" + imgname;

                if (imgtype == 1) {
                    selectedop = '';
                    selectedif = 'selected';
                    selectedelse = '';
                } else if (imgtype == '') {
                    selectedop = 'selected';
                    selectedif = '';
                    selectedelse = '';
                } else {
                    selectedop = '';
                    selectedelse = 'selected';
                    selectedif = '';
                }

                //dynamic div start
                countfrac++;
                counttotal++;
                var newxrayimagediv = "";
                var selectday = "";
                var selectimg = "";
                var selectop = "";
                var imgnum = "";
                var selectmonth = "";
                var selectyear = "";
                var btndelete = "";

                newxrayimagediv = "<div class='col-lg-5' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + countfrac + "'>";
                selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
<a class='fancybox' rel='group' href='" + loc + "'><img src = '" + loc + "' alt = '' /></a>\n\
<input type = 'hidden' name='xrayimgname" + countfrac + "' value ='" + imgname + "'/>\n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
<input type = 'hidden' name='opCount' id='opCount" + countfrac + "' value ='" + countfrac + "'class = 'default'/> </span>\n\
<input type = 'hidden' name='tCount'  id='tCount" + countfrac + "' value ='" + countfrac + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

                imgnum = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #" + countfrac + "</span>\n\
</select>\n\
</div>";

                selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = 'operation" + countfrac + "'id = 'operation' >\n\
            <option selected = 'selected' value = '' " + selectedop + " >Pre/Post</option>\n\
            <option value = '1'" + selectedif + "> Pre-Op </option>\n\
<option value = '0' " + selectedelse + "> Post-Op </option>\n\
</select>\n\
</div>";

                selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4 col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='day" + countfrac + "' id='day" + countfrac + "' ><option selected='selected' value=''>Day</option>";
                for (var day = 1; day <= 31; day++) {
                    if (day == dayfrac) {
                        selectedday = 'selected';
                        selectday += "<option value='" + day + "'" + selectedday + ">" + day + "</option>";
                    } else {
                        selectday += "<option value='" + day + "'>" + day + "</option>";
                    }
                }
                selectday += "</select>";
                selectday += "</div>";

                selectmonth = "<div class = 'col-lg-4 col-md-4 col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'month" + countfrac + "' id = 'month" + countfrac + "' >\n\
                                                                <option selected = 'selected' value = '' >Month</option>\n\
                                                                <option value = '1'> January </option>\n\
                                                                <option value = '2'> February </option>\n\
                                                                <option value = '3'> March </option>\n\
                                                                <option value = '4'> April </option>\n\
                                                                <option value = '5'> May </option>\n\
                                                                <option value = '6'> June </option>\n\
                                                                <option value = '7'> July </option>\n\
                                                                <option value = '8'> August </option>\n\
                                                                <option value = '9'> September </option>\n\
                                                                <option value = '10'> October </option>\n\
                                                                <option value = '11'> November </option>\n\
                                                                <option value = '12'> December </option>\n\
                                                                </select>\n\
                                                                </div>";


                var d = new Date();
                var n = d.getFullYear();
                selectyear = "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='year" + countfrac + "' id='year" + countfrac + "' ><option selected='selected' value=''>Year</option>";
                for (var year = 1970; year <= n + 10; year++) {
                    if (year == yearfrac) {
                        selectedyear = 'selected';
                        selectyear += "<option value='" + year + "' " + selectedyear + ">" + year + "</option>";
                    } else {
                        selectyear += "<option value='" + year + "'>" + year + "</option>";
                    }
                }
                selectyear += "</select>";
                selectyear += "</div></div>";

//                btndelete = "<div class='col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5'><a href='javascript:deleteDiv(" + countfrac + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";

                newxrayimagediv += selectimg;
                newxrayimagediv += imgnum;
                newxrayimagediv += selectop;
                newxrayimagediv += selectday;
                newxrayimagediv += selectmonth;
                newxrayimagediv += selectyear;
//                newxrayimagediv += btndelete;
                newxrayimagediv += "</div>";
                $("#xrayimageareaprescription").append(newxrayimagediv);
                //dynamic div end
                $("#month" + countfrac).val(monthfrac);
            }
        }

    }



</script>

<script src="<?php echo $baseurl; ?>assets/assets/summernote/dist/summernote.min.js"></script>

