<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading" >
                        Patient List
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <span class="tools pull-right">
                                <button class="btn  btn-success"  data-toggle="modal" href="#myModalAddPatient">
                                    Add Patient&nbsp;<i class="fa fa-plus"></i>
                                </button> 
                            </span>
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Sex</th>
                                     
                                        <th>Age</th>
                                        <th>Blood Group</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (sizeof($patientlist) > 0):

                                        foreach ($patientlist as $datarow):
                                            ?>
                                            <tr class="gradeX">
                                                <td><a href="#" onclick="showProfilePicture(<?php echo $datarow->id; ?>)"class="pull-center thumb p-thumb"><img class="avatar" src="<?php echo $baseurl . "assets/uploads/profile/" . $datarow->image_name; ?>" alt=""></a></td>

                                                <td><a href="#" onclick="editPatientInfo(<?php echo $datarow->id; ?>)"><?php echo $datarow->first_name." ".$datarow->last_name; ?></a>
                                                </td>
                                                <td><a href="#" onclick="editPatientInfo(<?php echo $datarow->id; ?>)"><?php echo $datarow->email; ?></a></td>
                                                <td><a href="#" onclick="editPatientInfo(<?php echo $datarow->id; ?>)"><?php echo $datarow->address; ?></a></td>
                                                <td><a href="#" onclick="editPatientInfo(<?php echo $datarow->id; ?>)"><?php echo $datarow->mobile; ?></a></td>
                                                <td><a href="#" onclick="editPatientInfo(<?php echo $datarow->id; ?>)"><?php echo $datarow->sex; ?></a></td>                                               
                                             
                                                <td>
                                                    <a href="#" onclick="editPatientInfo(<?php echo $datarow->id; ?>)"> <?php
//                                                    $bday = new DateTime($datarow->date_of_birth); //take date from database
//                                                    $today = new DateTime();
//                                                    $today->format('Y-m-d 00:00:00');
//                                                    $diff = $today->diff($bday);
//                                                    printf('%d Yr, %d Mon, %d Day', $diff->y, $diff->m, $diff->d);
                                                        echo $datarow->age . " Years";
                                                        ?></a>
                                                </td>
                                                <td><a href="#" onclick="editPatientInfo(<?php echo $datarow->id; ?>)"><?php echo $datarow->blood_group; ?></a></td>
                                                <td>

                                                    <button class="btn btn-danger btn-xs" onclick="checkPatientUse(this.value)" value="<?php echo $datarow->id; ?>"><i class="fa fa-trash-o "></i></button></td>
                                            </tr>         
                                            <?php
                                        endforeach;

                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->
<!--Add Patient Modal-->
<div class="modal fade top-modal" id="myModalAddPatient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Add Patient Information</h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; height: 480px;">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" id="addPatientForm" method="POST" action="<?php echo site_url('patient/Patient/addPatient'); ?>" enctype="multipart/form-data">
                        <!--                        <div class="form-group">
                                                    <label for="username" class="control-label col-lg-3">Username<span style="color: red">*</span></label>
                                                    <div class="col-lg-9">
                                                        <input class=" form-control" id="username" name="username" minlength="4" type="text" onchange="checkUniqueUser();" required />
                                                        <span id="userMsg"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="password" class="control-label col-lg-3">Password<span style="color: red">*</span></label>
                                                    <div class="col-lg-9">
                                                        <input class=" form-control" id="password" name="password" minlength="4" type="text" required />
                                                    </div>
                                                </div>-->
                        <div class="form-group">
                            <label for="fname" class="control-label col-lg-3">First Name<span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="fname" name="fname" minlength="1" type="text" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="control-label col-lg-3">Last Name</label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="lname" name="lname" minlength="1" type="text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sex" class="control-label col-lg-3">Sex<span style="color: red">*</span></label>
                            <div class="col-lg-9">                             
                                <input name="sex" id="sex" value="Male" type="radio" required  /> Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="sex" id="sex" value="Female" type="radio"/> Female                           
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bloodgroup" class="control-label col-lg-3">Blood Group</label>
                            <div class="col-lg-9">
                                <select class="form-control m-bot15" name="bloodgroup" id="bloodgroup">
                                    <option value=""> --Select-- </option> 
                                    <option value="A+"> A+ </option> 
                                    <option value="A-"> A- </option> 
                                    <option value="B+"> B+ </option> 
                                    <option value="B-"> B- </option> 
                                    <option value="AB+"> AB+ </option> 
                                    <option value="AB-"> AB- </option> 
                                    <option value="O+"> O+ </option> 
                                    <option value="O-"> O- </option> 
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="age" class="control-label col-lg-3">Age<span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class="form-control" id="age" name="age" type="text" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="control-label col-lg-3">Mobile<span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class="form-control" id="mobile" name="mobile" minlength="10" maxlength="14" type="tel" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label col-lg-3">E-Mail</label>
                            <div class="col-lg-9">
                                <input class="form-control " id="email" type="email" name="email"/>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="address" class="control-label col-lg-3">Address</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" id="address" name="address"></textarea>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="description" class="control-label col-lg-3">Description</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" id="description" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userfile" class="control-label col-lg-3">Image<br>Max (200&#10005;200)px</label>
                            <div class="col-lg-9">
                                <input class="form-control" id="userfile" name="userfile" type="file"  />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn btn-success" type="submit">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>  
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!--/Add Patient Modal-->


<!--Edit Patient Modal-->
<div class="modal fade top-modal" id="myModalEditPatient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Edit Patient Information</h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; height: 480px;">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" id="editPatientForm" method="POST" action="<?php echo site_url('patient/Patient/editPatient'); ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="username_edit" class="control-label col-lg-3">Username</label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="username_edit" name="username_edit"  type="text" onchange="checkUniqueUserEdit();"  />
                                <input type="hidden" id="id_edit" name="id_edit"/>
                                <span id="userMsgEdit"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label col-lg-3">Password</label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="password_edit" name="password_edit" type="text"  />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fname_edit" class="control-label col-lg-3">First Name<span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="fname_edit" name="fname_edit" minlength="1" type="text" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lname_edit" class="control-label col-lg-3">Last Name</label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="lname_edit" name="lname_edit" minlength="4" type="text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sex_edit" class="control-label col-lg-3">Sex<span style="color: red">*</span></label>
                            <div class="col-lg-9">                             
                                <input name="sex_edit" id="sex_edit" value="Male" type="radio" required  /> Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="sex_edit" id="sex_edit" value="Female" type="radio"/> Female                           
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bloodgroup_edit" class="control-label col-lg-3">Blood Group</label>
                            <div class="col-lg-9">
                                <select class="form-control m-bot15" name="bloodgroup_edit" id="bloodgroup_edit">
                                    <option value=""> --Select-- </option> 
                                    <option value="A+"> A+ </option> 
                                    <option value="A-"> A- </option> 
                                    <option value="B+"> B+ </option> 
                                    <option value="B-"> B- </option> 
                                    <option value="AB+"> AB+ </option> 
                                    <option value="AB-"> AB- </option> 
                                    <option value="O+"> O+ </option> 
                                    <option value="O-"> O- </option> 
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="age_edit" class="control-label col-lg-3">Age<span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class="form-control" id="age_edit" name="age_edit" type="text" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mobile_edit" class="control-label col-lg-3">Mobile<span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class="form-control" id="mobile_edit" name="mobile_edit" minlength="10" maxlength="14" type="tel" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email_edit" class="control-label col-lg-3">E-Mail</label>
                            <div class="col-lg-9">
                                <input class="form-control " id="email_edit" type="email" name="email_edit"/>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="address_edit" class="control-label col-lg-3">Address</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" id="address_edit" name="address_edit"></textarea>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="description_edit" class="control-label col-lg-3">Description</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" id="description_edit" name="description_edit"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn btn-success" type="submit">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>   
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!--/Edit Patient Modal-->

<!--delete Modal start-->
<div class="modal fade top-modal" id="myModalDeletePatient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="<?php echo site_url('patient/Patient/deletePatient'); ?>" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;">Delete Patient Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;Are you want to delete !!!
                                <input type="hidden" id="id_delete" name="id_delete"/>
                            </div>
                        </div>
                        <div class="modal-footer">                    
                            <button class="btn btn-danger" type="submit">Confirm</button>
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>
<!-- delete Modal end -->

<!-- delete info Modal start -->
<div class="modal fade top-modal" id="myModalDeleteInfoPatient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="#" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;">Delete Patient Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;This patient information is used in other section !!!
                                <input type="hidden" id="id_delete" name="id_delete"/>
                            </div>
                        </div>
                        <div class="modal-footer">                    

                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>
<!-- delete info Modal end -->

<!--change Profile start-->
<div class="modal fade top-modal" id="myModalEditProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="<?php echo site_url('patient/Patient/changeProfilePicture'); ?>" enctype="multipart/form-data" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;">Change Patient Picture</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">


                                <div  class="col-lg-8 col-lg-offset-3"><img id="img_edit" class="img-rounded" src="" alt="" ></div>
                            </div>
                            <div class="form-group">
                                <div  class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <input class="form-control" id="userfile_edit" name="userfile_edit" type="file" />
                                    <input type="hidden" id="id_img" name="id_img" />
                                    <input type="hidden" id="name_img" name="name_img" />
                                </div>
                                <div  class="col-lg-3"></div>
                            </div>
                        </div>
                        <div class="modal-footer">                                                  
                            <button class="btn btn-success" type="submit">Confirm</button>  
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>
<!-- change Profile end -->

<script>

    function checkUniqueUser() {
        var username = $("#username").val();
        var dataString = 'username=' + username;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('patient/patient/checkUniqueUser'); ?>",
            data: dataString,
            success: function (data)
            {

                if (data == 'free') {
                    $("#userMsg").text("");
                    $("#userMsg").text("Valid Username");
                    $("#userMsg").css('color', 'green');
                    document.getElementById('addPatientForm').onsubmit = function () {
                        return true;
                    }
                }
                if (data == 'booked') {
                    $("#userMsg").text("");
                    $("#userMsg").text("This Username Is Already Used !!");
                    $("#userMsg").css('color', 'red');
                    document.getElementById('addPatientForm').onsubmit = function () {
                        return false;
                    }
                }
            }
        });
    }


    function checkUniqueUserEdit() {

        var username = $("#username_edit").val();
        var dataString = 'username=' + username;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('patient/patient/checkUniqueUser'); ?>",
            data: dataString,
            success: function (data)
            {

                if (data == 'free') {
                    $("#userMsgEdit").text("");
                    $("#userMsgEdit").text("Valid Username");
                    $("#userMsgEdit").css('color', 'green');
                    document.getElementById('editPatientForm').onsubmit = function () {
                        return true;
                    }
                }
                if (data == 'booked') {
                    $("#userMsgEdit").text("");
                    $("#userMsgEdit").text("This Username Is Already Used !!");
                    $("#userMsgEdit").css('color', 'red');
                    document.getElementById('editPatientForm').onsubmit = function () {
                        return false;
                    }
                }
            }
        });
    }


    function editPatientInfo(patient_id) {

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('patient/patient/getPatientData'); ?>",
            data: 'patient_id=' + patient_id,
            success: function (data) {
                var getData = JSON.parse(data);
                $("#id_edit").val(getData[0].id);
                $("#username_edit").val(getData[0].username);
                $("#password_edit").val(getData[0].password);
                $("#fname_edit").val(getData[0].first_name);
                $("#lname_edit").val(getData[0].last_name);
                $('input[name=sex_edit]').val([getData[0].sex]);
                $("#bloodgroup_edit").val(getData[0].blood_group);
                $("#age_edit").val(getData[0].age);
//                var originalDate = getData[0].date_of_birth;
//                var res = originalDate.split("-");
//                var year = res[0];
//                var month = res[1];
//                var day = res[2];
//                if (day.charAt(0) == "0") {
//                    day = day.charAt(1);
//                }
//
//                $("#birthday_edit").val(day);
//                $("#birthmonth_edit").val(month);
//                $("#birthyear_edit").val(year);
                $("#mobile_edit").val(getData[0].mobile);
                $("#email_edit").val(getData[0].email);
                $("#address_edit").val(getData[0].address);
                $("#description_edit").val(getData[0].description);
                $('#myModalEditPatient').modal('show');
            }
        });

    }

    function checkPatientUse(patient_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('patient/patient/checkPatientUse'); ?>",
            data: 'patient_id=' + patient_id,
            success: function (data) {
                if (data == "exits") {
                    $('#myModalDeleteInfoPatient').modal('show');
                } else {
                    showDeleteModal(patient_id);
                }

            }
        });
    }

    function showDeleteModal(patient_id) {
        $("#id_delete").val(patient_id);
        $('#myModalDeletePatient').modal('show');
    }

    function showProfilePicture(patient_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('patient/patient/getPatientData'); ?>",
            data: 'patient_id=' + patient_id,
            success: function (data) {
                var getData = JSON.parse(data);
                $("#id_img").val(getData[0].id);
                $("#name_img").val(getData[0].username);
                var imgName = getData[0].image_name;
                var loc = "<?php echo $baseurl . "/assets/uploads/profile/"; ?>" + imgName;
                $("#img_edit").attr("src", loc);
                $('#myModalEditProfile').modal('show');
            }
        });
    }





</script>

