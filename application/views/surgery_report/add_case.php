
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Enter Patient Case Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->                    
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/addCaseData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px;" >
                                    <label for="casenumber" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Case Number</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class=" form-control" id="casenumber" name="casenumber" type="text" />
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label for="day" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Date Of Injury<span style="color: red"> *</span></label>
                                    <div class="col-lg-4 col-md-6  col-sm-8 col-xs-7">
<!--                                                                        <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4" >
                                            <select class="form-control m-bot15" name="day" id="day" required>
                                                <option selected="selected" value="">Day</option>
                                                <?php
                                                for ($day = 1; $day <= 31; $day++):
                                                    ?>
                                                    <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="month" id="month" required>
                                                <option selected="selected" value="">Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="year" id="year" required>
                                                <option selected="selected" value="">Year</option>
                                                <?php
                                                $currentyr = date('Y');
                                                for ($year = 1970; $year <= $currentyr + 50; $year++):
                                                    ?>
                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                            <input  id="unknown" name="unknown" onclick="resetDateFields();" value="unknown"  type="checkbox"/> Unknown
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group" >
                                    <label for="patientname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Patient Name<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="patientname" id="patientname" onchange="getPatientInfo(this.value)" required>
                                            <option selected="selected" value="">--Select Name--</option>
                                            <?php
                                            if (sizeof($patientlist) > 0):
                                                foreach ($patientlist as $datarow):
                                                    echo '<option value="' . $datarow->id . '">' . $datarow->username . '</option> ';
                                                endforeach;
                                            endif;
                                            ?>                                                                                          
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="sex" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Gender</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="sex" id="sex" value="Male" type="radio" required disabled /> Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="sex" id="sex" value="Female" type="radio" disabled/> Female                           
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bloodgroup" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Blood Group </label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control m-bot15" name="bloodgroup" id="bloodgroup" readonly required>
                                            <option value=""> --Select-- </option> 
                                            <option value="A+"> A+ </option> 
                                            <option value="A-"> A- </option> 
                                            <option value="B+"> B+ </option> 
                                            <option value="B-"> B- </option> 
                                            <option value="AB+"> AB+ </option> 
                                            <option value="AB-"> AB- </option> 
                                            <option value="O+"> O+ </option> 
                                            <option value="O-"> O- </option> 
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="age" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Age</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class="form-control" id="age" name="age" type="text" readonly required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="mobile" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Mobile</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class="form-control" id="mobile" name="mobile" minlength="10" maxlength="14" type="tel" readonly required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >E-Mail</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class="form-control " id="email" type="email" name="email" readonly/>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Address</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <textarea class="form-control" id="address" name="address" readonly></textarea>
                                    </div>
                                </div>
                                <hr/>                         
                                <div class="form-group">
                                    <label for="userfile_edit" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-4" >Image<br>Max (200&#10005;200)px</label>                                 
                                    <div  class="col-lg-8 col-md-8  col-sm-8 col-xs-8"><img id="img_show" class="img-rounded" src="" alt="" width="150" height="150"></div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-lg-9 col-md-9  col-sm-9 col-xs-9">
                                        <button class="btn "  style="background: #21BBC7" type="submit">Save & Continue</button>
                                        <a href="<?php echo site_url('surgery_report/SurgeryCase'); ?>"> <button class="btn "  style="background: #21BBC7;color: #000" type="button">Cancel</button></a>   
                                    </div>
                                   
                                </div>
                            </form>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>

    function getPatientInfo(patient_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('surgery_report/SurgeryCase/getPatientData'); ?>",
            data: 'patient_id=' + patient_id,
            success: function (data) {
                var getData = JSON.parse(data);
                $('input[name=sex]').val([getData[0].sex]);
                $("#bloodgroup").val(getData[0].blood_group);
                $("#age").val(getData[0].age);
                $("#mobile").val(getData[0].mobile);
                $("#email").val(getData[0].email);
                $("#address").val(getData[0].address);
                var imgName = getData[0].image_name;
                var loc = "<?php echo $baseurl . "/assets/uploads/profile/"; ?>" + imgName;
                $("#img_show").attr("src", loc);
            }
        });
    }

    function resetDateFields() {
        if ($("#unknown").prop("checked") == true) {
            $('#day').prop("required", "");
            $('#month').prop("required", "");
            $('#year').prop("required", "");

            $('#day').prop('selectedIndex', 0);
            $('#month').prop('selectedIndex', 0);
            $('#year').prop('selectedIndex', 0);
        } else {
            $('#day').prop("required", "required");
            $('#month').prop("required", "required");
            $('#year').prop("required", "required");
        }
    }



</script>



