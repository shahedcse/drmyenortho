
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Surgical Case                 
                    </header>
                    <div class="panel-body">                 
                        <!--  start surgical cases-->                    
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <span class="tools pull-right">
                                <a style="color: #000" href="<?php echo site_url('surgery_report/SurgeryCase/addCase') ?>">
                                    <button class="btn "  style="background: #21BBC7">
                                        Add Case&nbsp;<i class="fa fa-plus"></i>
                                    </button> 
                                </a>
                            </span>
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>X-rays</th>
                                        <th>Input Date</th>
                                        <th>Patient Name</th>
                                        <th>Surgery Date</th>                                              
                                        <th>Case Number</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (sizeof($surgerycase) > 0):
                                        foreach ($surgerycase as $datarow):
                                            ?>
                                            <tr class="gradeX">
                                                <td><a href="<?php
                                                    $id = $datarow->id;
                                                    echo site_url('surgery_report/SurgeryCase/editCase/?id=' . $id);
                                                    ?>">&nbsp;&nbsp;
                                                           <?php
                                                           $imgcaseid = $datarow->id;
                                                           $querySurgeryId = $this->db->query("SELECT id FROM ortho_surgery WHERE case_id='$imgcaseid'");
                                                           if ($querySurgeryId->num_rows() > 0):
                                                               foreach ($querySurgeryId->result() as $rows):
                                                                   $imgsurgeryid = $rows->id;
                                                                   $queryFractureId = $this->db->query("SELECT id FROM ortho_fracture WHERE surgery_id='$imgsurgeryid'");
                                                                   if ($queryFractureId->num_rows() > 0):

                                                                       $fracimgnum = 0;
                                                                       foreach ($queryFractureId->result() as $rows):
                                                                           $imgfractureid = $rows->id;
                                                                           $query = $this->db->query("SELECT image_name FROM ortho_image_fracture WHERE fracture_id='$imgfractureid'");
                                                                           $imgnumf = 0;
                                                                           if ($query->num_rows() > 0):
                                                                               foreach ($query->result() as $rows):
                                                                                   $allimgname = $rows->image_name;
                                                                                   if ($allimgname != ""):
                                                                                       $imgnumf = substr_count($allimgname, ",");
                                                                                       $imgnumf = $imgnumf + 1;
                                                                                   endif;
                                                                               endforeach;
                                                                           endif;
//                                                           echo $imgnum."---";
                                                                           $fracimgnum = $fracimgnum + $imgnumf;

                                                                           $queryFollowupId = $this->db->query("SELECT id FROM ortho_followup WHERE fracture_id='$imgfractureid'");
                                                                           if ($queryFollowupId->num_rows() > 0):
                                                                               foreach ($queryFollowupId->result() as $rows):
                                                                                $imgfollowupid = $rows->id;
                                                                           $query = $this->db->query("SELECT image_name FROM ortho_image_followup WHERE followup_id='$imgfollowupid'");
                                                                           $imgnumfw = 0;
                                                                           if ($query->num_rows() > 0):
                                                                               foreach ($query->result() as $rows):
                                                                                   $allimgname = $rows->image_name;
                                                                                   if ($allimgname != ""):
                                                                                       $imgnumfw = substr_count($allimgname, ",");
                                                                                       $imgnumfw = $imgnumfw + 1;
                                                                                   endif;
                                                                               endforeach;
                                                                           endif;
//                                                           echo $imgnumfw."---";
                                                                           $fracimgnum = $fracimgnum + $imgnumfw;
                                                                               endforeach;
                                                                           endif;
                                                                       endforeach;
                                                                       echo $fracimgnum;

                                                                   endif;
                                                               endforeach;

                                                           endif;
                                                           ?></a></td>   
                                                <td><a href="<?php
                                                    $id = $datarow->id;
                                                    echo site_url('surgery_report/SurgeryCase/editCase/?id=' . $id)
                                                    ?>"><?php
                                                           $date = new DateTime($datarow->input_date);
                                                           $inputdate = $date->format('d-m-Y');
                                                           echo $inputdate;
                                                           ?></a></td>
                                                <td><a href="<?php
                                                    $id = $datarow->id;
                                                    echo site_url('surgery_report/SurgeryCase/editCase/?id=' . $id)
                                                    ?>"><?php
                                                           $patientId = $datarow->patient_id;
                                                           $query = $this->db->query("SELECT username FROM user WHERE id='$patientId'");
                                                           if (sizeof($query->row()) > 0):
                                                               echo $query->row()->username;
                                                           else:
                                                               echo "";
                                                           endif;
                                                           ?></a></td>
                                                <td><a href="<?php
                                                    $id = $datarow->id;
                                                    echo site_url('surgery_report/SurgeryCase/editCase/?id=' . $id)
                                                    ?>"><?php
                                                           $surdate = $datarow->surgerydate;
                                                           if ($surdate == ""):
                                                               echo $surdate;
                                                           else:
                                                               $date = new DateTime($surdate);
                                                               $surgerydate = $date->format('d-m-Y');
                                                               echo $surgerydate;
                                                           endif;
                                                           ?></a></td>                                            
                                                <td><a href="<?php
                                                    $id = $datarow->id;
                                                    echo site_url('surgery_report/SurgeryCase/editCase/?id=' . $id)
                                                    ?>"><?php echo $datarow->case_num; ?></a></td>
                                                <td>  
                                                    <button class="btn  btn-xs" style="background: #21BBC7" onclick="showDeleteModal(this.value);" value="<?php echo $datarow->id; ?>"><i class="fa fa-trash-o "></i></button></td>
                                            </tr>         
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<div class="modal fade top-modal" id="myModalDeleteCase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/deleteCaseData'); ?>" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;">Delete Case Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;Are you want to delete !!!
                                <input type="hidden" id="id_delete" name="id_delete"/>
                            </div>
                        </div>
                        <div class="modal-footer">                    
                            <button class="btn "  style="background: #21BBC7" type="submit">Confirm</button>
                            <button data-dismiss="modal" class="btn "  style="background: #21BBC7" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>



<script>
    function showDeleteModal(case_id) {
        $("#id_delete").val(case_id);
        $('#myModalDeleteCase').modal('show');
    }

</script>

