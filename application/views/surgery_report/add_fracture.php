
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Enter Fracture Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  
                        <div class="form" style="padding-bottom: 20px;">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 5px;">
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-6" style="font-weight: bold;color: #167F52;">Patient Name :<span style="font-weight:normal;color: #000;"><?php
                                            $patientId = $orthocase->patient_id;
                                            $query = $this->db->query("SELECT username FROM user WHERE id='$patientId'");
                                            if (sizeof($query->row()) > 0):
                                                echo $query->row()->username;
                                            else:
                                                echo "";
                                            endif;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-6" style="font-weight: bold;color: #167F52;">Case Number :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->case_num ?></span></label>
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-6" style="font-weight: bold;color: #167F52;">Date of Injury :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthocase->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-6" style="font-weight: bold;color: #167F52;">Date of Surgery :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthosurgery->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Case Id :<span  style="font-weight:normal;color: #000;"> <?php echo $orthocase->id ?></span></label>                                  
                                </div>
                            </form>
                        </div> 
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addFractureForm" name="addFractureForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/addFractureData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px;" >
                                    <label for="surgeonname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Surgeon(s)</label>
                                    <label for="surgeonname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="color: #000">
                                        <?php
                                        $surgeons_id = $orthosurgery->surgeon_id;
                                        $surgeon_name = "";
                                        $len = substr_count($surgeons_id, ',');
                                        $originalId = explode(",", $surgeons_id);
                                        for ($i = 0; $i <= $len; $i++) {
                                            $id = $originalId[$i];
                                            $queryName = $this->db->query("SELECT name FROM ortho_surgeon WHERE id='$id'");
                                            $surgeon_name .= $queryName->row()->name . "&nbsp&nbsp&nbsp&nbsp";
                                        }
                                        echo $surgeon_name;
                                        ?>                                         
                                    </label>                                  
                                    <input type="hidden" name="surgery_id" value="<?php echo $orthosurgery->id ?>"/>
                                </div>

                                <div class="form-group" <?php
                                if (sizeof($codenum) > 0):
                                    echo"style='display: block'";
                                else:
                                    echo "style='display: none'";
                                endif;
                                ?>>
                                    <label for="codenumber" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Code Number</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="codenumber" id="codenumber"  >
                                            <option selected="selected" value="">--Select Code--</option>
                                            <?php
                                            if (sizeof($codenum) > 0):
                                                foreach ($codenum as $datarow):
                                                    echo '<option value="' . $datarow->code_no . '">' . $datarow->code_no . '</option> ';
                                                endforeach;
                                            endif;
                                            ?>                                                                                          
                                        </select>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <label for="fractureside" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Fracture Side<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="fractureside" id="fractureside" value="left" type="radio" required/> Left &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="fractureside" id="fractureside" value="right" type="radio"/> Right                           
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="surgicalapp" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Surgical Approach<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="surgicalapp" id="surgicalapp" required>
                                            <option value=""> Select </option> 
                                            <option value="antegrade femur"> Antegrade Femur </option> 
                                            <option value="antegrade humerus"> Antegrade Humerus </option> 
                                            <option value="retrograde femur"> Retrograde Femur </option> 
                                            <option value="tibia"> Tibia </option> 
                                            <option value="hip fracture"> Hip Fracture </option>                                                    
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="locfrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-4" >Location Of Fracture</label>
                                    <label for="locfrac" class="control-label control-label col-lg-2 col-md-2  col-sm-2 col-xs-4" style="font-style: italic;color: #000;text-align: left">Check All That Apply</label>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">                             
                                        <input name="proximal" id="proximal" value="proximal" type="checkbox"/> Proximal &nbsp;
                                        <input name="middle" id="middle" value="middle" type="checkbox"/> Middle &nbsp;
                                        <input name="distal" id="distal" value="distal" type="checkbox"   /> Distal &nbsp;
                                        <input name="segmental" id="segmental" value="segmental" type="checkbox"/> Segmental &nbsp;
                                        <input name="femoralneck" id="femoralneck" value="femoralneck" type="checkbox"/> Femoral Neck &nbsp;
                                        <input name="intertrochanteric" id="intertrochanteric" value="intertrochanteric" type="checkbox"   /> Intertrochanteric &nbsp;
                                        <input name="subtrochanteric" id="subtrochanteric" value="subtrochanteric" type="checkbox"/> Subtrochanteric
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="typefrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Type Of Fracture<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="typefrac" onchange="typeFrac()" id="typefrac"  required>
                                            <option value=""> Select </option> 
                                            <option value="closed"> Closed </option> 
                                            <option value="gustilo|"> Gustilo | </option> 
                                            <option value="gustilo||"> Gustilo || </option> 
                                            <option value="gustilo|||a"> Gustilo |||a </option> 
                                            <option value="gustilo|||b"> Gustilo |||b </option> 
                                            <option value="gustilo|||c"> Gustilo |||c </option>                                               
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="stafrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-4" >Stability Of Fracture</label>
                                    <label for="locfrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-4" style="font-style: italic;color: #000;text-align: left">Check All That Apply</label>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">                             
                                        <input name="stable" id="stable" value="stable" type="checkbox" /> Stable &nbsp;&nbsp;
                                        <input name="unstableposteriormedialfragment" id="unstableposteriormedialfragment" value="unstableposteriormedialfragment " type="checkbox"/> Unstable Posterior Medial Fragment &nbsp;&nbsp;
                                        <input name="unstablelateralfemurwall" id="unstablelateralfemurwall" value="unstablelateralfemurwall " type="checkbox"   /> Unstable Lateral Femur Wall 

                                    </div>
                                </div> 
                                <div class="form-group" id="timeDebridement" style="display: none">
                                    <label for="timeDebridement" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Time From Injury To Debridement</label>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <input class=" form-control" id="timeDebridementHour" name="timeDebridementHour"  type="text"   onchange="resetField(this.id);"/>Hours, or  &nbsp;&nbsp;&nbsp;                                         
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">                                    
                                        <input class=" form-control" id="timeDebridementDay" name="timeDebridementDay"  type="text"  onchange="resetField(this.id);"/>Days 
                                    </div>                                                                    
                                </div>
                                <div class="form-group" id="timeSkinClouser" style="display: none">
                                    <label for="timeSkinClouserDay" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Time From Injury To Skin Clouser</label>                              
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">                                    
                                        <input class=" form-control" id="timeSkinClouserDay" name="timeSkinClouserDay"  type="text"  />Days 
                                    </div>                                                                    
                                </div>
                                <div class="form-group" id="woundClosure" style="display: none">
                                    <label for="stafrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Wound Closure</label>
                                    <label for="locfrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-style: italic;color: #000;text-align: left">Check All That Apply</label>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">                             
                                        <input name="primary" id="primary" value="primary" type="checkbox"   /> Primary &nbsp;&nbsp;
                                        <input name="skingraft" id="skingraft" value="skingraft" type="checkbox"/> Skin Graft &nbsp;&nbsp;
                                        <input name="musicflap" id="musicflap" value="musicflap" type="checkbox"   /> Muscle Flap &nbsp;&nbsp;
                                        <input name="secondary" id="stafrac" value="secondary" type="checkbox"   /> Secondary &nbsp;&nbsp;
                                        <input name="other" id="other" value="other" onclick="hideOther();" type="checkbox"/> Other &nbsp;&nbsp;
                                        <input name="otherval" id="otherval"  type="text" style="display: none"  /> 
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label for="nonunion" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nonunion</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="nonunion" id="nonunion" value="1" type="radio"   /> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="nonunion" id="nonunion" value="0" type="radio"/> No                           
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="preimplant" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Previous Implant Used</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="preimplant" id="preimplant" value="1" onclick="hidePreImplant(this.value)" type="radio"   /> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="preimplant" id="preimplant" value="0" onclick="hidePreImplant(this.value)" type="radio"/> No                           
                                    </div>
                                </div>
                                <div class="form-group" id="preimplantCheck" style="display: none">                                
                                    <label for="locfrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-2" style="font-style: italic;color: #000;text-align: left">Check All That Apply</label>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">                             
                                        <input name="externalfixation" id="externalfixation"  value="externalfixation" type="checkbox" onclick="hideExFixation()"     /> External Fixation &nbsp;&nbsp;
                                        <input name="plate" id="plate" value="plate" type="checkbox"/> Plate &nbsp;&nbsp;
                                        <input name="imnail" id="imnail" value="imnail" type="checkbox"   /> IM Nail &nbsp;&nbsp;
                                        <input name="wire" id="wire" value="wire" type="checkbox"   /> Wire &nbsp;&nbsp;                                      
                                    </div>
                                </div>
                                <div class="form-group" id="fixationPlace" style="display: none">
                                    <label for="timeExFixationDay" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >How Long Was The External Fixation In Place?</label>                              
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">                                    
                                        <input class=" form-control" id="timeExFixationDay" name="timeExFixationDay"  type="text"  />Days 
                                    </div>                                                                    
                                </div>
                                <div class="form-group" id="fixationSign" style="display: none">
                                    <label for="timeRemovalExFixationDay" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Time Between Removal Of External Fixation And SIGN?</label>                              
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">                                    
                                        <input class=" form-control" id="timeRemovalExFixationDay" name="timeRemovalExFixationDay"  type="text"  />Days 
                                    </div>                                                                    
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="reaming" id="reaming" >
                                            <option value=""> Select </option> 
                                            <option value="none"> None </option> 
                                            <option value="hand"> Hand </option> 
                                            <option value="power"> Power </option> 

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="fracreduc" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Fracture Reduction</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="fracreduc" id="fracreduc" value="1" type="radio"   /> Open &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="fracreduc" id="fracreduc" value="0" type="radio"/> Closed                                                 
                                    </div>
                                </div>                      
                                <hr/>
                                <div class="form-group ">
                                    <label for="comments" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Comments</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <textarea class="form-control" id="comments" name="comments"></textarea>
                                    </div>
                                </div>



                                <!--                            </form>
                                                        </div>-->

                                <h6 class="panel-heading" >
                                    Enter Nail Information
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="nailInfoForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureData');         ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Type Of Nail Used</label>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-7 ">                             
                                        <input name="typenail" id="typenail" value="standard nail" onchange="hideNailInfo(this.value)" type="radio"   /> Standard Nail &nbsp;&nbsp;
                                        <input name="typenail" id="typenail" value="standard fin nail" onchange="hideNailInfo(this.value)"  type="radio"/> Standard Fin Nail &nbsp;&nbsp; 
                                        <input name="typenail" id="typenail" value="pediatric fin nail" onchange="hideNailInfo(this.value)"  type="radio"   /> Pediatric Fin Nail &nbsp;&nbsp;
                                        <input name="typenail" id="typenail" value="standard hip nail" onchange="hideNailInfo(this.value)"  type="radio"/> Standard Hip Nail &nbsp;&nbsp;
                                        <input name="typenail" id="typenail" value="fin hip nail" onchange="hideNailInfo(this.value)"  type="radio"/> Fin Hip Nail &nbsp;&nbsp;
                                    </div>
                                </div>

                                <div class="form-group" id="l_nail" style="display: none">
                                    <label for="length_nail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Length Of Nail </label>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                                        <select class="form-control" name="length_nail" id="length_nail" onchange="getDiameterNailInfo(this.value)" >
                                            <option value="">Select</option> 
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="d_nail"  style="display: none">
                                    <label for="diameter_nail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Diameter Of Nail </label>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                                        <select class="form-control" name="diameter_nail" id="diameter_nail" >
                                            <option value="">Select</option> 

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="p_weight"  style="display: none;" >
                                    <label for="patientweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Patient Weight</label>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <input class=" form-control" id="patientweight" name="patientweight"  type="text"  />Kg
                                    </div>
                                </div>
                                <!--                            </form>
                                                        </div>-->


                                <h6 class="panel-heading" >
                                    Enter Screw Information
                                </h6>
                                <!--                       <div class="form">
                                                            <form class="form-horizontal table-bordered" id="screwInfoForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureData');         ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="">
                                    <label for="statusscrew" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Did you use screw with this nail?</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="screwnail" id="screwnail" onchange="hideScrewInfo(this.value);" value="1" type="radio" /> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="screwnail" id="screwnail" onchange="hideScrewInfo(this.value);" value="0" type="radio"/> No                           
                                    </div>
                                </div>
                                <div class="form-group" id="si_screw"  style="display: none">
                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-11 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table table-bordered" id="editable-sample">
                                            <thead>
                                                  <th colspan="12">Standard Interlocking Screws</th> 
                                                <tr>
                                                   
                                                    <th>Length in mm</th>
                                                    <th>25</th>
                                                    <th>30</th>
                                                    <th>35</th>
                                                    <th>40</th>
                                                    <th>45</th>
                                                    <th>50</th>
                                                    <th>55</th>
                                                    <th>60</th>
                                                    <th>65</th>
                                                    <th>70</th>
                                                    <th>75</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td># Of Proximal</td>
                                                    <td><input type="text" id="proximal25" name="proximal25" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal30" name="proximal30" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal35" name="proximal35" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal40" name="proximal40" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal45" name="proximal45" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal50" name="proximal50" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal55" name="proximal55" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal60" name="proximal60" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal65" name="proximal65" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal70" name="proximal70" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="proximal75" name="proximal75" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                </tr>  
                                                <tr class="">
                                                    <td># Of Distal</td>
                                                    <td><input type="text" id="distal25" name="distal25" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal30" name="distal30" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal35" name="distal35" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal40" name="distal40" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal45" name="distal45" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal50" name="distal50" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal55" name="distal55" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal60" name="distal60" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal65" name="distal65" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal70" name="distal70" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="distal75" name="distal75" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                </tr>
                                            </tbody>
                                        </table>                          
                                    </div>
                                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4">
                                         <img src = '<?php echo $baseurl . "/assets/uploads/xray/screw.PNG"; ?>' alt = '' width="300" height="100" />
                                     </div>
                                </div>

                                <div class="form-group"  id="c_screw"  style="display: none" >
                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-11 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table table-bordered" id="editable-sample">
                                            <thead>
                                                  <th colspan="13">Compression Screws</th> 
                                                <tr>
                                                    <th>Length in mm</th>
                                                    <th>60</th>
                                                    <th>65</th>
                                                    <th>70</th>
                                                    <th>75</th>
                                                    <th>80</th>
                                                    <th>85</th>
                                                    <th>90</th>
                                                    <th>95</th>
                                                    <th>100</th>
                                                    <th>105</th>
                                                    <th>110</th>
                                                    <th>115</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>Qty</td>
                                                    <td><input type="text" id="csqty60" name="csqty60" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty65" name="csqty65" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty70" name="csqty70" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty75" name="csqty75" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty80" name="csqty80" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty85" name="csqty85" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty90" name="csqty90" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty95" name="csqty95" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty100" name="csqty100" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty105" name="csqty105" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty110" name="csqty110" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="csqty115" name="csqty115" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                </tr>  

                                            </tbody>
                                        </table>                          
                                    </div>
                                    
                                    
                                    
                                </div>

                                <div class="form-group"  id="shcp_screw"  style="display: none">
                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-11 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table  table-bordered" id="editable-sample">
                                            <thead>
                                                  <th colspan="9">SHC Proximal Interlocking Screws</th> 
                                                <tr>
                                                    <th>Length in mm</th>
                                                    <th>60</th>
                                                    <th>65</th>
                                                    <th>70</th>
                                                    <th>75</th>
                                                    <th>80</th>
                                                    <th>85</th>
                                                    <th>90</th>
                                                    <th>95</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>Qty</td>
                                                    <td><input type="text" id="shcpqty60" name="shcpqty60" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shcpqty65" name="shcpqty65" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shcpqty70" name="shcpqty70" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shcpqty75" name="shcpqty75" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shcpqty80" name="shcpqty80" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shcpqty85" name="shcpqty85" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shcpqty90" name="shcpqty90" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shcpqty95" name="shcpqty95" class="form-control small" style="padding: 0" maxlength="1" ></td>

                                                </tr>  

                                            </tbody>
                                        </table>                          
                                    </div>
                                </div>

                                <div class="form-group"  id="shcc_screw"  style="display: none">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-11 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table  table-bordered" id="editable-sample">
                                            <thead>
                                                   <th colspan="5">SHC Cortical Screws</th> 
                                                <tr>
                                                    <th>Length in mm</th>

                                                    <th>30</th>
                                                    <th>35</th>
                                                    <th>40</th>
                                                    <th>45</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>Qty</td>
                                                    <td><input type="text" id="shccqty30" name="shccqty30" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shccqty35" name="shccqty35" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shccqty40" name="shccqty40" class="form-control small" style="padding: 0" maxlength="1" ></td>
                                                    <td><input type="text" id="shccqty45" name="shccqty45" class="form-control small" style="padding: 0" maxlength="1" ></td>

                                                </tr>  

                                            </tbody>
                                        </table>                          
                                    </div>
                                </div>
                                <hr/>



                                <!--                            </form>
                                                        </div>-->

                                <h6 class="panel-heading" >
                                    Enter Plate Components Used
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="plateInfoForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureData');         ?>" enctype="multipart/form-data">-->
                                <div class="form-group " style="">
                                    <label for="platesused" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Plates Used</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="platesused" id="stdNail" value="1" onclick="hidePlate(this.value);" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="platesused" id="stdFinNail" value="0"  onclick="hidePlate(this.value);" type="radio"/> No  &nbsp;&nbsp; 

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="one">
                                    <label for="rpu" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Rod Plate Used</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="rpu" id="rpu1" value="1" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="rpu" id="rpu2" value="0" type="radio"/> No  &nbsp;&nbsp; 

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="two">
                                    <label for="rc" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Rod Connector</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="rc" id="rc1" value="1" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="rc" id="rc2" value="0" type="radio"/> No  &nbsp;&nbsp; 

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="three">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Unicortical Screw</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="us" id="us1" value="1" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="us" id="us2" value="0" type="radio"/> No  &nbsp;&nbsp; 
                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="four">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >3 Hole Plate 75 mm</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="3hp" id="3hp1" value="1" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="3hp" id="3hp2" value="0" type="radio"/> No  &nbsp;&nbsp; 

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="five">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Hv 2 Hole Plate</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="h2hp" id="h2hp1" value="1" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="h2hp" id="h2hp2" value="0" type="radio"/> No  &nbsp;&nbsp; 

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="six">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Hv 3 Hole Plate</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="h3hp" id="h3hp1" value="1" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="h3hp" id="h3hp2" value="0" type="radio"/> No  &nbsp;&nbsp; 

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="seven">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Hv 4 Hole Plate</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <input name="h4hp" id="h4hp1" value="1" type="radio"   /> Yes &nbsp;&nbsp;
                                        <input name="h4hp" id="h4hp2" value="0" type="radio"/> No  &nbsp;&nbsp; 
                                    </div>
                                </div>
                                <!--                            </form>
                                                        </div>-->

                                <h6 class="panel-heading" >
                                    X-Ray Images
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="xRayImageForm" name="xRayImageForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureXrayImage');      ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="display: block;"  id="img">
                                    <div class="col-lg-11 col-lg-offset-1" id="xrayimagearea">

                                    </div>
                                </div>

                                <div class="form-group" style="padding-top: 10px;">
                                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-lg-9 col-md-9  col-sm-9 col-xs-9">
                                        <button onclick="addnewxrayimage()" class="btn "  style="background: #21BBC7" type="button" >&nbsp;&nbsp;+ Add An X-Ray Image To This Fracture&nbsp;&nbsp;</button>                               
                                    </div>
                                </div>
                                <!--                                        end formgroup-->

                                <hr/>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-lg-9 col-md-9  col-sm-9 col-xs-9">
                                        <button class="btn "  style="background: #21BBC7" type="submit">Save & Continue</button>
                                        <a href="<?php echo site_url('surgery_report/SurgeryCase/addSurgery'); ?>"> <button class="btn "  style="background: #21BBC7;color: #000" type="button">Cancel</button></a> 
                                    </div>
                                </div>
                            </form>
                        </div>                                                        
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>

    var count = 0;
    function addnewxrayimage() {
        count++;
        var newxrayimagediv = "";
        var selectday = "";
        var selectimg = "";
        var selectop = "";
        var imgnum = "";
        var selectmonth = "";
        var selectyear = "";
        var btndelete = "";

        newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + count + "'>";
        selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '' alt = '' /> \n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
 <span class = 'btn btn-white btn-file'>\n\
<span class = 'fileupload-new'> <i class = 'fa fa-paper-clip' > </i> Select image</span>\n\
 <span class = 'fileupload-exists'> <i class = 'fa fa-undo'> </i> Change</span> \n\
<input type = 'file' multiple name='userfile[]' class = 'default' required/> </span> \n\
<input type = 'hidden' name='opCount' value ='" + count + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

        imgnum = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #</span>\n\
</select>\n\
</div>";

        selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = 'operation" + count + "'id = 'operation' required>\n\
            <option selected = 'selected' value = '' >Select</option>\n\
            <option value = '1'> Pre-Op </option>\n\
<option value = '0'> Post-Op </option>\n\
</select>\n\
</div>";

        selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='day" + count + "' id='day" + count + "' required><option selected='selected' value=''>Day</option>";
        for (var day = 1; day <= 31; day++) {
            selectday += "<option value='" + day + "'>" + day + "</option>";
        }
        selectday += "</select>";
        selectday += "</div>";

        selectmonth = "<div class = 'col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'month" + count + "' id = 'month' required>\n\
                                                                <option selected = 'selected' value = '' >Month</option>\n\
                                                                <option value = '01'> January </option>\n\
                                                                <option value = '02'> February </option>\n\
                                                                <option value = '03'> March </option>\n\
                                                                <option value = '04'> April </option>\n\
                                                                <option value = '05'> May </option>\n\
                                                                <option value = '06'> June </option>\n\
                                                                <option value = '07'> July </option>\n\
                                                                <option value = '08'> August </option>\n\
                                                                <option value = '09'> September </option>\n\
                                                                <option value = '10'> October </option>\n\
                                                                <option value = '11'> November </option>\n\
                                                                <option value = '12'> December </option>\n\
                                                                </select>\n\
                                                                </div>";


        var d = new Date();
        var n = d.getFullYear();
        selectyear = "<div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='year" + count + "' id='year" + count + "' required><option selected='selected' value=''>Year</option>";
        for (var year = 1970; year <= n + 10; year++) {
            selectyear += "<option value='" + year + "'>" + year + "</option>";
        }
        selectyear += "</select>";
        selectyear += "</div></div>";

        btndelete = "<div class='col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5'><a href='javascript:deleteDiv(" + count + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";

        newxrayimagediv += selectimg;
        newxrayimagediv += imgnum;
        newxrayimagediv += selectop;
        newxrayimagediv += selectday;
        newxrayimagediv += selectmonth;
        newxrayimagediv += selectyear;
        newxrayimagediv += btndelete;
        newxrayimagediv += "</div>";
        $("#xrayimagearea").append(newxrayimagediv);

    }

    function deleteDiv(v) {
        count--;
        $("#xrayimagediv" + v).remove();
    }


    function typeFrac() {
        var val = $("#typefrac").val();
        if (val == "closed") {
            $("#timeDebridementHour").val("");
            $("#timeDebridementDay").val("");
            $("#timeSkinClouserDay").val("");
            $("#timeDebridement").hide();
            $("#timeSkinClouser").hide();
            $("#woundClosure").hide();
        } else {
            $("#timeDebridementHour").val("");
            $("#timeDebridementDay").val("");
            $("#timeSkinClouserDay").val("");
            $("#timeDebridement").show();
            $("#timeSkinClouser").show();
            $("#woundClosure").show();
        }
    }
    function hidePreImplant(val) {
        if (val == "1") {
            $("#preimplantCheck").show();
        } else {
            $('#externalfixation').prop('checked', false);
            $('#plate').prop('checked', false);
            $('#imnail').prop('checked', false);
            $('#wire').prop('checked', false);
            $("#preimplantCheck").hide();
        }
    }



    function hideExFixation() {
        if (document.getElementById('externalfixation').checked) {
            $("#fixationPlace").show();
            $("#fixationSign").show();
        }
        else {
            $("#timeExFixationDay").val("");
            $("#timeRemovalExFixationDay").val("");
            $("#fixationPlace").hide();
            $("#fixationSign").hide();
        }
    }

    function resetField(id) {
        var caseId = id;
        switch (caseId) {
            case 'timeDebridementDay':
                $("#timeDebridementHour").val("");
                break;
            case 'timeDebridementHour':
                $("#timeDebridementDay").val("");
                break;
            default:
        }
    }

    function hideScrewInfo(val) {
        var caseVal = val;
        if (caseVal == '1') {
            $("#si_screw").show();
            $("#c_screw").show();
            $("#shcp_screw").show();
            $("#shcc_screw").show();
        } else {
            $("#proximal25").val("");
            $("#proximal30").val("");
            $("#proximal35").val("");
            $("#proximal40").val("");
            $("#proximal45").val("");
            $("#proximal50").val("");
            $("#proximal55").val("");
            $("#proximal60").val("");
            $("#proximal65").val("");
            $("#proximal70").val("");
            $("#proximal75").val("");

            $("#distal25").val("");
            $("#distal30").val("");
            $("#distal35").val("");
            $("#distal40").val("");
            $("#distal45").val("");
            $("#distal50").val("");
            $("#distal55").val("");
            $("#distal60").val("");
            $("#distal65").val("");
            $("#distal70").val("");
            $("#distal75").val("");

            $("#csqty60").val("");
            $("#csqty65").val("");
            $("#csqty70").val("");
            $("#csqty75").val("");
            $("#csqty80").val("");
            $("#csqty85").val("");
            $("#csqty90").val("");
            $("#csqty95").val("");
            $("#csqty100").val("");
            $("#csqty105").val("");
            $("#csqty110").val("");
            $("#csqty115").val("");

            $("#shcpqty60").val("");
            $("#shcpqty65").val("");
            $("#shcpqty70").val("");
            $("#shcpqty75").val("");
            $("#shcpqty80").val("");
            $("#shcpqty85").val("");
            $("#shcpqty90").val("");
            $("#shcpqty95").val("");

            $("#shccqty30").val("");
            $("#shccqty35").val("");
            $("#shccqty40").val("");
            $("#shccqty45").val("");



            $("#si_screw").hide();
            $("#c_screw").hide();
            $("#shcp_screw").hide();
            $("#shcc_screw").hide();
        }
    }

    function hidePlate(val) {
        var caseVal = val;
        if (caseVal == '1') {
            $("#one").show();
            $("#two").show();
            $("#three").show();
            $("#four").show();
            $("#five").show();
            $("#six").show();
            $("#seven").show();
        } else {
            document.getElementById('rpu1').checked = false;
            document.getElementById('rpu2').checked = false;
            document.getElementById('rc1').checked = false;
            document.getElementById('rc2').checked = false;
            document.getElementById('us1').checked = false;
            document.getElementById('us2').checked = false;
            document.getElementById('3hp1').checked = false;
            document.getElementById('3hp2').checked = false;
            document.getElementById('h2hp1').checked = false;
            document.getElementById('h2hp2').checked = false;
            document.getElementById('h3hp1').checked = false;
            document.getElementById('h3hp2').checked = false;
            document.getElementById('h4hp1').checked = false;
            document.getElementById('h4hp2').checked = false;
            $("#one").hide();
            $("#two").hide();
            $("#three").hide();
            $("#four").hide();
            $("#five").hide();
            $("#six").hide();
            $("#seven").hide();
        }
    }


    function hideNailInfo(val) {
        var caseVal = val;
        switch (caseVal) {
            case 'standard nail':
                $("#l_nail").show();
                $('#length_nail')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="">Select</option>\n\
<option value="190 mm">190 mm</option>\n\
<option value="200 mm">200 mm</option>\n\
<option value="220 mm">220 mm</option>\n\
<option value="240 mm">240 mm</option>\n\
<option value="260 mm">260 mm</option>\n\
<option value="280 mm">280 mm</option>\n\
<option value="300 mm">300 mm</option>\n\
<option value="320 mm">320 mm</option>\n\
<option value="340 mm">340 mm</option>\n\
<option value="360 mm">360 mm</option>\n\
<option value="380 mm">380 mm</option>\n\
<option value="400 mm">400 mm</option>\n\
<option value="420 mm">420 mm</option>');

                $("#d_nail").show();
                $("#diameter_nail").val("");
                $("#patientweight").val("");
                $("#p_weight").hide();
                break;
            case 'standard fin nail':
                $("#l_nail").show();
                $('#length_nail')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="">Select</option>\n\
<option value="160 mm">160 mm</option>\n\
<option value="190 mm">190 mm</option>\n\
<option value="240 mm">240 mm</option>\n\
<option value="280 mm">280 mm</option>\n\
<option value="320 mm">320 mm</option>\n\
<option value="340 mm">340 mm</option>\n\
<option value="360 mm">360 mm</option>');

                $("#d_nail").show();
                $("#diameter_nail").val("");
                $("#patientweight").val("");
                $("#p_weight").hide();
                break;
            case 'pediatric fin nail':
                $("#l_nail").show();
                $('#length_nail')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="">Select</option>\n\
<option value="140 mm">140 mm</option>\n\
<option value="170 mm">170 mm</option>\n\
<option value="200 mm">200 mm</option>\n\
<option value="240 mm">240 mm</option>');
                $("#d_nail").show();
                $("#diameter_nail").val("");
                $("#patientweight").val("");
                $("#p_weight").show();
                break;
            case 'standard hip nail':
                $("#l_nail").show();
                $('#length_nail')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="">Select</option>\n\
<option value="280 mm">280 mm</option>\n\
<option value="320 mm">320 mm</option>');
                $("#d_nail").show();
                $("#diameter_nail").val("");
                $("#patientweight").val("");
                $("#p_weight").hide();
                break;
            case 'fin hip nail':
//                $('#length_nail')
//                        .find('option')
//                        .remove()
//                        .end();
//
//                $('#diameter_nail')
//                        .find('option')
//                        .remove()
//                        .end();
                $("#l_nail").hide();
                $("#d_nail").hide();
                $("#diameter_nail").val("");
                $("#length_nail").val("");
                $("#patientweight").val("");
                $("#p_weight").hide();
                break;
            default:
        }
    }

    function getDiameterNailInfo(lVal) {

        var typeNail = $('input[name=typenail]:checked', '#addFractureForm').val();

        if (typeNail == "standard nail") {
            switch (lVal) {
                case '190 mm':

                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '200 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '220 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '240 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '260 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '280 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '300 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '320 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '340 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '360 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '380 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '400 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '420 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                default:
            }

        } else if (typeNail == "standard fin nail") {
            switch (lVal) {
                case '160 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '190 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '240 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '280 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '320 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                case '340 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                case '360 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>');
                    break;

                default:
            }

        } else if (typeNail == "pediatric fin nail") {
            switch (lVal) {
                case '140 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '170 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '200 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '240 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                default:
            }

        } else if (typeNail == "standard hip nail") {
            switch (lVal) {
                case '280 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                case '320 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">Select</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                default:
            }
        }
    }


    function hideOther() {
        if (document.getElementById('other').checked) {
            $("#otherval").show();
        } else {

            $('#otherval').val('');
            $("#otherval").hide();
        }
    }

</script>



