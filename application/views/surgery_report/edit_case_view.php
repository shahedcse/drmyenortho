<style>
    a:link {
        text-decoration: underline;
    }

    a:visited {
         text-decoration: underline;
    }

    a:hover {
        text-decoration: underline;
    }

    a:active {
        text-decoration: underline;
    }

</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Patient Case Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->                    
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">  

                                <div class="col-lg-12" style="padding-top: 20px;">                                   
                                    <a style="color: #000" href="<?php echo site_url('surgery_report/Research'); ?>"> <button class="btn btn-navbar" style="background: #21BBC7" type="button">Return to Research</button></a> 
                                </div>

                                <div class="form-group" style="margin: 0;padding: 0">
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-3" style="font-weight: bold;color: #167F52;text-align: left"><h4>Patient Case Information</h4></label>
                                </div>
                                <hr/>
                                <div class="form-group" style="margin: 0;padding: 0">
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Patient Name :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->username ?></span></label>
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Address :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->address ?></span></label>
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Gender :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->sex ?></span></label>
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Case Num :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->case_num ?></span></label>
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Phone :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->mobile ?></span></label>
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Injury Date :<span style="font-weight:normal;color: #000;"> <?php
                                            $originalDate = $orthocase->date;
                                            $newDate = date("d-m-Y", strtotime($originalDate));
                                            echo $newDate;
                                            ?></span></label> 
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Input Date :<span style="font-weight:normal;color: #000;"> <?php
                                            $originalDate = $orthocase->input_date;
                                            $newDate = date("d-m-Y", strtotime($originalDate));
                                            echo $newDate;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Email :<span style="font-weight:normal;color: #000;"> <?php echo $patientlist->email ?></span></label>
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left">Case Id :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->id ?></span></label>
                                </div> 
                                <hr/>
                            </form>
                        </div>  
                        </br>
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="margin: 0;padding: 0">
                                    <label for="address" class="control-label col-lg-4 col-md-4  col-sm-4 col-xs-4" style="font-weight: bold;color: #167F52;text-align: left;"><h4>Surgeries</h4></label> 
                                    <input type="hidden" id="case_id" name="case_id" value="<?php echo $case_id ?>" />
                                </div>
                                <hr/>
                                <div id="allsurgery">
                                    <?php
                                    if (sizeof($resultallsurgery)):
                                        foreach ($resultallsurgery as $surgeryrow):
                                            ?>
                                            <div id="newsurgery" style="margin: 0;padding: 0" >
                                                <div class="form-group" style="margin: 0;padding: 0">
                                                    <label for="surgery" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-3" style="font-weight: bold;text-align: left;color: #000">Surgery <?php echo $surgeryrow->surgery_id; ?> :<a style="color: #000" href="<?php
                                                        $id = $surgeryrow->id;
                                                        echo site_url('surgery_report/Research/editSurgeryView/?id=' . $id)
                                                        ?>">&nbsp;&nbsp;
                                                                                                                                                                                                                                      <?php
                                                                                                                                                                                                                                      echo date("d-m-Y", strtotime($surgeryrow->date));
                                                                                                                                                                                                                                      ?> </a></label>

                                                </div>  

                                                <?php
                                                $queryallfracture = $this->db->query("SELECT ortho_fracture.fracture_side,ortho_fracture.surgical_approch,ortho_fracture.fracture_type, ortho_fracture.id,ortho_fracture.fracture_id  FROM ortho_fracture JOIN ortho_surgery ON ortho_surgery.id = ortho_fracture.surgery_id WHERE ortho_surgery.surgery_id = '$surgeryrow->surgery_id' AND ortho_surgery.case_id='$case_id'");
                                                $resultallfracture = $queryallfracture->result();



                                                if (sizeof($resultallfracture)):
                                                    ?>
                                                    <div id="allfracture">
                                                        <?php
                                                        foreach ($resultallfracture as $fracturerow):
                                                            ?>

                                                            <div class="form-group" style="margin: 0;padding: 0" id="newfracture" >
                                                                <label for="fracture" class="control-label col-lg-6 col-md-6  col-sm-6 col-xs-6 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1" style="font-weight: bold;text-align: left;color: #000">Fracture <?php echo $fracturerow->fracture_id; ?>:<a style="color: #000" href="<?php
                                                                    $id = $fracturerow->id;
                                                                    echo site_url('surgery_report/Research/editFractureView/?id=' . $id)
                                                                    ?>">&nbsp;&nbsp;
                                                                                                                                                                                                                                                                                                                     <?php
                                                                                                                                                                                                                                                                                                                     $imgfractureid = $fracturerow->id;
                                                                                                                                                                                                                                                                                                                     $query = $this->db->query("SELECT image_name FROM ortho_image_fracture WHERE fracture_id='$imgfractureid'");
                                                                                                                                                                                                                                                                                                                     $imgnum = 0;
                                                                                                                                                                                                                                                                                                                     if ($query->num_rows() > 0):
                                                                                                                                                                                                                                                                                                                         $allimgname = $query->row()->image_name;
                                                                                                                                                                                                                                                                                                                         if ($allimgname != ""):
                                                                                                                                                                                                                                                                                                                             $imgnum = substr_count($allimgname, ",");
                                                                                                                                                                                                                                                                                                                             $imgnum = $imgnum + 1;
                                                                                                                                                                                                                                                                                                                         endif;
                                                                                                                                                                                                                                                                                                                     endif;
                                                                                                                                                                                                                                                                                                                     echo $imgnum . " X-Rays (" . $fracturerow->fracture_side . "," . $fracturerow->surgical_approch . "," . $fracturerow->fracture_type . ")";
                                                                                                                                                                                                                                                                                                                     ?></a></label>
                                                                <input type="hidden" id="fracture_id" name="fracture_id" value="<?php echo $fracturerow->id; ?>" />

                                                            </div>

                                                            <?php
                                                            $queryallfollowup = $this->db->query("SELECT *  FROM ortho_followup WHERE fracture_id = '$fracturerow->id'");
                                                            $resultallfollowup = $queryallfollowup->result();
                                                            if (sizeof($resultallfollowup)):
                                                                ?>

                                                                <div id="allfollowup">
                                                                    <?php
                                                                    foreach ($resultallfollowup as $followuprow):
                                                                        ?>
                                                                        <div class="form-group" style="margin: 0;padding: 0" id="newfracture" >
                                                                            <label for="fracture" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-3 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2" style="font-weight: bold;text-align: left;color: #000">Followup <?php echo $followuprow->followup_id; ?>:<a style="color: #000" href="<?php
                                                                                $id = $followuprow->id;
                                                                                echo site_url('surgery_report/Research/editFollowupView/?id=' . $id)
                                                                                ?>">&nbsp;&nbsp;
                                                                                                                                                                                                                                                                                                                                 <?php
                                                                                                                                                                                                                                                                                                                                 $imgfollowupid = $followuprow->id;
                                                                                                                                                                                                                                                                                                                                 $query = $this->db->query("SELECT image_name FROM ortho_image_followup WHERE followup_id='$imgfollowupid'");
                                                                                                                                                                                                                                                                                                                                 $imgnum = 0;
                                                                                                                                                                                                                                                                                                                                 if ($query->num_rows() > 0):
                                                                                                                                                                                                                                                                                                                                     $allimgname = $query->row()->image_name;
                                                                                                                                                                                                                                                                                                                                     if ($allimgname != ""):
                                                                                                                                                                                                                                                                                                                                         $imgnum = substr_count($allimgname, ",");
                                                                                                                                                                                                                                                                                                                                         $imgnum = $imgnum + 1;
                                                                                                                                                                                                                                                                                                                                     endif;
                                                                                                                                                                                                                                                                                                                                 endif;
                                                                                                                                                                                                                                                                                                                                 echo $imgnum . " X-Rays (" . date("d-m-Y", strtotime($followuprow->date)) . ")";
                                                                                                                                                                                                                                                                                                                                 ?></a></label>
                                                                            <input type="hidden" id="followup_id" name="followup_id" value="<?php echo $followuprow->id; ?>" />
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php else:
                                                    ?>
                                                <?php
                                                endif;
                                                ?>
                                            </div>
                                            <hr/>
                                            <?php
                                        endforeach;
                                        ?>
                                        <?php
                                    endif;
                                    ?>

                                </div>                                     
                            </form>
                        </div>
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>




</script>



