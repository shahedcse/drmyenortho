
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                      Fracture Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  
                        <div class="form" style="padding-bottom: 20px;">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 5px;">
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-5" style="font-weight: bold;color: #167F52;">Patient Name :<span style="font-weight:normal;color: #000;"><?php
                                            $patientId = $orthocase->patient_id;
                                            $query = $this->db->query("SELECT username FROM user WHERE id='$patientId'");
                                            if (sizeof($query->row()) > 0):
                                                echo $query->row()->username;
                                            else:
                                                echo "";
                                            endif;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Case Number :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->case_num ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Date of Injury :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthocase->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-5" style="font-weight: bold;color: #167F52;">Date of Surgery :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthosurgery->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Case Id :<span  style="font-weight:normal;color: #000;"> <?php echo $orthocase->id ?></span></label>                                  
                                </div>
                            </form>
                        </div> 
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addFractureForm" name="addFractureForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/editFractureData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px;" >
                                    <label for="surgeonname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Surgeon(s)</label>

                                    <?php
                                    $surgeons_id = $orthosurgery->surgeon_id;
                                    $surgeon_name = "";
                                    $len = substr_count($surgeons_id, ',');
                                    $originalId = explode(",", $surgeons_id);
                                    for ($i = 0; $i <= $len; $i++) {
                                        $id = $originalId[$i];
                                        $queryName = $this->db->query("SELECT name FROM ortho_surgeon WHERE id='$id'");
                                        $surgeon_name .= $queryName->row()->name . "&nbsp&nbsp&nbsp&nbsp";
                                    }
                                    echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $surgeon_name . '</label>';
                                    ?>                                         

                                    <input type="hidden" name="surgery_id" value="<?php echo $orthosurgery->id; ?>"/>
                                    <input type="hidden" name="id_fracture" value="<?php echo $id_fracture; ?>"/>
                                </div>



                                <hr/>
                                <div class="form-group">
                                    <label for="fractureside" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Fracture Side</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->fracture_side . '</label>';
                                        ?> 


                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="surgicalapp" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Surgical Approach</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->surgical_approch . '</label>';
                                        ?> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="locfrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Location Of Fracture</label>

                                    <div class="col-lg-7"> 
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->fracture_location . '</label>';
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="typefrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Type Of Fracture</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->fracture_type . '</label>';
                                        ?> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="stafrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Stability Of Fracture</label>

                                    <div class="col-lg-7">                             
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->fracture_stability . '</label>';
                                        ?> 

                                    </div>
                                </div> 
                                <div class="form-group" id="timeDebridement" style="display: none">
                                    <label for="timeDebridement" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Time From Injury To Debridement</label>
                                    <div class="col-lg-2">
                                        <?php
                                        $durationinjury = $orthofracture->time_injury_debrid;
                                        if ($durationinjury < 24):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationinjury . " Hours" . '</label>';
                                        endif;
                                        if ($durationinjury >= 24):
                                            $durationinjury = $durationinjury / 24;
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationinjury . " Days" . '</label>';
                                        endif;
                                        ?>                                         
                                    </div>

                                </div>
                                <div class="form-group" id="timeSkinClouser" style="display: none">
                                    <label for="timeSkinClouserDay" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Time From Injury To Skin Clouser</label>                              
                                    <div class="col-lg-2">                                    
                                        <?php
                                        $durationClouser = $orthofracture->time_injury_skin;
                                        $durationClouser = $durationClouser / 24;
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationClouser . " Days" . '</label>';
                                        ?> 
                                    </div>                                                                    
                                </div>
                                <div class="form-group" id="woundClosure" style="display: none">
                                    <label for="stafrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Wound Closure</label>

                                    <div class="col-lg-7">                             
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->wound_closure_method . '</label>';
                                        ?>  
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label for="nonunion" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nonunion</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofracture->nonunion == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Yes" . '</label>';
                                        endif;

                                        if ($orthofracture->nonunion == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "No" . '</label>';
                                        endif;
                                        ?>                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="preimplant" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Previous Implant Used</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofracture->previous_implant_used == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Yes" . '</label>';
                                        endif;

                                        if ($orthofracture->previous_implant_used == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "No" . '</label>';
                                        endif;
                                        ?>                          
                                    </div>
                                </div>
                                <div class="form-group" id="preimplantCheck" style="display: none">                                

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2">                             
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->previous_implant_type . '</label>';
                                        ?>                                      
                                    </div>
                                </div>
                                <div class="form-group" id="fixationPlace" style="display: none">
                                    <label for="timeExFixationDay" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >How Long Was The External Fixation In Place?</label>                              
                                    <div class="col-lg-2">                                    
                                        <?php
                                        $durationEx = $orthofracture->ext_fix_duration;
                                        $durationEx = $durationEx / 24;
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationEx . " Days" . '</label>';
                                        ?> 
                                    </div>                                                                    
                                </div>
                                <div class="form-group" id="fixationSign" style="display: none">
                                    <label for="timeRemovalExFixationDay" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Time Between Removal Of External Fixation And SIGN?</label>                              
                                    <div class="col-lg-2">                                    
                                        <?php
                                        $removalEx = $orthofracture->remvoe_ext_fix_sign;
                                        $removalEx = $removalEx / 24;
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $removalEx . " Days" . '</label>';
                                        ?>
                                    </div>                                                                    
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->method_of_reaming . '</label>';
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="fracreduc" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Fracture Reduction</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofracture->fracture_reduction == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "open" . '</label>';
                                        endif;

                                        if ($orthofracture->fracture_reduction == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "closed" . '</label>';
                                        endif;
                                        ?>                                               
                                    </div>
                                </div>                      
                                <hr/>
                                <div class="form-group ">
                                    <label for="comments" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Comments</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->comments . '</label>' ?>
                                    </div>
                                </div>



                                <!--                            </form>
                                                        </div>-->

                                <h6 class="panel-heading" style="background-color: #21BBC7">
                                    Enter Nail Information
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="nailInfoForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureData');                                           ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="" id="typenaildiv">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Type Of Nail Used</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofracture->type_of_nail_used == "standard nail"): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "standard nail" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->type_of_nail_used == "standard fin nail"): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "standard fin nail" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->type_of_nail_used == "pediatric fin nail"): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "pediatric fin nail" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->type_of_nail_used == "standard hip nail"): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "standard hip nail" . '</label>';
                                        endif;
                                        ?>
                                        <?php
                                        if ($orthofracture->type_of_nail_used == "fin hip nail"): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "fin hip nail" . '</label>';
                                        endif;
                                        ?> 
                                    </div>
                                </div>

                                <div class="form-group" id="l_nail" style="display: none">
                                    <label for="length_nail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Length Of Nail </label>
                                    <div class="col-lg-2">
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->length_of_nail . '</label>';
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group" id="d_nail"  style="display: none">
                                    <label for="diameter_nail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Diameter Of Nail </label>
                                    <div class="col-lg-2">
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->diameter_of_nail . '</label>';
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group" id="p_weight"  style="display: none;" >
                                    <label for="patientweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Patient Weight</label>
                                    <div class="col-lg-2">
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofracture->patient_weight . " Kg" . '</label>';
                                        ?> 
                                    </div>
                                </div>
                                <!--                            </form>
                                                        </div>-->


                                <h6 class="panel-heading" style="background-color: #21BBC7">
                                    Enter Screw Information
                                </h6>
                                <!--                       <div class="form">
                                                            <form class="form-horizontal table-bordered" id="screwInfoForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureData');                                           ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="" id="screwnaildiv">
                                    <label for="statusscrew" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Did You Use Screw With This Nail?</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofracture->screw_with_nail == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->screw_with_nail == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>                        
                                    </div>
                                </div>
                                <div class="form-group" id="si_screw"  style="display: none">
                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table  table-bordered" id="editable-sample">
                                            <thead>
                                                 <tr>
                                                    <th colspan="12">Standard Interlocking Screws</th> 
                                                </tr>
                                                <tr>
                                                    <th>Length in mm</th>
                                                    <th>25</th>
                                                    <th>30</th>
                                                    <th>35</th>
                                                    <th>40</th>
                                                    <th>45</th>
                                                    <th>50</th>
                                                    <th>55</th>
                                                    <th>60</th>
                                                    <th>65</th>
                                                    <th>70</th>
                                                    <th>75</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td># Of Proximal</td>
                                                    <td><input type="text" id="proximal25" name="proximal25" value="<?php echo $orthofracture->sis_25_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal30" name="proximal30" value="<?php echo $orthofracture->sis_30_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal35" name="proximal35" value="<?php echo $orthofracture->sis_35_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal40" name="proximal40" value="<?php echo $orthofracture->sis_40_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal45" name="proximal45" value="<?php echo $orthofracture->sis_45_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal50" name="proximal50" value="<?php echo $orthofracture->sis_50_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal55" name="proximal55" value="<?php echo $orthofracture->sis_55_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal60" name="proximal60" value="<?php echo $orthofracture->sis_60_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal65" name="proximal65" value="<?php echo $orthofracture->sis_65_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal70" name="proximal70" value="<?php echo $orthofracture->sis_70_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="proximal75" name="proximal75" value="<?php echo $orthofracture->sis_75_prox; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                </tr>  
                                                <tr class="">
                                                    <td># Of Distal</td>
                                                    <td><input type="text" id="distal25" name="distal25" value="<?php echo $orthofracture->sis_25_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal30" name="distal30" value="<?php echo $orthofracture->sis_30_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal35" name="distal35" value="<?php echo $orthofracture->sis_35_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal40" name="distal40" value="<?php echo $orthofracture->sis_40_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal45" name="distal45" value="<?php echo $orthofracture->sis_45_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal50" name="distal50" value="<?php echo $orthofracture->sis_50_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal55" name="distal55" value="<?php echo $orthofracture->sis_55_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal60" name="distal60" value="<?php echo $orthofracture->sis_60_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal65" name="distal65" value="<?php echo $orthofracture->sis_65_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal70" name="distal70" value="<?php echo $orthofracture->sis_70_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="distal75" name="distal75" value="<?php echo $orthofracture->sis_75_dist; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                </tr>
                                            </tbody>
                                        </table>                          
                                    </div>
                                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-4 col-md-offset-4 col-sm-offset-4 col-xs-offset-4">
                                         <img src = '<?php echo $baseurl . "/assets/uploads/xray/screw.PNG"; ?>' alt = '' width="300" height="100" />
                                     </div>
                                </div>

                                <div class="form-group"  id="c_screw"  style="display: none" >
                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table  table-bordered" id="editable-sample">
                                            <thead>
                                                <th colspan="13">Compression Screws</th> 
                                                <tr>
                                                    <th>Length in mm</th>
                                                    <th>60</th>
                                                    <th>65</th>
                                                    <th>70</th>
                                                    <th>75</th>
                                                    <th>80</th>
                                                    <th>85</th>
                                                    <th>90</th>
                                                    <th>95</th>
                                                    <th>100</th>
                                                    <th>105</th>
                                                    <th>110</th>
                                                    <th>115</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>Qty</td>
                                                    <td><input type="text" id="csqty60" name="csqty60" value="<?php echo $orthofracture->cs_60; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty65" name="csqty65" value="<?php echo $orthofracture->cs_65; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty70" name="csqty70" value="<?php echo $orthofracture->cs_70; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty75" name="csqty75" value="<?php echo $orthofracture->cs_75; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty80" name="csqty80" value="<?php echo $orthofracture->cs_80; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty85" name="csqty85" value="<?php echo $orthofracture->cs_85; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty90" name="csqty90" value="<?php echo $orthofracture->cs_90; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty95" name="csqty95" value="<?php echo $orthofracture->cs_95; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty100" name="csqty100" value="<?php echo $orthofracture->cs_100; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty105" name="csqty105" value="<?php echo $orthofracture->cs_105; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty110" name="csqty110" value="<?php echo $orthofracture->cs_110; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="csqty115" name="csqty115" value="<?php echo $orthofracture->cs_115; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                </tr>  

                                            </tbody>
                                        </table>                          
                                    </div>
                                </div>

                                <div class="form-group"  id="shcp_screw"  style="display: none">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table  table-bordered" id="editable-sample">
                                            <thead>
                                                  <th colspan="9">SHC Proximal Interlocking Screws</th> 
                                                <tr>
                                                    <th>Length in mm</th>
                                                    <th>60</th>
                                                    <th>65</th>
                                                    <th>70</th>
                                                    <th>75</th>
                                                    <th>80</th>
                                                    <th>85</th>
                                                    <th>90</th>
                                                    <th>95</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>Qty</td>
                                                    <td><input type="text" id="shcpqty60" name="shcpqty60" value="<?php echo $orthofracture->shc_pis_60; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shcpqty65" name="shcpqty65" value="<?php echo $orthofracture->shc_pis_65; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shcpqty70" name="shcpqty70" value="<?php echo $orthofracture->shc_pis_70; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shcpqty75" name="shcpqty75" value="<?php echo $orthofracture->shc_pis_75; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shcpqty80" name="shcpqty80" value="<?php echo $orthofracture->shc_pis_80; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shcpqty85" name="shcpqty85" value="<?php echo $orthofracture->shc_pis_85; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shcpqty90" name="shcpqty90" value="<?php echo $orthofracture->shc_pis_90; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shcpqty95" name="shcpqty95" value="<?php echo $orthofracture->shc_pis_95; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>

                                                </tr>  

                                            </tbody>
                                        </table>                          
                                    </div>
                                </div>

                                <div class="form-group"  id="shcc_screw"  style="display: none">
                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">                             
                                        <table class="table  table-bordered" id="editable-sample">
                                            <thead>
                                                 <th colspan="5">SHC Cortical Screws</th> 
                                                <tr>
                                                    <th>Length in mm</th>

                                                    <th>30</th>
                                                    <th>35</th>
                                                    <th>40</th>
                                                    <th>45</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="">
                                                    <td>Qty</td>
                                                    <td><input type="text" id="shccqty30" name="shccqty30" value="<?php echo $orthofracture->shc_sc_30; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shccqty35" name="shccqty35" value="<?php echo $orthofracture->shc_sc_35; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shccqty40" name="shccqty40" value="<?php echo $orthofracture->shc_sc_40; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>
                                                    <td><input type="text" id="shccqty45" name="shccqty45" value="<?php echo $orthofracture->shc_sc_45; ?>" class="form-control small" style="padding: 0" maxlength="1"  readonly ></td>

                                                </tr>  

                                            </tbody>
                                        </table>                          
                                    </div>
                                </div>
                                <hr/>



                                <!--                            </form>
                                                        </div>-->

                                <h6 class="panel-heading" style="background-color: #21BBC7">
                                    Enter Plate Components Used
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="plateInfoForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureData');                                           ?>" enctype="multipart/form-data">-->
                                <div class="form-group " style="" id="platesuseddiv">
                                    <label for="platesused" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Plates Used</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->plate_used == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->plate_used == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="one">
                                    <label for="rpu" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Rod Plate Used</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->rod_plate == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->rod_plate == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="two">
                                    <label for="rc" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Rod Connector</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->rod_connector == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->rod_connector == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="three">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Unicortical Screw</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->unicort_screw == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofracture->unicort_screw == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="four">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >3 Hole Plate 75 mm</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->hole_plate3 == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>
                                        <?php
                                        if ($orthofracture->hole_plate3 == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="five">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Hv 2 Hole Plate</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->hv_2hole == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>
                                        <?php
                                        if ($orthofracture->hv_2hole == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="six">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Hv 3 Hole Plate</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->hv_3hole == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>
                                        <?php
                                        if ($orthofracture->hv_3hole == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group " style="display: none" id="seven">
                                    <label for="typenail" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Hv 4 Hole Plate</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                             
                                        <?php
                                        if ($orthofracture->hv_4hole == 1): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofracture->hv_4hole == 0): echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <!--                            </form>
                                                        </div>-->

                                <h6 class="panel-heading" style="background-color: #21BBC7">
                                    X-Ray Images
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="xRayImageForm" name="xRayImageForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureXrayImage');                                        ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="display: block;"  id="img">
                                    <div class="col-lg-11 col-lg-offset-1" id="xrayimagearea">

                                    </div>
                                </div>


                                <!--                                        end formgroup-->

                                <hr/>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-9">
                                        <a style="color: #000" href="<?php echo site_url('surgery_report/Research'); ?>"> <button class="btn " style="background: #21BBC7" type="button">Close</button></a> 
                                    </div>
                                </div>
                            </form>
                        </div>                                                        
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>

    var allimgname = '';
    var allimgtype = '';
    var allimgdate = '';
    var length;
    var i;
    var sepimgname, sepimgtype, sepimgdate, sepyearmonthday, yearfrac, monthfrac, dayfrac;
    var imgname = '';
    var imgtype = '';
    var imgdate = '';
    var count = 0;
    var selectedif, selectedelse, selectedday, selectedmonth, selectedyear;
    var allimgfracture = "<?php echo sizeof($orthoimgfracture); ?>";
    if (allimgfracture > 0) {
        allimgname = "<?php echo $orthoimgfracture->image_name; ?>";
        allimgtype = "<?php echo $orthoimgfracture->image_type; ?>";
        allimgdate = "<?php echo $orthoimgfracture->date; ?>";
        sepimgname = allimgname.split(",");
        sepimgtype = allimgtype.split(",");
        sepimgdate = allimgdate.split(",");
        length = '<?php echo substr_count($orthoimgfracture->image_name, ",") ?>';
       if (allimgname != "") {
       for (i = 0; i <= length; i++) {
            imgname = sepimgname[i];
            imgtype = sepimgtype[i];
            imgdate = sepimgdate[i];
            if (imgdate == '') {
                yearfrac = '';
                monthfrac = '';
                dayfrac = '';
            } else {
                sepyearmonthday = imgdate.split("-");
                yearfrac = sepyearmonthday[0];
                monthfrac = sepyearmonthday[1];
                dayfrac = sepyearmonthday[2];
            }

            var loc = "<?php echo $baseurl . "/assets/uploads/xray/"; ?>" + imgname;
            if (imgtype == 1) {
                selectedif = 'pre-op';
                selectedelse = '';
            } else if (imgtype == '') {
                selectedelse = 'selected';
                selectedif = '';
            } else {
                selectedif = 'post-op';
            }
            //dynamic div start
            count++;
            var newxrayimagediv = "";
            var selectday = "";
            var selectimg = "";
            var selectop = "";
            var imgnum = "";
            var selectmonth = "";
            var selectyear = "";
            var btndelete = "";
            newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + count + "'>";
            selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '" + loc + "' alt = '' /> \n\
<input type = 'hidden' name='xrayimgname" + count + "' value ='" + imgname + "'/>\n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
<input type = 'hidden' name='opCount' value ='" + count + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";
            imgnum = "<div class = 'col-lg-10 col-lg-offset-2'>\n\
         <span style='color:white'>X-Ray  #" + count + "</span>\n\
</select>\n\
</div>";
            selectop = "<div class = 'col-lg-12'>\n\
            <span style='color:white'>Operation  #" + selectedif + "</span>\n\
</div>";
            selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-12 '><span style='color:white'>Date #" + dayfrac + "-" + monthfrac + "-" + yearfrac + "</span></div>";
//            selectmonth = "<div class = 'col-lg-2 '><span style='color:white'>" + monthfrac + "</span></div>";
//            selectyear = "<div class='col-lg-2 '><span style='color:white'>" + yearfrac + "</span></div>";
            selectyear += "</div>";
            newxrayimagediv += selectimg;
            newxrayimagediv += imgnum;
            newxrayimagediv += selectop;
            newxrayimagediv += selectday;
            newxrayimagediv += selectmonth;
            newxrayimagediv += selectyear;
            newxrayimagediv += "</div>";
            $("#xrayimagearea").append(newxrayimagediv);
            //dynamic div end

            $("#month" + count).val(monthfrac);
        }    
    }
        
    }


    function typeFrac(val) {

        if (val == "closed") {

            $("#timeDebridement").hide();
            $("#timeSkinClouser").hide();
            $("#woundClosure").hide();
        } else {

            $("#timeDebridement").show();
            $("#timeSkinClouser").show();
            $("#woundClosure").show();
        }
    }
    function hidePreImplant(val) {
        if (val == "1") {
            $("#preimplantCheck").show();
        } else {
            $("#preimplantCheck").hide();
        }
    }



    function hideExFixation(val) {
        if (val == "externalfixation") {
            $("#fixationPlace").show();
            $("#fixationSign").show();
        }
        else {
            $("#timeExFixationDay").val("");
            $("#timeRemovalExFixationDay").val("");
            $("#fixationPlace").hide();
            $("#fixationSign").hide();
        }
    }

    function resetField(id) {
        var caseId = id;
        switch (caseId) {
            case 'timeDebridementDay':
                $("#timeDebridementHour").val("");
                break;
            case 'timeDebridementHour':
                $("#timeDebridementDay").val("");
                break;
            default:
        }
    }

    function hideScrewInfo(val) {
        var caseVal = val;
        if (caseVal == '1') {
            $("#si_screw").show();
            $("#c_screw").show();
            $("#shcp_screw").show();
            $("#shcc_screw").show();
        } else {
            $("#si_screw").hide();
            $("#c_screw").hide();
            $("#shcp_screw").hide();
            $("#shcc_screw").hide();
        }
    }

    function hidePlate(val) {
        var caseVal = val;
        if (caseVal == '1') {
            $("#one").show();
            $("#two").show();
            $("#three").show();
            $("#four").show();
            $("#five").show();
            $("#six").show();
            $("#seven").show();
        } else {

            $("#one").hide();
            $("#two").hide();
            $("#three").hide();
            $("#four").hide();
            $("#five").hide();
            $("#six").hide();
            $("#seven").hide();
        }
    }


    function hideNailInfo(val) {
        var caseVal = val;
        switch (caseVal) {
            case 'standard nail':
                $("#l_nail").show();
                $("#d_nail").show();
                $("#p_weight").hide();
                break;
            case 'standard fin nail':
                $("#l_nail").show();
                $("#d_nail").show();
                $("#p_weight").hide();
                break;
            case 'pediatric fin nail':
                $("#l_nail").show();
                $("#d_nail").show();
                $("#p_weight").show();
                break;
            case 'standard hip nail':
                $("#l_nail").show();
                $("#d_nail").show();
                $("#p_weight").hide();
                break;
            case 'fin hip nail':

                $("#l_nail").hide();
                $("#d_nail").hide();
                $("#p_weight").hide();
                break;
            default:
        }
    }

    function getDiameterNailInfo(lVal) {

        var typeNail = $('input[name=typenail]:checked', '#addFractureForm').val();
        if (typeNail == "standard nail") {
            switch (lVal) {
                case '190 mm':

                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '200 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '220 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '240 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '260 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '280 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '300 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '320 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '340 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '360 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '380 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '400 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '420 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                default:
            }

        } else if (typeNail == "standard fin nail") {
            switch (lVal) {
                case '160 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '190 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '240 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '280 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>\n\
<option value="11 mm">11 mm</option>\n\
<option value="12 mm">12 mm</option>');
                    break;
                case '320 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="8 mm">8 mm</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                case '340 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                case '360 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="9 mm">9 mm</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                default:
            }

        } else if (typeNail == "pediatric fin nail") {
            switch (lVal) {
                case '140 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '170 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '200 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                case '240 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="6 mm">6 mm</option>\n\
<option value="7 mm">7 mm</option>\n\
<option value="8 mm">8 mm</option>');
                    break;
                default:
            }

        } else if (typeNail == "standard hip nail") {
            switch (lVal) {
                case '280 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                case '320 mm':
                    $('#diameter_nail')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">--Select--</option>\n\
<option value="10 mm">10 mm</option>');
                    break;
                default:
            }
        }
    }

    $(document).ready(function () {



        typeFrac('<?php echo $orthofracture->fracture_type ?>');
        hidePreImplant('<?php echo $orthofracture->previous_implant_used ?>');
        var previous_implant_type = '<?php echo $orthofracture->previous_implant_type ?>';
        var sep_previous_implant_type = previous_implant_type.split(",");
        var exdata = sep_previous_implant_type[0];
        hideExFixation(exdata);
        hideNailInfo('<?php echo $orthofracture->type_of_nail_used ?>');
        hideScrewInfo('<?php echo $orthofracture->screw_with_nail ?>');
        hidePlate('<?php echo $orthofracture->plate_used ?>');
//        hideScrewInfo($('input[name=screwnail]:checked', '#screwnaildiv').val());

    });
    function hideOther() {
        if (document.getElementById('other').checked) {
            $("#otherval").show();
        } else {
            $("#otherval").hide();
        }
    }

</script>



