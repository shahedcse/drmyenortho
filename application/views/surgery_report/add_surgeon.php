
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Enter Surgeon Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->                    
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('surgery_report/Surgeon/addSurgeonData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px" >
                                    <label for="surgeonname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Name<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class=" form-control" id="surgeonname" name="surgeonname" type="text" required/>
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label for="email" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Email</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class=" form-control" id="email" name="email" type="email" />
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label for="hospitalname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Hospital<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class=" form-control" id="hospitalname" name="hospitalname" type="text" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="day" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Act. Date<span style="color: red"> *</span></label>
                                    <div class="col-lg-4 col-md-6  col-sm-8 col-xs-7">
<!--                                                                        <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="day" id="day" required>
                                                <option selected="selected" value="">Day</option>
                                                <?php
                                                for ($day = 1; $day <= 31; $day++):
                                                    ?>
                                                    <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="month" id="month" required>
                                                <option selected="selected" value="">Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="year" id="year" required>
                                                <option selected="selected" value="">Year</option>
                                                <?php
                                                $currentyr = date('Y');
                                                for ($year = 1970; $year <= $currentyr+10; $year++):
                                                    ?>
                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Status<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="status" id="status" value="1" type="radio" required  /> Active &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="status" id="status" value="0" type="radio" /> Inactive                           
                                    </div>
                                </div>
                                 <hr/> 
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-lg-9 col-md-9  col-sm-9 col-xs-9">
                                        <button class="btn "  style="background: #21BBC7" type="submit">&nbsp;&nbsp;Save & Continue&nbsp;&nbsp;</button>  
                                        <a href="<?php echo site_url('surgery_report/Surgeon'); ?>"> <button class="btn "  style="background: #21BBC7;color: #000" type="button">Cancel</button></a> 
                                    </div>
                                </div>
                            </form>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>

    

</script>



