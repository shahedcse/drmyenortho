
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Surgery Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  
                        <div class="form" style="padding-bottom: 20px;">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 5px;">
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-3" style="font-weight: bold;color: #167F52;">Patient Name :<span style="font-weight:normal;color: #000;"><?php
                                            $patientId = $orthocase->patient_id;
                                            $query = $this->db->query("SELECT username FROM user WHERE id='$patientId'");
                                            if (sizeof($query->row()) > 0):
                                                echo $query->row()->username;
                                            else:
                                                echo "";
                                            endif;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" style="font-weight: bold;color: #167F52;">Case Number :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->case_num ?></span></label>
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-3" style="font-weight: bold;color: #167F52;">Date of Injury :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthocase->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" style="font-weight: bold;color: #167F52;">Case Id :<span  style="font-weight:normal;color: #000;"> <?php echo $orthocase->id ?></span></label>                                  
                                </div>
                            </form>
                        </div> 
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/editSurgeryData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px;">
                                    <label for="birthday" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" >Surgery Date </label>
                                    <div class="col-lg-2 col-md-2  col-sm-2 col-xs-3">
<!--                                                                        <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                        <input type="hidden" name="case_id" value="<?php echo $orthocase->id ?>"/>
                                        <input type="hidden" name="id_surgery" value="<?php echo $id_surgery; ?>"/>
                                        <?php
                                        $databaseDate = $surgerycase->date;
                                        $originalDate = explode("-", $databaseDate);
                                        $yearSurgery = $originalDate[0];
                                        $monthSurgery = $originalDate[1];

                                        $daySurgery = $originalDate[2];


                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;">' . $daySurgery . "-" . $monthSurgery . "-" . $yearSurgery . '<label>';
                                        ?>



                                    </div>
                                </div>



                                <hr/>
                                <div class="form-group" >
                                    <label for="surgeonname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" >Surgeon </label>
                                    <div class="col-md-8">
                                        <?php
                                        $namearr=array();
                                        $allsurgeon_id = $surgerycase->surgeon_id;
                                        $len=  substr_count($allsurgeon_id, ",");
                                        $sepsurgeon_id=  explode(",", $allsurgeon_id);
                                        for($i=0;$i<=$len;$i++):
                                            $querysurgeonname=$this->db->query("SELECT name FROM ortho_surgeon WHERE id='$sepsurgeon_id[$i]'");
                                        $namearr[]=$querysurgeonname->row()->name;
                                        endfor;
                                        $surgeonname=implode(',', $namearr);
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $surgeonname . '</label>';
                                        ?>
                                    </div>

                                </div>

                                <hr/>
                                <div class="form-group" id="antibioticDiv">
                                    <label for="sex" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" >Antibiotics Used?</label>
                                    <div class="col-lg-2 col-md-2  col-sm-2 col-xs-3">                             
                                        <?php
                                        if ($surgerycase->antibiotics == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;">No</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($surgerycase->antibiotics == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;">Yes</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($surgerycase->antibiotics == 2):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;">No,Unknown</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($surgerycase->antibiotics == 3):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;">Yes,Unknown</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>
                                <div class="form-group" id="antibiotics_after_hours_days" style="display: none">
                                    <label for="antibiotics_after_hours_days" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" >How Long From Time Of Injury</label>
                                    <?php
                                    $durationinjury = $surgerycase->antibiotics_after_hours;
                                    $durationcoverage = $surgerycase->antibiotics_coverage;
                                    ?>

                                    <div class="col-lg-2">                                    
                                        <?php
                                        if ($durationinjury < 24):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationinjury . " Hours" . '</label>';
                                        endif;
                                        if ($durationinjury >= 24):
                                            $durationinjury = $durationinjury / 24;
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationinjury . " Days" . '</label>';
                                        endif;
                                        ?>
                                    </div>                                 
                                </div>
                                <div class="form-group" id="antibiotics_name" style="display: none">
                                    <label for="antibiotics_name" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" >Name Of Antibiotic</label>
                                    <div class="col-lg-2 col-md-2  col-sm-2 col-xs-3">
                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $surgerycase->antibiotics_name . '</label>'; ?>                                       
                                    </div>                                                                    
                                </div>
                                <div class="form-group" id="antibiotics_coverage_hours_days" style="display: none">
                                    <label for="antibiotics_coverage_hours_days" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" >Duration Of Antibiotic Coverage</label>

                                    <div class="col-lg-2">                                    
                                        <?php
                                        if ($durationcoverage <= 24):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationcoverage . " Hours" . '</label>';
                                        endif;
                                        if ($durationcoverage > 24):
                                            $durationcoverage = $durationcoverage / 24;
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $durationcoverage . " Days" . '</label>';
                                        endif;
                                        ?>
                                    </div>                                                                    
                                </div>
                                <hr/>
                                <div class="form-group ">
                                    <label for="comments" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-3" >Comments</label>
                                    <div class="col-lg-2 col-md-2  col-sm-2 col-xs-3">
                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $surgerycase->comments . '</label>'; ?>
                                    </div>
                                </div>
                                <hr/>                         
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-9">

                                        <a style="color: #000" href="<?php echo site_url('surgery_report/Research'); ?>"> <button class="btn " style="background: #21BBC7" type="button">Close</button></a> 
                                    </div>
                                </div>
                            </form>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>





    $(document).ready(function () {
        var val = '<?php echo $surgerycase->antibiotics; ?>';
        hideAntibiotics(val);

    });



    function hideAntibiotics(val) {
        if (val == "1") {

            $("#antibiotics_after_hours_days").show();
            $("#antibiotics_name").show();
            $("#antibiotics_coverage_hours_days").show();
        } else {


            $("#antibiotics_after_hours_days").hide();
            $("#antibiotics_name").hide();
            $("#antibiotics_coverage_hours_days").hide();

        }
    }




</script>



