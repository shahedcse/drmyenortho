
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Enter Surgical Code Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->                    
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCode/addCodeData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px" >
                                    <label for="codenumber" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Code<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class=" form-control" id="codenumber" name="codenumber" type="text" required/>
                                    </div>
                                </div>

                                <div class="form-group" >
                                    <label for="type" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Type<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="type" id="type" required>
                                            <option selected="selected" value="">--Select--</option>
                                            <option value="fracture">Fracture</option>
                                            <option value="surgery">Surgery</option>
                                            <option value="followup">Followup</option>                                                                                          
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="description" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Code Description</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class="form-control " id="description" type="text" name="description"/>
                                    </div>
                                </div>                                   
                                <hr/>                         
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-lg-9 col-md-9  col-sm-9 col-xs-9">
                                        <button class="btn "  style="background: #21BBC7" type="submit">Save & Continue</button> 
                                          <a href="<?php echo site_url('surgery_report/SurgeryCode'); ?>"> <button class="btn "  style="background: #21BBC7;color: #000" type="button">Cancel</button></a>   
                                    </div>
                                </div>

                            </form>
                        </div>                                                        
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>



</script>



