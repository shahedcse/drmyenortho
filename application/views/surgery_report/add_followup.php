
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Enter Followup Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  
                        <div class="form" style="padding-bottom: 20px;">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 5px;">
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-6" style="font-weight: bold;color: #167F52;">Patient Name :<span style="font-weight:normal;color: #000;"><?php
                                            $patientId = $orthocase->patient_id;
                                            $query = $this->db->query("SELECT username FROM user WHERE id='$patientId'");
                                            if (sizeof($query->row()) > 0):
                                                echo $query->row()->username;
                                            else:
                                                echo "";
                                            endif;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-6" style="font-weight: bold;color: #167F52;">Case Number :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->case_num ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-7" style="font-weight: bold;color: #167F52;">Date of Injury :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthocase->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Case Id :<span  style="font-weight:normal;color: #000;"> <?php echo $orthocase->id ?></span></label>                                  
                                </div>
                            </form>
                        </div> 
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/addFollowupData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top:20px;">
                                    <label for="birthday" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Followup Date<span style="color: red"> *</span></label>
                                    <div class="col-lg-4 col-md-6  col-sm-8 col-xs-7">

                                        <input type="hidden" name="case_id" value="<?php echo $orthocase->id ?>"/>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="day" id="day" required>
                                                <option selected="selected" value="">Day</option>
                                                <?php
                                                for ($day = 1; $day <= 31; $day++):
                                                    ?>
                                                    <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="month" id="month" required>
                                                <option selected="selected" value="">Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="year" id="year" required>
                                                <option selected="selected" value="">Year</option>
                                                <?php
                                                $currentyr = date('Y');
                                                for ($year = 1970; $year <= $currentyr + 10; $year++):
                                                    ?>
                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" <?php
                                if (sizeof($codenum) > 0):
                                    echo"style='display: block'";
                                else:
                                    echo "style='display: none'";
                                endif;
                                ?>>
                                    <label for="codenumber" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Code Number</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="codenumber" id="codenumber"  >

                                            <option selected="selected" value="">--Select Code--</option>
                                            <?php
                                            if (sizeof($codenum) > 0):
                                                foreach ($codenum as $datarow):
                                                    echo '<option value="' . $datarow->code_no . '">' . $datarow->code_no . '</option> ';
                                                endforeach;
                                            endif;
                                            ?>                                                                                          
                                        </select>
                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group">
                                    <label for="infection" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Infection<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="infection" id="infection" value="1" type="radio" onclick="hideInfection(this.value)"  required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="infection" id="infection" value="0" type="radio" onclick="hideInfection(this.value)"  /> No &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <input type="hidden" name="fracture_id" value="<?php echo $fractureid ?>"/>
                                </div>
                                <div class="form-group" id="infectionwound" style="display: none">
                                    <label for="infectionwound" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Incision of the wound?</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="infectionwound" id="infectionwound1" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="infectionwound" id="infectionwound2" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>
                                <div class="form-group" id="infectiondepth" style="display: none">
                                    <label for="infectiondepth" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Infection Depth</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="infectiondepth" id="infectiondepth1" value="1" type="radio"    /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="infectiondepth" id="infectiondepth2" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>


                                <div class="form-group" id="infectionduration" style="display: none">
                                    <label for="infectionduration" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Duration Of Infection</label>
                                    <div class="col-lg-2">
                                        <input class=" form-control" id="infectduration" name="infectionduration"  type="text" />Weeks                                          
                                    </div>                                                                                          
                                </div>
                                <div class="form-group" id="infectiontype" style="display: none">                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2">                                                                  
                                        <input id="osteomyelitis" name="osteomyelitis" value="osteomyelitis"  type="checkbox"/> Osteomyelitis
                                        <input id="amputation" name="amputation" value="amputation"  type="checkbox"/> Amputation
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label for="partialweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Partial weight bearing?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="partialweight" id="partialweight" value="1" type="radio"  required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="partialweight" id="partialweight" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="painlessweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Painless full weight bearing?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="painlessweight" id="painlessweight" value="1" type="radio"  required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="painlessweight" id="painlessweight" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="healingxray" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Healing by x-ray?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="healingxray" id="healingxray" value="1" type="radio" required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="healingxray" id="healingxray" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kneeflexion" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Knee flexion greater than 90 degrees?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="kneeflexion" id="kneeflexion" value="1" type="radio"  required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="kneeflexion" id="kneeflexion" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="screwbreakage" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Screw breakage?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="screwbreakage" id="screwbreakage" value="1" type="radio"   required /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="screwbreakage" id="screwbreakage" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="screwloosening" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Screw loosening?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="screwloosening" id="screwloosening" value="1" type="radio"   required /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="screwloosening" id="screwloosening" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nailbreakage" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nail breakage?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="nailbreakage" id="nailbreakage" value="1" type="radio"   required /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="nailbreakage" id="nailbreakage" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nailloosening" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nail loosening?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="nailloosening" id="nailloosening" value="1" type="radio"   required /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="nailloosening" id="nailloosening" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label for="deformity" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Deformity?<span style="color: red"> *</span> </label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="deformity" id="deformity" value="1" type="radio"  onclick="hideDeformity(this.value)"   required /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="deformity" id="deformity" value="0" type="radio"  onclick="hideDeformity(this.value)"  /> No &nbsp;&nbsp;&nbsp;(none over 10 degrees)
                                    </div>
                                </div>
                                <div class="form-group" id="alignment" style="display: none">  
                                    <label for="alignment" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-1" >Alignment?</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-11 col-lg-offset-2">                                                                  
                                        <input id="alignment1" name="alignment" value="10 varus"  type="radio" /> Over 10 degrees varus  &nbsp;&nbsp;&nbsp; 
                                        <input id="alignment2" name="alignment" value="20 varus"  type="radio"/> Over 20 degrees varus  &nbsp;&nbsp;&nbsp;

                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-11 col-lg-offset-2">                                                                                                       
                                        <input id="alignment3" name="alignment" value="10 valgus"  type="radio"/> Over 10 degrees valgus &nbsp;
                                        <input id="alignment4" name="alignment" value="20 valgus"  type="radio"/> Over 20 degrees valgus  &nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group" id="rotation" style="display: none">   
                                    <label for="rotation" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-1" >Rotation?</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-11 col-lg-offset-2">                                                                  
                                        <input id="rotation1" name="rotation" value="1"  type="checkbox" /> Over 30 degrees
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">  
                                    <label for="repeatsurgery" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Repeat surgery?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                                                                  
                                        <input name="repeatsurgery" id="repeatsurgery1" value="1" onclick="hideRepeatSurgery(this.value)" type="radio"  required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="repeatsurgery" id="repeatsurgery2" value="0" onclick="hideRepeatSurgery(this.value)" type="radio"  /> No &nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group" id="surgerycause" style="display: none">  
                                    <label for="repeatsurgery" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-2" style="font-style: italic;color: #000;text-align: left" >Check all That apply</label>                                  
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-11 col-lg-offset-2 ">                                                                  
                                        <input name="infectionrepeat" id="infectionrepeat" value="infection" type="checkbox"    /> For Infection &nbsp;&nbsp;&nbsp;
                                        <input name="deformityrepeat" id="deformityrepeat" value="deformity" type="checkbox"  /> For Deformity &nbsp;&nbsp;&nbsp;
                                        <input name="nonunionrepeat" id="nonunionrepeat" onclick="hideRepeatType(this.value)" value="nonunion" type="checkbox"  /> For Non-Union &nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group" id="surgerytype" style="display: none">  
                                    <label for="repeatsurgery" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-2" style="font-style: italic;color: #000;text-align: left" >Check all That apply</label>                                  
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-11 col-lg-offset-2 ">                                                                  
                                        <input name="dynamize" id="dynamize" value="dynamize" type="checkbox"    /> Dynamize &nbsp;&nbsp;&nbsp;
                                        <input name="exchangenail" id="exchangenail" value="exchangenail" type="checkbox"  /> Exchange Nail &nbsp;&nbsp;&nbsp;
                                        <input name="iliacrestbonegraft" id="iliacrestbonegraft" value="iliacrestbonegraft" type="checkbox"  /> Iliac Crest Bone Graft &nbsp;&nbsp;&nbsp;
                                        <input name="other" id="other" value="other" onclick="hideOther();" type="checkbox"  /> Other &nbsp;&nbsp;&nbsp;
                                        <input name="otherval" id="otherval"  type="text" style="display: none"  /> 
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group ">
                                    <label for="comments" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Comments</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <textarea class="form-control" id="comments" name="comments" ></textarea>
                                    </div>
                                </div>
                                <hr/>

                                <h6 class="panel-heading">
                                    Followup Images
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="xRayImageForm" name="xRayImageForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureXrayImage');                          ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                    <div class="col-lg-11 col-lg-offset-2" id="xrayimagearea">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-9">
                                        <button onclick="addnewxrayimage()" class="btn "  style="background: #21BBC7" type="button" >&nbsp;&nbsp;+ Add An X-Ray Image To This Followup&nbsp;&nbsp;</button>                               
                                    </div>
                                </div>
                                <hr/>
                                <h6 class="panel-heading" >
                                    Fracture X-Ray Images
                                </h6>                              
                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                    <div class="col-lg-11 col-lg-offset-2" id="xrayimageareafrac">
                                    </div>
                                </div>                                                           
                                <hr/>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-9">
                                        <button class="btn "  style="background: #21BBC7" type="submit">Save & Continue</button>
                                        <a href="<?php echo site_url('surgery_report/SurgeryCase/addCase'); ?>"> <button class="btn "  style="background: #21BBC7;color: #000" type="button">Cancel</button></a> 
                                    </div>
                                </div>
                            </form>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->
<script src="<?php echo $baseurl; ?>assets/assets/fancybox/source/jquery.fancybox.js"></script>
<script>
// fracture x-ray image begins here
    var allimgname = '';
    var allimgtype = '';
    var allimgdate = '';
    var length;
    var i;
    var sepimgname, sepimgtype, sepimgdate, sepyearmonthday, yearfrac, monthfrac, dayfrac;
    var imgtype = '';
    var imgname = '';
    var imgdate = '';
    var countfrac = 0;
    var selectedif, selectedelse, selectedday, selectedmonth, selectedyear, selectedop;
    var allimgfracture = "<?php echo sizeof($orthoimgfracture); ?>";

    if (allimgfracture > 0) {
        allimgname = "<?php echo $orthoimgfracture->image_name; ?>";
        allimgtype = "<?php echo $orthoimgfracture->image_type; ?>";
        allimgdate = "<?php echo $orthoimgfracture->date; ?>";

        sepimgname = allimgname.split(",");
        sepimgtype = allimgtype.split(",");
        sepimgdate = allimgdate.split(",");

        length = '<?php echo substr_count($orthoimgfracture->image_name, ",") ?>';

        for (i = 0; i <= length; i++) {
            imgname = sepimgname[i];
            imgtype = sepimgtype[i];
            imgdate = sepimgdate[i];

            sepyearmonthday = imgdate.split("-");
            yearfrac = sepyearmonthday[0];
            monthfrac = sepyearmonthday[1];
            dayfrac = sepyearmonthday[2];


            var loc = "<?php echo $baseurl . "/assets/uploads/xray/"; ?>" + imgname;
            if (imgtype == 1) {
                selectedop = '';
                selectedif = 'selected';
                selectedelse = '';
            } else if (imgtype == '') {
                selectedop = 'selected';
                selectedif = '';
                selectedelse = '';
            } else {
                selectedop = '';
                selectedelse = 'selected';
                selectedif = '';
            }
            //dynamic div start
            countfrac++;
            var newxrayimagediv = "";
            var selectday = "";
            var selectimg = "";
            var selectop = "";
            var imgnumfrac = "";
            var imgnumfollowup = "";
            var selectmonth = "";
            var selectyear = "";
            var btndelete = "";

            newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + countfrac + "'>";
            selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
<a class='fancybox' rel='group' href='" + loc + "'><img src = '" + loc + "' alt = '' /></a>\n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
</div>\n\
</div>";
            imgnumfrac = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #" + countfrac + "</span>\n\
</select>\n\
</div>";

            selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = '#" + countfrac + "'id = '#'  >\n\
            <option selected = 'selected' value = '' " + selectedop + " >Select</option>\n\
            <option value = '1' " + selectedif + " > Pre-Op </option>\n\
<option value = '0' " + selectedelse + "> Post-Op </option>\n\
</select>\n\
</div>";

            selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='#" + countfrac + "' id='#" + countfrac + "' ><option selected='selected' value=''>Day</option>";
            for (var day = 1; day <= 31; day++) {
                if (day == dayfrac) {
                    selectedday = 'selected';
                    selectday += "<option value='" + day + "'" + selectedday + ">" + day + "</option>";
                } else {
                    selectday += "<option value='" + day + "'>" + day + "</option>";
                }

            }
            selectday += "</select>";
            selectday += "</div>";

            selectmonth = "<div class = 'col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'mon" + countfrac + "' id = 'mon" + countfrac + "' >\n\
                                                                <option selected = 'selected' value = '' >Month</option>\n\
                                                                <option value = '01'> January </option>\n\
                                                                <option value = '02'> February </option>\n\
                                                                <option value = '03'> March </option>\n\
                                                                <option value = '04'> April </option>\n\
                                                                <option value = '05'> May </option>\n\
                                                                <option value = '06'> June </option>\n\
                                                                <option value = '07'> July </option>\n\
                                                                <option value = '08'> August </option>\n\
                                                                <option value = '09'> September </option>\n\
                                                                <option value = '10'> October </option>\n\
                                                                <option value = '11'> November </option>\n\
                                                                <option value = '12'> December </option>\n\
                                                                </select>\n\
                                                                </div>";


            var d = new Date();
            var n = d.getFullYear();
            selectyear = "<div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='#" + countfrac + "' id='#" + countfrac + "' ><option selected='selected' value=''>Year</option>";
            for (var year = 1970; year <= n + 10; year++) {
                if (year == yearfrac) {
                    selectedyear = 'selected';
                    selectyear += "<option value='" + year + "'>" + year + "</option>";
                } else {
                    selectyear += "<option value='" + year + "' " + selectedyear + ">" + year + "</option>";
                }

            }
            selectyear += "</select>";
            selectyear += "</div></div>";



            newxrayimagediv += selectimg;
            newxrayimagediv += imgnumfrac;
            newxrayimagediv += selectop;
            newxrayimagediv += selectday;
            newxrayimagediv += selectmonth;
            newxrayimagediv += selectyear;

            newxrayimagediv += "</div>";
            $("#xrayimageareafrac").append(newxrayimagediv);
            //dynamic div end
            $("#mon" + countfrac).val(monthfrac);

        }
    }

//fracture x-ray image end here

// followup x-ray image begin here

    var count = 0;
    function addnewxrayimage() {
        count++;
        var newxrayimagediv = "";
        var selectday = "";
        var selectimg = "";
        var selectop = "";
        var selectmonth = "";
        var selectyear = "";
        var btndelete = "";

        newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + count + "'>";
        selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '' alt = '' /> \n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
 <span class = 'btn btn-white btn-file'>\n\
<span class = 'fileupload-new'> <i class = 'fa fa-paper-clip' > </i> Select image</span>\n\
 <span class = 'fileupload-exists'> <i class = 'fa fa-undo'> </i> Change</span> \n\
<input type = 'file' multiple name='userfile[]' class = 'default' required/> </span> \n\
<input type = 'hidden' name='opCount' value ='" + count + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

        imgnumfollowup = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #</span>\n\
</select>\n\
</div>";

        selectop = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
            <select class = 'form-control input-sm' style='padding:5px 0px'  name = 'operation" + count + "'id = 'operation'  required>\n\
            <option selected = 'selected' value = '' >Select</option>\n\
            <option value = '1'> Pre-Op </option>\n\
<option value = '0'> Post-Op </option>\n\
</select>\n\
</div>";

        selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='day" + count + "' id='day" + count + "' required><option selected='selected' value=''>Day</option>";
        for (var day = 1; day <= 31; day++) {
            selectday += "<option value='" + day + "'>" + day + "</option>";
        }
        selectday += "</select>";
        selectday += "</div>";

        selectmonth = "<div class = 'col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'>\n\
                                                    <select class = 'form-control input-sm' style='padding:5px 0px' name = 'month" + count + "' id = 'month' required>\n\
                                                                <option selected = 'selected' value = '' >Month</option>\n\
                                                                <option value = '01'> January </option>\n\
                                                                <option value = '02'> February </option>\n\
                                                                <option value = '03'> March </option>\n\
                                                                <option value = '04'> April </option>\n\
                                                                <option value = '05'> May </option>\n\
                                                                <option value = '06'> June </option>\n\
                                                                <option value = '07'> July </option>\n\
                                                                <option value = '08'> August </option>\n\
                                                                <option value = '09'> September </option>\n\
                                                                <option value = '10'> October </option>\n\
                                                                <option value = '11'> November </option>\n\
                                                                <option value = '12'> December </option>\n\
                                                                </select>\n\
                                                                </div>";


        var d = new Date();
        var n = d.getFullYear();
        selectyear = "<div class='col-lg-4 col-md-4  col-sm-4 col-xs-4' style='padding:0'><select class='form-control input-sm' style='padding:5px 0px' name='year" + count + "' id='year" + count + "' required><option selected='selected' value=''>Year</option>";
        for (var year = 1970; year <= n + 10; year++) {
            selectyear += "<option value='" + year + "'>" + year + "</option>";
        }
        selectyear += "</select>";
        selectyear += "</div></div>";

        btndelete = "<div class='col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5'><a href='javascript:deleteDiv(" + count + ");' style='color:white'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> </div>";

        newxrayimagediv += selectimg;
        newxrayimagediv += imgnumfollowup;
        newxrayimagediv += selectop;
        newxrayimagediv += selectday;
        newxrayimagediv += selectmonth;
        newxrayimagediv += selectyear;
        newxrayimagediv += btndelete;
        newxrayimagediv += "</div>";
        $("#xrayimagearea").append(newxrayimagediv);

    }


    function deleteDiv(v) {
        count--;
        $("#xrayimagediv" + v).remove();
    }

    function hideInfection(val) {

        if (val == "1") {
            $("#infectionwound").show();
            $("#infectiondepth").show();
            $("#infectionduration").show();
            $("#infectiontype").show();
        } else {
            document.getElementById('infectionwound1').checked = false;
            document.getElementById('infectionwound2').checked = false;
            document.getElementById('infectiondepth1').checked = false;
            document.getElementById('infectiondepth2').checked = false;
            $("#infectduration").val("");
            $('#osteomyelitis').prop('checked', false);
            $('#amputation').prop('checked', false);
            $("#infectionwound").hide();
            $("#infectiondepth").hide();
            $("#infectionduration").hide();
            $("#infectiontype").hide();

        }
    }

    function hideDeformity(val) {
        if (val == "1") {
            $("#alignment").show();
            $("#rotation").show();

        } else {
            document.getElementById('alignment1').checked = false;
            document.getElementById('alignment2').checked = false;
            document.getElementById('alignment3').checked = false;
            document.getElementById('alignment4').checked = false;
            $('#rotation1').prop('checked', false);
            $("#alignment").hide();
            $("#rotation").hide();


        }
    }


    function hideRepeatSurgery(val) {
        if (val == "1") {
            $("#surgerycause").show();
            $("#surgerytype").hide();

        } else {

            $('#infectionrepeat').prop('checked', false);
            $('#deformityrepeat').prop('checked', false);
            $('#nonunionrepeat').prop('checked', false);
            $('#dynamize').prop('checked', false);
            $('#exchangenail').prop('checked', false);
            $('#iliacrestbonegraft').prop('checked', false);
            $('#other').prop('checked', false);
            $('#otherval').val('');

            $("#surgerycause").hide();
            $("#surgerytype").hide();
        }
    }


    function hideRepeatType(val) {
        if (document.getElementById('nonunionrepeat').checked) {
            $("#surgerytype").show();
        } else {
            $('#dynamize').prop('checked', false);
            $('#exchangenail').prop('checked', false);
            $('#iliacrestbonegraft').prop('checked', false);
            $('#other').prop('checked', false);
            $('#otherval').val('');
            $("#surgerytype").hide();
        }
    }

    function hideOther() {
        if (document.getElementById('other').checked) {
            $("#otherval").show();
        } else {
            $('#otherval').val('');
            $("#otherval").hide();
        }
    }
    
       $(function() {
        //    fancybox
        jQuery(".fancybox").fancybox();
    });


</script>



