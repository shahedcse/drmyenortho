
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Surgical Code                 
                    </header>
                    <div class="panel-body">                 
                        <!--  start surgical cases-->                    
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <span class="tools pull-right">
                                <a style="color: #000" href="<?php echo site_url('surgery_report/SurgeryCode/addCode') ?>">
                                    <button class="btn  "  style="background: #21BBC7">
                                        Add Code&nbsp;<i class="fa fa-plus"></i>
                                    </button> 
                                </a>
                            </span>
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                         <th>Type</th> 
                                        <th>Description</th>
                                       
                                        <th>Options</th>    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (sizeof($codelist) > 0):
                                        foreach ($codelist as $datarow):
                                            $id = $datarow->id;
                                            $code_no = $datarow->code_no;
                                            $description = $datarow->description;
                                            $type = $datarow->type;
                                            $value = $id . "/" . $code_no . "/" . $type . "/" . $description;
                                            ?>
                                            <tr class="gradeX">
                                                <td><a href="#" onclick="editSurgeryCodeInfo('<?php echo $value; ?>')"><?php echo $code_no; ?></a></td>   
                                                <td><a href="#" onclick="editSurgeryCodeInfo('<?php echo $value; ?>')"><?php echo $type; ?></a></td>
                                                <td><a href="#" onclick="editSurgeryCodeInfo('<?php echo $value; ?>')"><?php echo $description; ?></a></td>
                                                <td><button class="btn  btn-xs" style="background: #21BBC7" onclick="deleteSurgeryCodeInfo(this.value)" value="<?php echo $datarow->id; ?>"><i class="fa fa-trash-o "></i></button></td>
                                            </tr>         
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<!--Edit  Modal-->
<div class="modal fade top-modal" id="myModalEditSurgeryCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Edit Surgery Code Information</h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; max-height: 480px;">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" id="editAppointmentForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCode/editSurgeryCodeData'); ?>" enctype="multipart/form-data">
                        <div class="form-group" style="padding-top: 20px" >
                            <label for="codenumber" class="control-label col-lg-3" >Code<span style="color: red"> *</span></label>
                            <div class="col-lg-8">
                                <input class=" form-control" id="codenumber" name="codenumber" type="text" required/>
                                  <input class=" form-control" id="idEdit" name="idEdit" type="hidden" />
                            </div>
                        </div>

                        <div class="form-group" >
                            <label for="type" class="control-label col-lg-3" >Type<span style="color: red"> *</span></label>
                            <div class="col-lg-8">
                                <select class="form-control" name="type" id="type" required>
                                    <option selected="selected" value="">--Select--</option>
                                    <option value="fracture">Fracture</option>
                                    <option value="surgery">Surgery</option>
                                    <option value="followup">Followup</option>                                                                                          
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="description" class="control-label col-lg-3" >Code Description</label>
                            <div class="col-lg-8">
                                <input class="form-control " id="description" type="text" name="description"/>
                            </div>
                        </div>  

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn "  style="background: #21BBC7" type="submit">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button> 
                                <button data-dismiss="modal" class="btn "  style="background: #21BBC7" type="button">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>          
        </div>
    </div>
</div>
<!--/Edit  Modal-->

<!--delete  Modal-->
<div class="modal fade top-modal" id="myModalDeleteSurgeryCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="<?php echo site_url('surgery_report/SurgeryCode/deleteSurgeryCodeData'); ?>" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;">Delete Surgery Code Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;Do you want to delete !!!
                                <input type="hidden" id="id_delete" name="id_delete"/>
                            </div>
                        </div>
                        <div class="modal-footer">                         
                            <button class="btn " style="background: #21BBC7" type="submit">Confirm</button>
                            <button data-dismiss="modal" class="btn "  style="background: #21BBC7" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>
<!--end delete  Modal-->

<script>

    function deleteSurgeryCodeInfo(id) {
        $("#id_delete").val(id);
        $('#myModalDeleteSurgeryCode').modal('show');
    }

    function editSurgeryCodeInfo(info) {
        var data = info.split("/");
        var id = data[0];
        var code_no = data[1];
        var type = data[2];
        var description = data[3];



        $("#idEdit").val(id);
        $("#codenumber").val(code_no);
        $("#type").val(type);
        $("#description").val(description);

        $('#myModalEditSurgeryCode').modal('show');
    }

</script>



