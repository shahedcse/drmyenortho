
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Followup Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  
                        <div class="form" style="padding-bottom: 20px;">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 5px;">
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-5" style="font-weight: bold;color: #167F52;">Patient Name :<span style="font-weight:normal;color: #000;"><?php
                                            $patientId = $orthocase->patient_id;
                                            $query = $this->db->query("SELECT username FROM user WHERE id='$patientId'");
                                            if (sizeof($query->row()) > 0):
                                                echo $query->row()->username;
                                            else:
                                                echo "";
                                            endif;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Case Number :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->case_num ?></span></label>
                                    <label for="address" class="control-label col-lg-3 col-md-3  col-sm-3 col-xs-5" style="font-weight: bold;color: #167F52;">Date of Injury :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthocase->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Case Id :<span  style="font-weight:normal;color: #000;"> <?php echo $orthocase->id ?></span></label>                                  
                                </div>
                            </form>
                        </div> 
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/editFollowupData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px;">
                                    <label for="birthday" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Followup Date<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">

                                        <input type="hidden" name="case_id" value="<?php echo $orthocase->id ?>"/>
                                        <?php
                                        $databaseDate = $orthofollowup->date;
                                        $originalDate = explode("-", $databaseDate);
                                        $yearFollowup = $originalDate[0];
                                        $monthFollowup = $originalDate[1];

                                        $dayFollowup = $originalDate[2];
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $dayFollowup . "-" . $monthFollowup . "-" . $yearFollowup . '</label>';
                                        ?>

                                    </div>
                                </div>

                                <hr/>

                                <div class="form-group" id="infectiondiv">
                                    <label for="infection" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Infection<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->infection == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>
                                        <?php
                                        if ($orthofollowup->infection == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>
                                    </div>
                                    <input type="hidden" name="fracture_id" value="<?php echo $fractureid ?>"/>
                                    <input type="hidden" name="id_followup" value="<?php echo $id_followup ?>"/>
                                </div>
                                <div class="form-group" id="infectionwound" style="display: none">
                                    <label for="infectionwound" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Incision of the wound?</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->incision_wound == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofollowup->incision_wound == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>
                                <div class="form-group" id="infectiondepth" style="display: none">
                                    <label for="infectiondepth" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Infection Depth</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->infection_depth == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofollowup->infection_depth == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>


                                <div class="form-group" id="infectionduration" style="display: none">
                                    <label for="infectionduration" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Duration Of Infection</label>
                                    <div class="col-lg-2 col-md-2  col-sm-2 col-xs-5">
                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofollowup->infection_duration . " Weeks" . '</label>'; ?>                                         
                                    </div>                                                                                          
                                </div>
                                <div class="form-group" id="infectiontype" style="display: none">                                   
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2"> 

                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofollowup->infection_type . '</label>'; ?>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label for="partialweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Partial weight bearing?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->partial_weight_bear == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofollowup->partial_weight_bear == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="painlessweight" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Painless full weight bearing?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->painless_full_weight_bear == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofollowup->painless_full_weight_bear == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="healingxray" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Healing by x-ray?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->healing_by_xray == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofollowup->healing_by_xray == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kneeflexion" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Knee flexion greater than 90 degrees?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->knee_flex_greater_90deg == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofollowup->knee_flex_greater_90deg == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="screwbreakage" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Screw breakage?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->screw_breakage == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofollowup->screw_breakage == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="screwloosening" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Screw loosening?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->screw_loosen == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofollowup->screw_loosen == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nailbreakage" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nail breakage?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->nail_breakage == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofollowup->nail_breakage == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nailloosening" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Nail loosening?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->nail_loosen == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofollowup->nail_loosen == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group" id="deformitydiv">
                                    <label for="deformity" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Deformity?<span style="color: red"> *</span> </label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <?php
                                        if ($orthofollowup->deformity == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?>  
                                        <?php
                                        if ($orthofollowup->deformity == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group" id="alignment" style="display: none">  
                                    <label for="alignment" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-1" >Alignment?</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">                                                                  
                                        <?php
                                        if ($orthofollowup->deformity_alignment == "10 varus"):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Over 10 degrees varus" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofollowup->deformity_alignment == "20 varus"):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Over 20 degrees varus" . '</label>';
                                        endif;
                                        ?>


                                        <?php
                                        if ($orthofollowup->deformity_alignment == "10 valgus"):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . " Over 10 degrees valgus" . '</label>';
                                        endif;
                                        ?>
                                        <?php
                                        if ($orthofollowup->deformity_alignment == "20 valgus"):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . " Over 20 degrees valgus" . '</label>';
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group" id="rotation" style="display: none">   
                                    <label for="rotation" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-1" >Rotation?</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">                                                                  
                                        <?php
                                        if ($orthofollowup->deformity_rotaion == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "Over 30 degrees" . '</label>';
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group" id="repeatsurgerydiv">  
                                    <label for="repeatsurgery" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Repeat surgery?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">                                                                  
                                        <?php
                                        if ($orthofollowup->repeat_surgery == 1):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "yes" . '</label>';
                                        endif;
                                        ?> 
                                        <?php
                                        if ($orthofollowup->repeat_surgery == 0):
                                            echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . "no" . '</label>';
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group" id="surgerycause" style="display: none">  

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2 ">                                                                  
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofollowup->repeat_surgery_cause . '</label>';
                                        ?> 
                                    </div>
                                </div>
                                <div class="form-group" id="surgerytype" style="display: none">  

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2 ">                                                                  
                                        <?php
                                        echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofollowup->repeat_surgery_type . '</label>';
                                        ?> 
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group ">
                                    <label for="comments" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Comments</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <?php echo '<label style="font-weight: normal;margion-top:2px;padding-top: 5px;" >' . $orthofollowup->comments . '</label>'; ?>
                                    </div>
                                </div>
                                <hr/>

                                <h6 class="panel-heading" style="background: #21BBC7">
                                    Followup Images
                                </h6>
                                <!--                        <div class="form">
                                                            <form class="form-horizontal table-bordered" id="xRayImageForm" name="xRayImageForm" method="POST" action="<?php //echo site_url('surgery_report/SurgeryCase/addFractureXrayImage');                                     ?>" enctype="multipart/form-data">-->
                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                    <div class="col-lg-11 col-lg-offset-1" id="xrayimagearea">

                                    </div>
                                </div>


                                <hr/>
                                <h6 class="panel-heading" style="background-color: #21BBC7">
                                    Fracture X-Ray Images
                                </h6>                              
                                <div class="form-group" style="padding-top: 20px;display: block;"  id="img">
                                    <div class="col-lg-11 col-lg-offset-1" id="xrayimageareafrac">
                                    </div>
                                </div>                                                           
                                <hr/>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-9">
                                        <a style="color: #000" href="<?php echo site_url('surgery_report/Research'); ?>"> <button class="btn " style="background: #21BBC7" type="button">Close</button></a>  
                                    </div>
                                </div>
                            </form>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>

    var allimgname = '';
    var allimgtype = '';
    var allimgdate = '';
    var length;
    var i;
    var sepimgname, sepimgtype, sepimgdate, sepyearmonthday, yearfrac, monthfrac, dayfrac;
    var imgtype = '';
    var imgname = '';
    var imgdate = '';
    var countfrac = 0;
    var countfollowup = 0;
    var selectedif, selectedelse, selectedday, selectedmonth, selectedyear;
    var allimgfracture = "<?php echo sizeof($orthoimgfracture); ?>";

    if (allimgfracture > 0) {
        allimgname = "<?php echo $orthoimgfracture->image_name; ?>";
        allimgtype = "<?php echo $orthoimgfracture->image_type; ?>";
        allimgdate = "<?php echo $orthoimgfracture->date; ?>";

        sepimgname = allimgname.split(",");
        sepimgtype = allimgtype.split(",");
        sepimgdate = allimgdate.split(",");

        length = '<?php echo substr_count($orthoimgfracture->image_name, ",") ?>';
        if (allimgname != "") {
            for (i = 0; i <= length; i++) {
                imgname = sepimgname[i];
                imgtype = sepimgtype[i];
                imgdate = sepimgdate[i];

                if (imgdate == '') {
                    yearfrac = '';
                    monthfrac = '';
                    dayfrac = '';
                } else {
                    sepyearmonthday = imgdate.split("-");
                    yearfrac = sepyearmonthday[0];
                    monthfrac = sepyearmonthday[1];
                    dayfrac = sepyearmonthday[2];
                }


                var loc = "<?php echo $baseurl . "/assets/uploads/xray/"; ?>" + imgname;
                if (imgtype == 1) {
                    selectedif = 'pre-op';
                    selectedelse = '';
                } else if (imgtype == '') {
                    selectedelse = 'selected';
                    selectedif = '';
                } else {
                    selectedif = 'post-op';
                }
                //dynamic div start
                countfrac++;
                var newxrayimagediv = "";
                var selectday = "";
                var selectimg = "";
                var imgnumfrac = "";
                var imgnumfollowup = "";
                var selectop = "";
                var selectmonth = "";
                var selectyear = "";
                var btndelete = "";

                newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + countfrac + "'>";
                selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '" + loc + "' alt = '' /> \n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
</div>\n\
</div>";

                imgnumfrac = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #" + countfrac + "</span>\n\
</select>\n\
</div>";

                selectop = "<div class = 'col-lg-12'>\n\
            <span style='color:white'>Operation  #" + selectedif + "</span>\n\
</div>";

                selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-12 space'><span style='color:white'>Date #" + dayfrac + "-" + monthfrac + "-" + yearfrac + "</span></div>";
//            selectmonth = "<div class = 'col-lg-2 space'><span style='color:white'>" + monthfrac + "</span></div>";
//            selectyear = "<div class='col-lg-2 space'><span style='color:white'>" + yearfrac + "</span></div>";
                selectyear += "</div>";



                newxrayimagediv += selectimg;
                newxrayimagediv += imgnumfrac;
                newxrayimagediv += selectop;
                newxrayimagediv += selectday;
                newxrayimagediv += selectmonth;
                newxrayimagediv += selectyear;

                newxrayimagediv += "</div>";
                $("#xrayimageareafrac").append(newxrayimagediv);
                //dynamic div end


            }
        }

    }


////////////////////////////////////



    var allimgfollowup = "<?php echo sizeof($orthoimgfollowup); ?>";

    if (allimgfollowup > 0) {
        allimgname = "<?php echo $orthoimgfollowup->image_name; ?>";
        allimgtype = "<?php echo $orthoimgfollowup->image_type; ?>";
        allimgdate = "<?php echo $orthoimgfollowup->date; ?>";

        sepimgname = allimgname.split(",");
        sepimgtype = allimgtype.split(",");
        sepimgdate = allimgdate.split(",");

        length = '<?php echo substr_count($orthoimgfollowup->image_name, ",") ?>';
  if (allimgname != "") {
       for (i = 0; i <= length; i++) {
            imgname = sepimgname[i];
            imgtype = sepimgtype[i];
            imgdate = sepimgdate[i];

            if (imgdate == '') {
                yearfrac = '';
                monthfrac = '';
                dayfrac = '';
            } else {
                sepyearmonthday = imgdate.split("-");
                yearfrac = sepyearmonthday[0];
                monthfrac = sepyearmonthday[1];
                dayfrac = sepyearmonthday[2];
            }


            var loc = "<?php echo $baseurl . "/assets/uploads/xray/"; ?>" + imgname;
            if (imgtype == 1) {
                selectedif = 'pre-op';
                selectedelse = '';
            } else if (imgtype == '') {
                selectedelse = 'selected';
                selectedif = '';
            } else {
                selectedif = 'post-op';
            }
            //dynamic div start
            countfollowup++;
            var newxrayimagediv = "";
            var selectday = "";
            var selectimg = "";
            var selectop = "";
            var selectmonth = "";
            var selectyear = "";
            var btndelete = "";

            newxrayimagediv = "<div class='col-lg-2' style='background: #0075b0; padding: 10px;margin:2px;'id='xrayimagediv" + countfollowup + "'>";
            selectimg = "<div class ='col-md-6'>\n\
<div class = 'fileupload fileupload-new' data-provides = 'fileupload'>\n\
<div class = 'fileupload-new thumbnail' style = 'width: 125px; height: 125px;'>\n\
 <img src = '" + loc + "' alt = '' /> \n\
 <input type = 'hidden' name='xrayimgname" + countfollowup + "' value ='" + imgname + "'/>\n\
</div>\n\
 <div class = 'fileupload-preview fileupload-exists thumbnail' style = 'width: 125px; height: 125px; line-height: 20px;'>\n\
 </div> \n\
<div> \n\
<input type = 'hidden' name='opCount' value ='" + countfollowup + "'class = 'default'/> </span>\n\
 </div>\n\
</div>\n\
</div>";

            imgnumfollowup = "<div class = 'col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8'>\n\
         <span style='color:white'>X-Ray  #" + countfollowup + "</span>\n\
</select>\n\
</div>";

            selectop = "<div class = 'col-lg-12'>\n\
            <span style='color:white'>Operation  #" + selectedif + "</span>\n\
</div>";

            selectday = "<div class='row' style='margin: 0px 3px;'><div class='col-lg-12 space'><span style='color:white'>Date #" + dayfrac + "-" + monthfrac + "-" + yearfrac + "</span></div>";
//            selectmonth = "<div class = 'col-lg-2 space'><span style='color:white'>" + monthfrac + "</span></div>";
//            selectyear = "<div class='col-lg-2 space'><span style='color:white'>" + yearfrac + "</span></div>";
            selectyear += "</div>";



            newxrayimagediv += selectimg;
            newxrayimagediv += imgnumfollowup;
            newxrayimagediv += selectop;
            newxrayimagediv += selectday;
            newxrayimagediv += selectmonth;
            newxrayimagediv += selectyear;

            newxrayimagediv += "</div>";
            $("#xrayimagearea").append(newxrayimagediv);
            //dynamic div end
            $("#month" + countfollowup).val(monthfrac);

        } 
    }
      
    }






////////////////////////////////////



    function hideInfection(val) {
        if (val == "1") {
            $("#infectionwound").show();
            $("#infectiondepth").show();
            $("#infectionduration").show();
            $("#infectiontype").show();
        } else {

            $("#infectionwound").hide();
            $("#infectiondepth").hide();
            $("#infectionduration").hide();
            $("#infectiontype").hide();

        }
    }

    function hideDeformity(val) {
        if (val == "1") {
            $("#alignment").show();
            $("#rotation").show();

        } else {

            $("#alignment").hide();
            $("#rotation").hide();


        }
    }


    function hideRepeatSurgery(val) {
        if (val == "1") {
            $("#surgerycause").show();
            $("#surgerytype").hide();

        } else {
//            document.getElementById('alignment1').checked = false;
//            document.getElementById('alignment2').checked = false;
//            document.getElementById('alignment3').checked = false;
//            document.getElementById('alignment4').checked = false;
//            $('#rotation1').prop('checked', false);
            $("#surgerycause").hide();
            $("#surgerytype").hide();
        }
    }


    function hideRepeatType(val) {
        if (document.getElementById('nonunionrepeat').checked) {
            $("#surgerytype").show();
        } else {
//            document.getElementById('alignment1').checked = false;
//            document.getElementById('alignment2').checked = false;
//            document.getElementById('alignment3').checked = false;
//            document.getElementById('alignment4').checked = false;
//            $('#rotation1').prop('checked', false);        
            $("#surgerytype").hide();
        }
    }

    function hideOther() {
        if (document.getElementById('other').checked) {
            $("#otherval").show();
        } else {
//            document.getElementById('alignment1').checked = false;
//            document.getElementById('alignment2').checked = false;
//            document.getElementById('alignment3').checked = false;
//            document.getElementById('alignment4').checked = false;
//            $('#rotation1').prop('checked', false);        
            $("#otherval").hide();
        }
    }


    $(document).ready(function () {
        var monthval = '<?php echo $monthFollowup; ?>';

        $("#month").val(monthval);
        hideInfection("<?php echo $orthofollowup->infection; ?>");


        hideDeformity("<?php echo $orthofollowup->deformity; ?>");

        hideRepeatSurgery("<?php echo $orthofollowup->repeat_surgery; ?>");

        //Repeat surgery cause

        var allsurgerycause = "<?php echo $orthofollowup->repeat_surgery_cause; ?>";
        var length = "<?php echo substr_count($orthofollowup->repeat_surgery_cause, ",") ?>";
        var sepsurgerycause = allsurgerycause.split(",");
        var surgerycause = '';
        var i;
        for (i = 0; i <= length; i++) {
            surgerycause = sepsurgerycause[i];
            if (surgerycause == "nonunion") {
                $("#surgerytype").show();
            } else {
                $("#surgerytype").hide();
            }

        }

//        hideRepeatType($("#nonunionrepeat").val());





    });


</script>



