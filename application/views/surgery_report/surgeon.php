
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Surgeons                 
                    </header>
                    <div class="panel-body">                 
                        <!--  start surgical cases-->                    
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <span class="tools pull-right">
                                <a style="color: #000"  href="<?php echo site_url('surgery_report/Surgeon/addSurgeon') ?>">
                                    <button class="btn  " style="background: #21BBC7">
                                        Add Surgeon&nbsp;<i class="fa fa-plus"></i>
                                    </button> 
                                </a>
                            </span>
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Hospital</th>
                                        <th>Act. Date</th>
                                        <th>Status</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (sizeof($surgeonlist) > 0):
                                        foreach ($surgeonlist as $datarow):
                                        
                                            $databaseDate = $datarow->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            $value=$datarow->name . "/" . $datarow->email . "/" . $datarow->hospital . "/" . $day . "/" . $month . "/" . $year . "/" . $datarow->status . "/" . $datarow->id;
                                            ?>
                                            <tr class="gradeX">
                                                <td><a href="#" onclick="editSurgeonInfo('<?php echo $value; ?>')"><?php echo $datarow->name; ?></a></td>                                            
                                                <td><a href="#" onclick="editSurgeonInfo('<?php echo $value; ?>')"><?php echo $datarow->email; ?></a></td>
                                                <td><a href="#" onclick="editSurgeonInfo('<?php echo $value; ?>')"><?php echo $datarow->hospital; ?></a></td>
                                                <td><a href="#" onclick="editSurgeonInfo('<?php echo $value; ?>')"><?php
                                                    $databaseDate = $datarow->date;
                                                    $originalDate = explode("-", $databaseDate);
                                                    $year = $originalDate[0];
                                                    $month = $originalDate[1];
                                                    $day = $originalDate[2];
                                                    $date = $day . "-" . $month . "-" . $year;
                                                    echo $date;
                                                    ?></a></td>   
                                                <td><a href="#" onclick="editSurgeonInfo('<?php echo $value; ?>')"><?php
                                                    $status = $datarow->status;
                                                    if ($status == 1):
                                                        echo "Active";
                                                    else :
                                                        echo "Inactive";
                                                    endif;
                                                    ?></a></td>   
                                                <td>
                                                  
                                                    <button class="btn btn-xs" style="background: #21BBC7" onclick="checkSurgeonUse(this.value)" value="<?php echo $datarow->id; ?>"><i class="fa fa-trash-o "></i></button></td>
                                            </tr>         
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<!--Edit  Modal-->
<div class="modal fade top-modal" id="myModalEditAppoint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Edit Requested Appointment Information</h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; max-height: 480px;">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" id="editAppointmentForm" method="POST" action="<?php echo site_url('surgery_report/Surgeon/editSurgeonData'); ?>" enctype="multipart/form-data">
                        <div class="form-group" >
                            <label for="surgeonname" class="control-label col-lg-3" >Name<span style="color: red"> *</span></label>
                            <div class="col-lg-8">
                                <input class=" form-control" id="surgeonname" name="surgeonname" type="text" required/>
                                <input class=" form-control" id="idEdit" name="idEdit" type="hidden" />
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for="email" class="control-label col-lg-3" >Email</label>
                            <div class="col-lg-8">
                                <input class=" form-control" id="email" name="email" type="email" />
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for="hospitalname" class="control-label col-lg-3" >Hospital<span style="color: red"> *</span></label>
                            <div class="col-lg-8">
                                <input class=" form-control" id="hospitalname" name="hospitalname" type="text" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="day" class="control-label col-lg-3" >Act. Date<span style="color: red"> *</span></label>
                            <div class="col-lg-8">
<!--                                                                        <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                <div class="col-lg-4">
                                    <select class="form-control m-bot15" name="day" id="day" required>
                                        <option selected="selected" value="">--Day--</option>
                                        <?php
                                        for ($day = 1; $day <= 31; $day++):
                                            ?>
                                            <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                            <?php
                                        endfor;
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control m-bot15" name="month" id="month" required>
                                        <option selected="selected" value="">--Month--</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control m-bot15" name="year" id="year" required>
                                        <option selected="selected" value="">--Year--</option>
                                        <?php
                                        $currentyr = date('Y');
                                        for ($year = 1970; $year <= $currentyr + 10; $year++):
                                            ?>
                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                            <?php
                                        endfor;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="status" class="control-label col-lg-3" >Status<span style="color: red"> *</span></label>
                            <div class="col-lg-8">                             
                                <input name="status" id="status" value="1" type="radio" required  /> Active &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="status" id="status" value="0" type="radio" /> Inactive                           
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn "  style="background: #21BBC7" type="submit">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button> 
                                <button data-dismiss="modal" class="btn "  style="background: #21BBC7" type="button">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>          
        </div>
    </div>
</div>
<!--/Edit  Modal-->



<!--delete  Modal-->
<div class="modal fade top-modal" id="myModalDeleteSurgeon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="<?php echo site_url('surgery_report/Surgeon/deleteSurgeonData'); ?>" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;">Delete Surgeon Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;Do you want to delete !!!
                                <input type="hidden" id="id_delete" name="id_delete"/>
                            </div>
                        </div>
                        <div class="modal-footer">                         
                            <button class="btn " style="background: #21BBC7" type="submit">Confirm</button>
                            <button data-dismiss="modal" class="btn "  style="background: #21BBC7" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>
<!--end delete  Modal-->

<div class="modal fade top-modal" id="myModalDeleteInfoSurgeon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="#" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;">Delete Surgeon Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;This Surgeon information is used in other section !!!
                                <input type="hidden" id="id_delete" name="id_delete"/>
                            </div>
                        </div>
                        <div class="modal-footer">                    

                            <button data-dismiss="modal" class="btn "  style="background: #21BBC7" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>

<script>

    function editSurgeonInfo(info) {
        var data = info.split("/");
        var name = data[0];
        var email = data[1];
        var hospital = data[2];
        var day = data[3];
        if (day.charAt(0) == "0") {
            day = day.charAt(1);
        }
        var month = data[4];
        var year = data[5];
        var status = data[6];
        var idEdit = data[7];

        $("#surgeonname").val(name);
        $("#email").val(email);
        $("#hospitalname").val(hospital);
        $("#day").val(day);
        $("#month").val(month);
        $("#year").val(year);
        $('input[name=status]').val([status]);
        $("#idEdit").val(idEdit);
        $('#myModalEditAppoint').modal('show');
    }
    function deleteSurgeonInfo(id) {
        $("#id_delete").val(id);
        $('#myModalDeleteSurgeon').modal('show');
    }
    
     function checkSurgeonUse(surgeon_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('surgery_report/Surgeon/checkSurgeonUse'); ?>",
            data: 'surgeon_id=' + surgeon_id,
            success: function (data) {
             
                if (data == "exits") {
                    $('#myModalDeleteInfoSurgeon').modal('show');
                } else {
                    deleteSurgeonInfo(surgeon_id);
                }

            }
        });
    }

</script>





