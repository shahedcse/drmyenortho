
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Edit Surgery Information            
                    </header>
                    <div class="panel-body">  
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->  
                        <div class="form" style="padding-bottom: 20px;">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 5px;">
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-7" style="font-weight: bold;color: #167F52;">Patient Name :<span style="font-weight:normal;color: #000;"><?php
                                            $patientId = $orthocase->patient_id;
                                            $query = $this->db->query("SELECT username FROM user WHERE id='$patientId'");
                                            if (sizeof($query->row()) > 0):
                                                echo $query->row()->username;
                                            else:
                                                echo "";
                                            endif;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2col-xs-7" style="font-weight: bold;color: #167F52;">Case Number :<span style="font-weight:normal;color: #000;"> <?php echo $orthocase->case_num ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-6" style="font-weight: bold;color: #167F52;">Date of Injury :<span style="font-weight:normal;color: #000;"> <?php
                                            $databaseDate = $orthocase->date;
                                            $originalDate = explode("-", $databaseDate);
                                            $year = $originalDate[0];
                                            $month = $originalDate[1];
                                            $day = $originalDate[2];
                                            $date = $day . "-" . $month . "-" . $year;
                                            echo $date;
                                            ?></span></label>
                                    <label for="address" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" style="font-weight: bold;color: #167F52;">Case Id :<span  style="font-weight:normal;color: #000;"> <?php echo $orthocase->id ?></span></label>                                  
                                </div>
                            </form>
                        </div> 
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;" id="addPatientForm" method="POST" action="<?php echo site_url('surgery_report/SurgeryCase/editSurgeryData'); ?>" enctype="multipart/form-data">
                                <div class="form-group" style="padding-top: 20px;">
                                    <label for="birthday" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Surgery Date<span style="color: red"> *</span></label>
                                    <div class="col-lg-4 col-md-6  col-sm-8 col-xs-7">
<!--                                                                        <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                        <input type="hidden" name="case_id" value="<?php echo $orthocase->id ?>"/>
                                        <input type="hidden" name="id_surgery" value="<?php echo $id_surgery; ?>"/>
                                        <?php
                                        $databaseDate = $surgerycase->date;
                                        $originalDate = explode("-", $databaseDate);
                                        $yearSurgery = $originalDate[0];
                                        $monthSurgery = $originalDate[1];

                                        $daySurgery = $originalDate[2];
                                        if (substr($daySurgery, 0, 1) == 0):
                                            $daySurgery = substr($daySurgery, 1, 2);
                                        endif;
                                        ?>
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="day" id="day" required>
                                                <option selected="selected" value="">--Select Day--</option>
                                                <?php
                                                for ($day = 1; $day <= 31; $day++):
                                                    ?>
                                                    <?php
                                                    if ($day == $daySurgery):
                                                        ?>   
                                                        <option value="<?php echo $day; ?>" selected><?php echo $day; ?></option>
                                                        <?php
                                                    else:
                                                        ?>
                                                        <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                    <?php
                                                    endif;
                                                    ?>
                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="month" id="month" required>
                                                <option selected="selected" value="">--Select Month--</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">
                                            <select class="form-control m-bot15" name="year" id="year" required>
                                                <option selected="selected" value="">--Select Year--</option>
                                                <?php
                                                $currentyr = date('Y');
                                                for ($year = 1970; $year <= $currentyr + 10; $year++):
                                                    ?>
                                                    <?php
                                                    if ($year == $yearSurgery):
                                                        ?>   
                                                        <option value="<?php echo $year; ?>" selected><?php echo $year; ?></option>
                                                        <?php
                                                    else:
                                                        ?>
                                                        <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                    <?php
                                                    endif;
                                                    ?>

                                                    <?php
                                                endfor;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" <?php
                                if (sizeof($codenum) > 0):
                                    echo"style='display: block'";
                                else:
                                    echo "style='display: none'";
                                endif;
                                ?>>
                                    <label for="codenumber" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Code Number</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="codenumber" id="codenumber"  >

                                            <option selected="selected" value="">--Select Code--</option>
                                            <?php
                                            if (sizeof($codenum) > 0):
                                                foreach ($codenum as $datarow):
                                                    echo '<option value="' . $datarow->code_no . '">' . $datarow->code_no . '</option> ';
                                                endforeach;
                                            endif;
                                            ?>                                                                                          
                                        </select>
                                    </div>
                                </div>


                                <hr/>
                                <div class="form-group" >
                                    <label for="surgeonname" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Surgeon :<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                                            <select class="form-control m-bot15 selectpicker" name="surgeonval" id="testname" data-live-search="true" required>
                                                <option value=""> --Select Surgeon-- </option> 
                                                <?php
                                                if (sizeof($surgeonlist) > 0):
                                                    foreach ($surgeonlist as $datarow):
                                                        if ($datarow->id == $surgerycase->surgeon_id):
                                                            echo '<option value="' . $datarow->id . '"selected >' . $datarow->name . '</option> ';
                                                        else:
                                                            echo '<option value="' . $datarow->id . '">' . $datarow->name . '</option> ';
                                                        endif;

                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                            <input type="hidden" name="testcountdata" id="testcountdata">
                                           
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <button class="btn btn-primary" id="addtest" type="button">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                        </div>
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <p style="color: #167F52" id="surgeonnameshow"></p>
                                            <span id="surgeondescription"></span>
                                        </div>
                                    </div>

                                </div>

                                <hr/>
                                <div class="form-group" id="antibioticDiv">
                                    <label for="sex" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Antibiotics Used?<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="antibiotic" id="antibiotic" value="1" type="radio" <?php
                                        if ($surgerycase->antibiotics == 1 || $surgerycase->antibiotics == 3):
                                            echo 'checked';
                                        endif;
                                        ?>  onchange="hideAntibiotics(this.value);"  required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="antibiotic" id="antibiotic" value="0" type="radio"  <?php
                                        if ($surgerycase->antibiotics == 0 || $surgerycase->antibiotics == 2):
                                            echo 'checked';
                                        endif;
                                        ?> onchange="hideAntibiotics(this.value);"  /> No &nbsp;&nbsp;&nbsp;
                                        <input id="unknown" name="unknown" value="2"  type="checkbox" <?php
                                        if ($surgerycase->antibiotics == 2 || $surgerycase->antibiotics == 3):
                                            echo 'checked';
                                        endif;
                                        ?> /> Unknown
                                    </div>
                                </div>
                                <div class="form-group" id="antibiotics_after_hours_days" style="display: none">
                                    <label for="antibiotics_after_hours_days" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >How long from time of injury</label>
                                    <?php
                                    $durationinjury = $surgerycase->antibiotics_after_hours;
                                    $durationcoverage = $surgerycase->antibiotics_coverage;
                                    ?>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <input class=" form-control" id="antibiotics_after_hours" name="antibiotics_after_hours"  type="text" onchange="resetField(this.id);" value="<?php
                                        if ($durationinjury < 24):
                                            echo $durationinjury;
                                        endif;
                                        ?>"  />Hours, or  &nbsp;&nbsp;&nbsp;                                         

                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">                                    
                                        <input class=" form-control" id="antibiotics_after_days" name="antibiotics_after_days"  type="text"  onchange="resetField(this.id);" value="<?php
                                        if ($durationinjury >= 24):
                                            $durationinjury = $durationinjury / 24;
                                            echo $durationinjury;
                                        endif;
                                        ?>" />Days 
                                    </div>                                 
                                </div>
                                <div class="form-group" id="antibiotics_name_div" style="display: none">
                                    <label for="antibiotics_name" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Name Of Antibiotic</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <input class=" form-control" id="antibiotics_name" name="antibiotics_name"  type="text"  value="<?php echo $surgerycase->antibiotics_name; ?>" />                                         
                                    </div>                                                                    
                                </div>
                                <div class="form-group" id="antibiotics_coverage_hours_days" style="display: none">
                                    <label for="antibiotics_coverage_hours_days" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Duration Of Antibiotic Coverage</label>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <input class=" form-control" id="antibiotics_coverage_hours" name="antibiotics_coverage_hours"  type="text"   onchange="resetField(this.id);" value="<?php
                                        if ($durationcoverage <= 24):
                                            echo $durationcoverage;
                                        endif;
                                        ?>" />Hours, or  &nbsp;&nbsp;&nbsp;                                         
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">                                    
                                        <input class=" form-control" id="antibiotics_coverage_days" name="antibiotics_coverage_days"  type="text"  onchange="resetField(this.id);" value="<?php
                                        if ($durationcoverage > 24):
                                            $durationcoverage = $durationcoverage / 24;
                                            echo $durationcoverage;
                                        endif;
                                        ?>"/>Days 
                                    </div>                                                                    
                                </div>
                                <hr/>
                                <div class="form-group ">
                                    <label for="comments" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Comments</label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <textarea class="form-control" id="comments" name="comments"  ><?php echo $surgerycase->comments; ?></textarea>
                                    </div>
                                </div>
                                <hr/>                         
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-lg-9 col-md-9  col-sm-9 col-xs-9">
                                        <button class="btn "  style="background: #21BBC7" type="submit">Save & Continue</button>
                                        <a href="<?php echo site_url('surgery_report/SurgeryCase'); ?>"> <button class="btn "  style="background: #21BBC7;color: #000" type="button">Cancel</button></a> 
                                    </div>
                                </div>
                            </form>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script>



    var testcount = 0;
    $("#addtest").click(function () {
        var surgeonval = $("#testname").val();
        var surgeonname = $("#testname option:selected").text();

        if (surgeonval == "") {
            $("#surgeondescription").text("Please Select a Surgeon Name.");
            $("#surgeondescription").css('color', 'red');
            return false;
        }
        testcount = testcount + 1;
        var surgeonmsg = "<div id='medicine" + testcount + "'><input type='hidden' name='" + testcount + "'value='" + surgeonval + "'/><p style='margin: 0 0 8px 0'>" + testcount + ". " + surgeonname + "</p></div>";
        $("#surgeonnameshow").append(surgeonmsg);
        $("#surgeondescription").text("");
        $("#testcountdata").val(testcount);
    });


    function resetField(id) {
        var caseId = id;
        switch (caseId) {
            case 'antibiotics_after_hours':
                $("#antibiotics_after_days").val("");
                break;
            case 'antibiotics_after_days':
                $("#antibiotics_after_hours").val("");
                break;
            case 'antibiotics_coverage_hours':
                $("#antibiotics_coverage_days").val("");
                break;
            case 'antibiotics_coverage_days':
                $("#antibiotics_coverage_hours").val("");
                break;
            default:
        }
    }

    $(document).ready(function () {
//        var val = $("#antibiotic").val();
//        hideAntibiotics(val);
         hideAntibiotics($('input[name=antibiotic]:checked', '#antibioticDiv').val());
        var monthval = '<?php echo $monthSurgery; ?>';
        $("#month").val(monthval);
    });



    function hideAntibiotics(val) {
        if (val == "1") {
            //    $("#unknown").removeAttr("disabled");           
            $("#antibiotics_after_hours_days").show();
            $("#antibiotics_name_div").show();
            $("#antibiotics_coverage_hours_days").show();
        } else {
            //   $('#unknown').attr('checked', false);
            $("#antibiotics_after_days").val("");
            $("#antibiotics_after_hours").val("");
            $("#antibiotics_name").val("");
            $("#antibiotics_coverage_days").val("");
            $("#antibiotics_coverage_hours").val("");

            $("#antibiotics_after_hours_days").hide();
            $("#antibiotics_name_div").hide();
            $("#antibiotics_coverage_hours_days").hide();
            // $("#unknown").attr("disabled", true);                 
        }
    }


  

</script>



