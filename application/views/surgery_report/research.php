
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Search Cases                 
                    </header>
                    <div class="panel-body">                 
                        <!--  start surgical cases-->                    
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->                    
                        <div class="form">
                            <form class="form-horizontal table-bordered" style="background: #bbd7cb;"  id="addPatientForm" method="POST" action="#" enctype="multipart/form-data">
                                <div class="form-group " style="padding-top: 20px" >
                                    <label for="category" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 " >Search Category<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="category" id="category" onchange="getParameter();"  required>
                                            <option selected="selected" value="">--Select Category--</option>
                                            <option  value="ortho_case">Case</option>
                                            <option value="ortho_surgery">Surgery</option>
                                            <option value="ortho_fracture">Fracture</option>
                                            <option value="ortho_followup">Followup</option>

                                        </select>
                                    </div>
                                </div>

                                <hr/>


                                <div class="form-group" >
                                    <label for="parameter" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 " >Search Parameter<span style="color: red"> *</span></label>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 ">
                                        <select class="form-control" name="parameter" id="parameter" onchange="getExtraOption(this.value);"  required>
                                            <option selected="selected" value="">--Select Parameter--</option>                                                                                                                                
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="daterangediv" style="display: none">
                                    <!--                                    <label class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5">Date Range</label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <div class="input-group input-large" >
                                            <input type="text" class="form-control dpd1" value="<?php echo date("d-m-Y") ?>" id="from" name="from">
                                            <span class="input-group-addon">To</span>
                                            <input type="text" class="form-control dpd2" value="<?php echo date("d-m-Y") ?>"  id="to" name="to">
                                        </div>
                                        <span class="help-block">Select date range</span>
                                    </div>
                                </div>

                                <div class="form-group" id="antibioticdiv" style="display: none">
<!--                                    <label for="sex" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Antibiotics Used?<span style="color: red"> *</span></label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="antibiotic" id="antibiotic" value="1" type="radio"  required  /> Yes &nbsp;&nbsp;&nbsp;
                                        <input name="antibiotic" id="antibiotic" value="0" type="radio"  /> No &nbsp;&nbsp;&nbsp;

                                    </div>
                                </div>

                                <div class="form-group" id="fracturesidediv" style="display: none">
<!--                                    <label for="fractureside" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Fracture Side<span style="color: red"> *</span></label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="fractureside" id="fractureside" value="left" type="radio" required/> Left &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="fractureside" id="fractureside" value="right" type="radio"/> Right                           
                                    </div>
                                </div>

                                <div class="form-group" id="surgicalappdiv" style="display: none">
<!--                                    <label for="surgicalapp" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Surgical Approach<span style="color: red"> *</span></label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="surgicalapp" id="surgicalapp" required>
                                            <option value=""> Select </option> 
                                            <option value="antegrade femur"> Antegrade Femur </option> 
                                            <option value="antegrade humerus"> Antegrade Humerus </option> 
                                            <option value="retrograde femur"> Retrograde Femur </option> 
                                            <option value="tibia"> Tibia </option> 
                                            <option value="hip fracture"> Hip Fracture </option>                                                    
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="typefracdiv" style="display: none">
<!--                                    <label for="typefrac" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Type Of Fracture<span style="color: red"> *</span></label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="typefrac"  id="typefrac"  required>
                                            <option value=""> Select </option> 
                                            <option value="closed"> Closed </option> 
                                            <option value="gustilo|"> Gustilo | </option> 
                                            <option value="gustilo||"> Gustilo || </option> 
                                            <option value="gustilo|||a"> Gustilo |||a </option> 
                                            <option value="gustilo|||b"> Gustilo |||b </option> 
                                            <option value="gustilo|||c"> Gustilo |||c </option>                                               
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="reamingdiv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="reaming" id="reaming" >
                                            <option value=""> Select </option> 
                                            <option value="none"> None </option> 
                                            <option value="hand"> Hand </option> 
                                            <option value="power"> Power </option> 

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="fracreducdiv" style="display: none">
                                    <!--                                    <label for="fracreduc" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Fracture Reduction</label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">                             
                                        <input name="fracreduc" id="fracreduc" value="1" type="radio"   /> Open &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input name="fracreduc" id="fracreduc" value="0" type="radio"/> Closed                                                 
                                    </div>
                                </div>

                                <div class="form-group" id="length_naildiv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="length_nail" id="length_nail" >
                                            <option value=""> Select </option> 
                                            <option value="140 mm">140 mm</option>
                                            <option value="160 mm">160 mm</option>
                                            <option value="140 mm">170 mm</option>
                                            <option value="190 mm">190 mm</option>
                                            <option value="200 mm">200 mm</option>
                                            <option value="220 mm">220 mm</option>
                                            <option value="240 mm">240 mm</option>
                                            <option value="260 mm">260 mm</option>
                                            <option value="280 mm">280 mm</option>
                                            <option value="300 mm">300 mm</option>
                                            <option value="320 mm">320 mm</option>
                                            <option value="340 mm">340 mm</option>
                                            <option value="360 mm">360 mm</option>
                                            <option value="380 mm">380 mm</option>
                                            <option value="400 mm">400 mm</option>
                                            <option value="420 mm">420 mm</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="alignmentdiv" style="display: none">  
                                    <!--                                    <label for="alignment" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5 col-lg-offset-1" >Alignment?</label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5"> 
                                        <select class="form-control" name="alignment" id="alignment" >
                                            <option value="10 varus"> Over 10 degrees varus</option> 
                                            <option value="20 varus"> Over 20 degrees varus</option>  
                                            <option value="10 valgus"> Over 10 degrees valgus</option> 
                                            <option value="20 valgus"> Over 20 degrees valgus </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="diameter_naildiv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="diameter_nail" id="diameter_nail" >
                                            <option value=""> Select </option> 
                                            <option value="6 mm">6 mm</option>
                                            <option value="7 mm">7 mm</option>
                                            <option value="8 mm">8 mm</option>
                                            <option value="9 mm">9 mm</option>
                                            <option value="10 mm">10 mm</option>
                                            <option value="11 mm">11 mm</option>
                                            <option value="12 mm">12 mm</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="locfracdiv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="locfrac" id="locfrac" >                                           
                                            <option value="proximal">Proximal</option>
                                            <option value="middle">Middle</option>
                                            <option value="distal">Distal</option>
                                            <option value="segmental">Segmental</option>
                                            <option value="femoralneck">Femoral Neck</option>
                                            <option value="intertrochanteric">Intertrochanteric</option>
                                            <option value="subtrochanteric">Subtrochanteric</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="stafracdiv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="stafrac" id="stafrac" >                                           
                                            <option value="stable">Stable</option>
                                            <option value="unstableposteriormedialfragment">Unstable Posterior Medial Fragment</option>
                                            <option value="unstablelateralfemurwall">Unstable Lateral Femur Wall </option>                                          
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="woundclosurediv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="woundclosure" onchange="hideOther();" id="woundclosure" >                                           
                                            <option value="primary">Primary</option>
                                            <option value="skingraft">Skin Graft</option>
                                            <option value="musicflap">Muscle Flap</option> 
                                            <option value="secondary">Secondary</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="previousimplanttypediv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="previousimplanttype"  id="previousimplanttype" >                                           
                                            <option value="externalfixation">External Fixation</option>
                                            <option value="plate">Plate</option>
                                            <option value="imnail">IM Nail</option> 
                                            <option value="wire">Wire</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="typenaildiv" style="display: none">
                                    <!--                                    <label for="reaming" class="control-label col-lg-2 col-md-2  col-sm-2 col-xs-5" >Method Of Reaming </label>-->
                                    <div class="col-lg-offset-2 col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                        <select class="form-control" name="typenail"  id="typenail" >                                           
                                            <option value="standard nail">Standard Nail</option>
                                            <option value="standard fin nail">Standard Fin Nail</option>
                                            <option value="pediatric fin nail">Pediatric Fin Nail</option> 
                                            <option value="standard hip nail">Standard Hip Nail</option>
                                            <option value="fin hip nail">Fin Hip Nail</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="parametervaluediv">                                  
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2">
                                        <input class=" form-control" id="parametervalue" name="parametervalue"  placeholder="Parameter Value" type="text" required/>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-lg-offset-2">
                                        <button class="btn "  style="background: #21BBC7" onclick="getSearchData();" type="button">Search</button>
                                    </div>

                                </div>
                            </form>
                        </div> 

                        <br/>  <br/> <br/>
                        <div class="adv-table">

                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Case Number</th>
                                        <th>Patient Name</th>                                                                                 
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody id="researchbody">
                                    <?php
                                    if (sizeof($allorthocase)):
                                        foreach ($allorthocase as $datarow):
                                            $id = $datarow->id;
                                            $casenum = $datarow->case_num;
                                            $patient_id = $datarow->patient_id;
                                            $query2 = $this->db->query("SELECT * FROM user WHERE id='$patient_id'");
                                            if ($query2->num_rows() > 0):
                                                $patient_name = $query2->row()->username;
                                            else:
                                                $patient_name = '';
                                            endif;
                                            ?>
                                            <tr>
                                                <td><?php echo $casenum ?></td>
                                                <td><?php echo $patient_name ?> </td>
                                                <td> <a href = "<?php echo site_url('surgery_report/Research/editCaseView/?id=' . $id) ?>">
                                                        <button class = "btn btn-primary btn-xs"><i class = "fa fa-edit"></i>&nbsp;
                                                            View</button></a>
                                                </td>
                                            </tr>

                                            <?php
                                        endforeach;
                                    endif;
                                    ?>


                                </tbody>
                            </table>
                        </div>                                                         
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->


<script>
    function getParameter() {
        var category = $("#category").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('surgery_report/Research/getParameter'); ?>",
            data: 'category=' + category,
            success: function (data) {
                document.getElementById("parameter").innerHTML = data;
//                var viewData = JSON.parse(data);
//                for (var i in viewData) {
//                    var id = viewData[i].Field;
//                    $("#parameter").val(id);
//                }
            }
        });
    }

    function getSearchData() {

        var category = $("#category").val();
        var parameter = $("#parameter").val();
        var parametervalue = $("#parametervalue").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var antibiotic = $('input[name=antibiotic]:checked', '#antibioticdiv').val();
        var fractureside = $('input[name=fractureside]:checked', '#fracturesidediv').val();
        var surgicalapp = $("#surgicalapp").val();
        var typefrac = $("#typefrac").val();
        var reaming = $("#reaming").val();
        var fracreduc = $('input[name=fracreduc]:checked', '#fracreducdiv').val();
        var length_nail = $("#length_nail").val();
        var diameter_nail = $("#diameter_nail").val();
        var alignment = $("#alignment").val();
        var locfrac = $("#locfrac").val();
        var stafrac = $("#stafrac").val();
        var woundclosure = $("#woundclosure").val();
        var previousimplanttype = $("#previousimplanttype").val();
         var typenail = $("#typenail").val();

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('surgery_report/Research/getSearchData'); ?>",
            data: 'category=' + category + '&parameter=' + parameter + '&parametervalue=' + parametervalue + '&from=' + from + '&to=' + to + '&antibiotic=' + antibiotic + '&fractureside=' + fractureside + '&surgicalapp=' + surgicalapp + '&typefrac=' + typefrac + '&reaming=' + reaming + '&fracreduc=' + fracreduc + '&length_nail=' + length_nail + '&diameter_nail=' + diameter_nail + '&alignment=' + alignment + '&locfrac=' + locfrac + '&stafrac=' + stafrac + '&woundclosure=' + woundclosure + '&previousimplanttype=' + previousimplanttype+ '&typenail=' + typenail,
            success: function (data) {
                document.getElementById("researchbody").innerHTML = data;

            }
        });
    }

    function getExtraOption(value) {
        if (value == 'date' || value == 'input_date') {
            $("#daterangediv").show();
            $("#parametervaluediv").hide();
            $("#fracturesidediv").hide();
            $("#antibioticdiv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }
        else if (value == 'antibiotics' || value == 'nonunion' || value == 'previous_implant_used' || value == 'screw_with_nail' || value == 'plate_used'
                || value == 'infection' || value == 'incision_wound' || value == 'infection_depth' || value == 'partial_weight_bear' || value == 'painless_full_weight_bear' || value == 'healing_by_xray' || value == 'knee_flex_greater_90deg'
                || value == 'screw_breakage' || value == 'screw_loosen' || value == 'nail_breakage' || value == 'nail_loosen' || value == 'deformity' || value == 'deformity_rotaion' || value == 'repeat_surgery') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#fracturesidediv").hide();
            $("#antibioticdiv").show();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }
        else if (value == 'fracture_side') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").show();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }
        else if (value == 'surgical_approch') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").show();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }

        else if (value == 'fracture_type') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").show();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }

        else if (value == 'method_of_reaming') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").show();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }

        else if (value == 'fracture_reduction') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").show();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }

        else if (value == 'length_of_nail') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").show();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }
        else if (value == 'diameter_of_nail') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").show();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }

        else if (value == 'deformity_alignment') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").show();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }

        else if (value == 'fracture_location') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").show();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }
        else if (value == 'fracture_stability') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").show();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }
        else if (value == 'wound_closure_method') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").show();
            $("#previousimplanttypediv").hide();
               $("#typenaildiv").hide();
        }

        else if (value == 'previous_implant_type') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").show();
               $("#typenaildiv").hide();
        }
         else if (value == 'type_of_nail_used') {
            $("#daterangediv").hide();
            $("#parametervaluediv").hide();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
            $("#typenaildiv").show();
        }
        
        else {
            $("#daterangediv").hide();
            $("#parametervaluediv").show();
            $("#antibioticdiv").hide();
            $("#fracturesidediv").hide();
            $("#surgicalappdiv").hide();
            $("#typefracdiv").hide();
            $("#reamingdiv").hide();
            $("#fracreducdiv").hide();
            $("#length_naildiv").hide();
            $("#diameter_naildiv").hide();
            $("#alignmentdiv").hide();
            $("#locfracdiv").hide();
            $("#stafracdiv").hide();
            $("#woundclosurediv").hide();
            $("#previousimplanttypediv").hide();
             $("#typenaildiv").hide();

        }
    }

    function hideOther() {
        var getval = $("#woundclosure").val();
        if (getval == "other") {
            $("#parametervaluediv").show();
        } else {
            $("#parametervalue").val("");
            $("#parametervaluediv").hide();
        }
    }


</script>


