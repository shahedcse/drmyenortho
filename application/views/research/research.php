
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">                  
                        Search Prescription                 
                    </header>
                    <div class="panel-body">                 
                        <!--  start surgical cases-->                    
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <!--  start surgical cases-->                    
                        <div class="form">
                            <form class="form-horizontal table-bordered"   id="addPatientForm" method="POST" action="<?php //echo site_url('research/Research/getSearchData');                                                                                       ?>" enctype="multipart/form-data">
                                <div class="container-fluid">

                                    <div class="form-group" id="daterangediv" style="padding-top: 20px;padding-left: 16px;">
                                        <span class="help-block">Select date range</span>
                                        <div class="input-group input-small col-lg-3">
                                            <input type="text" class="form-control dpd1" value="<?php
                                            echo date("01-01-Y");
                                            ?>" id="from" name="from">
                                            <span class="input-group-addon">To</span>
                                            <input type="text" class="form-control dpd2" value="<?php
                                            echo date("d-m-Y");
                                            ?>"  id="to" name="to">
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group" >
                                        <label for="patientid" class="control-label col-lg-2"><b>Search Parameter:</b></label>
                                        <div class="col-lg-3">
                                            <select class="form-control" name="" id="searchpara" onchange="getParameter();">
                                                <option value="">--Select--</option>
                                                <option value="area">Area Of Treatment</option>
                                                <option value="others">Others, Area Of Treatment</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group" id="areaDiv" hidden="hidden">
                                        <div class="col-lg-3">
                                            <select class="form-control" name="" id="reference1id" onchange="getReferunder(this.value)">                                               
                                                <?php
                                                if (sizeof($referencelist) > 0):
                                                    echo ' <option value="">Select Area Of Treatment1</option> ';
                                                    foreach ($referencelist as $datarow):
                                                        echo '<option value="' . $datarow->id . ',' . $datarow->ref_name . '">' . $datarow->ref_name . '</option> ';
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                            <span id="reference1text"> </span>
                                        </div>
                                        <div class="col-lg-3" id="area2" hidden="hidden">
                                            <select class="form-control" name="" id="reference2id" onchange="getReferunder2(this.value)">                                               
                                                <option value="">Select Area Of Treatment2</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3" id="area3" hidden="hidden">
                                            <select class="form-control" name="" id="reference3id" onchange="getReferunder3(this.value)">                                               
                                                <option value="">Select Area Of Treatment3</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3" id="area4" hidden="hidden">
                                            <select class="form-control" name="" id="reference4id">                                               
                                                <option value="">Select Area Of Treatment4</option>
                                            </select>
                                        </div>                                       
                                    </div> 


                                    <div class="form-group" id="otherrefDiv" hidden="hidden">                                   
                                        <div class="col-lg-6 col-lg-offset-2">
                                            <input  type="text" id="otherref" name="otherref">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-10 col-lg-offset-2">
                                            <button class="btn btn-primary" onclick="getSearchData();"   type="button">Search</button>
                                        </div>
                                    </div>



                                </div>

                                <div class="container-fluid">
                                    <div class="col-lg-10">
                                        <span class="help-block" id="treatment1" style="color: #000"></span>                                     
                                    </div>
                                    <div class="col-lg-10">
                                        <span class="help-block" id="res" style="color: #000"></span>                                     
                                    </div>

                                </div>


                            </form>
                        </div> 

                        <br/> 
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Prescription Id</th>
                                        <th>Patient Name</th>     
                                        <th>Email</th>   
                                        <th>Mobile</th>   
                                        <th>Address</th>   
                                        <th> Blood Group </th>
                                        <th> Sex </th>
                                        <th> Age </th>
                                        <th>Area Of Treatment</th>
                                        <th>Date</th>                                       
                                    </tr>
                                </thead>
                                <tbody id="researchbody">
                                    <?php
                                    if (sizeof($presmasterData)):
                                        foreach ($presmasterData as $datarow):
                                            ?>
                                            <tr>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->pres_id; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->first_name . ' ' . $datarow->last_name; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->email; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->mobile; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->address; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->blood_group; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->sex; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->age; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo $datarow->reference_value; ?></a></td>
                                                <td><a href="<?php                                               
                                                    echo site_url('research/research/researchdetails?id=' . $datarow->id);
                                                    ?>"><?php echo date('d-m-Y h:i A', strtotime($datarow->datetime)); ?></a></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>


                                </tbody>
                            </table>
                        </div>                                                        
                    </div>
                    <!--        end surgical cases-->                                                          
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->


<script>


    function getParameter() {

        var para = $("#searchpara").val();
        if (para == "area") {
            $("#otherref").val("");
            $("#otherrefDiv").hide();
            $("#areaDiv").show();
        } else {

            $("#treatment1").val("");
            $("#reference1id").val("");
            $("#reference2id").val("");
            $("#reference3id").val("");
            $("#reference4id").val("");
            $("#areaDiv").hide();
            $("#otherrefDiv").show();
        }
    }



    function getReferunder(id) {
        var curr = id.split(',');
        $("#reference1").val(curr[0]);
        $("#reference1name").val(curr[1]);
        $("#reference1text").text("");
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('research/Research/getFirstReference1'); ?>",
            data: 'id=' + id,
            success: function (data) {
                if (data != 'no data found') {
                    $("#area2").show();
                    $("#reference2id").html(data);
                    $("#reference3id").val("");
                    $("#area3").hide();
                    $("#reference4id").val("");
                    $("#area4").hide();
                } else {
                    $("#reference2id").val("");
                    $("#area2").hide();
                }
            }
        });
    }



    function getReferunder2(id) {
        var curr = id.split(',');
        $("#reference2").val(curr[0]);
        $("#reference2name").val(curr[1]);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('research/Research/getFirstReference2'); ?>",
            data: 'id=' + id,
            success: function (data) {
                if (data != 'no data found') {
                    $("#area3").show();
                    $("#reference3id").html(data);
                    $("#reference4id").val("");
                    $("#area4").hide();
                } else {
                    $("#reference3id").val("");
                    $("#area3").hide();
                }
            }
        });
    }

    function getReferunder3(id) {
        var curr = id.split(',');
        $("#reference3").val(curr[0]);
        $("#reference3name").val(curr[1]);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('research/Research/getFirstReference3'); ?>",
            data: 'id=' + id,
            success: function (data) {
                if (data != 'no data found') {
                    $("#area4").show();
                    $("#reference4id").html(data);
                } else {
                    $("#reference4id").val("");
                    $("#area4").hide();
                }
            }
        });
    }




    function getSearchData() {

        var area1 = $("#reference1id").val();
        var area2 = $("#reference2id").val();
        var area3 = $("#reference3id").val();
        var area4 = $("#reference4id").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var otherref = $("#otherref").val();


        var area1value = "", area2value = "", area3value = "", area4value = "";
        if (area1 != "") {
            var area = area1.split(",");
            area1value = area[1];
        }
        if (area2 != "") {
            var area = area2.split(",");
            area2value = area[1];
        }
        if (area3 != "") {
            var area = area3.split(",");
            area3value = area[1];
        }
        if (area4 != "") {
            var area = area4.split(",");
            area4value = area[1];
        }


        if (area1value != "") {
            $("#treatment1").html("<b>Area Of Treatment : </b>" + area1value);
        }
        if (area2value != "") {
            $("#treatment1").html("<b>Area Of Treatment : </b>" + area1value + "->" + area2value);
        }
        if (area3value != "") {
            $("#treatment1").html("<b>Area Of Treatment : </b>" + area1value + "->" + area2value + "->" + area3value);
        }
        if (area4value != "") {
            $("#treatment1").html("<b>Area Of Treatment : </b>" + area1value + "->" + area2value + "->" + area3value + "->" + area4value);
        }


        var id;
        if (area4 != "") {
            id = area4;
        } else if (area3 != "") {
            id = area3;
        } else if (area2 != "") {
            id = area2;
        } else if (area1 != "") {
            id = area1;
        } else {
            id = "";
        }

        if (area1 != "") {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('research/Research/getSearchData'); ?>",
                data: 'id=' + id + '&from=' + from + '&to=' + to,
                success: function (data) {
                    var count = (data.match(/gradeX/g) || []).length;
                    $("#res").html("<b>Total result found :</b> " + count);
                    document.getElementById("researchbody").innerHTML = data;
                }
            });
        } else if (otherref != "") {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('research/Research/getSearchDataOthers'); ?>",
                data: 'other=' + otherref + '&from=' + from + '&to=' + to,
                success: function (data) {
                    var count = (data.match(/gradeX/g) || []).length;
                    $("#res").html("<b>Total result found :</b> " + count);
                    document.getElementById("researchbody").innerHTML = data;
                }
            });
        }
        else {
//            $("#reference1text").text("Please select a value");
//            $("#reference1text").css("color", "red");
        }


    }


</script>


