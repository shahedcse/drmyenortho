<style>
    .space{
        padding: 0px;
        margin: 0px;
    }  
    a:link {
        color: green;
        background-color: transparent;
        text-decoration: none;
    }
    a:visited {
        color: pink;
        background-color: transparent;
        text-decoration: none;
    }
    a:hover {
        color: red;
        background-color: transparent;
        text-decoration: underline;
    }
    a:active {
        color: yellow;
        background-color: transparent;
        text-decoration: underline;
    }
</style>


<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">             
                <section class="panel">
                    <header class="panel-heading ">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#cases"  style="font-weight: bold">Surgical Cases</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#codes"  style="font-weight: bold">Surgical Codes</a>
                            </li>                                                           
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">  
                            <!--        start surgical cases-->
                            <div id="cases" class="tab-pane active">
                                <?php
                                if ($this->session->userdata('successfull')):
                                    echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                                    $this->session->unset_userdata('successfull');
                                endif;
                                if ($this->session->userdata('failed')):
                                    echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                                    $this->session->unset_userdata('failed');
                                endif;
                                ?>
                                <div class="adv-table">
                                    <span class="tools pull-right">
                                        <button class="btn  btn-success"  data-toggle="tab" href="#addSurgery">
                                            Add Surgery Case&nbsp;<i class="fa fa-plus"></i>
                                        </button> 
                                    </span>
                                    <table  class="display table table-bordered table-striped" id="pms-datatable">
                                        <thead>
                                            <tr>
                                                <th>X-rays</th>
                                                <th>Input Date</th>
                                                <th>Patient Name</th>
                                                <th>Surgery Date</th>                                              
                                                <th>Program Manager</th>
                                                <th>Case Number</th>
                                                <th>Options</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>                                                         
                            </div>
                            <!--        end surgical cases-->
                            <!--        start surgical codes-->
                            <div id="codes" class="tab-pane">
                                <?php
                                if ($this->session->userdata('successfull')):
                                    echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                                    $this->session->unset_userdata('successfull');
                                endif;
                                if ($this->session->userdata('failed')):
                                    echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                                    $this->session->unset_userdata('failed');
                                endif;
                                ?>
                                <div class="adv-table">
                                    <span class="tools pull-right">
                                        <button class="btn  btn-success"  data-toggle="tab" href="#addSurgeryCodes">
                                            Add Surgery Codes&nbsp;<i class="fa fa-plus"></i>
                                        </button> 
                                    </span>
                                    <table  class="display table table-bordered table-striped" id="codes-datatable">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Description</th>
                                                <th>Type</th>

                                                <th>Options</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                            <!--        end surgical codes-->





                            <!--     start add   surgery-->
                            <div id="addSurgery" class="tab-pane ">                              
                                <h6 class="panel-heading" style="background-color: #23527c">
                                    Enter Patient Case Information
                                </h6>
                                <div class="form">
                                    <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">


                                        <div class="form-group" style="padding-top: 20px" >
                                            <label for="casenumber" class="control-label col-lg-3" style="font-weight: bold">Case Number</label>
                                            <div class="col-lg-8">
                                                <input class=" form-control" id="casenumber" name="casenumber" type="text"/>
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="codenumber" class="control-label col-lg-3" style="font-weight: bold">Code</label>
                                            <div class="col-lg-8">
                                                <input class=" form-control" id="codenumber" name="codenumber" type="text" />
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="form-group" >
                                            <label for="patientname" class="control-label col-lg-3" style="font-weight: bold">Patient Name<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="patientname" id="patientname" required>
                                                    <option selected="selected" value="">--Select Name--</option>
                                                    <option value="01">Rahim</option>
                                                    <option value="02">Karim</option>
                                                    <option value="03">Setu</option>                                                                                          
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="sex" class="control-label col-lg-3" style="font-weight: bold">Gender<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <input name="sex" id="sex" value="Male" type="radio" required  /> Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input name="sex" id="sex" value="Female" type="radio"/> Female                           
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="birthday" class="control-label col-lg-3" style="font-weight: bold">Date Of Birth<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">
                <!--                                <input class="form-control form-control-inline input-medium default-date-picker"  data-date-format="dd-mm-yyyy" id="birthday"  name="birthday"  size="16" type="text" value="" />-->
                                                <div class="col-lg-4">
                                                    <select class="form-control m-bot15" name="birthday" id="birthday" required>
                                                        <option selected="selected" value="">--Select Day--</option>
                                                        <?php
                                                        for ($day = 1; $day <= 31; $day++):
                                                            ?>
                                                            <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                            <?php
                                                        endfor;
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <select class="form-control m-bot15" name="birthmonth" id="birthmonth" required>
                                                        <option selected="selected" value="">--Select Month--</option>
                                                        <option value="01">January</option>
                                                        <option value="02">February</option>
                                                        <option value="03">March</option>
                                                        <option value="04">April</option>
                                                        <option value="05">May</option>
                                                        <option value="06">June</option>
                                                        <option value="07">July</option>
                                                        <option value="08">August</option>
                                                        <option value="09">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <select class="form-control m-bot15" name="birthyear" id="birthyear" required>
                                                        <option selected="selected" value="">--Select Year--</option>
                                                        <?php
                                                        $currentyr = date('Y');
                                                        for ($year = 1901; $year <= $currentyr; $year++):
                                                            ?>
                                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                            <?php
                                                        endfor;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="mobile" class="control-label col-lg-3" style="font-weight: bold">Mobile<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">
                                                <input class="form-control" id="mobile" name="mobile" minlength="10" maxlength="14" type="tel" required />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="email" class="control-label col-lg-3" style="font-weight: bold">E-Mail</label>
                                            <div class="col-lg-8">
                                                <input class="form-control " id="email" type="email" name="email"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label for="address" class="control-label col-lg-3" style="font-weight: bold">Address</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" id="address" name="address"></textarea>
                                            </div>
                                        </div>
                                        <hr/>                         
                                        <div class="form-group">
                                            <label for="userfile" class="control-label col-lg-3" style="font-weight: bold">Image<br>Max (200&#10005;200)px</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" id="userfile" name="userfile" type="file"  />
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <h6 class="panel-heading" style="background-color: #23527c">
                                    Enter Fracture Name
                                </h6>
                                <div class="form">
                                    <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">

                                        <div class="form-group" style="padding-top: 20px;" >
                                            <label for="patientname" class="control-label col-lg-3" style="font-weight: bold">Surgeon(s)<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">
                                                <input class=" form-control" id="patientname" name="patientname" minlength="4" type="text" required />
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="form-group">
                                            <label for="sex" class="control-label col-lg-3" style="font-weight: bold">Fracture Side<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <input name="sex" id="sex" value="left" type="radio" required  /> Left &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input name="sex" id="sex" value="right" type="radio"/> Right                           
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="hospitalname" class="control-label col-lg-3" style="font-weight: bold">Surgical Approach<span style="color: red"> *</span> </label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="hospitalname" id="hospitalname" required>
                                                    <option value=""> --Select-- </option> 
                                                    <option value="A+"> Antegrade Femur </option> 
                                                    <option value="A-"> Antegrade Humerus </option> 
                                                    <option value="B+"> Retrograde Femur </option> 
                                                    <option value="B-"> Tibia </option> 
                                                    <option value="AB+"> Hip Fracture </option>                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="locfrac" class="control-label col-lg-3" style="font-weight: bold">Location Of Fracture<span style="color: red"> *</span></label>
                                            <label for="locfrac" class="control-label col-lg-2" style="font-style: italic;color: #000">Check All That Apply</label>
                                            <div class="col-lg-7">                             
                                                <input name="locfrac" id="locfrac" value="proximal" type="radio" required  /> Proximal &nbsp;
                                                <input name="locfrac" id="locfrac" value="middle" type="radio"/> Middle &nbsp;
                                                <input name="locfrac" id="locfrac" value="distal" type="radio" required  /> Distal &nbsp;
                                                <input name="locfrac" id="locfrac" value="segmental" type="radio"/> Segmental &nbsp;
                                                <input name="locfrac" id="locfrac" value="femoralneck" type="radio"/> Femoral Neck &nbsp;
                                                <input name="locfrac" id="locfrac" value="intertrochanteric" type="radio" required  /> Intertrochanteric &nbsp;
                                                <input name="locfrac" id="locfrac" value="subtrochanteric" type="radio"/> Subtrochanteric
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="typefrac" class="control-label col-lg-3" style="font-weight: bold">Type Of Fracture<span style="color: red"> *</span> </label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="typefrac" id="typefrac" required>
                                                    <option value=""> --Select-- </option> 
                                                    <option value="closed"> Closed </option> 
                                                    <option value="gustilo|"> Gustilo | </option> 
                                                    <option value="gustilo||"> Gustilo || </option> 
                                                    <option value="gustilo|||a"> Gustilo |||a </option> 
                                                    <option value="gustilo|||b"> Gustilo |||b </option> 
                                                    <option value="gustilo|||c"> Gustilo |||c </option>                                               
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="stafrac" class="control-label col-lg-3" style="font-weight: bold">Stability Of Fracture<span style="color: red"> *</span></label>
                                            <label for="locfrac" class="control-label col-lg-2" style="font-style: italic;color: #000">Check All That Apply</label>
                                            <div class="col-lg-7">                             
                                                <input name="stafrac" id="stafrac" value="stable" type="radio" required  /> Stable &nbsp;&nbsp;
                                                <input name="stafrac" id="stafrac" value="unstableposteriormedialfragment " type="radio"/> Unstable Posterior Medial Fragment &nbsp;&nbsp;
                                                <input name="stafrac" id="stafrac" value="unstablelateralfemurwall " type="radio" required  /> Unstable Lateral Femur Wall 

                                            </div>
                                        </div>                      
                                        <hr/>
                                        <div class="form-group">
                                            <label for="nonunion" class="control-label col-lg-3" style="font-weight: bold">Nonunion<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <input name="nonunion" id="nonunion" value="yes" type="radio" required  /> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input name="nonunion" id="nonunion" value="no" type="radio"/> No                           
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="preimplant" class="control-label col-lg-3" style="font-weight: bold">Previous Implant Used<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <input name="preimplant" id="preimplant" value="yes" type="radio" required  /> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input name="preimplant" id="preimplant" value="no" type="radio"/> No                           
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="form-group">
                                            <label for="reaming" class="control-label col-lg-3" style="font-weight: bold">Method Of Reaming<span style="color: red"> *</span> </label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="reaming" id="reaming" required>
                                                    <option value=""> --Select-- </option> 
                                                    <option value="none"> None </option> 
                                                    <option value="hand"> Hand </option> 
                                                    <option value="power"> Power </option> 

                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="fracreduc" class="control-label col-lg-3" style="font-weight: bold">Fracture Reduction<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <input name="fracreduc" id="fracreduc" value="open" type="radio" required  /> Open &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input name="fracreduc" id="fracreduc" value="closed" type="radio"/> Closed                                                 
                                            </div>
                                        </div>                      
                                        <hr/>
                                        <div class="form-group ">
                                            <label for="address" class="control-label col-lg-3" style="font-weight: bold">Comments</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" id="address" name="address"></textarea>
                                            </div>
                                        </div>



                                    </form>
                                </div>

                                <h6 class="panel-heading" style="background-color: #23527c">
                                    Enter Nail Information
                                </h6>
                                <div class="form">
                                    <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                        <div class="form-group" style="padding-top: 20px;">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Type Of Nail Used<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <input name="typenail" id="typenail" value="standard nail" type="radio" required  /> Standard Nail &nbsp;&nbsp;
                                                <input name="typenail" id="typenail" value="standard fin nail" type="radio"/> Standard Fin Nail &nbsp;&nbsp; 
                                                <input name="typenail" id="typenail" value="pediatric fin nail" type="radio" required  /> Pediatric Fin Nail &nbsp;&nbsp;
                                                <input name="typenail" id="typenail" value="standard hip nail" type="radio"/> Standard Hip Nail &nbsp;&nbsp;
                                                <input name="typenail" id="typenail" value="fin hip nail" type="radio"/> Fin Hip Nail &nbsp;&nbsp;
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="hospitalname" class="control-label col-lg-3" style="font-weight: bold">Length Of Nail<span style="color: red"> *</span> </label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="hospitalname" id="hospitalname" required>
                                                    <option value=""> --Select-- </option> 

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="hospitalname" class="control-label col-lg-3" style="font-weight: bold">Diameter Of Nail<span style="color: red"> *</span> </label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="hospitalname" id="hospitalname" required>
                                                    <option value=""> --Select-- </option> 

                                                </select>
                                            </div>
                                        </div>

                                    </form>
                                </div>


                                <h6 class="panel-heading" style="background-color: #23527c">
                                    Enter Screw Information
                                </h6>
                                <div class="form">
                                    <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                        <div class="form-group" style="padding-top: 20px;">
                                            <label for="statusscrew" class="control-label col-lg-3" style="font-weight: bold">Did You Use Screw With This Nail?<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <input name="statusscrew" id="statusscrew" value="yes" type="radio" required  /> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input name="statusscrew" id="statusscrew" value="no" type="radio"/> No                           
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label for="sex" class="control-label col-lg-3" style="font-weight: bold">Standard Interlocking Screws<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                                                    <thead>
                                                        <tr>
                                                            <th>Length in mm</th>
                                                            <th>25</th>
                                                            <th>30</th>
                                                            <th>35</th>
                                                            <th>40</th>
                                                            <th>45</th>
                                                            <th>50</th>
                                                            <th>55</th>
                                                            <th>60</th>
                                                            <th>65</th>
                                                            <th>70</th>
                                                            <th>75</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="">
                                                            <td># Of Proximal</td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                        </tr>  
                                                        <tr class="">
                                                            <td># Of Distal</td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                        </tr>
                                                    </tbody>
                                                </table>                          
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="sex" class="control-label col-lg-3" style="font-weight: bold">Compression Screws<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                                                    <thead>
                                                        <tr>
                                                            <th>Length in mm</th>
                                                            <th>25</th>
                                                            <th>30</th>
                                                            <th>35</th>
                                                            <th>40</th>
                                                            <th>45</th>
                                                            <th>50</th>
                                                            <th>55</th>
                                                            <th>60</th>
                                                            <th>65</th>
                                                            <th>70</th>
                                                            <th>75</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="">
                                                            <td>Qty</td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                        </tr>  

                                                    </tbody>
                                                </table>                          
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="sex" class="control-label col-lg-3" style="font-weight: bold">SHC Proximal Interlocking Screws<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                                                    <thead>
                                                        <tr>
                                                            <th>Length in mm</th>
                                                            <th>25</th>
                                                            <th>30</th>
                                                            <th>35</th>
                                                            <th>40</th>
                                                            <th>45</th>
                                                            <th>50</th>
                                                            <th>55</th>
                                                            <th>60</th>
                                                            <th>65</th>
                                                            <th>70</th>
                                                            <th>75</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="">
                                                            <td>Qty</td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                        </tr>  

                                                    </tbody>
                                                </table>                          
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="sex" class="control-label col-lg-3" style="font-weight: bold">SHC Cortical Screws<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">                             
                                                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                                                    <thead>
                                                        <tr>
                                                            <th>Length in mm</th>
                                                            <th>25</th>
                                                            <th>30</th>
                                                            <th>35</th>
                                                            <th>40</th>
                                                            <th>45</th>
                                                            <th>50</th>
                                                            <th>55</th>
                                                            <th>60</th>
                                                            <th>65</th>
                                                            <th>70</th>
                                                            <th>75</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="">
                                                            <td>Qty</td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                            <td><input type="text" class="form-control small" ></td>
                                                        </tr>  

                                                    </tbody>
                                                </table>                          
                                            </div>
                                        </div>
                                        <hr/>



                                    </form>
                                </div>

                                <h6 class="panel-heading" style="background-color: #23527c">
                                    Enter Plate Components Used
                                </h6>
                                <div class="form">
                                    <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                        <div class="form-group space" style="padding-top: 20px;">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Plates Used<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="typenail" id="stdNail" value="standard nail" onclick="displayPlate();" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="typenail" id="stdFinNail" value="standard fin nail"  onclick="displayPlate();" type="radio"/> No  &nbsp;&nbsp; 

                                            </div>
                                        </div>
                                        <div class="form-group space" style="padding-top: 20px;display: none" id="one">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Rod Plate Used<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="rpu" id="rpu1" value="standard nail" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="rpu" id="rpu2" value="standard fin nail" type="radio"/> No  &nbsp;&nbsp; 

                                            </div>
                                        </div>
                                        <div class="form-group space" style="padding-top: 20px;display: none" id="two">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Rod Connector<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="rc" id="rc1" value="standard nail" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="rc" id="rc2" value="standard fin nail" type="radio"/> No  &nbsp;&nbsp; 

                                            </div>
                                        </div>
                                        <div class="form-group space" style="padding-top: 20px;display: none" id="three">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Unicortical Screw<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="us" id="us1" value="standard nail" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="us" id="us2" value="standard fin nail" type="radio"/> No  &nbsp;&nbsp; 
                                            </div>
                                        </div>
                                        <div class="form-group space" style="padding-top: 20px;display: none" id="four">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">3 Hole Plate 75 mm<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="3hp" id="3hp1" value="standard nail" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="3hp" id="3hp2" value="standard fin nail" type="radio"/> No  &nbsp;&nbsp; 

                                            </div>
                                        </div>
                                        <div class="form-group space" style="padding-top: 20px;display: none" id="five">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Hv 2 Hole Plate<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="h2hp" id="h2hp1" value="standard nail" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="h2hp" id="h2hp2" value="standard fin nail" type="radio"/> No  &nbsp;&nbsp; 

                                            </div>
                                        </div>
                                        <div class="form-group space" style="padding-top: 20px;display: none" id="six">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Hv 3 Hole Plate<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="h3hp" id="h3hp1" value="standard nail" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="h3hp" id="h3hp2" value="standard fin nail" type="radio"/> No  &nbsp;&nbsp; 

                                            </div>
                                        </div>
                                        <div class="form-group space" style="padding-top: 20px;display: none" id="seven">
                                            <label for="typenail" class="control-label col-lg-3" style="font-weight: bold">Hv 4 Hole Plate<span style="color: red"> *</span></label>
                                            <div class="col-lg-8 space">                             
                                                <input name="h4hp" id="h4hp1" value="standard nail" type="radio" required  /> Yes &nbsp;&nbsp;
                                                <input name="h4hp" id="h4hp2" value="standard fin nail" type="radio"/> No  &nbsp;&nbsp; 

                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <h6 class="panel-heading" style="background-color: #23527c">
                                    X-Ray Images
                                </h6>
                                <div class="form">
                                    <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                        <div class="form-group" style="padding-top: 20px;">
                                            <div class="col-lg-4 col-lg-offset-3">
                                                <div class="col-lg-6" style="background: #0075b0; padding: 10px;">                              
                                                    <img class="col-lg-offset-2"  src="<?php echo $baseurl . "/assets/uploads/profile/default.jpg" ?>" alt="" height="150" width="100">
                                                    <span class="col-lg-offset-2" style="color: #000;font-weight: bold">X-Ray #24567</span>
                                                    <div class="col-lg-offset-2 col-lg-8" >
                                                        <select class="form-control input-sm"  name="birthmonth" id="birthmonth" required>
                                                            <option selected="selected" value="">--Select--</option>
                                                            <option value="01">Pre-Op</option>                                                                                                                           
                                                        </select>
                                                    </div>
                                                    <div class="row" style="margin: 0px 3px;">                      
                                                        <div class="col-lg-4 space">
                                                            <select class="form-control input-sm" name="birthday" id="birthday" required>
                                                                <option selected="selected" value="">--Day--</option>
                                                                <?php
                                                                for ($day = 1; $day <= 31; $day++):
                                                                    ?>
                                                                    <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                                    <?php
                                                                endfor;
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 space">
                                                            <select class="form-control input-sm" name="birthmonth" id="birthmonth" required>
                                                                <option selected="selected" value="">--Month--</option>
                                                                <option value="01">January</option>
                                                                <option value="02">February</option>
                                                                <option value="03">March</option>
                                                                <option value="04">April</option>
                                                                <option value="05">May</option>
                                                                <option value="06">June</option>
                                                                <option value="07">July</option>
                                                                <option value="08">August</option>
                                                                <option value="09">September</option>
                                                                <option value="10">October</option>
                                                                <option value="11">November</option>
                                                                <option value="12">December</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 space">
                                                            <select class="form-control input-sm" name="birthyear" id="birthyear" required>
                                                                <option selected="selected" value="">--Year--</option>
                                                                <?php
                                                                $currentyr = date('Y');
                                                                for ($year = 1901; $year <= $currentyr; $year++):
                                                                    ?>
                                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                                    <?php
                                                                endfor;
                                                                ?>
                                                            </select>
                                                        </div>                                                       
                                                    </div>                                       
                                                    <div class="col-lg-offset-2 col-lg-8" >
                                                        <a href="#">&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> 
                                                    </div>
                                                </div>
                                                <!--                                            end div-->
                                                <div class="col-lg-6" style="background: #18d4cb; padding: 10px;">                                         
                                                    <img class="col-lg-offset-2" src="<?php echo $baseurl . "/assets/uploads/profile/default.jpg" ?>" alt="" height="150" width="100">                                                  
                                                    <span class="col-lg-offset-3" style="color: #000;font-weight: bold">X-Ray #24568</span>
                                                    <div class="col-lg-offset-2 col-lg-8" >
                                                        <select class="form-control input-sm"  name="birthmonth" id="birthmonth" required>
                                                            <option selected="selected" value="">--Select--</option>
                                                            <option value="01">Post-Op</option>                                                                                                                           
                                                        </select>
                                                    </div>
                                                    <div class="row" style="margin: 0px 3px;">                                                 
                                                        <div class="col-lg-4 space">
                                                            <select class="form-control input-sm" name="birthday" id="birthday" required>
                                                                <option selected="selected" value="">--Day--</option>
                                                                <?php
                                                                for ($day = 1; $day <= 31; $day++):
                                                                    ?>
                                                                    <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                                    <?php
                                                                endfor;
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 space">
                                                            <select class="form-control input-sm" name="birthmonth" id="birthmonth" required>
                                                                <option selected="selected" value="">--Month--</option>
                                                                <option value="01">January</option>
                                                                <option value="02">February</option>
                                                                <option value="03">March</option>
                                                                <option value="04">April</option>
                                                                <option value="05">May</option>
                                                                <option value="06">June</option>
                                                                <option value="07">July</option>
                                                                <option value="08">August</option>
                                                                <option value="09">September</option>
                                                                <option value="10">October</option>
                                                                <option value="11">November</option>
                                                                <option value="12">December</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 space">
                                                            <select class="form-control input-sm" name="birthyear" id="birthyear" required>
                                                                <option selected="selected" value="">--Year--</option>
                                                                <?php
                                                                $currentyr = date('Y');
                                                                for ($year = 1901; $year <= $currentyr; $year++):
                                                                    ?>
                                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                                    <?php
                                                                endfor;
                                                                ?>
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-offset-2 col-lg-8" >
                                                        <a>&nbsp;&nbsp;Delete&nbsp;&nbsp;</a> 
                                                    </div>

                                                </div>  
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-9">
                                                <button class="btn btn-success" type="submit">&nbsp;&nbsp;+ Add An X-Ray Image To This Fracture&nbsp;&nbsp;</button>                               
                                            </div>
                                        </div>
                                        <!--                                        end formgroup-->

                                        <hr/>
                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-9">
                                                <button class="btn btn-success" type="submit">&nbsp;&nbsp;Save & Continue&nbsp;&nbsp;</button>                               
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                            <!--     end add   surgery-->
                            <!--   start add surgery codes-->
                            <div id="addSurgeryCodes" class="tab-pane "> 
                                <h6 class="panel-heading" style="background-color: #23527c">
                                    Enter New Surgical Codes
                                </h6>
                                <div class="form">
                                    <form class="form-horizontal table-bordered" id="addPatientForm" method="POST" action="<?php echo site_url('#'); ?>" enctype="multipart/form-data">
                                        <div class="form-group" style="padding-top: 20px" >
                                            <label for="codenumber" class="control-label col-lg-3" style="font-weight: bold">Code<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">
                                                <input class=" form-control" id="codenumber" name="codenumber" type="text" required/>
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="patientname" class="control-label col-lg-3" style="font-weight: bold">Type<span style="color: red"> *</span></label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="patientname" id="patientname" required>
                                                    <option selected="selected" value="">--Select--</option>
                                                    <option value="01">Fracture</option>
                                                    <option value="02">Surgery</option>
                                                    <option value="03">Followup</option>                                                                                          
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="email" class="control-label col-lg-3" style="font-weight: bold">Code Description</label>
                                            <div class="col-lg-8">
                                                <input class="form-control " id="description" type="text" name="description"/>
                                            </div>
                                        </div>                                   
                                        <hr/>                         
                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-9">
                                                <button class="btn btn-success" type="submit">&nbsp;&nbsp;Save & Continue&nbsp;&nbsp;</button>                               
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            <!--   end add surgery codes-->
                        </div>
                    </div>
                </section>              
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<script language="javascript">

    function  displayPlate() {
        if (document.getElementById('stdNail').checked) {
            document.getElementById("one").style.display = 'block';
            document.getElementById("two").style.display = 'block';
            document.getElementById("three").style.display = 'block';
            document.getElementById("four").style.display = 'block';
            document.getElementById("five").style.display = 'block';
            document.getElementById("six").style.display = 'block';
            document.getElementById("seven").style.display = 'block';

        } else if (document.getElementById('stdFinNail').checked) {
            document.getElementById("one").style.display = 'none';
            document.getElementById('rpu1').checked = false;
            document.getElementById('rpu2').checked = false;
            document.getElementById("two").style.display = 'none';
            document.getElementById('rc1').checked = false;
            document.getElementById('rc2').checked = false;
            document.getElementById("three").style.display = 'none';
            document.getElementById('us1').checked = false;
            document.getElementById('us2').checked = false;
            document.getElementById("four").style.display = 'none';
            document.getElementById('3hp1').checked = false;
            document.getElementById('3hp2').checked = false;
            document.getElementById("five").style.display = 'none';
            document.getElementById('h2hp1').checked = false;
            document.getElementById('h2hp2').checked = false;
            document.getElementById("six").style.display = 'none';
            document.getElementById('h3hp1').checked = false;
            document.getElementById('h3hp2').checked = false;
            document.getElementById("seven").style.display = 'none';
            document.getElementById('h4hp1').checked = false;
            document.getElementById('h4hp2').checked = false;
        }
    }


    function submitForms() {
        document.form1.submit();
        document.form1.submit();
    }
</script>



