<!--footer start-->
<footer class="site-footer">
    <div class="text-center">
        <?php echo date("Y"); ?> &copy; <a style="color: #FFFFFF" href="http://clouditbd.com" target="_blank">Cloud IT Ltd.</a>
        <a href="#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>
<!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster --> 
<!-- custom search start here-->
<script type="text/javascript" src="<?php echo $baseurl; ?>assets/customsearch/js/bootstrap-select.js"></script>
<!-- custom search end here-->
<script src="<?php echo $baseurl; ?>assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo $baseurl; ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo $baseurl; ?>assets/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="<?php echo $baseurl; ?>assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/owl.carousel.js" ></script>
<script src="<?php echo $baseurl; ?>assets/js/jquery.customSelect.min.js" ></script>
<script src="<?php echo $baseurl; ?>assets/js/respond.min.js" ></script>
<!--jquery form validation -->
<script type="text/javascript" src="<?php echo $baseurl; ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/form-validation-script.js"></script>
<!--/jquery form validation -->
<!--wysihtml5 start-->
<script type="text/javascript" src="<?php echo $baseurl; ?>assets/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<!--/wysihtml5 start-->
<!--summernote-->
<script src="<?php echo $baseurl; ?>assets/assets/summernote/dist/summernote.min.js"></script>
<!--/summernote-->
<!--dynamic table initialization -->
<script src="<?php echo $baseurl; ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/dynamic_table_init.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $baseurl; ?>assets/js/jquery.dataTables.js"></script>
<!-- <script type="text/javascript" language="javascript" src="<?php echo $baseurl; ?>assets/assets/advanced-datatable/media/js/jquery.dataTables.js"></script> -->
<script type="text/javascript" src="<?php echo $baseurl; ?>assets/assets/data-tables/DT_bootstrap.js"></script>

<!--right slidebar-->
<script src="<?php echo $baseurl; ?>assets/js/slidebars.min.js"></script>
<!--common script for all pages-->
<script src="<?php echo $baseurl; ?>assets/js/common-scripts.js"></script>
<!--script for this page-->
<script src="<?php echo $baseurl; ?>assets/js/sparkline-chart.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/easy-pie-chart.js"></script>
<script src="<?php echo $baseurl; ?>assets/js/count.js"></script>
<!--script for datepicker-->
<script src="<?php echo $baseurl; ?>assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!--script for timepicker-->
<script src="<?php echo $baseurl; ?>assets/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<!--script for datepicker range-->
<script  src="<?php echo $baseurl; ?>assets/assets/bootstrap-daterangepicker/moment.min.js"></script>
<script  src="<?php echo $baseurl; ?>assets/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<!--script for multiselect-->
<script src="<?php echo $baseurl; ?>assets/assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo $baseurl; ?>assets/assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
<!--toastq-->
<script src="<?php echo $baseurl; ?>assets/assets/toastr-master/toastr.js"></script>
<!--file upload-->
<script src="<?php echo $baseurl; ?>assets/js/bootstrap-fileupload.js"></script>
<script>
    //owl carousel

    $(document).ready(function () {
        $('#pms-datatable').dataTable({
            "sPaginationType": "full_numbers",
           "ordering": false
        });

        $('#codes-datatable').dataTable({
            "sPaginationType": "full_numbers",
         
        });


    });

    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            autoPlay: true

        });
    });

    //custom select box

    $(function () {
        $('select.styled').customSelect();
    });



    $('.my_multi_select3').multiSelect({
        selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
        selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
        afterInit: function (ms) {
            var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });


    //wysihtml5 start

    $('.wysihtml5').wysihtml5();

    //wysihtml5 end

    $(function () {
        $('#from').datepicker({
            format: "dd-mm-yyyy"
       
        });
    });
    $(function () {
        $('#to').datepicker({
            format: "dd-mm-yyyy"

        });
    });

</script>
</body>
</html>