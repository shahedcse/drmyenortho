<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <?php if (in_array($this->session->userdata('user_role'), array(512))): ?>
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'dashboard')) ? 'active' : ''; ?>"  href="<?php echo site_url('home'); ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'doctor')) ? 'active' : ''; ?>" href="<?php echo site_url('doctor/doctor'); ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Doctor</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $baseurl; ?>/home/logout">
                        <i class="fa fa-laptop"></i>
                        <span>Logout</span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (in_array($this->session->userdata('user_role'), array(256))): ?>
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'dashboard')) ? 'active' : ''; ?>"  href="<?php echo site_url('home'); ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'patient')) ? 'active' : ''; ?>" href="<?php echo site_url('patient/patient'); ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Patient</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a href="javascript:;" class="<?php echo (isset($active_menu) && ($active_menu == 'appointment')) ? 'active' : ''; ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Appointment</span>
                    </a>
                    <ul class="sub">
                        <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'appointmentlist')) ? 'active' : ''; ?>"><a  href="<?php echo site_url('appointment/appointment'); ?>">Appointment List</a></li>
                        <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'requestedappointment')) ? 'active' : ''; ?>"><a  href="<?php echo site_url('appointment/appointment/requestedappointment'); ?>">Requested Appointment</a></li>
                    </ul>
                </li>
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'prescription')) ? 'active' : ''; ?>" href="<?php echo site_url('prescription/prescription'); ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Prescription</span>
                    </a>
                </li>
                <!-- this three menu insert into settings menu's              
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'medicine')) ? 'active' : ''; ?>" href="<?php echo site_url('medicine/medicine'); ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Medicine</span>
                    </a>
                </li>
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'test')) ? 'active' : ''; ?>" href="<?php echo site_url('test/test'); ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Test</span>
                    </a>
                </li>
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'test')) ? 'active' : ''; ?>" href="<?php echo site_url('reporttype/reporttype'); ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Report Type</span>
                    </a>
                </li>
                -->
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php echo (isset($active_menu) && ($active_menu == 'settings')) ? 'active' : ''; ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Settings</span>
                    </a>
                    <ul class="sub">
                        <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'medicine')) ? 'active' : ''; ?>"><a  href="<?php echo site_url('medicine/medicine'); ?>">Medicine</a></li> 
                        <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'test')) ? 'active' : ''; ?>"><a  href="<?php echo site_url('test/test'); ?>">Test</a></li>    
                        <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'reporttype')) ? 'active' : ''; ?>"><a  href="<?php echo site_url('reporttype/reporttype'); ?>">Report Type</a></li>   
    <!--                        <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'surgeon')) ? 'active' : ''; ?>"><a  href="<?php //echo site_url('surgery_report/Surgeon');  ?>">Surgeons</a></li>    
                        <li class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'surgery_code')) ? 'active' : ''; ?>"><a  href="<?php //echo site_url('surgery_report/SurgeryCode');  ?>">Surgical Codes</a></li> -->
                        <li><a href="<?php echo $baseurl; ?>dbbackup/">Database Backup</a></li> 
                    </ul>
                </li>
                <!--    <li class="sub-menu">
                        <a href="javascript:;" class="<?php // echo (isset($active_menu) && ($active_menu == 'surgery')) ? 'active' : '';  ?>">
                            <i class="fa fa-laptop"></i>
                            <span>Surgery Report</span>
                        </a>
                        <ul class="sub">
                            <li class="<?php //echo (isset($active_sub_menu) && ($active_sub_menu == 'surgery_case')) ? 'active' : '';  ?>"><a  href="<?php //echo site_url('surgery_report/SurgeryCase');  ?>">Surgical Cases</a></li> 


                        </ul>
                    </li> -->
                <li >
                    <a class="<?php echo (isset($active_sub_menu) && ($active_sub_menu == 'research')) ? 'active' : ''; ?>" href="<?php echo site_url('research/Research'); ?>">    <i class="fa fa-laptop"></i><span>Research</span></a>

                </li>   
                <li>
                    <a class="<?php echo (isset($active_menu) && ($active_menu == 'changepassword')) ? 'active' : ''; ?>" href="<?php echo site_url('changepassword/changepassword'); ?>">
                        <i class="fa fa-laptop"></i>
                        <span>Change Password</span>
                    </a>
                </li>


                <li>
                    <a href="<?php echo $baseurl; ?>/home/logout">
                        <i class="fa fa-laptop"></i>
                        <span>Logout</span>
                    </a>
                </li>
            <?php endif; ?>


        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->