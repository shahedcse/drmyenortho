<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">              
                <section class="panel">
                    <header class="panel-heading">
                        Doctor List
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Well done !!!  </strong>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oh snap !!!  </strong>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <span class="tools pull-right">
                                <button class="btn btn-success" data-toggle="modal" href="#myModalAddDoc">
                                    Add Doctor&nbsp;&nbsp;<i class="fa fa-plus"></i>
                                </button>                        
                            </span>
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Mobile</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (sizeof($doctorlist) > 0):
                                        foreach ($doctorlist as $datarow):
                                            ?>
                                            <tr class="gradeX">
                                                <td><?php echo $datarow->username; ?></td>
                                                <td><?php echo $datarow->username; ?></td>
                                                <td><?php echo $datarow->first_name; ?></td>
                                                <td><?php echo $datarow->last_name; ?></td>
                                                <td><?php echo $datarow->email; ?></td>
                                                <td><?php echo $datarow->address; ?></td>                                               
                                                <td><?php echo $datarow->mobile; ?></td>
                                                <td><?php echo $datarow->username; ?></td>
                                            </tr>         
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->
<!--Doctor Add Modal-->
<div class="modal fade" id="myModalAddDoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Add Doctor Information</h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; height: 480px;">
                <div class=" form">
                    <form class="cmxform form-horizontal tasi-form" id="adddoctorform" method="POST" action="<?php echo site_url('doctor/doctor/adddoctor'); ?>">
                        <div class="form-group">
                            <label for="username" class="control-label col-lg-3">Username <span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="username" name="username" minlength="4" autocomplete="none" type="text" onchange="return checkuniquePin()" required />
                                <span id="userPinMsg"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="control-label col-lg-3">Password <span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="password" name="password" minlength="4" type="text" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fname" class="control-label col-lg-3">First Name <span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="fname" name="fname" minlength="4" type="text" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="control-label col-lg-3">Last Name</label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="lname" name="lname" minlength="4" type="text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sex" class="control-label col-lg-3">Sex <span style="color: red">*</span></label>
                            <div class="col-lg-9">                             
                                <input name="sex" id="sex" value="Male" type="radio" checked /> Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="sex" id="sex" value="Female" type="radio"/> Female                           
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="control-label col-lg-3">Specialty <span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <select class="form-control selectpicker" name="position_id" id="position_id" required data-live-search="true">
                                    <option value=""> --Select-- </option> 
                                    <?php
                                    if (sizeof($roledata) > 0):
                                        foreach ($roledata as $datarow):
                                            echo '<option value="' . $datarow->id . '">' . $datarow->position_name . '</option> ';
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="control-label col-lg-3">Mobile <span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <input class=" form-control" id="mobile" name="mobile" minlength="12" type="text" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label col-lg-3">E-Mail </label>
                            <div class="col-lg-9">
                                <input class="form-control " id="email" type="email" name="email" />
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="address" class="control-label col-lg-3">Address <span style="color: red">*</span></label>
                            <div class="col-lg-9">
                                <textarea class="form-control " id="address" name="address"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn btn-success" type="submit">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button> 
                                 <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!--/Doctor Add Modal-->
<script>
    function checkuniquePin() {
        var username = $("#username").val();
        var dataString = 'username=' + username;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('doctor/doctor/checkUniquePin'); ?>",
            data: dataString,
            success: function(data)
            {            
                if (data == 'free') {
                    $("#userPinMsg").text("");
                    $("#userPinMsg").text("Valid account No");
                    $("#userPinMsg").css('color', 'green');
                }
                if (data == 'booked') {
                    $("#userPinMsg").text("");
                    $("#userPinMsg").text("This number already used !!");
                    $("#userPinMsg").css('color', 'red');
                }
            }
        });
    }
</script>