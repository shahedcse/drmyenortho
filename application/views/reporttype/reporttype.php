<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Appointment List
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <span class="tools pull-right">
                                <button class="btn  btn-success"  data-toggle="modal" href="#myModalReport">
                                    Add ReportType&nbsp;<i class="fa fa-plus"></i>
                                </button> 
                            </span>
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>Report Type</th>
                                        <th>Action</th>                                     
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    if (sizeof($reportlist) > 0):
                                        foreach ($reportlist as $datarow):
                                            ?>
                                            <tr class="gradeX">                                           
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $datarow->reporttype; ?></td>   
                                                <td><a href="#deleteModal-<?php echo $datarow->id; ?>"data-toggle="modal" ><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i>&nbsp;Delete</button></a></td>
                                            </tr>
                                        <div class="modal fade" id="deleteModal-<?php echo $datarow->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <form class="cmxform form-horizontal tasi-form" id="signupForm2" method="post" action="<?php echo site_url('reporttype/reporttype/delete_reporttype'); ?>" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Delete Report Type</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="panel-body">
                                                                <p>Do You Want to Delete Report Type Data ???</p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer" >
                                                            <input type="hidden" name="rid" value="<?php echo $datarow->id; ?>">
                                                            <button class="btn btn-danger" type="submit">Delete</button>
                                                            <button style="margin-right: 40%" data-dismiss="modal" class="btn btn-danger" type="button">Close</button>                                                            
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<!--Add Medicine Modal-->
<div class="modal fade top-modal-without-space" id="myModalReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Add Report Information</h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; max-height: 480px;">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" id="addAppointmentForm" method="POST" action="<?php echo site_url('reporttype/reporttype/addReporttype'); ?>">
                        <div class="form-group">
                            <label for="patientname" class="control-label col-lg-3">Report Type</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="reporttype" id="reporttype" required="required"> 
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn btn-success" type="submit"  >&nbsp;&nbsp;Submit&nbsp;&nbsp;</button>   
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>          
        </div>
    </div>
</div>
<!--/Add Medicine Modal-->