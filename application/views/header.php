<!DOCTYPE html>
<html lang="bn">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Mosaddek">
        <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <link rel="shortcut icon" href="<?php echo $baseurl; ?>assets/img/favicon.ico">

        <title><?php echo $title; ?></title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo $baseurl; ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $baseurl; ?>assets/css/bootstrap-reset.css" rel="stylesheet">
        <!--external css-->
        <link href="<?php echo $baseurl; ?>assets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo $baseurl; ?>assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
        <link rel="stylesheet" href="<?php echo $baseurl; ?>assets/css/owl.carousel.css" type="text/css">

        <!--right slidebar-->
        <link href="<?php echo $baseurl; ?>assets/css/slidebars.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo $baseurl; ?>assets/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link href="<?php echo $baseurl; ?>assets/assets/summernote/dist/summernote.css" rel="stylesheet">

        <!-- Custom styles for this template -->

        <link href="<?php echo $baseurl; ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo $baseurl; ?>assets/css/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo $baseurl; ?>assets/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <!--dynamic table-->
        <link href="<?php echo $baseurl; ?>assets/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="<?php echo $baseurl; ?>assets/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo $baseurl; ?>assets/assets/data-tables/DT_bootstrap.css" />

        <script type="text/javascript" src="<?php echo $baseurl; ?>assets/js/jquery.js"></script>
        <!-- custom search start here-->
        <link rel="stylesheet" type="text/css" href="<?php echo $baseurl; ?>assets/customsearch/css/bootstrap-select.css">
        <!-- custom search end here-->
        <link href="<?php echo $baseurl; ?>assets/css/custom.css" rel="stylesheet">
        <!-- file upload-->
        <link href="<?php echo $baseurl; ?>assets/css/bootstrap-fileupload.css" rel="stylesheet">
        <!--datepicker-->
        <link href="<?php echo $baseurl; ?>assets/assets/bootstrap-datepicker/css/datepicker.css" rel="stylesheet">
        <link href="<?php echo $baseurl; ?>assets/assets/bootstrap-datepicker/less/datepicker.less" rel="stylesheet">
        <!--timepicker-->
        <link href="<?php echo $baseurl; ?>assets/assets/bootstrap-timepicker/compiled/timepicker.css" rel="stylesheet">
        <!--date range -->
        <link rel="stylesheet" type="text/css" href="<?php echo $baseurl; ?>assets/assets/bootstrap-daterangepicker/daterangepicker-bs3.css" />
        <!--multiselect-->
        <link href="<?php echo $baseurl; ?>assets/assets/jquery-multi-select/css/multi-select.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <section id="container" >
            <header class="header white-bg">
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
                </div>
                <a href="<?php echo base_url('dashboard/dashboard'); ?>" class="logo">PM<span>S</span></a>
                <div class="top-nav " >
                    <!--search & user info start-->
                    <span class="tools pull-right" >
                        <a href="<?php echo site_url('prescription/prescription/addprescription'); ?>">
                            <button class="btn btn-primary"  >
                                Add prescription&nbsp;&nbsp;<i class="fa fa-plus"></i>
                            </button>
                        </a>                        
                    </span>
                </div>
            </header>