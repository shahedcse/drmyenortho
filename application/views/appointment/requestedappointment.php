<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Requested Appointment List
                    </header>
                    <div class="panel-body">
                        <?php
                        if ($this->session->userdata('successfull')):
                            echo '<div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('successfull') . '</div>';
                            $this->session->unset_userdata('successfull');
                        endif;
                        if ($this->session->userdata('failed')):
                            echo '<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $this->session->userdata('failed') . '</div>';
                            $this->session->unset_userdata('failed');
                        endif;
                        ?>
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="pms-datatable">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Patient Name</th>
                                        <th>Doctor Name</th>
                                        <th>Options</th>                                     
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (sizeof($appointmentlist) > 0):
                                        foreach ($appointmentlist as $datarow):
                                            ?>
                                            <tr class="gradeX">                                           
                                                <td><?php
                                                    $currentDateTime = $datarow->date;
                                                    $newDateTime = date('d-m-Y h:i A', strtotime($currentDateTime));
                                                    echo $newDateTime;
                                                    ?>

                                                </td>
                                                <td><?php
                                                    $patient_id = $datarow->patient_id;
                                                    $query = $this->db->query("SELECT username FROM user WHERE id='$patient_id'");
                                                    $patient_name = $query->row()->username;
                                                    echo $patient_name;
                                                    ?></td>
                                                <td><?php
                                                    $doctor_id = $datarow->doctor_id;
                                                    $query = $this->db->query("SELECT username FROM user WHERE id='$doctor_id'");
                                                    $doctor_name = $query->row()->username;
                                                    echo $doctor_name;
                                                    ?></td>
                                                <td><button class="btn btn-primary btn-xs" onclick="editRequestedAppointmentInfo(this.value)" value="<?php echo $newDateTime . "/" . $patient_id . "/" . $doctor_id . "/" . $datarow->status . "/" . $datarow->id; ?>"><i class="fa fa-edit"></i>&nbsp;edit</button>
                                                    <button class="btn btn-danger btn-xs" onclick="deleteRequestedAppointmentInfo(this.value)" value="<?php echo $datarow->id; ?>"><i class="fa fa-trash-o "></i>&nbsp;delete</button></td>                                                                                          
                                            </tr>         
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<!--main content end-->

<!--Edit Requested Appointment Modal-->
<div class="modal fade top-modal" id="myModalEditAppoint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align: center;">Edit Requested Appointment Information</h4>
            </div>
            <div class="modal-body" style="overflow-y: scroll; max-height: 480px;">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" id="editAppointmentForm" method="POST" action="<?php echo site_url('appointment/appointment/editRequestedAppointment'); ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="appointDateEdit" class="control-label col-lg-3">Date</label>
                            <div class="col-lg-4">
                                <input class="form-control form-control-inline input-medium default-date-picker" data-date-format="dd-mm-yyyy"  size="16" type="text"   id="appointDateEdit" name="appointDateEdit" onchange="checkAppointmentEdit();" required/>
                                <input type="hidden" id="apIdEdit" name="apIdEdit" />
                                <span id="appointmsg"></span>
                            </div>
                            <label for="appointTimeEdit" class="control-label col-md-1">Time</label>
                            <div class="col-md-4">
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" class="form-control timepicker-default" id="appointTimeEdit" name="appointTimeEdit" onchange="checkAppointmentEdit();" required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div >
                                <span class="col-md-3"></span>
                                <span style="padding-top: 5px;" class="col-md-9" id="dateTimeSmsEdit" name="dateTimeSms"></span> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="patientIdEdit" class="control-label col-lg-3">Patient Name</label>
                            <div class="col-lg-9">
                                <select class="form-control m-bot15" name="patientIdEdit" id="patientIdEdit" data-live-search="true"  required>
                                    <option value=""> --Select-- </option> 
                                    <?php
                                    if (sizeof($patientlist) > 0):
                                        foreach ($patientlist as $datarow):
                                            echo '<option value="' . $datarow->id . '">' . $datarow->username . '</option> ';
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="doctorIdEdit" class="control-label col-lg-3">Doctor Name</label>
                            <div class="col-lg-9">
                                <select class="form-control m-bot15" name="doctorIdEdit" id="doctorIdEdit" data-live-search="true"  required>
                                    <?php
                                    if (sizeof($doctorlist) > 0):
                                        foreach ($doctorlist as $datarow):
                                            echo '<option value="' . $datarow->id . '">' . $datarow->username . '</option> ';
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="statusEdit" class="control-label col-lg-3">Status</label>
                            <div class="col-lg-9">                             
                                <input name="statusEdit" id="statusEdit" value="approved" type="radio" required  /> Approved &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="statusEdit" id="statusEdit" value="reject" type="radio"/> Reject &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                <input name="statusEdit" id="statusEdit" value="pending" type="radio"/> Pending
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn btn-success" type="submit">&nbsp;&nbsp;Submit&nbsp;&nbsp;</button> 
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>          
        </div>
    </div>
</div>
<!--/Edit Requested Appointment Modal-->

<!--delete Appointment Modal-->
<div class="modal fade top-modal" id="myModalDeleteRequestedAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="form">
                    <form class="cmxform form-horizontal tasi-form" method="POST" action="<?php echo site_url('appointment/appointment/deleteRequestedAppointment'); ?>" > 
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style="text-align: center;font-weight: bolder;">Delete Patient Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;&nbsp;Are you want to delete !!!
                                <input type="hidden" id="id_delete" name="id_delete"/>
                            </div>
                        </div>
                        <div class="modal-footer">                         
                            <button class="btn btn-danger" type="submit">Confirm</button>
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>                                            
        </div>
    </div>
</div>
<!-- delete Appointment Modal-->
<script>

    function editRequestedAppointmentInfo(info) {
        var data = info.split("/");
        var dateTime = data[0];
        var newDateTime = dateTime.split(" ");
        var date = newDateTime[0];
        var time = newDateTime[1] + " " + newDateTime[2];
        var patientId = data[1];
        var doctorId = data[2];
        var statusEdit = data[3];
        var apIdEdit = data[4];

        $("#appointDateEdit").val(date);
        $("#appointTimeEdit").val(time);
        $("#patientIdEdit").val(patientId);
        $("#doctorIdEdit").val(doctorId);
        $('input[name=statusEdit]').val([statusEdit]);
        $("#apIdEdit").val(apIdEdit);
        $('#myModalEditAppoint').modal('show');
    }


    function deleteRequestedAppointmentInfo(id) {
        $("#id_delete").val(id);
        $('#myModalDeleteRequestedAppointment').modal('show');
    }

    function checkAppointmentEdit() {

        var dateTime = $('#appointDateEdit').val() + " " + $('#appointTimeEdit').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('appointment/appointment/checkAppointmentEdit'); ?>",
            data: 'dateTime=' + dateTime,
            success: function (data) {

                if (data == 'free') {
                    $("#dateTimeSmsEdit").text("Appointment available");
                    $("#dateTimeSmsEdit").css('color', 'green');

                    document.getElementById('editAppointmentForm').onsubmit = function () {
                        return true;
                    }

                }
                if (data == 'booked') {
                    $("#dateTimeSmsEdit").text("Appointment not available !!");
                    $("#dateTimeSmsEdit").css('color', 'red');

                    document.getElementById('editAppointmentForm').onsubmit = function () {
                        return false;
                    }

                }
            }
        });
    }

</script>