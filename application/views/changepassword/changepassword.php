<section id="main-content" >
    <section class="wrapper site-min-height">
        <section class="panel">
            <header class="panel-heading">
                Change Password
            </header>
            <style>
                #myModalLabel{
                    font-weight: bold
                }
            </style> 
            <?php
            if ($this->session->userdata('successfull')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('successfull') . '</div>';
                $this->session->unset_userdata('successfull');
            endif;
            if ($this->session->userdata('failed')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('failed') . '</div>';
                $this->session->unset_userdata('failed');
            endif;
            ?>
            <div class="form" style="margin-top: 20px">
                <form class="cmxform form-horizontal tasi-form" method="post" action="<?php echo site_url('changepassword/changepassword/updatepass') ?>">
                    <div class="form-group ">
                        <label for="currentpassword" class="control-label col-lg-4">Current Password :</label>
                        <div class="col-lg-6">
                            <input class=" form-control" id="cpassword" name="cpassword" type="password"/>
                            <span id ="update_cpassword"></span>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="newpassword" class="control-label col-lg-4">New Password :</label>
                        <div class="col-lg-6">
                            <input class=" form-control" id="npassword" name="npassword"  type="password"/>
                            <span id="update_npassword"></span>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="verifynewpassword" class="control-label col-lg-4">Verify New Password :</label>
                        <div class="col-lg-6">
                            <input class=" form-control" id="rpassword" name="rpassword" type="password" />
                            <span id="update_rpassword"></span>
                        </div>
                    </div>
                    <div class="form-group" style="margin-left: 0px; padding-bottom: 20px;">
                        <div class="col-lg-offset-5 col-lg-8">
                            <button type="submit" id="updatepass" class="btn btn-success" onclick="return updatepassword()">Update Password</button>
                            <button type="reset" class="btn btn-default">Clear</button>
                        </div>
                    </div>
                      
                </form>
            </div>
        </section>
    </section>
</section>

<script>
    function updatepassword() {

        var cpassword = $("#cpassword").val();
        var npassword = $("#npassword").val();
        var rpassword = $("#rpassword").val();
        if (cpassword == '') {
            $("#update_cpassword").css('color', 'red');
            $("#update_cpassword").text("Current Password required !!!");
            return false;
        }
        else if (npassword == '') {
            $("#update_cpassword").text("");
            $("#update_npassword").css('color', 'red');
            $("#update_npassword").text("New Password required !!!");
            return false;
        }
        else if (rpassword == '') {
            $("#update_npassword").text("");
            $("#update_rpassword").css('color', 'red');
            $("#update_rpassword").text("Repeat Password required !!!");
            return false;
        }
        else if (npassword != rpassword) {
            $("#update_rpassword").text("");
            $("#update_rpassword").css('color', 'red');
            $("#update_rpassword").text("Password not match !!!");
            return false;
        }
        else {
            return true;
        }
    }
</script>