<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('savelogdata')) {

    function savelogdata($action, $details) {
        $CI = & get_instance();
        if ($CI->session->userdata('username')):
            $userPin = $CI->session->userdata('username');
            $userLevel = $CI->session->userdata('user_role');
        else:
            $userPin = "N/A";
            $userLevel = 0;
        endif;
        $CI->load->library('user_agent');
        $device = 'Host Name: ' . gethostname();
        $device = $device . '  | ' . 'OS: ' . $CI->agent->platform();
        if ($CI->agent->is_browser()) {
            $browser = $CI->agent->browser() . ' ' . $CI->agent->version();
        } elseif ($CI->agent->is_robot()) {
            $browser = $CI->agent->robot();
        } elseif ($CI->agent->is_mobile()) {
            $browser = $CI->agent->mobile();
        } else {
            $browser = 'Undefined User Agent';
        }
        $ip = $_SERVER['REMOTE_ADDR'];

        $device = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $locationinfo = json_decode(file_get_contents("http://ipinfo.io/$ip/json"));
        $location = "";
        if (isset($locationinfo->city)):
            $location .= ' City: ' . $locationinfo->city;
        endif;
        if (isset($locationinfo->region)):
            $location .= ', Region: ' . $locationinfo->region;
        endif;
        if (isset($locationinfo->country)):
            $location .= ', Country: ' . $locationinfo->country;
        endif;
        $timezone = +6; //(GMT -5:00) EST (U.S. & Canada)
        $gmtdate = gmdate("Y-m-d H:i:s", time() + 3600 * ($timezone + date("I")));
        $logdataarray = array(
            'userpin' => $userPin,
            'userlevel' => $userLevel,
            'action' => $action,
            'device' => $device,
            'browser' => $browser,
            'ip' => $ip,
            'location' => $location,
            'details' => $details,
            'datetime' => $gmtdate
        );
        $savestatus = $CI->db->insert('logdata', $logdataarray);
    }

}

