-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pms.reference
CREATE TABLE IF NOT EXISTS `reference` (
  `id` int(10) unsigned NOT NULL,
  `ref_name` varchar(100) NOT NULL,
  `image_name` varchar(50) DEFAULT NULL,
  `ref_under` int(10) unsigned NOT NULL,
  `role_details_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Surgery Reference';

-- Dumping data for table pms.reference: ~465 rows (approximately)
DELETE FROM `reference`;
/*!40000 ALTER TABLE `reference` DISABLE KEYS */;
INSERT INTO `reference` (`id`, `ref_name`, `image_name`, `ref_under`, `role_details_id`) VALUES
	(2, 'CMF', NULL, 1, 44),
	(3, 'Proximal humerus', NULL, 1, 44),
	(4, 'Humeral shaft', NULL, 1, 44),
	(5, 'Distal humerus', NULL, 1, 44),
	(6, 'Proximal forearm', NULL, 1, 44),
	(7, 'Forearm shaft', NULL, 1, 44),
	(8, 'Distal forearm', NULL, 1, 44),
	(9, 'Hand', NULL, 1, 44),
	(12, 'Spine', NULL, 1, 44),
	(14, 'Acetabulum', NULL, 1, 44),
	(15, 'Proximal femur', NULL, 1, 44),
	(16, 'Femoral shaft', NULL, 1, 44),
	(17, 'Distal femur', NULL, 1, 44),
	(18, 'Patella', NULL, 1, 44),
	(19, 'Proximal tibia', NULL, 1, 44),
	(20, 'Tibial shaft', NULL, 1, 44),
	(21, 'Distal tibia', NULL, 1, 44),
	(22, 'Malleoli', NULL, 1, 44),
	(23, 'Foot', NULL, 1, 44),
	(51, 'Skull base & Cranial vault', 'skull_base.png', 2, 44),
	(52, 'Midface', 'Midface.png', 2, 44),
	(53, 'Dentoalveolar trauma', 'Dentoalvaeolar.png', 2, 44),
	(54, 'Mandible', 'Mandible.png', 2, 44),
	(55, 'Unifokal extra articular  ', 'proximal_humerus_a.gif', 3, 44),
	(56, 'Bifocalextra articular', 'proximal_humerus_b.gif', 3, 44),
	(58, 'Articular', 'proximal_humerus_c.gif', 3, 44),
	(59, 'Simple Fractures', 'tibia_shaft_a.gif', 4, 44),
	(61, 'Wedge fractures', 'tibia_shaft_b.gif', 4, 44),
	(62, 'Complex Fractures', 'tibia_shaft_c.gif', 4, 44),
	(63, 'Extra Articular', '13_ExtraArticular.gif', 5, 44),
	(64, 'Partial Articul', '13_PartiallyArticular.gif', 5, 44),
	(65, 'Complete Articular', '13_CompleteArticular.gif', 5, 44),
	(66, 'Extra-Articular', '21_type-A.gif', 6, 44),
	(67, 'Articular one bone ', '21_type-B.gif', 6, 44),
	(68, 'Articular both bones', '21_type-C.gif', 6, 44),
	(69, 'Simple Fractures', 'radius_shaft_a.gif', 7, 44),
	(70, 'Wedge Fractures', 'radius_shaft_b.gif', 7, 44),
	(71, 'Complex Frctures', 'radius_shaft_c.gif', 7, 44),
	(72, 'Extra Articular', '23-a.gif', 8, 44),
	(73, 'Partically Articular', '23-b.gif', 8, 44),
	(74, 'Complete Articular', '23-c.gif', 8, 44),
	(75, 'Distal phalanges', 'Distal_phalanx.png', 9, 44),
	(76, 'Middle phalanges', 'Middle_phalanx.png', 9, 44),
	(77, 'Proximal Phalanges', 'Proximal_phalanx.png', 9, 44),
	(78, 'Metacarpals', 'Metacarpals.png', 9, 44),
	(79, 'Carpal bones', 'Carpal_bones.png', 9, 44),
	(80, 'Thumb', 'Thumb.png', 9, 44),
	(81, 'Trauma-Thoracic and lumbar trauma', 'Trauma.png', 12, 44),
	(82, 'Elemental Fracture Types', 'Acetabulum.png', 14, 44),
	(83, 'Associated Fracture Types', 'Acetabulum.png', 14, 44),
	(84, 'Troch-anter', 'proximaler_femur_a.gif', 15, 44),
	(85, 'Neck', 'proximaler_femur_b.gif', 15, 44),
	(86, 'Head', 'proximaler_femur_c.gif', 15, 44),
	(87, 'Simple fractures', 'tibia_shaft_a.gif', 16, 44),
	(88, 'Wedge fractures', 'tibia_shaft_b.gif', 16, 44),
	(89, 'Complex Frctures', 'tibia_shaft_c.gif', 16, 44),
	(90, 'Extra Articular', '33-A-type.gif', 17, 44),
	(91, 'Partial Articular', '33-B-type.gif', 17, 44),
	(92, 'Complete Articular ', '33-C-type.gif', 17, 44),
	(93, 'Extra Articular ', 'knee_patella_a.gif', 18, 44),
	(94, 'Partial Articular ', 'knee_patella_b.gif', 18, 44),
	(95, 'Complete Articular ', 'knee_patella_c.gif', 18, 44),
	(96, 'Extra Articular ', 'proximal_tibia_a.gif', 19, 44),
	(97, 'Partial Articular ', 'proximal_tibia_b.gif', 19, 44),
	(98, 'Complete Articular ', 'proximal_tibia_c.gif', 19, 44),
	(99, 'Simple fractures', 'tibia_shaft_a.gif', 20, 44),
	(100, 'Wedge fractures', 'tibia_shaft_b.gif', 20, 44),
	(101, 'Complex Frctures', 'tibia_shaft_c.gif', 20, 44),
	(102, 'Extra Articular ', 'dist-tibia_a.gif', 21, 44),
	(103, 'Partial Articular ', 'dist-tibia_b.gif', 21, 44),
	(104, 'Complete Articular ', 'dist-tibia_c.gif', 21, 44),
	(105, 'Infrasnydesmotic ', 'malleolar_a.gif', 22, 44),
	(106, 'Trans-syndes-motic', 'malleolar_b.gif', 22, 44),
	(107, 'Supra-syndes-motic', 'malleolar_c.gif', 22, 44),
	(108, 'Phalanges', 'Phalanges.png', 23, 44),
	(109, 'Metatarsals', 'Metatarsals.png', 23, 44),
	(110, 'Midfoot', 'Midfoot.png', 23, 44),
	(111, 'Calcaneus', 'Calcaneus.png', 23, 44),
	(112, 'Talus', 'Talus.png', 23, 44),
	(113, 'Deformity (AIS)', 'Deformity.png', 12, 44),
	(201, 'Frontal sinus', 'frontal_sinus.gif', 51, 44),
	(202, 'Skull base', 'skull_base.gif', 51, 44),
	(203, 'Cranial vault', 'cranial_vault.gif', 51, 44),
	(204, 'Palato-alveolar', 'palato.gif', 52, 44),
	(205, 'Le Fort', 'le-fort.gif', 52, 44),
	(206, 'Nasal/NOE', 'noe_nasal.gif', 52, 44),
	(207, 'Orbit', 'orbit.gif', 52, 44),
	(208, 'Zygoma', 'zygoma.gif', 52, 44),
	(209, 'Tooth fracture', 'tooth_frac.gif', 53, 44),
	(210, 'Tooth luxation', 'tooth_lux.gif', 53, 44),
	(211, 'Alveolar fracture', 'alveolarfracture.gif', 53, 44),
	(212, 'Symphysis and parasymphysis', 'Symphysis.gif', 54, 44),
	(213, 'Body', 'Body.gif', 54, 44),
	(214, 'Angle and ramus', 'Angle.gif', 54, 44),
	(215, 'Condylar process and head', 'Condyle.gif', 54, 44),
	(217, '11-A2 (Impacted Metaphyseal)', '11-a22.gif', 55, 44),
	(218, '11-A3(Non-impacted metaphyseal)', '11-a32.gif', 55, 44),
	(220, '11-B2 Without metaphyseal impaction', '11-b22.gif', 56, 44),
	(221, '11-B3 With glenohumeral dislocation ', '11-b32.gif', 56, 44),
	(225, '12-A1 Spiral', '12-A1_Class_117x97.gif', 59, 44),
	(226, '12-A2 Oblique (>30°)', '12-A22_class.gif', 59, 44),
	(227, '12-A3 Transverse (<30°)', '12-A32_class.gif', 59, 44),
	(228, '12-B1\nSpiral Wedge ', '12-B13_class.gif', 61, 44),
	(229, '12-B2 Bending Wedge', '12-B22_class.gif', 61, 44),
	(230, '12-B3 Fragmented Wedge ', '12-B32_class.gif', 61, 44),
	(231, '12-C1 Spiral', '12-C11_class.gif', 62, 44),
	(232, '12-C2 Segmental', '12-C22_class.gif', 62, 44),
	(233, '12-C3 Irregular', '12-C31_class.gif', 62, 44),
	(237, '13-B1 Lateral sagittal', '13-B12_Class.gif', 64, 44),
	(240, '13-C1 Simple ', '13-C12_Class.gif', 65, 44),
	(241, '\n13-C2 Metaphyseal Comminution', '13-C22_Class.gif', 65, 44),
	(242, '\n13-C3 Multifragmentary \n', '13-C32_Class.gif', 65, 44),
	(246, '21-B1 Isolated unla', '21-B1_Class.gif', 67, 44),
	(247, '21-B2 Isolated radius', '21-B2_Class.gif', 67, 44),
	(249, '21-C1 Simple ', '21-C1_Class.gif', 68, 44),
	(251, '21-C3 Multifragmentary ', '21-C3_Class.gif', 68, 44),
	(254, '22-A3 Both bones', '22_a3-2.gif', 69, 44),
	(257, '22-B3 one bone wedge,other simple or wedge', '22_b3-2.gif', 70, 44),
	(258, '22-C1 Unla complex,radius simple', '22_c1-2.gif', 71, 44),
	(259, '22-C2 Radius complex,unla simple', '22_c2-2.gif', 71, 44),
	(263, '\n23-A3 Radius,Multifragmentary', '23_a3.gif', 72, 44),
	(266, '23-B3 Radius,frontal,volar rim', '23_b3.gif', 73, 44),
	(267, '23-C1 Simple,metaphyseal simple', '23_c1.gif', 74, 44),
	(268, '23-C2 Simple,metaphyseal simple multifragmentary ', '23_c2.gif', 74, 44),
	(269, '\n23-C3 multifragmentary ', '23_c3.gif', 74, 44),
	(270, '\nDistal & Shaft ', 'Distal_phx_Distal_Shaft.gif', 75, 44),
	(271, '\nProximal', 'Distal_phx_Shaft.gif', 75, 44),
	(272, 'Distal', 'middle_phx_Distal.gif', 76, 44),
	(273, 'Shaft', 'middle_phx_Shaft.gif', 76, 44),
	(274, 'Proximal', 'middle_phx_Proximal.gif', 76, 44),
	(275, 'Distal: articular', 'proxy_phx_Distal-Articular.gif', 77, 44),
	(276, 'Distal: Metaphyseal', 'proxy_phx_Distal-metaphyseal.gif', 77, 44),
	(277, 'Shaft', 'proxy_phx_Shaft.gif', 77, 44),
	(278, 'Proximal: Metaphyseal', 'proxy_phx_Proximal-metaphyseal.gif', 77, 44),
	(279, 'Proximal: Articular', 'proxy_phx_Proximal-Articular.gif', 77, 44),
	(280, 'Head', 'Metacarpal_Head.gif', 78, 44),
	(281, 'Shaft', 'Metacarpal_Shaft.gif', 78, 44),
	(282, 'Base', 'Metacarpal_Base.gif', 78, 44),
	(283, 'Scaphoid', 'Carpal_Scaphoid.gif', 79, 44),
	(284, 'Perilunate', 'Carpal_Perilunate.gif', 79, 44),
	(285, 'Distal phalanx: Distal Shaft', 'Thumb_DistalPhalanx_top.gif', 80, 44),
	(286, 'Distal phalanx: Base', 'Thumb_DistalPhalanx_base.gif', 80, 44),
	(287, 'Proximal phalanx:Articular', 'Thumb_Prox_phalanx_3.gif', 80, 44),
	(288, 'Proximal phalanx: Metaphyseal', 'Thumb_Prox_phalanx_2.gif', 80, 44),
	(289, 'Proximal phalanx: Base-Shaft', 'Thumb_Prox_phalanx.gif', 80, 44),
	(290, 'Metacarpal', 'Thumb_Metacarpal.gif', 80, 44),
	(291, 'Type A', '53_A_Class.png', 81, 44),
	(292, 'Type B', '53_B_Class.png', 81, 44),
	(293, 'Type C', '53_C_Class.png', 81, 44),
	(294, 'Adolescent Idiopathic Scoliosis', '55_Class.png', 113, 44),
	(296, '62 Posterior wall', '62_elem_a.gif', 82, 44),
	(297, '62 Posterior column', '62_elem_b.gif', 82, 44),
	(298, '62 Anterior wall', '62_elem_c.gif', 82, 44),
	(299, '62 Anterior column', '62_elem_d.gif', 82, 44),
	(300, '62 Posterior column/wall', '62_assoc_a.gif', 83, 44),
	(301, '62 Transverse/Posterior wall', '62_assoc_b.gif', 83, 44),
	(302, '62 T-type', '62_assoc_c.gif', 83, 44),
	(303, '62 Ant.column/post hemi-Transverse', '62_assoc_d.gif', 83, 44),
	(304, '62 Both column', '62_assoc_e.gif', 83, 44),
	(305, '31-A1 Pertrochanteric simple', '31_a1-2.gif', 84, 44),
	(306, '31-A2 Pertrochanteric multifragmentary', '31_a2-2.gif', 84, 44),
	(307, '31-A3 Intertrochanteric', '31_a3-2.gif', 84, 44),
	(308, '31-B1 Subcapital, with slight displacement', '31_b1-2.gif', 85, 44),
	(309, '31-B2 Transcervical', '31_b2-2.gif', 85, 44),
	(310, '31-B3 Subcapital, displaced non impacted', '31_b3-2.gif', 85, 44),
	(311, '31-C1 Split (Pipkin)', '31_c1-2.gif', 86, 44),
	(312, '31-C2 With depression', '31_c2-2.gif', 86, 44),
	(313, '31-C3 With neck fracture', '31_c3-2.gif', 86, 44),
	(314, '32-A1 Spiral', '32_A12_117.gif', 87, 44),
	(315, '32-A2 Oblique (>30°)', '32_A22_117.gif', 87, 44),
	(316, '32-A3 Transverse (<30°)', '32_A32_117.gif', 87, 44),
	(317, '32-B1 Spiral wedge', '32_B12_117.gif', 88, 44),
	(318, '32-B2 Bending wedge', '32_B22_117.gif', 88, 44),
	(319, '32-B3 Fragmented wedge', '32_B32_117.gif', 88, 44),
	(320, '32-C1 Complex spiral ', '32_C1_117.gif', 89, 44),
	(321, '32-C2 Complex segmental', '32_C2_117.gif', 89, 44),
	(322, '32-C3 Complex irregular ', '32_C3_117.gif', 89, 44),
	(323, '33-A1.1 Simple ', '33_A11_218.gif', 90, 44),
	(324, '33-A1.2/3 Simple ', '33_A12_218.gif', 90, 44),
	(325, '33-A2 Metaphyseal wedge ', '33_a2.gif', 90, 44),
	(326, '33-B1.1/2 lateral Sagittal ', '33_B11_218.gif', 91, 44),
	(327, '33-B1.3 lateral Sagittal ', '33_B13_218.gif', 91, 44),
	(328, '33-B2.1/2 Medial Sagittal', '33_B21_218.gif', 91, 44),
	(329, '33-C1 Simple ', '33_c1.gif', 92, 44),
	(330, '33-C2 Metaphyseal Comminution', '33_c2.gif', 92, 44),
	(331, '33-C3 Multifragmentary ', '33_c3.gif', 92, 44),
	(332, '34-A1 Avulsion', '34_a1.gif', 93, 44),
	(333, '34-A2 Isolated body ', '34_a2.gif', 93, 44),
	(334, '34-B1.1 Vertical, lateral ', '34_b11_218.gif', 94, 44),
	(335, '34-B1.2 Vertical, lateral ', '34_b12_218.gif', 94, 44),
	(336, '34-C1.1/2 Transverse', '34_c11_218.gif', 95, 44),
	(337, '34-C1.3 Transverse', '34_c13_218.gif', 95, 44),
	(338, '34-C2 Transverse plus second fragment ', '34_c2.gif', 95, 44),
	(339, '41-A1.1 Avulsion', '41_a1-1.gif', 96, 44),
	(340, '41-A1.2 Avulsion', '41_a1-2.gif', 96, 44),
	(341, '41-A1.3 Avulsion', '41_a1-3.gif', 96, 44),
	(342, '41-B1 Pure Split ', '41_b1-2.gif', 97, 44),
	(343, '41-B2 Pure depression ', '41_b2-2.gif', 97, 44),
	(344, '41-B3  Split-depression', '41_b3-2.gif', 97, 44),
	(345, '41-C1 Simple, metaphyseal simple ', '41_c1-2.gif', 98, 44),
	(346, '41-C2 Simple, metaphyseal multifragmentary ', '41_c2-2.gif', 98, 44),
	(347, '41-C3 Multifragmentary ', '41_c3-1.gif', 98, 44),
	(348, '42-A1 Spiral', '42_a1-2.gif', 99, 44),
	(349, '42-A2 Oblique (>30°)', '42_a2-2.gif', 99, 44),
	(350, '42-A3 Transverse (<30°)', '42_a3-2.gif', 99, 44),
	(351, '42-B1 Spiral wedge', '42_b1-2.gif', 100, 44),
	(352, '42-B2 Bending wedge', '42_b2-2.gif', 100, 44),
	(353, '42-B3 Fragmented wedge', '42_b3-2.gif', 100, 44),
	(354, '42-C1 Spiral ', '42_c1-2.gif', 101, 44),
	(355, '42-C2 Segmental', '42_c2-2.gif', 101, 44),
	(356, '42-C3 Irregular ', '42_c3-2.gif', 101, 44),
	(357, '43-A1 Metaphyseal simple ', '43-A1_Class_117x97.gif', 102, 44),
	(358, '43-A2 Metaphyseal wedge', '43-A2_Class_117x97.gif', 102, 44),
	(359, '43-A3 Metaphyseal complex ', '43-A3_Class_117x97.gif', 102, 44),
	(360, '43-B1 Pure Split', '43-B1_Class_117x97.gif', 103, 44),
	(361, '43-B2 Split depression', '43-B2_Class_117x97.gif', 103, 44),
	(362, '43-B3 Multifragmentary depression', '43-B3_Class_117x97.gif', 103, 44),
	(363, '43-C1 Articular simple ', '43-C1_Class_117x97.gif', 104, 44),
	(364, '43-C2 Articular simple,metaphyseal multifargmenatr', '43-C2_Class_117x97.gif', 104, 44),
	(365, '43-C2 Articular multifargmenatr', '43-C3_Class_117x97.gif', 104, 44),
	(372, '44-C1 Simple disphyseal', '44-C12_117x97.gif', 107, 44),
	(373, '44-C2 Multifragmentary', '44-C22_117x97.gif', 107, 44),
	(374, '44-C3 Proximal ', '44-C32_117x97.gif', 107, 44),
	(375, 'Fractures of the hallux', 'Phalanges.png', 108, 44),
	(376, 'Fractures of the 2nd-5th row', 'Phalanges.png', 108, 44),
	(377, 'Fractures of the metatarsals 2-4', 'Metatarsals.png', 109, 44),
	(378, 'Fractures of the 5th metatarsal', 'Metatarsals.png', 109, 44),
	(379, 'Navicular fractures', 'Midfoot.png', 110, 44),
	(380, 'TMT (Lisfranc) injuries', 'Midfoot.png', 110, 44),
	(381, 'Cuboid fractures', 'Midfoot.png', 110, 44),
	(382, 'Intertarsal injuries and fractures', 'Midfoot.png', 110, 44),
	(383, 'Body fractures', 'Calcaneus.png', 111, 44),
	(384, 'Sustentacular fractures', 'Calcaneus.png', 111, 44),
	(385, 'Neck fractures', 'Talus.png', 112, 44),
	(386, 'Body fractures', 'Talus.png', 112, 44),
	(387, 'Process fractures', 'Talus.png', 112, 44),
	(388, 'Special considerations', 'Talus.png', 112, 44),
	(389, 'Thoracic and lumbar trauma type A0', '53_A0.png', 291, 44),
	(390, 'Thoracic and lumbar trauma type A1', '53_A1.png', 291, 44),
	(391, 'Thoracic and lumbar trauma type A2', '53_A2.png', 291, 44),
	(392, 'Thoracic and lumbar trauma type A3', '53_A3.png', 291, 44),
	(393, 'Thoracic and lumbar trauma type A4', '53_A4.png', 291, 44),
	(394, 'Thoracic and lumbar trauma type B1', '53_B1.png', 292, 44),
	(395, 'Thoracic and lumbar trauma type B2', '53_B2.png', 292, 44),
	(396, 'Thoracic and lumbar trauma type B3', '53_B3.png', 292, 44),
	(397, 'Thoracic and lumbar trauma type C', '53_C.png', 293, 44),
	(398, 'Lenke 1', '55_L1.png', 294, 44),
	(399, 'Lenke 2', '55_L2.png', 294, 44),
	(400, 'Lenke 3', '55_L3.png', 294, 44),
	(401, 'Lenke 4', '55_L4.png', 294, 44),
	(402, 'Lenke 5', '55_L5.png', 294, 44),
	(403, 'Lenke 6', '55_L6.png', 294, 44),
	(404, '62 Transverse', '62_elem_e.gif', 82, 44),
	(405, '33-A3 Metaphyseal complex', '33_a3.gif', 90, 44),
	(406, '33-B2.3 Medial Sagittal', '33_B23_218.gif', 91, 44),
	(407, '33-B3.1 Frontal', '33_B31_218.gif', 91, 44),
	(408, '33-B3.2/3 Frontal', '33_B32_218.gif', 91, 44),
	(409, '34-B2.1 Vertical,Medial', '34_b21_218.gif', 94, 44),
	(410, '34-B2.2 Vertical,Medial', '34_b22_218.gif', 94, 44),
	(411, '34-C3.1 Complex', '34_c31_218.gif', 95, 44),
	(412, '34-C3.2 Complex', '34_c32_218.gif', 95, 44),
	(413, '41-A2 Metaphyseal simple ', '41_a2-2.gif', 96, 44),
	(414, '41-A3 Metaphyseal multifragmentary ', '41_a3-2.gif', 96, 44),
	(501, 'Anterior table', 'frontal_sinus-anterior_table.gif', 201, 44),
	(502, 'Frontal recess', 'frontal_sinus-recess.gif', 201, 44),
	(503, 'Posterior table', 'frontal_sinus-posterior_table.gif', 201, 44),
	(504, 'Skull base', 'skull_base-skull_base.gif', 202, 44),
	(505, 'Temporal bone(lateral skull base)', 'skull_base-temporal_bone.gih', 202, 44),
	(506, 'Cranial vault', 'cranial_vault-cranial_vault.gif', 203, 44),
	(507, 'Simple', 'palato-alveolar-simple.gif', 204, 44),
	(508, 'Comminuted', 'palato-alveolar-complex.gif', 204, 44),
	(509, 'Le Fort I', 'le-fort-l.gif', 205, 44),
	(510, 'Le Fort II', 'le-fort-ll.gif', 205, 44),
	(511, 'Le Fort III', 'le-fort-lll.gif', 205, 44),
	(512, 'Nasal bone', 'noe_nasal-nasal.gif', 206, 44),
	(513, 'NOE', 'noe_nasal-noe.gif', 206, 44),
	(514, 'Floor', 'orbital-floor.gif', 207, 44),
	(515, 'Medial wall', 'orbital-medial-wall.gif', 207, 44),
	(516, 'Combined fractures', 'orbital-combined.gif', 207, 44),
	(517, 'Roof', 'orbital-roof.gif', 207, 44),
	(518, 'Lateral wall', 'orbital-lateral-wall.gif', 207, 44),
	(519, 'Isolated zygomatic arch fracture', 'zygoma-arch.gif', 208, 44),
	(520, 'Fracture of the zygomatic complex', 'zygoma-complex.gif', 208, 44),
	(521, 'Enamel only / Dentin exposure', 'enamel-dentin.gif', 209, 44),
	(522, 'Pulp exposure', 'pulp-exposure.gif', 209, 44),
	(523, 'Crown-root fracture', 'crown-root.gif', 209, 44),
	(524, 'Root fracture', 'root.gif', 209, 44),
	(525, 'Without displacement', 'without-displacement.gif', 210, 44),
	(526, 'With displacement', 'with-displacement.gif', 210, 44),
	(527, 'Avulsion', 'Avulsion2.gif', 210, 44),
	(528, 'Segmental alveolar fracture', 'alveolar.gif', 211, 44),
	(529, 'Simple', 'Symphysis_simple.gif', 212, 44),
	(530, 'Complex', 'Symphysis_complex.gif', 212, 44),
	(531, 'Simple', 'Body_simple.gif', 213, 44),
	(532, 'Complex', 'Body_complex.gif', 213, 44),
	(533, 'Simple', 'Angle_simple.gif', 214, 44),
	(534, 'Complex', 'Angle_complex.gif', 214, 44),
	(535, 'Simple / complex', 'Condyle_simple-complex.gif', 215, 44),
	(536, '11-A1.1 Undisplaced greater tuberosity', '11-A11_218.gif', 55, 44),
	(537, '11-A1.2 Displaced greater tuberosity', '11-A12_218.gif', 55, 44),
	(538, '11-A1.3 With a glenohumeral dislocation ', '11-A13_218.gif', 55, 44),
	(539, '11-B1.1/3 With metaphyseal impaction', '11-B11_218.gif', 56, 44),
	(540, '11-B1.2 With metaphyseal impaction', '11-B12_218.gif', 56, 44),
	(541, '11.C1.1  With slight displacement', '11-C11_218.gif', 58, 44),
	(542, '11-C1.2 With slight displacement\n', '11-C12_218.gif', 58, 44),
	(543, '11.C1.3 With slight displacement', '11-C13_218.gif', 58, 44),
	(544, '11.C2.1  Impacted with marked displacement', '11-C21_218.gif', 58, 44),
	(545, '11-C2.2  Impacted with marked displacement', '11-C22_218.gif', 58, 44),
	(546, '11.C2.3  Impacted with marked displacement', '11-C23_218.gif', 58, 44),
	(547, '11-C3.1 Dislocated', '11-C31_218.gif', 58, 44),
	(548, '11-C3.2 Dislocated', '11-C32_218.gif', 58, 44),
	(549, '11-C3.3 Dislocated', '11-C33_218.gif', 58, 44),
	(550, '13-A1.1 Avulsion ', '13-A11_LagScrew_1a_218.gif', 63, 44),
	(551, '13-A1.2 Avulsion', '13-A12.gif', 63, 44),
	(552, '13-A1.3 Avulsion', '13-A12_nonCanScr_1a_248.gif', 63, 44),
	(553, '13-A2.1/2 Simple', '13-A21_ComprPlate_1a2_218.gif', 63, 44),
	(554, '13-A2.3 Simple', '13-A23.gif', 63, 44),
	(555, '13-A3.1/2 Multifragmentary', '13-A32.gif', 63, 44),
	(556, '13-A3.3 Multifragmentary', '13-A33.gif', 63, 44),
	(557, '13-B2.1 Medial sagittal', '13-B21.gif', 64, 44),
	(558, '13.B2.2 Medial sagittal', '13-B22.gif', 64, 44),
	(559, '13.B2.3 Medial sagittal', '13-B22.gif', 64, 44),
	(560, '13-B3.1 Frontal', '13-B31.gif', 64, 44),
	(561, '13.B3.2 Frontal', '13-B31.gif', 64, 44),
	(562, '13.B3.3 Frontal', '13-B33.gif', 64, 44),
	(563, '21-A1.1 Unla', '21_a11.gif', 66, 44),
	(564, '21-A1.2 Unla', '21_a12.gif', 66, 44),
	(565, '21-A1.3 Unla', '21_a13.gif', 66, 44),
	(566, '21-A2.1 Radius', '21_a21.gif', 66, 44),
	(567, '21-A2.2 Radius', '21_a22.gif', 66, 44),
	(568, '21-A2.3 Radius', '21_a23.gif', 66, 44),
	(569, '21-A3.1 Both bones', '21_a31.gif', 66, 44),
	(570, '21-A3.2/3 Both bones', '21_a32.gif', 66, 44),
	(577, '21-B3.1 One articular,other extraacticular', '21_b31.gif', 67, 44),
	(578, '21-B3.2 One articular,other extraacticular', '21_b32.gif', 67, 44),
	(579, '21-B3.3 One articular,other extraacticular', '21_b33.gif', 67, 44),
	(583, '21-C2.1 One simple,other multifragmentary', '21_c21.gif', 68, 44),
	(584, '21-C2.2/3 One simple,other multifragmentary', '21_c22.gif', 68, 44),
	(588, '22-A1.1 unla,radius intact - Oblique', '22_a1-1_218.gif', 69, 44),
	(589, '22-A1.2 unla,radius intact - Transverse', '22_a1-2_218.gif', 69, 44),
	(590, '22-A1.3 unla,radius intact - With dislocation of radial head (Monteggia)', '22_a1-3_218.gif', 69, 44),
	(591, '22-A2.1 radius, ulna intact - Oblique', '22_a2-1_218.gif', 69, 44),
	(592, '22-A2.2 radius, ulna intact - Transverse', '22_a2-2_218.gif', 69, 44),
	(593, '22-A2.3 radius, ulna intact - With dislocation distal radioulnar joint (Galeazzi)', '22_a2-3_218.gif', 69, 44),
	(594, '22-B1.1/2 Unla,radius intact - Intact or fragmented wedge', '22_b1-2_218.gif', 70, 44),
	(595, '22-B1.3  Unla,radius intact - With dislocation of radial head (Monteggia)', '22_b1-3_218.gif', 70, 44),
	(596, '22-B2.1/2  Radius,unla intact - Intact or fragmented wedge', '22_b2-2_218.gif', 70, 44),
	(597, '22-B2.3  Radius,unla intact - With dislocation of the distal radioulnar ', '22_b2-3_218.gif', 70, 44),
	(598, '22-C3.1 Both bones complex - Bifocal', '22_c3-1_218.gif', 71, 44),
	(599, '22-C3.2 Bifocal of one bone, irregular of the othe', '22_c3-2_218.gif', 71, 44),
	(600, '22-C3.3 Both bones complex - Irregular', '22_c3-3_218.gif', 71, 44),
	(601, '23-A1.1  Ulna,radius intact', '23-A11.gif', 72, 44),
	(602, '23-A1.2 Ulna,radius intact', '23-A12.gif', 72, 44),
	(603, '23-A1.3 Ulna,radius intact', '23-A13.gif', 72, 44),
	(604, '23-A2.1 Radius,simple and impacted', '23_A21.gif', 72, 44),
	(605, '23-A2.2 Radius,simple and impacted', '23_A22.gif', 72, 44),
	(606, '23-A2.3 Radius,simple and impacted', '23_A23.gif', 72, 44),
	(607, '23-B1.1 Radius,sagittal', '23_B11.gif', 73, 44),
	(608, '23-B1.2 Radius,sagittal', '23_B12.gif', 73, 44),
	(609, '23-B1.3 Radius,sagittal', '23_B13.gif', 73, 44),
	(610, '23-B2.1 Radius,frontal,dorsal rim', '23_B21.gif', 73, 44),
	(611, '23-B2.2 Radius,frontal,dorsal rim', '23_B22.gif', 73, 44),
	(612, '23-B2.3 Radius,frontal,dorsal rim', '23_B23.gif', 73, 44),
	(613, 'Transverse', 'Distal_Transverse.gif', 270, 44),
	(614, 'Multifragmentary ', 'Distal_Multifragmentary.gif', 270, 44),
	(615, 'Dislocation ', 'Dislocation.gif', 271, 44),
	(616, 'Avulsion', 'Avulsion.gif', 271, 44),
	(617, 'Unicondylar', 'MiddlePhx_Distal_Oblique.gif', 272, 44),
	(618, 'Bicondylar', 'MiddlePhx_Distal_Bicondylar.gif', 272, 44),
	(619, 'Transverse', 'MiddlePhx_Shaft_Transverse.gif', 273, 44),
	(620, 'oblique', 'MiddlePhx_Shaft_Oblique.gif', 273, 44),
	(621, 'Multifragmentary ', 'MiddlePhx_Shaft_multifragmentary.gif', 273, 44),
	(622, 'Dislocation ', 'MiddlePhx_Proxi_Dislocation.gif', 274, 44),
	(623, 'Avulsion', 'MiddlePhx_Proxi_Avulsion.gif', 274, 44),
	(624, 'Multifragmentary ', 'MiddlePhx_Proxi_Compression.gif', 274, 44),
	(625, 'Oblique', 'Articular_Oblique.gif', 275, 44),
	(626, 'Unicondylar fracture dislocation', 'Articular_Unicondyla.gif', 275, 44),
	(627, 'Bicondylar', 'Articular_Bicondyla.gif', 275, 44),
	(628, 'Transverse', 'Metaphys_Transverse.gif', 276, 44),
	(629, 'Oblique', 'Metaphys_Oblique.gif', 276, 44),
	(630, 'Multifragmentary ', 'Metaphys_Multifx.gif', 276, 44),
	(631, 'Transverse', 'ProxPhx_Shaft_Transverse.gif', 277, 44),
	(632, 'Oblique', 'ProxPhx_Shaft_Oblique.gif', 277, 44),
	(633, 'Multifragmentary ', 'ProxPhx_Shaft_Multifx.gif', 277, 44),
	(634, 'Transverse', 'ProxPhx_ProxMetaphys_Transverse.gif', 278, 44),
	(635, 'Oblique', 'ProxPhx_ProxMetaphys_Oblique.gif', 278, 44),
	(636, 'Avulsion', 'ProxPhx_ProxArticular_Avulsion.gif', 279, 44),
	(637, 'Shearing', 'ProxPhx_ProxArticular_Shearing.gif', 279, 44),
	(638, 'Multifragmentary', 'ProxPhx_ProxArticular_Compression.gif', 279, 44),
	(639, 'Subcapital', 'Metacarpal_Head_Subcapital.gif', 280, 44),
	(640, 'Intra Articular', 'Metacarpal_Head_Intraarticular.gif', 280, 44),
	(641, 'Transverse', 'Metacarpal_Shaft_Transverse.gif', 281, 44),
	(642, 'Oblique', 'Metacarpal_Shaft_Oblique.gif', 281, 44),
	(643, 'Multifragmentary ', 'Metacarpal_Shaft_Multifragmentary.gif', 281, 44),
	(644, 'Intra-articular', 'Metacarpal_Base_Intraarticular.gif', 282, 44),
	(645, 'Undisplaced', 'Carpal_Scaphoid_undisplaced.gif', 283, 44),
	(646, 'Displaced', 'Carpal_Scaphoid_Displaced.gif', 283, 44),
	(647, 'Proximal pole', 'Carpal_Scaphoid_ProximalPole.gif', 283, 44),
	(648, 'Dissociation', 'Carpal_Perilunate_Dissociation.gif', 284, 44),
	(649, 'Dislocation', 'Carpal_Perilunate_Dislocation.gif', 284, 44),
	(650, 'Fracture', 'Carpal_Perilunate_Fracture.gif', 284, 44),
	(651, 'Transverse', 'Distal_Transverse.gif', 285, 44),
	(652, 'Multifragmentary', 'Distal_Multifragmentary.gif', 285, 44),
	(653, 'Dislocation', 'Dislocation.gif', 286, 44),
	(654, 'Avulsion', 'Avulsion.gif', 286, 44),
	(655, 'Oblique', 'Articular_Oblique.gif', 287, 44),
	(656, 'Unicondylar with fx dislocation', 'Articular_Unicondyla.gif', 287, 44),
	(657, 'Bicondylar', 'Articular_Bicondyla.gif', 287, 44),
	(658, 'Transverse', 'Metaphys_Transverse.gif', 288, 44),
	(659, 'Oblique', 'Metaphys_Oblique.gif', 288, 44),
	(660, 'Multifragmentary ', 'Metaphys_Multifx.gif', 288, 44),
	(661, 'Skier\'s thumb', 'Thumb_skiers.gif', 289, 44),
	(662, 'Pilon', 'Thumb_pilon.gif', 289, 44),
	(663, 'Multifragmentary', 'Thumb_multifragmentary.gif', 289, 44),
	(664, 'Base', 'Thumb_base.gif', 290, 44),
	(665, 'Bennett', 'Thumb_benett.gif', 290, 44),
	(666, 'Rolando', 'Thumb_rolando.gif', 290, 44),
	(678, '44-A1.1/2 Unifocal', '44_a1-2.gif', 105, 44),
	(679, '4-A1.3  Unifocal', '44_a1-3.gif', 105, 44),
	(680, '44-A2.1 Bifocal', '44_a2-1.gif', 105, 44),
	(681, '44-A2.2/3 Bifocal', '44_a2-2.gif', 105, 44),
	(682, '44-A3.1 Circumferential', '44_a3-1.gif', 105, 44),
	(683, '44-A3.2/3 Circumferential', '44_a3-3.gif', 105, 44),
	(684, '44-B1.1/2 Isolated lateral', '44_b1-1.gif', 106, 44),
	(685, '44-B1.3 Isolated lateral', '44_b1-3.gif', 106, 44),
	(686, '44-B2.1 Lateral and Medial', '44_b2-1.gif', 106, 44),
	(687, '44-B2.2 Lateral and Medial', '44_b2-2.gif', 106, 44),
	(688, '44-B2.3 Lateral and Medial', '44_b2-3.gif', 106, 44),
	(689, '44-B3.1 Lateral, Medial and posterior', '44_b3-1.gif', 106, 44),
	(690, '44-B3.2 Lateral, Medial and posterior', '44_b3-2.gif', 106, 44),
	(691, '44-B3.3 Lateral, Medial and posterior', '44_b3-3.gif', 106, 44),
	(692, 'Proximal phalanx - diaphysis - simple', 'Phalanges.png', 375, 44),
	(694, 'Proximal phalanx - base - shear fracture', 'Phalanges.png', 375, 44),
	(696, 'Distal phalanx - base - impression fracture', 'Phalanges.png', 375, 44),
	(698, 'MTP-joint dislocation', 'Phalanges.png', 376, 44),
	(700, 'Phalanges of the 3rd - 5th row', 'Phalanges.png', 376, 44),
	(702, 'Distal phalanx - multifragmentary fracture', 'Phalanges.png', 376, 44),
	(704, 'Proximal metaphyseal fractures', 'Metatarsals.png', 377, 44),
	(706, 'Diaphyseal fractures', 'Metatarsals.png', 377, 44),
	(708, 'Subcapital fractures', 'Metatarsals.png', 377, 44),
	(710, 'Fracture of the base (Jones fracture)', 'Metatarsals.png', 378, 44),
	(712, 'Long oblique diaphyseal fractures', 'Metatarsals.png', 378, 44),
	(714, 'Multifragmentary diaphyseal fractures', 'Metatarsals.png', 378, 44),
	(716, 'Split / stress fractures', 'Midfoot.png', 379, 44),
	(719, 'Multifragmentary fractures', 'Midfoot.png', 379, 44),
	(721, 'TMT (Lisfranc) injuries', 'Midfoot.png', 380, 44),
	(722, 'Simple fractures', 'Midfoot.png', 381, 44),
	(724, 'Crush fractures', 'Midfoot.png', 381, 44),
	(726, 'Intertarsal fractures', 'Midfoot.png', 382, 44),
	(728, 'Crush injuries', 'Midfoot.png', 382, 44),
	(730, 'Simple undisplaced fractures', 'Calcaneus.png', 383, 44),
	(732, 'Displaced fractures', 'Calcaneus.png', 383, 44),
	(736, 'Extreme tongue-type (beak) fractures', 'Calcaneus.png', 383, 44),
	(738, 'Sustentacular fractures (ORIF - screw fixation)', 'Calcaneus.png', 384, 44),
	(739, 'Undisplaced fractures (Nonoperative treatment)', 'Talus.png', 385, 44),
	(741, 'Displaced fractures (ORIF - screw fixation)', 'Talus.png', 385, 44),
	(743, 'Multifragmentary fractures (ORIF - plate fixation)', 'Talus.png', 385, 44),
	(745, 'Body fractures (ORIF - screw and plate fixation)', 'Talus.png', 386, 44),
	(746, 'Lateral process fractures (ORIF - screw fixation)', 'Talus.png', 387, 44),
	(748, 'Posterior process fractures (ORIF - screw fixation)', 'Talus.png', 387, 44),
	(750, 'Lateral process fractures (ORIF - screw fixation)', 'Talus.png', 388, 44),
	(751, 'Posterior process fractures (ORIF - screw fixation)', 'Talus.png', 388, 44);
/*!40000 ALTER TABLE `reference` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
